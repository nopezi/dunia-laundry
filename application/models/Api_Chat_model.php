<?php
class Api_Chat_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function set_message($input)
	{
		$this->form_validation->set_rules('from_user_id', 'from_user_id', 'required');
		$this->form_validation->set_rules('to_user_id', 'to_user_id', 'required'); // or shop_id 
		$this->form_validation->set_rules('message', 'message', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

		$fromUserExst = $this->db->where('user_id',$input['from_user_id'])->get('la_user')->num_rows();
		if($fromUserExst > 0)
		{
			$toUserExst = $this->db->where('user_id',$input['to_user_id'])->get('la_user')->num_rows();
			if($toUserExst > 0)
			{
				$from_user_id = $input['from_user_id'];
				$to_user_id = $input['to_user_id'];
				$message = $input['message'];
				$check_block = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND block_status = '0') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND block_status = '0') ")->num_rows();
				if($check_block > 0)
				{
					$checkRow = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND blocked_by = '$from_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND blocked_by = '$from_user_id') ");
					if($checkRow->num_rows() > 0)
					{
						$user = $checkRow->row_array();
						if($from_user_id == $user['blocked_by'])
						{
							$uData = $this->db->where('user_id',$from_user_id)->get('la_user')->row();
						}else{
							$uData = $this->db->where('user_id',$to_user_id)->get('la_user')->row();
						}
						$this->Api_Chat_model->responseFailed(0, $uData->name.' is blocked');
						exit();
					}else{
						$this->Api_Chat_model->responseFailed(0, 'You are blocked');
						exit();
					}
				}
				else
				{ 
					$check = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id') ");
					if($check->num_rows() > 0)
					{
						$msg_head = $check->row_array();
						$message_head_id = $msg_head['message_head_id'];
						$data = array(
							'updated_at' => date('Y-m-d H:i:s')
								);
						$this->db->where('message_head_id',$message_head_id);
						$this->db->update('la_message_head',$data);
					}else{
						$data1 = array(
							'from_user_id' => $from_user_id,
							'to_user_id' => $to_user_id,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
								);
						$this->db->insert('la_message_head',$data1);
						$message_head_id = $this->db->insert_id();

					}
					return $message_head_id;
				}
			}
			else
			{
				//shop_id
				$shopExist = $this->db->where('shop_id',$input['to_user_id'])->get('la_laundry_shop');
				if($shopExist->num_rows() > 0)
				{
					$shop = $shopExist->row_array();

					$to_user_id =$shop['user_id'];
					$from_user_id = $input['from_user_id'];
					$message = $input['message'];
					$check_block = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND block_status = '0') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND block_status = '0') ")->num_rows();
					if($check_block > 0)
					{
						$checkRow = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id' AND blocked_by = '$from_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id' AND blocked_by = '$from_user_id') ");
						if($checkRow->num_rows() > 0)
						{
							$user = $checkRow->row_array();
							if($from_user_id == $user['blocked_by'])
							{
								$uData = $this->db->where('user_id',$from_user_id)->get('la_user')->row();
							}else{
								$uData = $this->db->where('user_id',$to_user_id)->get('la_user')->row();
							}
							$this->Api_Chat_model->responseFailed(0, $uData->name.' is blocked');
							exit();
						}else{
							$this->Api_Chat_model->responseFailed(0, 'You are blocked');
							exit();
						}
					}
					else
					{ 
						$check = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$from_user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$from_user_id') ");
						if($check->num_rows() > 0)
						{
							$msg_head = $check->row_array();
							$message_head_id = $msg_head['message_head_id'];
							$data = array(
								'updated_at' => date('Y-m-d H:i:s')
									);
							$this->db->where('message_head_id',$message_head_id);
							$this->db->update('la_message_head',$data);
						}else{
							$data1 = array(
								'from_user_id' => $from_user_id,
								'to_user_id' => $to_user_id,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
									);
							$this->db->insert('la_message_head',$data1);
							$message_head_id = $this->db->insert_id();

						}
						return $message_head_id;
					}

				}
				else
				{
					$this->Api_Chat_model->responseFailed(0, 'to_user_id is not exist');
					exit();
				}

			}
		}
		else
		{
			$this->Api_Chat_model->responseFailed(0, 'from_user_id is not exist');
			exit();
		}

	}

	public function get_message_history($user_id)
	{
		$result = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$user_id' OR to_user_id = '$user_id') Order by updated_at DESC")->result();

		$get_chats = array();
		foreach($result as $i => $r)
		{
			$get_chats[$i] = array();
			$message_head_id = $r->message_head_id;
			$get_chats[$i]['message_head_id'] = $message_head_id;
			$get_chats[$i]['updated_at'] = round(strtotime($r->updated_at));
			$messageObjData = $this->db->query("SELECT * FROM la_message WHERE message_head_id = '$message_head_id' Order by message_id DESC limit 1")->row();
			$get_chats[$i]['message'] = $messageObjData->message;
			if($user_id==$r->from_user_id){
				$userObjData = $this->db->where('user_id',$r->to_user_id)->get('la_laundry_shop')->row();
				$userObjData->name = $userObjData->shop_name;
			} else {
				$userObjData = $this->db->where('user_id',$r->from_user_id)->get('la_user')->row();
			}
			$get_chats[$i]['user_name'] = $userObjData->name;
			$get_chats[$i]['user_id'] = $userObjData->user_id;
			$get_chats[$i]['user_image'] = 'assets/images/user/'.$userObjData->image;

		}
		$this->Api_Chat_model->responseSuccess(1, MSG_SEND, $get_chats);
	}

	public function get_message($input)
	{
		$user_id = $input['user_id'];

		$shopExist = $this->db->where('shop_id',$input['to_user_id'])->get('la_laundry_shop');
		if($shopExist->num_rows() > 0)
		{
			$shop = $shopExist->row_array();

			$to_user_id =$shop['user_id'];
			
		}
		else{
			$to_user_id = $input['to_user_id'];
		}

		$check = $this->db->query("SELECT * FROM la_message_head WHERE (from_user_id = '$user_id' AND to_user_id = '$to_user_id') OR (from_user_id = '$to_user_id' AND to_user_id = '$user_id') ");
		if($check->num_rows() > 0)
		{
			$msg_head = $check->row_array();
			$message_head_id = $msg_head['message_head_id'];
			$this->db->where('message_head_id',$message_head_id);
			$this->db->order_by('message_id', 'Asc');
			$userObjMsg = $this->db->get('la_message')->result(); 
			foreach ($userObjMsg as $i => $r) 
            {
                $get_msg[$i] = array();
                $get_msg[$i]['message_id'] = $r->message_id;
                $get_msg[$i]['message_head_id'] = $r->message_head_id;
                $get_msg[$i]['from_user_id'] = $r->from_user_id;
                $get_msg[$i]['to_user_id'] = $r->to_user_id;
                $get_msg[$i]['message'] = $r->message;
                $get_msg[$i]['type'] = $r->type;
                $get_msg[$i]['media'] = $r->media;
                $get_msg[$i]['is_read'] = $r->is_read;
                $get_msg[$i]['created_at'] = round(strtotime($r->created_at));
            }

			if($user_id==$msg_head['from_user_id']){
				$userObjData = $this->db->where('user_id',$msg_head['to_user_id'])->get('la_user')->row();

			} else {
				$userObjData = $this->db->where('user_id',$msg_head['from_user_id'])->get('la_user')->row();

			}
			$userData['user_name'] = $userObjData->name;
			$userData['user_image'] = 'assets/images/user/'.$userObjData->image;
			$userData['block_status'] = $msg_head['block_status'];
			//to_user_id detail
			// $toUserData = $this->db->where('user_id',$to_user_id)->get('user')->row();
			// $userData['to_user_name'] = $toUserData->name;
			// $userData['to_user_image'] = $toUserData->image;
			$arr = array('status' => 1,'message' => '', 'data'=> $get_msg, 'user'=>$userData); 
			header('Content-Type: application/json');      
			echo json_encode($arr); 
		}
		else{
		$this->Api_Chat_model->responseFailed(0, NOTFOUND);
		}
	}

	public function getSingleRow($table, $condition)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->row();       
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

    public function responseSuccessWethoutdata($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}