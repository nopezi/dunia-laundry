<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_order extends CI_Model {

	public function menu_order($status_pesanan)
	{
		
		$nama_tab = ['Pending', 'Confirmed', 'Picked Up', 'In Progress', 'Shipped', 'Delivered', 'Cancel'];

	    for ($i=0; $i < sizeof($nama_tab); $i++) { 

	      if ($status_pesanan == $i) {

	        $aktif[] = [
	          'id'     => $i,
	          'name'   => $nama_tab[$i],
	          'active' => 'active'
	        ];

	      } else {

	        $aktif[] = [
	          'id'     => $i,
	          'name'   => $nama_tab[$i],
	          'active' => false
	        ];

	      }

	    }

	    return $aktif;

	}

	public function order($status_pesanan = null, $tanggal = null)
    {

        $ses = $_SESSION;
        $regional = $this->db->get_where('la_user', ['user_id'=>$ses['id']])->first_row();

        if($ses['type'] == '2' || $ses['type'] == '1') {
            
        	$this->db->select('od.*')
                     ->from('la_order_details od')
                     ->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id')
                     ->where('ls.user_id',$ses['id']);
            
            if ($status_pesanan !== null){
                $this->db->where('order_status', $status_pesanan);
            } else {
                $this->db->where('order_status', 0);
            }

            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get()->result();

            if (!empty($query)) {
                
                foreach ($query as $key => $value) {
                    $d_user = $this->db->get_where('la_user', ['user_id'=>$value->user_id])->first_row();
                    $value->customer = $d_user->name;
                }

            }

            return $query;

        } elseif ($ses['type'] == '3') {

        	if ($status_pesanan !== null){
                $this->db->where('order_status', $status_pesanan);
            } else {
                $this->db->where('order_status', 0);
            }

            if (!empty($tanggal)) {
                $this->db->like('lo.created_at', $tanggal, 'both');
            }

           $data = $this->db->select('lo.*')
		           			->from('la_order_details lo')
		           			->join('la_laundry_shop ll', 'll.shop_id=lo.shop_id')
		           			->join('la_user lu', 'lu.user_id=ll.user_id', 'left')
		           			->where('lu.regional', $ses['regional'])
		           			->get()
		           			->result();

            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";
            // die();

            // return $this->db->query($query)->result();
            return $data;

        } else {

            $this->db->order_by('s_no','DESC');
                     
            if (!empty($tanggal))
                $this->db->like('created_at', $tanggal);

            if ($status_pesanan !== null)
                $this->db->where('order_status', $status_pesanan);
            else
                $this->db->where('order_status', 0);

            $res = $this->db->get('la_order_details')->result();
            return $res;

        }

    }	

}

/* End of file Model_order.php */
/* Location: ./application/models/Model_order.php */