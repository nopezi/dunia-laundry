<?php
class Base_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }


    public function login($email,$password)
    {
        $data['email']    = $email;
        $data['password'] = $password;

        $sess_array       = array();
        $getdata = $this->Base_model->getSingleRow('la_admin', $data);
        if($getdata)
        {
            $this->session->unset_userdata($sess_array);
            $sess_array = array(
                'owner_name' => $getdata->name,
                'email' => $getdata->email,
                'id' => $getdata->id,
                'status' => $getdata->status,
                'role' => 'admin',
                'type' => '10',
            );
            
            $this->session->set_userdata($sess_array);
            $dataget['get_data'] = $getdata;
            $dataget['see_data'] = $sess_array;
            redirect('user/dashboard');
        } else {
            
            $this->session->set_flashdata('error', LOGIN_FLASH);
            redirect('login');
            
        }
    }

    /*Get single row data*/
    public function getSingleRow($table, $condition)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->row();       
    }

    public function resetPassword($email)
    {
        $q = $this->db->where('email',$email)->get('ec_admin');
        if($q->num_rows() > 0)
        {
            $password = rand(111111,999999);
            $data = array(
                'password' => $password,
                'update_dt' => date('Y-m-d H:i:s')
                );
            $this->db->update('ec_admin',$data);
            return 1;
        }
        else
        {
            return 0;
        }
    }

    /*Add new data*/
    public function insert($table,$data)
    {
        if($this->db->insert($table, $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }

    public function update($table,$data,$id)
    {
        if($id !='')
        {
            $this->db->where('user_id',$id);
            $this->db->update($table, $data);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function ckeck_row($table,$condition)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->row();
    }

}