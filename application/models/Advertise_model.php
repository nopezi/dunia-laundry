<?php
class Advertise_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function advertisement()
    {
    	$res = $this->db->get('la_advertisement')->result();
    	return $res;
    }

    public function submitAdvertisement($input,$img)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('short_description', 'short_description', 'required');
        $this->form_validation->set_rules('long_description', 'long_description', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red', ALL_FIELD_MANDATORY);
            redirect('advertisement');
            exit();
        }

        $id = $input['advertisement_id'];
        if($id !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/advertise/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'url' => $input['url']?$input['url']:'',
                    'short_description' => $input['short_description'],
                    'long_description' => $input['long_description'],
                    'updated_at' => date('Y-m-d H:i:s')
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/advertise/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'url' => $input['url']?$input['url']:'',
                    'short_description' => $input['short_description'],
                    'long_description' => $input['long_description'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                        );
            }
            $this->db->where('advertisement_id',$id);
            $this->db->update('la_advertisement',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('advertisement');
        }
        else
        {
            // INSERT DATA HEAR...!!
            $img = $_FILES['image']['name'];
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/advertise/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            
            // $item_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
            $data = array(
                'title' => $input['title'],
                'url' => $input['url']?$input['url']:'',
                'short_description' => $input['short_description'],
                'long_description' => $input['long_description'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('la_advertisement', $data);
            $this->session->set_flashdata('error', DATA_SUBMIT);
            redirect('advertisement');
            
        }
    }

    public function changeAdvertisementStatus($id)
    {
    	$this->db->where('advertisement_id',$id);
        $query = $this->db->get('la_advertisement');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('advertisement_id',$id);
            $this->db->update('la_advertisement',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('advertisement_id',$id);
            $this->db->update('la_advertisement',$data);
        }
    }

    public function updateAdvertisement($id)
    {
    	$data = $this->db->where('advertisement_id',$id)->get('la_advertisement')->row_array();
        return $data;
    }


}
