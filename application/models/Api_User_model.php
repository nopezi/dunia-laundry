<?php
class Api_User_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function userUpdate($input)
    {
        //$name = $input['name'];
        $country_code = $input['country_code'];
        //$mobile = $input['mobile'];
        //$address = $input['address'];
        //$latitude = $input['latitude'];
        //$longitude = $input['longitude'];
        $user_id = $input['user_id'];

        $data = array(
            //'name' => $name,
            'country_code' => $country_code,
            //'mobile' => $mobile,
            //'address' => $address,
            //'latitude' => $latitude,
            //'longitude' => $longitude,
            'updated_at' => date('Y-m-d H:i:s')
        );

        if ( isset( $input['name'] ) ) {
            $data['name'] = $input['name'];
        }

        if ( isset( $input['mobile'] ) ) {
            $data['mobile'] = $input['mobile'];
        }

        if ( isset( $input['address'] ) ) {
            $data['address'] = $input['address'];
        }

        if ( isset( $input['latitude'] ) ) {
            $data['latitude'] = $input['latitude'];
        }

        if ( isset( $input['longitude'] ) ) {
            $data['longitude'] = $input['longitude'];
        }

        $this->db->where('user_id',$user_id);
        $q = $this->db->update('la_user',$data);
        if($q)
        {

            $res = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        
            $userData = array(
                    'user_id' => $res['user_id'],
                    'name' => $res['name'],
                    'email' => $res['email'],
                    'password' => $res['password'],
                    'country_code' => $res['country_code'],
                    'mobile' => $res['mobile'],
                    'image' => 'assets/images/user/'.$res['image'],
                    'address' => $res['address'],
                    'device_type' => $res['device_type'],
                    'device_token' => $res['device_token'],
                    'latitude' => $res['latitude'],
                    'longitude' => $res['longitude'],
                    'landmark' => $res['landmark'],
                    'status' => $res['status'],
                    'type' => $res['type'],
                    'created_at' => $res['created_at'],
                    'updated_at' => $res['updated_at'],
                    );
                return $userData;
        }
        
    }

    public function updateSingleRow($table, $where, $data)
    {                 
        $this->db->where($where);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() > 0)
        {
          	return TRUE;
        }
        else
        {
          	return FALSE;
        }
    }

    public function getUserDataById($user_id)
    {
        $res = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        
        $userData = array(
            'user_id' => $res['user_id'],
            'name' => $res['name'],
            'email' => $res['email'],
            'password' => $res['password'],
            'country_code' => $res['country_code'],
            'mobile' => $res['mobile'],
            'image' => $res['image'],
            'background' => $res['background'],
            'address' => $res['address'],
            'device_type' => $res['device_type'],
            'device_token' => $res['device_token'],
            'latitude' => $res['latitude'],
            'longitude' => $res['longitude'],
            'landmark' => $res['landmark'],
            'status' => $res['status'],
            'type' => $res['type'],
            'created_at' => $res['created_at'],
            'updated_at' => $res['updated_at'],
            );
        return $userData;
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}