<?php
class Laundry_Owner_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function login($email,$password)
    {
        
        $data['email']    = $email;
        $data['password'] = $password;

        $sess_array       = array();
        $getdata = $this->db->where('email',$email)
                            ->where('password',$password)
                            ->where('status', '1')
                            ->where_in('type',['3','2','1'])
                            ->get('la_user')
                            ->row();
        if(!empty($getdata)) {

            $this->session->unset_userdata($sess_array);
            $sess_array = array(
                'owner_name' => $getdata->name,
                'email' => $getdata->email,
                'id' => $getdata->user_id,
                'status' => $getdata->type,
                'role' => 'mitra',
                'type' => $getdata->type,
                'regional' => $getdata->regional
            );
            
            $this->session->set_userdata($sess_array);
            $dataget['get_data'] = $getdata;
            $dataget['see_data'] = $sess_array;
            redirect('user/dashboard');

        } else {
            
            $this->session->set_flashdata('error', LOGIN_FLASH);
            redirect('laundryOwner/login');
            
        }

    }

    public function laundryOwner()
    {
        if ($_SESSION['type'] == 3) {
            $result = $this->db->where('regional', $_SESSION['regional'])
                               ->where('user_id != ', $_SESSION['id'])
                               ->order_by('s_no', 'desc')
                               ->get('la_user')
                               ->result();
        } else {
            $result = $this->db->where_in('type',['2','1'])->order_by('s_no', 'desc')->get('la_user')->result();
        }
    	
    	return $result;
    }

    public function submitLaundryOwner($input,$img, $franchise=null)
    {

    	$this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('regional', 'regional', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            if (!empty($franchise)) {
                redirect('franchise');
            } else {
                redirect('laundryOwner');
            }
            exit();
        }

        # 1 kota/kabupaten hanya 1 franchise
        if (empty($input['user_id']) && ($input['type'] == 3)) {

            $cek_regional = $this->db->get_where('la_user', ['regional'=>$input['regional']])->first_row();
            if (!empty($cek_regional)) {
                $this->session->set_flashdata('error', 'Untuk saat ini hanya terdapat 1 franchise Untuk setiap kota/kabupaten');
                redirect('laundry/updateLaundryOwner/'.$input['user_id']);
                exit();
            }

        }

        if($input['user_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile'       => $input['mobile'],
                    'password'     => $input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'type'         => $input['type'] ?:'1',
                    'updated_at'   => date('Y-m-d H:i:s'),
                        );

            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile'       => $input['mobile'],
                    'password'     => $input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'image'        => $image,
                    'type'         => $input['type'] ?:'1',
                    'updated_at'   => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('user_id',$input['user_id']);
            $this->db->update('la_user',$data);
            $this->session->set_flashdata('success', DATA_UPDATE);

            if (!empty($franchise)) {
                redirect('franchise');
            } else {
                redirect('laundryOwner');
            }

        } else {

            $email = $input['email'];
            $getdata = $this->db->where('email',$email)->get('la_user')->num_rows();

            if($getdata > 0)
            {
                $this->session->set_flashdata('error', EMAIL_EXIST);
                if (!empty($franchise)) {
                    redirect('franchise');
                } else {
                    redirect('laundryOwner');
                }
            } else {

                if($img == '')
                {
                    $image = "default.png";
                } else {

                    $config['upload_path']   = 'assets/images/user/';
                    // return $config['upload_path']; exit();
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['overwrite']     = TRUE;
                    $config['max_size']      = 10000;
                    $config['file_name']     = time();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    $image = "";
                    if($this->upload->do_upload('image')) {
                        $uploadData = $this->upload->data(); 
                        $image = $uploadData['file_name']; 
                    }
                }
                // $user_id = rand(100000,999999);
                $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

                $data = array( 
                    'user_id'      => $user_id,
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile'       => $input['mobile'],
                    'password'     => $input['password'],
                    'address'      => $input['address']?$input['address']:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude']?$input['latitude']:'',
                    'longitude'    => $input['longitude']?$input['longitude']:'',
                    'image'        => $image,
                    'type'         => $input['type'],
                    'created_at'   => date('Y-m-d H:i:s')
                );
                $this->db->insert('la_user',$data);
            
                $this->session->set_flashdata('success',DATA_SUBMIT);
                if (!empty($franchise)) {
                    redirect('franchise');
                } else {
                    redirect('laundryOwner');
                }

            }
        }
    }

    public function changeLaundryOwnerStatus($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('la_user');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
    }

    public function changeLaundryOwnerStatus_v2($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('la_user');
        $data = $query->row_array();
        if($data['status'] == '1'){
            echo "string";
            $data = ['status'=>'0'];

        } else {
            echo "masok";
            $data = ['status'=>'1'];
            
        }
        $this->db->update('la_user', $data, ['user_id'=>$id]);
        $this->db->update('la_laundry_shop', $data, ['user_id'=>$id]);
        return $this->db->get_where('la_user', ['user_id'=>$id])->first_row();
    }

    public function updateLaundryOwner($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        return $query;
    }

    public function paymentHistory($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_payments')->result();
        return $query;
    }

    public function viewOwnerDetail($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row();
        return $query;
    }

    public function subscriptionPackage()
    {
        $query = $this->db->where('status','1')->get('la_subscription_package')->result();
        return $query;
    }

    public function subscriptionPayments($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->where('payment_type','2')->get('la_payments')->result();
        return $query;
    }

    public function submitOwnerPackage($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('package_id', 'package_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error_red',ALL_FIELD_MANDATORY);
            redirect('laundry/ownerPackages/'.$input['user_id']);
            exit();
        }

        $payment_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

        $user_id = $input['user_id'];
        $package_id = $input['package_id'];
        $pkg = $this->db->where('package_id',$package_id)->get('la_subscription_package')->row_array();
        $no_of_days = $pkg['no_of_days'];
        $no = $no_of_days+'1';
        $date = date('Y-m-d', strtotime("+$no days"));
        $data = [
               'user_id' => $user_id,
               'payment_id' => $payment_id,
               'amount' => $pkg['amount'],
               'package_id' => $package_id,
               'end_subscription_date' => $date,
               'payment_type' => '2',
               'created_at' => date('Y-m-d H:i:s'),
            ];

        $insert = $this->db->insert('la_payments', $data);

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        if($user['end_subscription_date'] !='')
        {
            if($user['end_subscription_date'] >= date('Y-m-d'))
            {
                $end_subscription_date = $user['end_subscription_date'];
                $now = time(); // or your date as well
                $your_date = strtotime("$end_subscription_date");
                $datediff =  $your_date - $now;

                $day = round($datediff / (60 * 60 * 24));
                $num = $day+$no;
                $dates = date('Y-m-d', strtotime("+$num days"));
                // echo $dates; die;
            }
            else
            {
                $no = $no_of_days+'1';
                $dates = date('Y-m-d', strtotime("+$no days"));
            }
            
        }
        else
        {
            $no = $no_of_days+'1';
            $dates = date('Y-m-d', strtotime("+$no days"));
        }

        $data1 = array(
            'end_subscription_date' => $dates
        );
        $sucess = $this->db->where('user_id',$user_id)->update('la_user',$data1);
        if($sucess)
        {
            $email = $user['email'];
            $name = $user['name'];
            $amount = $pkg['amount'];
            $subject = SUBSCRIPTION_HEAD;
            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $amount Rs. and it will be available till $dates, thank you.";
            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
        }
        $this->session->set_flashdata('error',DATA_SUBMIT);
        redirect('laundry/ownerPackages/'.$input['user_id']);  
        
    }

    public function delete($id)
    {
        return $this->db->delete('la_user', array('user_id' => $id));
    }


}
