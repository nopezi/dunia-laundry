<?php
class Komisi_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getKomisi()
    {
    	$row = $this->db->get('la_komisi')->row_array();
    	return $row;
    }

    public function updateKomisi($input)
    {
        $this->form_validation->set_rules('komisi_agen', 'komisi_agen', 'required');
        $this->form_validation->set_rules('type_komisi_agen', 'type_komisi_agen', 'required');

        $this->form_validation->set_rules('komisi_mitra', 'komisi_mitra', 'required');
    	$this->form_validation->set_rules('type_komisi_mitra', 'type_komisi_mitra', 'required');

        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('komisi');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'komisi_agen' => $input['komisi_agen'],
            'type_komisi_agen' => $input['type_komisi_agen'],
            'komisi_mitra' => $input['komisi_mitra'],
            'type_komisi_mitra' => $input['type_komisi_mitra'],

            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('la_komisi',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('komisi');
        
    }

    public function komisi()
    {
        $ses = $_SESSION;
        if($ses['type'] == '2' || $ses['type'] == '1')
        {

            $this->db->select('od.*, lu.type, ls.user_id as id_agen_mitra');
            $this->db->from('la_order_details od');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
            $this->db->where('ls.user_id',$ses['id']);
            $this->db->where('od.payment_status', '1');
            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get();
            return $query->result();
        
        } else if ($ses['type'] == '3') {

            $regional = $this->db->get_where('la_user', ['user_id'=>$ses['id']])->first_row();

            $this->db->select('od.*, lu.type, lu.user_id as id_agen_mitra')
                     ->from('la_order_details od')
                     ->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id')
                     ->join('la_user lu', 'lu.user_id = ls.user_id')
                     // ->where('ls.user_id',$ses['id'])
                     ->where('lu.regional', $regional->regional)
                     ->where('od.payment_status', '1')
                     ->order_by('od.s_no','DESC');
            return $this->db->get()->result();
            

        } else {   

            $this->db->select('od.*, lu.type');
            $this->db->from('la_order_details od');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
            $this->db->where('od.payment_status', '1');
            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get();
            return $query->result();

        }

    }
    
}