<?php
class User_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function user()
    {
    	$result = $this->db->where('type','0')->order_by('s_no','DESC')->get('la_user')->result();
    	return $result;
    }

    public function submitUser($input,$img)
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('users');
            exit();
        }

        if($input['user_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'type' => $input['type'],
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'type' => $input['type'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('user_id',$input['user_id']);
            $this->db->update('la_user',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('users');
        }
        else
        {
            $email = $input['email'];
            $getdata = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($getdata > 0)
            {
                $this->session->set_flashdata('error', EMAIL_EXIST);
                redirect('users');
            }
            else
            {

                if($img == '')
                {
                    $image = "default.png";
                }else{
                    $config['upload_path']   = 'assets/images/user/';
                    // return $config['upload_path']; exit();
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['overwrite']     = TRUE;
                    $config['max_size']      = 10000;
                    $config['file_name']     = time();
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    $image = "";
                    if($this->upload->do_upload('image')) {
                        $uploadData = $this->upload->data(); 
                        $image = $uploadData['file_name']; 
                    }
                }
                $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

                $data = array( 
                    'user_id' => $user_id,
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'type' => $input['type'],
                    'image' => $image,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('la_user',$data);
            
                $this->session->set_flashdata('success',DATA_SUBMIT);
                redirect('users');

            }
        }
    }

    public function submitUser_v2($input,$img)
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('user/updateUser/'.$input['user_id']);
            exit();
        }

        if(!empty($input['user_id']))
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'type' => $input['type'],
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'type' => $input['type'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('user_id',$input['user_id']);
            $this->db->update('la_user',$data);
            $this->session->set_flashdata('success', DATA_UPDATE);
            redirect('user/updateUser/'.$input['user_id']);
        }

    }

    public function changeUserStatus($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('la_user');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
    }

    public function updateUser($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        return $query;
    }

    public function getUserCount()
    {
        $num = $this->db->where('type','1')->get('la_user')->num_rows();
        return $num;
    }

    public function getLoundryOwnerCount()
    {
        $num = $this->db->where('type','2')->get('la_user')->num_rows();
        return $num;
    }

    public function user_limit($regional=null)
    {
        
        $this->db->where('status','1');

        if (!empty($regional)) {
            $this->db->where('regional', $regional)
                     ->where('user_id !=', $_SESSION['id']);
        } else {
            $this->db->where('type','0');
        }

        $this->db->order_by("s_no", "desc");
        $this->db->limit('10');
        $res = $this->db->get('la_user')->result();
        return $res;
        
    }

    public function getUserName($user_id)
    {
        $r = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        return $r['name'];
    }


}