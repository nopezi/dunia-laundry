<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {

	public function login($email,$password)
    {
        
        $data['email']    = $email;
        $data['password'] = $password;

        $sess_array       = array();
        $getdata = $this->db->where('email',$email)
                            ->where('password',$password)
                            ->where('status', '1')
                            ->where_in('type',['3','2','1'])
                            ->get('la_user')
                            ->row();

        $data_user = $this->db->where('status', '1')
			        		 ->where('type !=', '0')
			        		 ->get('la_user')
			        		 ->first_row();

        $shop = $this->db->get_where('la_laundry_shop', ['user_id'=>$getdata->user_id])->first_row();

        if(!empty($data_user) && !empty($getdata)) {

            $this->session->unset_userdata($sess_array);

            if ($getdata->type == '3') {

                $sess_array = [
                    'owner_name' => $getdata->name,
                    'email'      => $getdata->email,
                    'id'         => $getdata->user_id,
                    'status'     => $getdata->type,
                    'role'       => 'mitra',
                    'type'       => $getdata->type,
                    'regional'   => $getdata->regional,
                    'type_shop'  => $shop->type_shop,
                    'premium'    => $getdata->premium
                ];
                
                $this->session->set_userdata($sess_array);
                $dataget['get_data'] = $getdata;
                $dataget['see_data'] = $sess_array;

                redirect('franchise/home');
                
            } else if (!empty($shop)) {
                
                $this->session->unset_userdata($sess_array);

                $sess_array = [
                    'owner_name' => $getdata->name,
                    'email'      => $getdata->email,
                    'id'         => $getdata->user_id,
                    'status'     => $getdata->type,
                    'role'       => 'mitra',
                    'type'       => $getdata->type,
                    'regional'   => $getdata->regional,
                    'type_shop'  => $shop->type_shop,
                    'premium'    => $getdata->premium
                ];
                
                $this->session->set_userdata($sess_array);
                $dataget['get_data'] = $getdata;
                $dataget['see_data'] = $sess_array;

                if ($getdata->type == '3') {
                    redirect('franchise/home');
                } else {
                    redirect('mitra_agen/home');
                    // $this->session->sess_destroy();
                    // $this->session->set_flashdata('error', 'halaman agen/mitra sedang dalam pengembangan');
                    // redirect('laundryOwner/login');
                }

            } else {

                $this->session->set_flashdata('error', 'user belum terdaftar outlet / agen');
                redirect('laundryOwner/login');

            }

        } else {
            
            $this->session->set_flashdata('error', LOGIN_FLASH);
            redirect('laundryOwner/login');
            
        }

    }

    public function reset_password($input=null)
    {
        
        $subject = "[Win Laundry] Registrasi Akun Baru";
        $email = 'nopezisapura@gmail.com';
        $pesan = 'masok pak eko';
        return $this->send_email_by_msg($email, $subject, $pesan);

    }

    public function send_email_by_msg($email_id,$subject,$msg)
    {
       $KEY=$this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));
       $authKey=$KEY->msg91_key;

        // $authKey = MSG_AUTH_KEY;
        $from= SENDER_EMAIL;

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'to' => $email_id,
            'from' => $from,
            'subject' => $subject,
            'body' => $msg
        );

        print_r($postData);

       //API URL
        $url="https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $authKey ) ."&to=" . urlencode( $email_id ) . "&from=" . urlencode( $from ) ."&body=" . urlencode( $msg ) ."&subject=" . urlencode( $subject );
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);
        //Print error if any
        if(curl_errno($ch))
        {
          echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }

}

/* End of file Model_login.php */
/* Location: ./application/models/Model_login.php */