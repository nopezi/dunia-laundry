<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_agen extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Order_model');
	}

    public function detail_agen($shop_id)
    {
        $query = $this->db->where('shop_id',$shop_id)->get('la_laundry_shop')->row_array();
        return $query;
    }

    public function data_agen()
    {
        $ses = $_SESSION;
        
        $this->db->select('ls.*, lu.image as foto_profile');
        $this->db->from('la_laundry_shop ls');
        $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
        $this->db->where('lu.type', '1');
        if ($ses['type'] == 3) {
            $this->db->where('lu.regional', $ses['regional'])
                     ->where('lu.user_id != ', $ses['id']);
        }
        $this->db->order_by('ls.s_no','DESC');
        $query = $this->db->get();

        return $query->result();
        
    }

    public function get_mitra()
    {

        $result = $this->db->get_where('la_user', ['type'=>1])->result();
        
        return $result;
    }

    public function ganti_status($id)
    {
        $this->db->where('shop_id',$id);
        $query = $this->db->get('la_laundry_shop');
        $data = $query->row_array();
        if($data['status'] == '1'){
            $data = ['status'=>'0'];

        } else {
            $data = ['status'=>'1'];
            
        }
        // $this->db->update('la_user', $data, ['user_id'=>$id]);
        $this->db->update('la_laundry_shop', $data, ['shop_id'=>$id]);
        return $this->db->get_where('la_laundry_shop', ['shop_id'=>$id])->first_row();
    }

	public function tambah($input,$img)
    {
        
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
    	$this->form_validation->set_rules('shop_name', 'shop_name', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        // UPDATE DATA HEAR...!!
        if(!empty($img)) {

            $config['upload_path']   = 'assets/images/Laundry/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite']     = TRUE;
            $config['max_size']      = 10000;
            $config['file_name']     = time();
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            $image = "";
            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data(); 
                $image = $uploadData['file_name']; 
            }

        } else {

            $image = "default.png";

        }

        $shop_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

        $data = [
            'shop_id' => $shop_id,
            'user_id' => $input['user_id'],
            'shop_name' => $input['shop_name'],
            'country_code' => '62',
            'mobile' => $input['mobile'],
            'address' => $input['address']?$input['address']:'',
            'latitude' => $input['latitude']?$input['latitude']:'',
            'longitude' => $input['longitude']?$input['longitude']:'',
            'opening_time' => $input['opening_time']?$input['opening_time']:'',
            'closing_time' => $input['closing_time']?$input['closing_time']:'',
            'mulai_hari' => $input['mulai_hari'],
            'sampai_hari' => $input['sampai_hari'],
            'image' => $image,
            'description' => $input['description']?$input['description']:'',
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $this->db->insert('la_laundry_shop',$data);
        $this->session->set_flashdata('success', DATA_UPDATE);

        return 'berhasil';

    }

	public function update($input,$img)
    {

    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('shop_name', 'shop_name', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        if(!empty($input['shop_id'])) {

            // UPDATE DATA HEAR...!!
            if(!empty($img)) {

                $config['upload_path']   = 'assets/images/Laundry/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => '62',
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'description' => $input['description']?$input['description']:'',
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => '62',
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'description' => $input['description']?$input['description']:'',
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            # update gambar 
            $this->db->update('la_laundry_shop', $data, ['shop_id'=>$input['shop_id']]);
            $this->session->set_flashdata('success', DATA_UPDATE);

            return 'berhasil';

        }

    }

    public function total_pendapatan($shop_id = null)
    {

        if (!empty($shop_id)) {
            
            $res = $this->db->select('sum(final_price) as final_price')
                             ->where('shop_id', $shop_id)
                             ->where('status != ', '5')
                             ->where('order_status !=', '6')
                             ->get('la_order_details')
                             ->result();

            if (!empty($res[0]->final_price)) {
                return 'Rp.'.number_format($res[0]->final_price);
            } else {
                return 'Rp. 0';
            }

        } else {

            $res = $this->db->query("SELECT SUM(final_price) AS final_price FROM la_order_details")->result();
            return 'Rp.'.number_format($this->pembulatan($res[0]->final_price));

        }

    }

    public function total_komisi($shop_id)
    {

    	$persentasi_komisi = $this->Order_model->cekPersentasi(1);

    	$data = $this->db->select('final_price')
    					 ->from('la_order_details')
    					 ->where('shop_id', $shop_id)
    					 ->where('order_status !=', '6')
    					 ->get()
    					 ->result();

    	if (empty($data)) {
    		return 'Rp. 0';
    	} else {

    		foreach ($data as $key => $value) {
    			$cek_dolar  = str_replace('$', '', $value->final_price);
	            $cek_rupiah = str_replace('Rp', '', $cek_dolar);
	            $cek_rupiah = str_replace('₹', '', $cek_rupiah);
	            $cek_rupiah = str_replace('?', '', $cek_rupiah);
    			$total[]    = $cek_rupiah * $persentasi_komisi['nilai'];
    		}

    		return 'Rp.'.number_format(array_sum($total));

    	}

    }

    public function total_pembeli($shop_id)
    {

    	// $pembeli = $this->db->select('la_order_details.user_id')
					// 		->distinct()
		   //          		->join('la_user lu', 'lu.user_id=la_order_details.user_id')
		   //          		->where('la_order_details.shop_id', $shop_id)
		   //          		->where('la_order_details.status != ', '6')
		   //          		->get('la_order_details')
		   //          		->num_rows();
    	$pembeli = $this->db->select('user_id')
							->distinct()
		            		->where('shop_id', $shop_id)
		            		->where('order_status !=', '6')
		            		->get('la_order_details')
		            		->num_rows();

		return $pembeli?:0;

    }

    public function total_order($shop_id)
    {

    	$order = $this->db->select('user_id')
		            	  ->where('shop_id', $shop_id)
		            	  ->where('order_status !=', '6')
		            	  ->get('la_order_details')
		            	  ->num_rows();

		return $order?:0;

    }

    public function delete($shop_id)
    {
        $gambar = $this->db->get_where('la_laundry_shop', ['shop_id'=>$shop_id])->first_row();
        unlink('assets/images/Laundry/'.$gambar->image);
        return $this->db->delete('la_laundry_shop', array('shop_id' => $shop_id));
    }

}

/* End of file Model_agen.php */
/* Location: ./application/models/Model_agen.php */