<?php
class Api_Home_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getHomeData($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('latitude', 'latitude', 'required');
        $this->form_validation->set_rules('longitude', 'longitude', 'required');
        if ($this->form_validation->run() == false) {
            $this->Api_Auth_model->responseFailed(0, "Please fill all field");
            exit();
        }

        $userExist = $this->db->where('user_id',$input['user_id'])->where('status','1')->get('la_user')->num_rows();
        if($userExist > 0)
        {
            //Advertise
            $advertis = $this->db->where('status','1')->get('la_advertisement')->result();
            $advertiss = array();
            foreach($advertis as $advertis)
            {
                $advertis->image = "assets/images/advertise/".$advertis->image;
                array_push($advertiss, $advertis);
            }
            $data['advertis'] = $advertiss;

            //Service
            $service = $this->db->where('status','1')->get('la_service')->result();
            $services = array();
            foreach($service as $service)
            {
                $service->image = "assets/images/Service/".$service->image;
                array_push($services, $service);
            }
            $data['service'] = $services;

            //Laundry
            $laundry = $this->db->where('status','1')->get('la_laundry_shop')->result();
            $laundrys = array();
            foreach($laundry as $laundry)
            {
                $laundry->image = "assets/images/Laundry/".$laundry->image;
                array_push($laundrys, $laundry);
            }
            $data['laundry'] = $laundrys;

            //Offer
            // $offer = $this->db->where('status','1')->get('la_offer')->result();
            $offer = $this->get_offer();
            $offers = array();
            foreach($offer as $offer)
            {
                $offer->image = "assets/images/Offer/".$offer->image;
                $offer->description = "Get ".$offer->amount. " ".$offer->amount_type." off on ".$offer->service_name;
                $offer->detail = $offer->amount. " ".$offer->amount_type." off here your code ends";
                array_push($offers, $offer);
            }
            $data['offer'] = $offers;

            //Laundry near by
            $latitude = $input['latitude'];
            $longitude = $input['longitude'];
            $user_id = $input['user_id'];
            // $near_bys = array();
            $data['near_by'] =$this->Api_Home_model->getNearestData($latitude,$longitude,'la_laundry_shop',$user_id);
            
            return $data;
        }
        else
        {
            $this->Api_Home_model->responseFailed(3, DEACTIVE);
            exit();
        }

    }

    function get_offer()
    {
        $this->db->select('o.*,s.service_name');
        $this->db->from('la_offer o');
        $this->db->join('la_service s', 's.service_id = o.applicable_on_service');
        $this->db->where('o.status','1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getNearestData($lat,$lng,$table,$user_id)
    {
        
        $this->db->select("*, CONCAT('assets/images/Laundry/',image) as shop_image,
         ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
        $this->db->from($table); 
        $this->db->order_by('distance');                    
        // $this->db->limit(1, 0);
        $query =$this->db->get(); 
        $data = $query->result();
        if($data != '')
        {
            return $data;
        }
        else
        {
            $this->db->select("*");
            $this->db->from($table); 
            // $this->db->having('distance <= ', '10');                    
            // $this->db->order_by('distance');                    
            // $this->db->limit(1, 0);
            $query =$this->db->get(); 
            $data = $query->result();
            return $data;
        }
        
    }

    public function search($shop_name)
    {
        $this->form_validation->set_rules('shop_name', 'shop_name', 'required');
        if ($this->form_validation->run() == false) {
            $this->Api_Auth_model->responseFailed(0, "Please search any laundry mitra / agen");
            exit();
        }
        if ($shop_name) {
            $shops = $this->getAllDataWhereProducts1($shop_name, 'la_laundry_shop');
        } //$shop_name


        if ($shops) {
            $shop = array();
            foreach ($shops as $shops) {

                if (empty($shops['image'])) {
                    $shops['image'] = "assets/images/Laundry/default.jpg";
                } else {
                    $shops['image'] = "assets/images/Laundry/".$shops['image'];
                }

                array_push($shop, $shops);
            }

            $this->Api_Home_model->responseSuccess(1, 'All Laundry Agen / Mitra', $shop);
        } //$shops
        // else {
        //     // $this->Api_Home_model->responseFailed(0, NOT_RESPONDING);
        //     $pesan = 'Mitra/ Agen '.$shop_name.' tidak di temukan';
        //     $this->Api_Home_model->responseFailed(0, $pesan);
        // }
    }

    public function getAllDataWhereProducts1($keyword,$table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->like('shop_name', $keyword,"both");
        $this->db->where('status', '1');
        //$this->db->where('p_name','');
        return $this->db->get()->result_array();
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}