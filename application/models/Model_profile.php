<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_profile extends CI_Model {

	public function update($input, $img = null)
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return false;
        }

        if(!empty($input['user_id'])) {

            if(!empty($img)) {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                	'name' => $input['name'],
                    'email' => $input['email'],
                    'image' => $image,
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'type' => $input['type'],
                ];

            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                	'name' => $input['name'],
                    'email' => $input['email'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'password' => $input['password'],
                    'address' => $input['address'] ?: '',
                    'latitude' => $input['latitude'] ?: '',
                    'longitude' => $input['longitude'] ?: '',
                    'type' => $input['type'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
            $this->db->where('user_id',$input['user_id']);
            $this->db->update('la_user',$data);
            $this->session->set_flashdata('success', DATA_UPDATE);
            
            return true;
        }

    }	

}

/* End of file Model_profile.php */
/* Location: ./application/models/Model_profile.php */