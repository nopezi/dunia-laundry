<?php
class Api_Laundry_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getAllLaundryShop($input)
    {
      $this->form_validation->set_rules('count', 'count', 'required');
      $this->form_validation->set_rules('latitude', 'latitude', 'required');
      $this->form_validation->set_rules('longitude', 'longitude', 'required');
      if ($this->form_validation->run() == false) {
          $this->Api_Auth_model->responseFailed(0, "Please fill all field");
          exit();
      }
      $count = $input['count'];
      $lat = $input['latitude'];
      $lng = $input['longitude'];
      $table = 'la_laundry_shop';
      $query = $this->db->where('status','1')->get('la_laundry_shop')->num_rows();
      if($query > 0)
      {
        $this->db->select("*, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance ");
        $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
        $this->db->from($table);
        $this->db->where('status','1');
        // $this->db->having('distance <= ', '10');                    
        $this->db->order_by('distance');                    
        $this->db->limit($count);
        $query =$this->db->get(); 
        $res = $query->result(); 
        if($res !='')
        {
          return $res;
        }
        else
        {
          $this->db->select("*");
          $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
          $this->db->from($table);
          $this->db->where('status','1');
          // $this->db->having('distance <= ', '10');                    
          // $this->db->order_by('distance');                    
          $this->db->limit($count);
          $query =$this->db->get(); 
          $res = $query->result();
          return $res;
        }

      }else{
        return 0;
      }
    }

    public function getLaundryById($input)
    {
      $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
      if ($this->form_validation->run() == false) {
          $this->Api_Auth_model->responseFailed(0, "Please fill all field");
          exit();
      }
      $shop_id = $input['shop_id'];
      $table = 'la_laundry_shop';
      $query = $this->db->where('status','1')->get('la_laundry_shop')->num_rows();
      if($query > 0)
      {
        $this->db->select("*");
        $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
        $this->db->from($table);
        $this->db->where('status','1');
        $this->db->where('shop_id',$shop_id);
        $this->db->limit($count);
        $query =$this->db->get(); 
        $res = $query->row(); 
        if($res !='')
        {
          return $res;
        }
        else
        {
          $this->db->select("*");
          $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
          $this->db->from($table);
          $this->db->where('status','1');
          $this->db->where('shop_id',$shop_id);
          // $this->db->having('distance <= ', '10');                    
          // $this->db->order_by('distance');                    
          $this->db->limit($count);
          $query =$this->db->get(); 
          $res = $query->row();
          return $res;
        }

      }else{
        return 0;
      }
    }

    public function getLaundryByService($input)
    {
      $this->form_validation->set_rules('service_id', 'service_id', 'required');
      $this->form_validation->set_rules('latitude', 'latitude', 'required');
      $this->form_validation->set_rules('longitude', 'longitude', 'required');
      if ($this->form_validation->run() == false) {
          $this->Api_Auth_model->responseFailed(0, "Please fill all field");
          exit();
      }
      $query = $this->db->where('service_id',$input['service_id'])->get('la_price_list');
      if($query->num_rows() > 0)
      {
        $lat = $input['latitude'];
        $lng = $input['longitude'];
        $table = 'la_laundry_shop';
        $shop = $this->db->where('service_id',$input['service_id'])->group_by('shop_id')->get('la_price_list')->result(); 
        $near_bys = array();
        foreach($shop as $shop)
        {
            $shop_detail[] = $this->getAllDataWhere($table,$shop->shop_id,$lat,$lng);
        }

        if($shop_detail !='')
        {
          return $shop_detail; 
        }else
        {
          return $near_bys;
        }
        

        // $shop_id = $shop['shop_id'];
        // $lat = $input['latitude'];
        // $lng = $input['longitude'];
        // $table = 'la_laundry_shop';

        // $laundry = $this->db->where('shop_id',$shop_id)->where('status','1')->get('la_laundry_shop');
        // if($laundry->num_rows() > 0)
        // {
        //   $this->db->select("*, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance ");
        //   $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
        //   $this->db->from($table);
        //   $this->db->where('status','1');
        //   $this->db->where('shop_id',$shop_id);
        //   $this->db->having('distance <= ', '5');                    
        //   $this->db->order_by('distance');                    
        //   $this->db->limit('20');
        //   $query =$this->db->get(); 
        //   return $query->result(); 
        // }
        // else{
        //   return 0;
        // }
      }
      else
      {
        return 1;
      }

    }

    public function getAllDataWhere($table,$shop_id,$lat,$lng)
    {
        $this->db->select("*, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance ");
        $this->db->select('CONCAT("assets/images/Laundry/",image) as image');
        $this->db->from($table);
        $this->db->where('status','1');
        $this->db->where('shop_id',$shop_id);
        $this->db->having('distance <= ', '10');                    
        $this->db->order_by('distance');                    
        $this->db->limit('20');
        $query =$this->db->get(); 
        $data = $query->row_array(); 
        if($data !='')
        {
            return $data;
        }
        else
        {
            return $data = '';
        }
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}