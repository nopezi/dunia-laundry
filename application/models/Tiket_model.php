<?php
class Tiket_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function tiketSupport()
    {
    	$tiket = $this->db->order_by('tiket_id','DESC')->get('la_tiket')->result();
    	return $tiket;
    }

    public function getLastTiket($tiket_id)
    {
    	$this->db->where('tiket_id',$tiket_id);
    	$this->db->where('is_admin','0');
    	$this->db->order_by('tiket_detail_id','desc');
    	$this->db->limit('1');
    	$row = $this->db->get('la_tiket_detail')->row_array();
    	return $row;
    }

    public function getAllTiketData($tiket_id)
    {
    	$this->db->where('tiket_id',$tiket_id);
    	$row = $this->db->get('la_tiket_detail')->result();
    	return $row;
    }

    public function tiketApprve($status,$tiket_id)
    {
    	$data = array( 'status' => $status );
    	$this->db->where('tiket_id',$tiket_id)->update('la_tiket',$data);

        $tic = $this->db->where('tiket_id',$tiket_id)->get('la_tiket')->row_array();
        $user_id = $tic['user_id'];

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        $device_token = $user['device_token'];
        $type = 7005;
        $message = TICKET_APPROVED;
        $this->Notification_model->firebase_with_class($device_token, '', '',$type, 'Ticket', $message);
    }

    public function tiketReject($status,$tiket_id)
    {
    	$data = array( 'status' => $status );
    	$this->db->where('tiket_id',$tiket_id)->update('la_tiket',$data);

        $tic = $this->db->where('tiket_id',$tiket_id)->get('la_tiket')->row_array();
        $user_id = $tic['user_id'];

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        $device_token = $user['device_token'];
        $type = 7005;
        $message = TICKET_REJECTED;
        $this->Notification_model->firebase_with_class($device_token, '', '',$type, 'Ticket', $message);
        
    }

    public function changeTiketStatus($status,$tiket_id)
    {
    	$data = array( 'status' => $status );
    	$this->db->where('tiket_id',$tiket_id)->update('la_tiket',$data);

        if($status == '1')
        {
            $message = "Your ticket is inprocess.";
        }
        else if($status == '2')
        {
            $message = "Your ticket process is completed";
        }
        else if($status == '3')
        {
            $message = "Your ticket has been rejected.";
        }
        $tic = $this->db->where('tiket_id',$tiket_id)->get('la_tiket')->row_array();
        $user_id = $tic['user_id'];

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        $device_token = $user['device_token'];
        $type = 7005;
        $this->Notification_model->firebase_with_class($device_token, '', '',$type, 'Ticket', $message);
        // return $data;
    }

    public function setAdminMsg($message,$tiket_id)
    {
    	$data = array(
    		'tiket_id' => $tiket_id,
    		'message' => $message,
    		'is_admin' => '1',
    		'created_at' => date('Y-m-d H:i:s')
    	);
    	$this->db->insert('la_tiket_detail',$data);

        $tic = $this->db->where('tiket_id',$tiket_id)->get('la_tiket')->row_array();
        $user_id = $tic['user_id'];

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        $device_token = $user['device_token'];
        $type = 7005;
        $data = $this->Notification_model->firebase_with_class($device_token, '', '',$type, 'Ticket', $message);
        // return $data;
    }


}