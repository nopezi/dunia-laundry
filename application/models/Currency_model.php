<?php
class Currency_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function currency()
    {
    	$result = $this->db->get('la_currency')->result();
    	return $result;
    }

    public function submitCurrency($input)
    {
    	$this->form_validation->set_rules('currency_name', 'currency_name', 'required');
    	$this->form_validation->set_rules('currency_symbol', 'currency_symbol', 'required');
    	$this->form_validation->set_rules('currency_code', 'currency_code', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('currency/currency');
            exit();
        }
        $id = $input['currency_id'];
        if($id !='')
        {
        	//update
        	$data = array(
        		'currency_name' => $input['currency_name'],
		        'currency_symbol' => $input['currency_symbol'],
		        'currency_code' => $input['currency_code'],
		        'updated_at' => date('Y-m-d H:i:s')
        		);
        	$this->db->where('currency_id',$id);
        	$this->db->update('la_currency',$data);
        	return 0;
        }else{
        	//insert
        	$data = array(
        		'currency_name' => $input['currency_name'],
		        'currency_symbol' => $input['currency_symbol'],
		        'currency_code' => $input['currency_code'],
		        'created_at' => date('Y-m-d H:i:s')
        		);
        	$this->db->insert('la_currency',$data);
        	return 1;
        }
        
    }

    public function getActiveCurrency()
    {
    	$row = $this->db->where('status','1')->get('la_currency')->row();
    	return $row;
    }

    public function updateSingleRow($table, $where, $data)
    {                 
        $this->db->where($where);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0)
        {
          return TRUE;
        }
        else
        {
          return FALSE;
        }
    }

    public function updateCurrency($id)
    {
        $query = $this->db->where('currency_id',$id)->get('la_currency')->row_array();
        return $query;
    }

    public function deleteCurrency($id)
    {
        $query = $this->db->where('currency_id',$id)->delete('la_currency');
        return 1;
    }

}