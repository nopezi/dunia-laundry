<?php
class Notification_model extends CI_Model
{
    // protected $table = 'bd_user_detail';
    
    function __construct()
    {
        parent:: __construct();
    }

    public function notification()
    {
    	$res = $this->db->get('la_notification')->result();
    	return $res;
    }

    public function activeUser()
    {
    	$res = $this->db->where('status','1')->where('type','1')->get('la_user')->result();
    	return $res;
    }

    public function send_notification($input)
    {
        $uid = $input['uid'];
        if($uid == 'send_all')
        {
            $data = $this->db->where('status','1')->where('type','1')->get('la_user')->result();
            
            foreach($data as $k => $r)
            {
                $user_id = $r->user_id;
                $device_token = $r->device_token;
                if($device_token!='')
                {
                    $device_token;
                    $title   = $input['title'];
                    $msg     = $input['message'];
                    $type = '7001';
                    $this->Notification_model->firebase_with_class($device_token, '', '',$type, $title, $msg);
                    $data1['user_id']    = $user_id;
                    $data1['title']      = $title;
                    $data1['message']    = $msg;
                    $data1['type']       = $type;
                    $data1['created_at']   = date('Y-m-d H:i:s');
                    $this->db->insert('la_notification',$data1);
                }
               
            }
        }else{
        	
            $id = explode(',', $input['uid']); 

            $count = count($id);
            for ($i = 0; $i < $count; $i++) {
                $user_id = $id[$i];
                $title   = $input['title'];
                $msg     = $input['message'];
                if ($user_id != '') 
                {
                    $getToken     = $this->Base_model->getSingleRow('la_user', array(
                        'user_id' => $user_id
                    ));
                    $type = '2';
                    $device_token = $getToken->device_token;
                    $this->Notification_model->firebase_with_class($device_token, '', '',$type, $title, $msg);
                    $data2['user_id']    = $user_id;
                    $data2['title']      = $title;
                    $data2['message']    = $msg;
                    $data2['type']       = $type;
                    $data2['created_at']   = date('Y-m-d H:i:s');
                    $this->db->insert('la_notification', $data2);
                }
            }
        }

    }

    public function firebase_with_class($device_token, $sender_id, $senderName,$type, $title, $msg1)
    {
        $user = $this->Base_model->getSingleRow('la_user', array(
            'device_token' => $device_token
        ));

         $FIRE_BASE_KEY=$this->Base_model->getSingleRow('la_firebase_key',array('id'=>1));
         $api_key=$FIRE_BASE_KEY->firebase_key;
        // $FIRE_BASE_KEY = FIRE_BASE_KEY;
        // $api_key = $FIRE_BASE_KEY;

        if ($user->device_type == "ios") 
        {
            $API_ACCESS_KEY = $api_key;
            
            $msg = array(
                'body' => $msg1,
                'title' => $title,
                'icon' => 'myicon',
                'type' => $type,
                /*Default Icon*/
                'sound' => 'default'
                /*Default sound*/
            );
            $fields = array(
                'to' => $device_token,
                'notification' => $msg
            );
            
            
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        } 
        else 
        {
            $FCMPushNotification = new \BD\FCMPushNotification($api_key);
            $sDeviceToken        = $device_token;

            $senderName        = $user->name;
            $sender_id        = $user->user_id;
       
            $aPayload = array(
                'data' => array('title' =>$title,'type' => $type,"sender_id"=>$sender_id,'body'=> $msg1, "senderName"=>$senderName)
            );
            
         
            $aOptions = array(
                'time_to_live' => 15 //means messages that can't be delivered immediately are discarded. 
            );
            
            $aResult = $FCMPushNotification->sendToDevice(
                $sDeviceToken,      
                $aPayload,
                $aOptions // optional
            );
            // echo "<pre>";
            // print_r($aResult); die;

            return $aResult;
        }
    } 

}
