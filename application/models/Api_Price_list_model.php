<?php
class Api_Price_list_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getItembyShopId($shop_id)
    {
        $cek = $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        
        if ($this->form_validation->run() == false)
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }
    	$query = $this->db->where('shop_id',$shop_id)->where('status','1')->get('la_price_list')->num_rows();
    	if($query > 0)
    	{
            $price_list = $this->Api_Price_list_model->getPriceforService($shop_id);
            $price_lists = array();
            foreach($price_list as $price_list)
            {
                // $service = $this->Api_Price_list_model->getAllDataWhere(array(
                //     'service_id' => $price_list->service_id
                // ), 'la_price_list');
                $service = $this->Api_Price_list_model->getAllDataWhere1($price_list->service_id);
            
                $cur = $this->db->where('status','1')->get('la_currency')->row_array();
                $data['currency_code'] = $cur['currency_symbol'];
                
                $price_list->services = $service;
                array_push($price_lists, $price_list);
                $data['item_list'] = $price_lists;
            }
            return $data;

    	}else{
    		return 0;
    	}
    }

    public function getAllDataWhere($where, $table)
    {
        $this->db->where($where);
        $this->db->select('*,CONCAT("'.base_url().'assets/images/Product/",image) as image ');
        $this->db->from($table);
        $query = $this->db->get();          
        return $query->result();
    }

    public function getAllDataWhere1($service_id)
    {
        $this->db->select('pl.*,s.service_name');
        $this->db->select('CONCAT("'.base_url().'assets/images/Product/",pl.image) as image');
        $this->db->from('la_price_list pl');
        $this->db->join('la_service s', 's.service_id = pl.service_id');

        $this->db->where('pl.service_id',$service_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getPriceforService($shop_id)
    {
        $this->db->select('pl.service_id,s.service_name');

        $this->db->from('la_price_list pl');
        $this->db->join('la_service s', 's.service_id = pl.service_id');

        $this->db->where('pl.shop_id',$shop_id);
        $this->db->group_by('pl.service_id'); 
        $this->db->group_by('s.service_name'); 
        $query = $this->db->get();
        return $query->result();
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}