<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_franchise extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Order_model');
	}

    public function get_data()
    {
        $result = $this->db->where_in('type',['3'])->order_by('s_no', 'desc')->get('la_user')->result();

        foreach ($result as $key => $value) {
            $wilayah = null;
            $distrik = null;
            if (!empty($value->regional)) {
                $wilayah = $this->db->get_where('la_regencies', ['id'=>$value->regional])->first_row();
                $wilayah = $wilayah->name;
            }
            if (!empty($value->distrik)) {
                $distrik = $this->db->get_where('la_districts', ['id'=>$value->distrik])->first_row();
                $distrik = $distrik->name;
            }
            $value->wilayah = $wilayah;
            $value->kecamatan = $distrik;

        }
        
        return $result;
    }

    public function detail($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row();
        return $query;
    }

    public function payment_history($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_payments')->result();
        return $query;
    }

	public function total_komisi()
	{

		$p_franchise = $this->Order_model->cekPersentasi(3);
        $persentasi_franchise = $p_franchise['persentasi'] / 100;

        $lokasi_regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
        
        if (!empty($_SESSION)) {

        	$semua_mitra_agen = $this->db->select('ls.user_id, 
                                               ls.shop_id, 
                                               ls.shop_name,
                                               lu.regional,
                                               lu.type')
                                     ->distinct()
                                     ->from('la_laundry_shop ls')
                                     ->join('la_user lu', 'lu.user_id=ls.user_id')
                                     ->where('lu.regional', $_SESSION['regional'])
                                     ->get()
                                     ->result();

            foreach ($semua_mitra_agen as $key => $sma) {

            	$data_pembeli = $this->db->select('la_order_details.user_id')
            						 ->distinct()
				            		 ->join('la_user lu', 'lu.user_id=la_order_details.user_id')
				            		 ->where('la_order_details.shop_id', $sma->shop_id)
				            		 ->get('la_order_details')
				            		 ->num_rows();
            	
            	$d_order = $this->Order_model->komisi_all($sma->shop_id);
            	$persentasi_agen = $this->Order_model->cekPersentasi($sma->type);
            	$income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];
                $total_franchise = ($d_order['total']->total_pendapatan - $income) * $persentasi_franchise;
                $total_pendapatan_kotor[]  = $d_order['total']->total_pendapatan - $income;  
                $total_pendapatan_bersih[] = $total_franchise;
                $total_pesanan[]		   = $d_order['total_pesanan'];
                $total_pembeli[]		   = $data_pembeli;

            }
				            		 
			$hasil['total_pembeli']			  = array_sum($total_pembeli);
            $hasil['total_pesanan']           = number_format(array_sum($total_pesanan));
            $hasil['total_pendapatan_kotor']  = number_format(array_sum($total_pendapatan_kotor));
            $hasil['total_pendapatan_bersih'] = number_format(array_sum($total_pendapatan_bersih));

            return $hasil;

        }

        return $data;

	}

    public function tambah($input,$img)
    {

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('regional', 'regional', 'required');

        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        # 1 kota/kabupaten hanya 1 franchise
        if (empty($input['user_id']) && ($input['type'] == 3)) {

            $cek_regional = $this->db->get_where('la_user', [
                                'regional' =>$input['regional'],
                                'type'     => '3'
                            ])->first_row();
            if (!empty($cek_regional)) {
                $this->session->set_flashdata('error', 'Untuk saat ini hanya terdapat 1 franchise Untuk setiap kota/kabupaten');
                return 'error';
            }

        }

        $email = $input['email'];
        $getdata = $this->db->where('email',$email)->get('la_user')->num_rows();

        if($getdata > 0) {

            $this->session->set_flashdata('error', EMAIL_EXIST);
            return 'error';

        } else {

            if(empty($img)) {
                $image = "default.png";
            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = null;
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            // $user_id = rand(100000,999999);
            $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

            $data = [
                'user_id'      => $user_id,
                'name'         => $input['name'],
                'email'        => $input['email'],
                'country_code' => '62',//$input['country_code'],
                'mobile'       => $input['mobile'],
                'password'     => $input['password'],
                'address'      => $input['address']?$input['address']:'',
                'regional'     => $input['regional'],
                'distrik'      => $input['distrik'],
                'latitude'     => $input['latitude']?$input['latitude']:'',
                'longitude'    => $input['longitude']?$input['longitude']:'',
                'image'        => $image,
                'type'         => $input['type'],
                'created_at'   => date('Y-m-d H:i:s')
            ];
            $this->db->insert('la_user',$data);
        
            $this->session->set_flashdata('success',DATA_SUBMIT);
            return 'berhasil';

        }
    }

    public function update($input,$img)
    {

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('regional', 'regional', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        # 1 kota/kabupaten hanya 1 franchise
        if (empty($input['user_id']) && ($input['type'] == 3)) {

            $cek_regional = $this->db->get_where('la_user', ['regional'=>$input['regional']])->first_row();
            if (!empty($cek_regional)) {

                $this->session->set_flashdata('error', 'Untuk saat ini hanya terdapat 1 franchise Untuk setiap kota/kabupaten');
                return 'error';

            }

        }

        if(!empty($input['user_id'])) {

            // UPDATE DATA HEAR...!!
            if(empty($img)) {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => '62',
                    'mobile'       => $input['mobile'],
                    'password'     => $input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'type'         => $input['type'] ?:'1',
                    'updated_at'   => date('Y-m-d H:i:s'),
                ];

            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => '62',
                    'mobile'       => $input['mobile'],
                    'password'     => $input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'image'        => $image,
                    'type'         => $input['type'] ?:'1',
                    'updated_at'   => date('Y-m-d H:i:s'),
                ];
            }

            $this->db->update('la_user',$data, ['user_id'=>$input['user_id']]);
            $this->session->set_flashdata('success', DATA_UPDATE);

            return 'berhasil';

        } else {
            $this->session->set_flashdata('error', 'id user tidak boleh kosong');
            return 'error';
        }

    }

    public function ganti_status($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('la_user');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
    }

    public function delete($id)
    {
        $gambar = $this->db->get_where('la_user', ['user_id'=>$id_user])->first_row();
        unlink('assets/images/user/'.$gambar->image);
        return $this->db->delete('la_user', ['user_id' => $id]);
    }

    public function submitOwnerPackage($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('package_id', 'package_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        $payment_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

        $user_id = $input['user_id'];
        $package_id = $input['package_id'];
        $pkg = $this->db->where('package_id',$package_id)->get('la_subscription_package')->row_array();
        $no_of_days = $pkg['no_of_days'];
        $no = $no_of_days+'1';
        $date = date('Y-m-d', strtotime("+$no days"));
        $data = [
               'user_id' => $user_id,
               'payment_id' => $payment_id,
               'amount' => $pkg['amount'],
               'package_id' => $package_id,
               'end_subscription_date' => $date,
               'payment_type' => '2',
               'created_at' => date('Y-m-d H:i:s'),
            ];

        $insert = $this->db->insert('la_payments', $data);

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        if(!empty($user['end_subscription_date'])) {

            if($user['end_subscription_date'] >= date('Y-m-d')) {

                $end_subscription_date = $user['end_subscription_date'];
                $now = time(); // or your date as well
                $your_date = strtotime("$end_subscription_date");
                $datediff =  $your_date - $now;

                $day = round($datediff / (60 * 60 * 24));
                $num = $day+$no;
                $dates = date('Y-m-d', strtotime("+$num days"));
                // echo $dates; die;
            } else {
                $no = $no_of_days+'1';
                $dates = date('Y-m-d', strtotime("+$no days"));
            }
            
        } else {
            $no = $no_of_days+'1';
            $dates = date('Y-m-d', strtotime("+$no days"));
        }

        $data1 = array(
            'end_subscription_date' => $dates
        );
        $sucess = $this->db->where('user_id',$user_id)->update('la_user',$data1);
        if($sucess) {
            $email = $user['email'];
            $name = $user['name'];
            $amount = $pkg['amount'];
            $subject = SUBSCRIPTION_HEAD;
            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $amount Rs. and it will be available till $dates, thank you.";
            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
        }
        $this->session->set_flashdata('success',DATA_SUBMIT);
        return 'berhasil';
        
    }	

}

/* End of file Model_franchise.php */
/* Location: ./application/models/Model_franchise.php */