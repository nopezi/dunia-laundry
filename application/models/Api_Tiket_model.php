<?php
class Api_Tiket_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function addTiket($input)
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
    	//Generate New Tiket
    	$userExist = $this->db->where('user_id',$input['user_id'])->get('la_user');
        if($userExist->num_rows() > 0)
        {
        	$data = array(
        		'tiket_no' => rand(111100000,999999999),
        		'user_id' => $input['user_id'],
    			'title' => $input['title'],
                'description' => $input['description'],
        		'created_at' => date('Y-m-d H:i:s')
        			);
        	$this->db->insert('la_tiket',$data);
        	
            $ru = $userExist->row();
            $msg  = $input['description'];
            $type = 7005;
            $this->Notification_model->firebase_with_class($ru->device_token, $input['user_id'], $ru->name,$type, "Ticket", $msg);

        	return 1;
        	
        }else{
        	return 0;
        }
        
    }

    public function addTiketComment($input)
    {
        $this->form_validation->set_rules('tiket_id', 'tiket_id', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $tiketExist = $this->db->where('tiket_id',$input['tiket_id'])->get('la_tiket')->num_rows();
        if($tiketExist > 0)
        {
            $data1 = array(
                'tiket_id' => $input['tiket_id'],
                'message' => $input['message'],
                'created_at' => date('Y-m-d H:i:s')
                    );
            $this->db->insert('la_tiket_detail',$data1);
            return 1;
        }else{
            return 0;
        }

    }

    public function addTiketOld($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        // $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        if($input['tiket_id'] =='')
        {
            //Generate New Tiket
            $userExist = $this->db->where('user_id',$input['user_id'])->get('la_user')->num_rows();
            if($userExist > 0)
            {
                $data = array(
                    'tiket_no' => rand(111100000,999999999),
                    'user_id' => $input['user_id'],
                    'title' => $input['title'],
                    'created_at' => date('Y-m-d H:i:s')
                        );
                $this->db->insert('la_tiket',$data);
                $tiket_id = $this->db->insert_id();
                if($tiket_id !='')
                {
                    $data1 = array(
                        'tiket_id' => $tiket_id,
                        'message' => $input['message'],
                        'created_at' => date('Y-m-d H:i:s')
                            );
                    $this->db->insert('la_tiket_detail',$data1);
                    return 1;
                }else{
                    return 2;
                }
                
            }else{
                return 0;
            }
        }else{

            $data1 = array(
                'tiket_id' => $input['tiket_id'],
                'message' => $input['message'],
                'created_at' => date('Y-m-d H:i:s')
                    );
            $this->db->insert('la_tiket_detail',$data1);
            return 3;
        }
        
    }

    public function tiketList($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $row = $this->db->where('user_id',$input['user_id'])->get('la_user')->num_rows();
        if($row > 0)
        {
            $userExist = $this->db->where('user_id',$input['user_id'])->get('la_tiket');
            if($userExist->num_rows() > 0)
            {
                // $tiket_list = $userExist->result();
                $tiket_list = $this->getTiketData($input['user_id']);
                // $tiket_lists = array();
                // foreach($tiket_list as $tiket_list)
                // {
                //     $message = $this->getAllDataWhere(array(
                //         'tiket_id' => $tiket_list->tiket_id
                //     ), 'la_tiket_detail');

                //     $tiket_list->message = $message;
                //     array_push($tiket_lists, $tiket_list);
                //     $data['item_list'] = $tiket_lists;
                // }
                return $tiket_list;
            }
            else
            {
                return 1;
            }
        }else{
            return 0;
        }

    }

    public function tiketListOld($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        
        $row = $this->db->where('user_id',$input['user_id'])->get('la_user')->num_rows();
        if($row > 0)
        {
            $userExist = $this->db->where('user_id',$input['user_id'])->get('la_tiket');
            if($userExist->num_rows() > 0)
            {
                // $tiket_list = $userExist->result();
                $tiket_list = $this->getTiketData($input['user_id']);
                $tiket_lists = array();
                foreach($tiket_list as $tiket_list)
                {
                    $message = $this->getAllDataWhere(array(
                        'tiket_id' => $tiket_list->tiket_id
                    ), 'la_tiket_detail');

                    $tiket_list->message = $message;
                    array_push($tiket_lists, $tiket_list);
                    $data['item_list'] = $tiket_lists;
                }
                return $data;
            }
            else
            {
                return 1;
            }
        }else{
            return 0;
        }
        
    }

    public function getTiketComment($tiket_id)
    {
        $this->form_validation->set_rules('tiket_id', 'tiket_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $tiketExist = $this->db->where('tiket_id',$tiket_id)->get('la_tiket_detail');
        if($tiketExist->num_rows() > 0)
        {
            // $tiketComment = $tiketExist->result();
                $message = $this->getAllDataWhere(array(
                        'tiket_id' => $tiket_id
                    ), 'la_tiket_detail');
            return $message;
        }else{ 
            return 0;
        }
    }

    function getTiketData($user_id)
    {
        $result = $this->db->select('tiket_id,user_id,title,description,status,created_at')->where('user_id',$user_id)->get('la_tiket')->result();
        foreach ($result as $i => $r) 
        {
            $get_data[$i] = array();
            $get_data[$i]['tiket_id'] = $r->tiket_id;
            $get_data[$i]['user_id'] = $r->user_id;
            $get_data[$i]['title'] = $r->title;
            $get_data[$i]['description'] = $r->description;
            $get_data[$i]['status'] = $r->status;
            $get_data[$i]['created_at'] = round(strtotime($r->created_at));
        }
        return $get_data;
    }

    public function getAllDataWhere($where, $table)
    {
        $this->db->where($where);
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();          
        $result =  $query->result();

        foreach ($result as $i => $r) 
        {
            $get_data[$i] = array();
            $get_data[$i]['tiket_detail_id'] = $r->tiket_detail_id;
            $get_data[$i]['tiket_id'] = $r->tiket_id;
            $get_data[$i]['message'] = $r->message;
            $get_data[$i]['is_admin'] = $r->is_admin;
            $get_data[$i]['is_read'] = $r->is_read;
            $get_data[$i]['created_at'] = round(strtotime($r->created_at));
        }
        return $get_data;
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}