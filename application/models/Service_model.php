<?php
class Service_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function service()
    {
    	$result = $this->db->get('la_service')->result();
    	return $result;
    }

    public function submitService($input,$img)
    {
    	$id = $input['service_id'];
        if($id !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/Service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                'service_name' => $input['service_name'],
                'description' => $input['description'],
                'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            else
            {
                $config['upload_path']   = 'assets/images/Service/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                'service_name' => $input['service_name'],
                'description' => $input['description'],
                'image' => $image,
                'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('service_id',$id);
            $this->db->update('la_service',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('service');
        }
        else
        {
            // INSERT DATA HEAR...!!
            $img = $_FILES['image']['name'];
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/Service/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            
            $service_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
            $data = array(
            	'service_id' => $service_id,
                'service_name' => $input['service_name'],
                'description' => $input['description'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s'),
            );
            
            $this->db->insert('la_service', $data);
            $this->session->set_flashdata('error', DATA_SUBMIT);
            redirect('service');
            
        }
    }

    public function changeServiceStatus($id)
    {
    	$this->db->where('service_id',$id);
        $query = $this->db->get('la_service');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('service_id',$id);
            $this->db->update('la_service',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('service_id',$id);
            $this->db->update('la_service',$data);
        }
    }

    public function updateService($id)
    {
        $query = $this->db->where('service_id',$id)->get('la_service')->row_array();
        return $query;
    }


}