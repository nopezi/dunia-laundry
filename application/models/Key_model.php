<?php
class Key_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getFirebaseKey()
    {
    	$row = $this->db->get('la_firebase_key')->row_array();
    	return $row;
    }

    public function getMSG91Key()
    {
    	$row = $this->db->get('la_msg91_key')->row_array();
    	return $row;
    }

    public function getRazorpayKey()
    {
    	$row = $this->db->get('la_razorpay_key')->row_array();
    	return $row;
    }

    public function updateFirebaseKey($input)
    {
    	$this->form_validation->set_rules('firebase_key', 'firebase_key', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('apiKeys');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'firebase_key' => $input['firebase_key'],
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('la_firebase_key',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('apiKeys');
        
    }

    public function updateMSG91Key($input)
    {
    	$this->form_validation->set_rules('msg91_key', 'msg91_key', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('apiKeys');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'msg91_key' => $input['msg91_key'],
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('la_msg91_key',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('apiKeys');
        
    }

    public function updateRazorpayKey($input)
    {
    	$this->form_validation->set_rules('razorpay_key', 'razorpay_key', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('apiKeys');
            exit();
        }

        // UPDATE DATA HEAR...!!

        $data = array(
            'razorpay_key' => $input['razorpay_key'],
            'updated_at' => date('Y-m-d H:i:s'),
                );
            
        $this->db->where('id',$input['id']);
        $this->db->update('la_razorpay_key',$data);
        $this->session->set_flashdata('error', DATA_UPDATE);
        redirect('apiKeys');
        
    }
    
}