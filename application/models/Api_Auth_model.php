<?php
class Api_Auth_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function sendOtp($input)
    {
        $email = $input['email'];
        $otp = $input['otp'];
        if($email !='' || $otp !='')
        {
            $q = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($q > 0)
            {
                return 2;
            }else{
                
                $subject = VERI_OTP;
                $msg = "Use $otp as one time password (OTP) to verify your Win Laundry App account. Please do not share this OTP to anyone for security reasons.";
                $this->send_email_by_msg($email,$subject,$msg);
                
                $d_user = $this->db->get_where('la_user', ['email'=>$email])->first_row();

                # ganti 0 pada nomor hp menjadi +62
                $no_hp = '+62'.substr($d_user->mobile, 1, 15);

                # kirim otp ke sms
                $from  = "+17042865978";
                $to    = $no_hp;//"+6281943214722";
                $pesan = "From=".$from."&To=".$to."&Body=".$msg;
                $cek = api_twilio($pesan);

                # kirim otp ke whatsapp
                $cek_wa = kirim_pesan_wa($msg, $no_hp);
                
                return 1;
                
            }
        }else{
            return 3;
        }
        
    }

    public function signup($input)
    {
        $email = $input['email'];
        $name = $input['name'];
        $country_code = $input['country_code'];
        $mobile = $input['mobile'];
        // $name = strstr($email, '@', true);
        $password = $input['password'];
        $device_type = $input['device_type'];
        $device_token  = $input['device_token'];

        $address  = $input['address'];
        $latitude  = $input['latitude'];
        $longitude  = $input['longitude'];

        if($email=='' || $name=='' || $country_code=='' || $mobile=='' || $password == '' || $device_type == '' || $device_token == '')
        {
            return '2';

        } else {

            $query = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($query > '0')
            {
               return '1'; 
            }else{
                $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
                $otp_sms = random_char(5);

                $data = array(
                    'user_id'      => $user_id,
                    'otp_sms'      => $otp_sms,
                    'name'         => $name,
                    'email'        => $email,
                    'country_code' => $country_code,
                    'mobile'       => $mobile,
                    'password'     => $password, 
                    'device_type'  => $device_type,
                    'device_token' => $device_token,
                    'status'       => '0',
                    'created_at'   => date('Y-m-d H:i:s'),
                    'address'      => $address ?? '',
                    'latitude'     => $latitude ?? '0',
                    'longitude'    => $longitude ?? '0',
                    );
                $this->db->insert('la_user',$data);
                // return '0';
                // $url = 'https://laundryapp.windigitalkhatulistiwa.com/api/userActive?id=' . base64_encode($user_id);
                $url = base_url().'/api/userActive?id=' . base64_encode($user_id);
                $subject = "[Win Laundry] Registrasi Akun Baru";
                $msg = "Terima kasih telah mendaftar dengan Win Laundry! Akun Anda telah dibuat, Harap verifikasi akun Anda, Kode otp Winlaundry anda adalah ".$otp_sms."  atau dengan tautan di bawah ini. " . $url;
                // $msg = "Terima kasih telah mendaftar dengan Win Laundry! Akun Anda telah dibuat, Kode otp Winlaundry anda adalah ".$otp_sms;

                $this->send_email_by_msg($email, $subject, $msg);
                
                # ganti 0 pada nomor hp menjadi +62
                $no_hp = '+62'.substr($mobile, 1, 15);
                $pesan_sms = "<#> Kode otp Winlaundry anda adalah ".$otp_sms;
                # kirim otp ke sms
                $from  = "+17042865978";
                $to    = $no_hp;
                $pesan = "From=".$from."&To=".$to."&Body=".$pesan_sms;
                $cek   = api_twilio($pesan);

                # kirim otp ke whatsapp
                $cek_wa = kirim_pesan_wa($msg, $no_hp);
                
                $this->Api_Auth_model->responseSuccess(1, "Berhasil terdaftar, Periksa email Anda dan verifikasi akun Anda.", $data);
                exit();

            }
        }

    }

    public function userActive($user_id)
    {
        $get_user = $this->db->where('user_id',$user_id)->get('la_user')->row();
        if ($get_user) 
        {
            $data = array(
                'status' => '1',
                );
            $this->db->where('user_id',$get_user->user_id);
            $this->db->update('la_user',$data);
            return 1;
        }
    }

    public function forgotPassword($email)
    {
        if($email !='')
        {
            $q = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($q > 0)
            {
                // $password = substr( "abcdefghijklmnopqrstuvwxyz" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
                $length = 6;
                $password = "";
                $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
                $codeAlphabet.= "0123456789";
                $max = strlen($codeAlphabet); // edited

                for ($i=0; $i < $length; $i++) {
                    $password .= $codeAlphabet[random_int(0, $max-1)];
                }

                $data = array(
                    'password' => $password,
                );
                $this->db->where('email',$email);
                $q = $this->db->update('la_user',$data);
                if($q)
                {
                    $subject = FORG_PASS;
                    $msg = "Use $password as password your Win Laundry App account. Please do not share this Password to anyone for security reasons.";

                    $this->send_email_by_msg($email,$subject,$msg);
                    
                    $d_user = $this->db->get_where('la_user', ['email'=>$email])->first_row();

                    # ganti 0 pada nomor hp menjadi +62
                    $no_hp = '+62'.substr($d_user->mobile, 1, 15);

                    # kirim otp ke sms
                    $from  = "+17042865978";
                    $to    = $no_hp;//"+6281943214722";
                    $pesan = "From=".$from."&To=".$to."&Body=".$msg;
                    $cek = api_twilio($pesan);
                    
                    return 1;
                }
            }else{
                return 2;
            }
        }else{
            $this->responseFailed(0,ALL_FIELD_MANDATORY);
        }
        
    }

    public function verifyOtp($input)
    {
        $email = $input['email'];
        $email_token = $input['email_token'];
        $password = $input['password']; //new password when update..

        if($email == '' || $email_token == '' || $password == '')
        {
            return 0;
        }
        else
        {
            $q = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($q > 0)
            {
                $query = $this->db->where('email',$email)->where('email_token',$email_token)->get('la_user')->num_rows();
                if($query > 0)
                {
                    $data = array(
                    'password' => $password,
                        );
                    $this->db->where('email',$email);
                    $this->db->update('la_user',$data);
                    $q = $this->db->where('email',$email)->where('email_token',$email_token)->get('la_user')->row_array();
                    return $q;
                }else{
                    return 1;
                }
            }
            else{
                return 2;
            }
            
            
        }
    }

    public function login($input)
    {
        $email = $input['email'];
        $password = $input['password'];
        $device_type = $input['device_type'];
        $device_token = $input['device_token'];

        if($email=='' || $password == '')
        {
            return '4';
        }
        else{
            $query = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($query > '0')
            {
                $check_pass = $this->db->where('email',$email)->where('password',$password)->get('la_user')->num_rows();
                if($check_pass > '0')
                {
                    $this->db->where('email',$email);
                    $this->db->where('password',$password);
                    $this->db->where('status','1');
                    $this->db->where('type','0');
                    $check_status = $this->db->get('la_user')->num_rows();
                    if($check_status > '0')
                    {
                        return '0';
                    }
                    else{
                        return '3';
                    }
                }
                else{
                    return '2';
                }
            }
            else{
                return '1';
            }
        }
        
        
    }

    public function loginGoogle($input)
    {
        $email = $input['email'];
        $device_type = $input['device_type'];
        $device_token = $input['device_token'];

        if($email=='')
        {
            return '4';
        }
        else{
            $query = $this->db->where('email',$email)->get('la_user')->num_rows();
            if($query > '0')
            {
                $check_pass = $this->db->where('email',$email)->get('la_user')->num_rows();
                if($check_pass > '0')
                {
                    $this->db->where('email',$email);
                    $this->db->where('status','1');
                    // $this->db->where('type','1');
                    $check_status = $this->db->get('la_user')->num_rows();
                    if($check_status > '0')
                    {
                        return '0';
                    }
                    else{
                        return '3';
                    }
                }
                else{
                    return '2';
                }
            }
            else{
                return '1';
            }
        }
        
        
    }

    public function single_row_data_email($input)
    {
        $email = $input['email'];
        $device_type = $input['device_type'];
        $device_token = $input['device_token'];

        $query = $this->db->where('email',$email)->get('la_user')->num_rows();
        if($query)
        {
            // return 1;
            $data = array(
                'device_type' => $device_type,
                'device_token' => $device_token,
            );
            $this->db->where('email',$email);
            $q = $this->db->update('la_user',$data);
            if($q)
            {
                $res = $this->db->where('email',$email)->get('la_user')->row_array();

                $userData = array(
                    'user_id' => $res['user_id'],
                    'name' => $res['name'],
                    'email' => $res['email'],
                    'country_code' => $res['country_code'],
                    'mobile' => $res['mobile'],
                    'image' => $res['image'],
                    'password' => $res['password'],
                    'address' => $res['address'],
                    'status' => $res['status'],
                    'latitude' => $res['latitude'],
                    'longitude' => $res['longitude'],
                    'landmark' => $res['landmark'],
                    'device_type' => $res['device_type'],
                    'device_token' => $res['device_token'],
                    'email_token' => $res['email_token'],
                    'created_at' => $res['created_at'],
                    'background' => $res['background'],
                    );
                return $userData;
            }

        }
        
    }

    public function single_row_data($input)
    {
        $email = $input['email'];
        $password = $input['password'];
        $device_type = $input['device_type'];
        $device_token = $input['device_token'];

        $query = $this->db->where('email',$email)->where('password',$password)->get('la_user')->num_rows();
        if($query)
        {
            // return 1;
            $data = array(
                'device_type' => $device_type,
                'device_token' => $device_token,
            );
            $this->db->where('email',$email);
            $this->db->where('password',$password);
            $q = $this->db->update('la_user',$data);
            if($q)
            {
                $res = $this->db->where('email',$email)->where('password',$password)->get('la_user')->row_array();

                $userData = array(
                    'user_id' => $res['user_id'],
                    'name' => $res['name'],
                    'email' => $res['email'],
                    'country_code' => $res['country_code'],
                    'mobile' => $res['mobile'],
                    'image' => $res['image'],
                    'password' => $res['password'],
                    'address' => $res['address'],
                    'status' => $res['status'],
                    'latitude' => $res['latitude'],
                    'longitude' => $res['longitude'],
                    'landmark' => $res['landmark'],
                    'device_type' => $res['device_type'],
                    'device_token' => $res['device_token'],
                    'email_token' => $res['email_token'],
                    'created_at' => $res['created_at'],
                    'background' => $res['background'],
                    );
                return $userData;
            }

        }
        
    }

    public function logOut($input)
    {
        $user_id = $input['user_id'];

        $data = array(
                'device_token' => '12345'
            );
        $this->db->where('user_id',$user_id);
        $this->db->update('la_user',$data);
    }

    public function send_email_by_msg($email_id,$subject,$msg)
    {
         $KEY=$this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));
         $authKey=$KEY->msg91_key;

          // $authKey = MSG_AUTH_KEY;
          $from= SENDER_EMAIL;

          //Prepare you post parameters
          $postData = array(
              'authkey' => $authKey,
              'to' => $email_id,
              'from' => $from,
              'subject' => $subject,
              'body' => $msg
          );
         //API URL
          $url="https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $authKey ) ."&to=" . urlencode( $email_id ) . "&from=" . urlencode( $from ) ."&body=" . urlencode( $msg ) ."&subject=" . urlencode( $subject );
          // init the resource
          $ch = curl_init();
          curl_setopt_array($ch, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_POST => true,
              CURLOPT_POSTFIELDS => $postData
              //,CURLOPT_FOLLOWLOCATION => true
          ));

          //Ignore SSL certificate verification
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

          //get response
          $output = curl_exec($ch);
          //Print error if any
          if(curl_errno($ch))
          {
            echo 'error:' . curl_error($ch);
          }
          curl_close($ch);
    }

    public function deleteAccount($user_id)
    {
        $user = $this->db->where('user_id',$user_id)->get('la_user');
        if($user->num_rows() > 0)
        {
            $userData = $user->row_array();
            $data = array(
                'status' => '0'
                );
            $this->db->where('user_id',$user_id);
            $query = $this->db->update('la_user',$data);
            if($query)
            {
                return 1;
            }else{
                return 0;
            }
            
        }else{
            return 2;
        }
    }

    public function changePassword($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('new_password', 'new_password', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }

        $userIdExist = $this->db->where('user_id',$input['user_id'])->get('la_user')->num_rows();
        if($userIdExist > 0)
        {
            $passExist = $this->db->where('user_id',$input['user_id'])->where('password',$input['password'])->get('la_user')->num_rows();
            if($passExist > 0)
            {
                $data = array(
                    'password' => $input['new_password'],
                    );
                $sucess = $this->db->where('user_id',$input['user_id'])->update('la_user',$data);
                if($sucess)
                {
                    return 1;
                }else{
                    return 0;
                }
            }
            else
            {
                $this->responseFailed(0, "Password doesn't match on your old password");
                exit();
            }
        }else{
            return 2;
        }
    }

    public function responseSuccessWethoutData($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseSuccess($status, $message, $data = null)
    {
        $data = !empty($data) ? $data :[];
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message);
        header('Content-Type: application/json');
        echo json_encode($arr);
    }


}