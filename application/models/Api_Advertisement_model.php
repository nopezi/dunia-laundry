<?php
class Api_Advertisement_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getAdvertisement()
    {
    	$query = $this->db->where('status','1')->get('la_advertisement');
    	if($query->num_rows() > 0)
    	{
    		$res = $query->result();
    		return $res;
    	}else{
    		return 0;
    	}
    }

    public function responseSuccessImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailedImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}
