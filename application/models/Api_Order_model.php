<?php
class Api_Order_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function orderSubmit($input)
    {
    	$this->form_validation->set_rules('order_id', 'order_id', 'required');
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
    	$this->form_validation->set_rules('shop_id', 'shop_id', 'required');
    	$this->form_validation->set_rules('price', 'price', 'required');
    	$this->form_validation->set_rules('discount', 'discount', 'required');
    	$this->form_validation->set_rules('final_price', 'final_price', 'required');
    	$this->form_validation->set_rules('currency_code', 'currency_code', 'required');
    	$this->form_validation->set_rules('item_details', 'item_details', 'required');
    	$this->form_validation->set_rules('shipping_address', 'shipping_address', 'required');
    	// $this->form_validation->set_rules('landmark', 'landmark', 'required');
    	$this->form_validation->set_rules('latitude', 'latitude', 'required');
    	$this->form_validation->set_rules('longitude', 'longitude', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Api_Order_model->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }

        $orderIdExist = $this->db->where('order_id',$input['order_id'])->get('la_order_details')->row_array();
        if($orderIdExist > 0)
        {
            return 4;
        }
        else
        {
            $userExist = $this->db->where('user_id',$input['user_id'])->get('la_user')->row_array();
            if($userExist > 0)
            {
                $shopExist = $this->db->where('shop_id',$input['shop_id'])->get('la_laundry_shop')->row_array();
                if($shopExist > 0)
                {
                    $data = array(
                        'order_id' => $input['order_id'],
                        'user_id' => $input['user_id'],
                        'shop_id' => $input['shop_id'],
                        'price' => $input['price'],
                        'discount' => $input['discount'],
                        'final_price' => $input['final_price'],
                        'currency_code' => $input['currency_code'],
                        'item_details' => $input['item_details'],
                        'shipping_address' => $input['shipping_address'],
                        'landmark' => $input['landmark']?$input['landmark']:'',
                        'latitude' => $input['latitude'],
                        'longitude' => $input['longitude'],
                        'pickup_date' => $input['pickup_date'],
                        'pickup_time' => $input['pickup_time'],
                        'delivery_date' => date('d-M-Y', strtotime('+3 days')),
                        'delivery_time' => date('H:i'),
                        'created_at' => date('Y-m-d H:i:s')
                        );
                    $data = $this->db->insert('la_order_details',$data);
                    if($data)
                    {
                        //Notification
                        $getToken     = $this->Base_model->getSingleRow('la_user', array(
                        'user_id' => $input['user_id']
                        ));

                        $name = $getToken->name;
                        $device_token = $getToken->device_token;
                        $msg = "Thank you $name Your order has been submitted successfully.";
                        $title = "Order submitted successfully.";
                        $type = '7004';
                        $this->Notification_model->firebase_with_class($device_token, '', '',$type, $title, $msg);
                        $data1['user_id']    = $input['user_id'];
                        $data1['title']      = $title;
                        $data1['message']    = $msg;
                        $data1['type']       = $type;
                        $data1['status']       = 0;
                        $data1['created_at']   = date('Y-m-d H:i:s');
                        $this->db->insert('la_notification', $data1);
                        return 1;
                    }else{
                        return 0;
                    }
                }
                else
                {
                    return 2;
                }

            }
            else
            {
                return 3;
            }
        }

    }

    public function orderCancel($order_id)
    {
        $this->form_validation->set_rules('order_id', 'order_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Api_Order_model->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }

        $order = $this->db->where('order_id',$order_id)->get('la_order_details');
        if($order->num_rows() > 0)
        {
            $OS = $order->row_array();
            if($OS['order_status'] == '0' || $OS['order_status'] == '1' || $OS['order_status'] == '2')
            {
                $data = array(
                    'order_status' => '6'
                        );
                $this->db->where('order_id',$order_id);
                $this->db->update('la_order_details',$data);
                return 1;
            }else{
                return 2;
            }
            
        }else{
            return 0;
        }
    }

    public function getBookingList($user_id)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }

        $query = $this->db->where('user_id',$user_id)->get('la_order_details');

        if($query->num_rows() > 0)
        {
            $this->db->select('s.service_name,od.*,ls.shop_name');
            $this->db->select('CONCAT("assets/images/Laundry/",ls.image) as shop_image');
            $this->db->join('la_laundry_shop ls','ls.shop_id = od.shop_id');
            $this->db->join('la_price_list pl','pl.shop_id = "YZ65d0"'); // pake yg id winlaundry
            $this->db->join('la_service s','s.service_id = pl.service_id');
            $this->db->where('od.user_id',$user_id);
            $this->db->group_by('od.order_id');
            $this->db->order_by('od.s_no','DESC');
            $order = $this->db->get('la_order_details od')->result();

            //var_dump( $this->db->last_query() ); die();
            // return $res->result();
            $orders = array();
            $prev_order_id = '';
            foreach($order as $order)
            {
                if ( $prev_order_id === $order->order_id ) 
                    continue;

                $item_details = $order->item_details;
                $es1 = str_ireplace('"item_details":',"",$item_details);
                $json = json_decode($es1, true);
                $items_infos = array();
                foreach ($json as $key => $value) 
                {
                    array_push($items_infos, $value);
                }
                $order->item_details = $items_infos;
                $prev_order_id = $order->order_id;

                array_push($orders, $order);
            }

            $data['order_list'] = $orders;

            return $data;

        }else{
            return 0; 
        }
    }

    public function getDetailStore($store_id) {
        return $this->db->where('shop_id',$store_id)->get('la_laundry_shop')->row_array();
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}