<?php
class Api_Currency_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getCurrency()
    {
    	$q = $this->db->where('status','1')->get('la_currency');
    	if($q->num_rows() > 0)
    	{
    		$row = $q->row_array();
    		return $row;
    	}else{
    		return 0;
    	}
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}