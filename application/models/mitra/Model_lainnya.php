<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_lainnya extends CI_Model {

	public function get_order($user_id)
	{
		
		$this->db->select('od.*, lu.type, ls.user_id as id_agen_mitra');
        $this->db->from('la_order_details od');
        $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
        $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
        $this->db->where('ls.user_id',$user_id);
        $this->db->where('od.payment_status', '1');
        $this->db->order_by('od.s_no','DESC');
        return $this->db->get()->result();

	}

	public function cek_dana($user_id, $shop_id)
	{

		$user = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();
		
		$total_pendapatan = 0;
		$total_pendapatan_potongan = 0;

		$order = $this->get_order($user_id);

		if (!empty($order)) {

			for ($i=0; $i < sizeof($order); $i++) { 

				if ($i>0) {

					$nilai_persentasi = $this->Order_model->cekPersentasi($user->type);
                    $type_user = $this->Order_model->cekTypeUser($user->type);
                    $income = $order[$i]->final_price * $nilai_persentasi['nilai'];
					$tambah = $income + $tambah;
					$origin = $order[$i]->final_price + $origin;

				} else {

					$nilai_persentasi = $this->Order_model->cekPersentasi($user->type);
                    $type_user = $this->Order_model->cekTypeUser($user->type);
                    $income = $order[$i]->final_price * $nilai_persentasi['nilai'];
					$tambah = $income;
					$origin = $order[$i]->final_price;

				}
			}

		}

		$pencairan_terakhir = $this->db->select('sum(jumlah) as total_pencairan')
									  ->where('status', '2')
									  ->get('la_pencairan_dana')
									  ->result();

		$origin = $origin - $pencairan_terakhir[0]->total_pencairan;
		$tambah = $tambah - $pencairan_terakhir[0]->total_pencairan;

		$total_pendapatan = $origin;//'Rp. '.number_format($origin);
		$total_pendapatan_potongan = $tambah;//'Rp. '.number_format($tambah);

		return [
			'total_pendapatan' => $total_pendapatan,
			'total_pendapatan_potongan' => $total_pendapatan_potongan
		];

	}

	public function get_profile($user_id, $shop_id)
	{
		
		$user = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

		if (!empty($user)) {
			
			$user->url_image = base_url().'assets/images/user/default.png';
			$user->url_background = base_url().'assets/images/user/background/default.png';
			if (!empty($user->image)) {
				$user->url_image = base_url().'assets/images/user/'.$user->image;
			}

			if (!empty($user->background)) {
				$user->url_background = base_url().'asets/images/user/background/'.$user->background;
			}

			$user->total_pendapatan = 0;
			$user->total_pendapatan_potongan = 0;

			$order = $this->get_order($user_id);

			if (!empty($order)) {

				for ($i=0; $i < sizeof($order); $i++) { 

					if ($i>0) {

						$nilai_persentasi = $this->Order_model->cekPersentasi($user->type);
                        $type_user = $this->Order_model->cekTypeUser($user->type);
                        $income = $order[$i]->final_price * $nilai_persentasi['nilai'];
						$tambah = $income + $tambah;
						$origin = $order[$i]->final_price + $origin;

					} else {

						$nilai_persentasi = $this->Order_model->cekPersentasi($user->type);
                        $type_user = $this->Order_model->cekTypeUser($user->type);
                        $income = $order[$i]->final_price * $nilai_persentasi['nilai'];
						$tambah = $income;
						$origin = $order[$i]->final_price;

					}
				}

			}

			$pencairan_terakhir = $this->db->select('sum(jumlah) as total_pencairan')
										  ->where('status', '2')
										  ->get('la_pencairan_dana')
										  ->result();

			$origin = $origin - $pencairan_terakhir[0]->total_pencairan;
			$tambah = $tambah - $pencairan_terakhir[0]->total_pencairan;

			$user->total_pendapatan = 'Rp. '.number_format($origin);
			$user->total_pendapatan_potongan = 'Rp. '.number_format($tambah);

			# data shop
			$shop = $this->db->get_where('la_laundry_shop', ['user_id'=>$user_id])->first_row();
			$user->shop = $shop;

			# data rekening
			$rekening = $this->db->get_where('la_bank', ['user_id'=>$user_id])->first_row();
			$user->rekening = !empty($rekening)?$rekening:null;

			return $user;

		}

	}

	public function update_foto_profile($user_id, $img)
	{
		
		$config['upload_path']   = 'assets/images/user/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 100000000;
        $config['file_name']     = time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        $image = "";
        if ($this->upload->do_upload('image')) {
            $uploadData = $this->upload->data(); 
            $image = $uploadData['file_name']; 
        }

        $this->db->update('la_user', ['image' => $image], ['user_id'=>$user_id]);
        return $this->get_profile($user_id, null);

	}

	public function update_foto_background($user_id, $img)
	{
		
		$config['upload_path']   = 'assets/images/user/background/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 100000000;
        $config['file_name']     = time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        $image = "";
        if ($this->upload->do_upload('image')) {
            $uploadData = $this->upload->data(); 
            $image = $uploadData['file_name']; 
        }

        $this->db->update('la_user', ['background' => $image], ['user_id'=>$user_id]);
        return $this->get_profile($user_id, null);

	}	

}

/* End of file Model_lainnya.php */
/* Location: ./application/models/mitra/Model_lainnya.php */