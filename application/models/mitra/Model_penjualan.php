<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_penjualan extends CI_Model {

	public function tambah($input)
	{
		
		$this->db->insert('la_order_details', [
			'order_id' => $input['order_id'],
            'user_id' => $input['user_id'],
            'shop_id' => $input['shop_id'],
            'price' => $input['price'],
            'discount' => $input['discount'],
            'final_price' => $input['final_price'],
            'currency_code' => $input['currency_code'],
            'item_details' => json_encode($input['item_details']),
            'shipping_address' => $input['shipping_address'],
            'landmark' => $input['landmark']?$input['landmark']:'',
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
            'pickup_date' => $input['pickup_date'],
            'pickup_time' => $input['pickup_time'],
            'delivery_date' => $input['delivery_date'],
            'delivery_time' => $input['delivery_time'],
            'created_at' => date('Y-m-d H:i:s')
		]);

		//Notification
        $getToken     = $this->Base_model->getSingleRow('la_user', array(
        'user_id' => $input['user_id']
        ));

        $name = $getToken->name;
        $device_token = $getToken->device_token;
        $msg = "Thank you $name Your order has been submitted successfully.";
        $title = "Order submitted successfully.";
        $type = '7004';

        $data_msg = [
        	'body' => $msg,
            'title' => $title,
            'icon' => 'myicon',
            'type' => $type,
            /*Default Icon*/
            'sound' => 'default'
            /*Default sound*/
        ];
        kirim_firebase($data_msg, $device_token);
        // $this->Notification_model->firebase_with_class($device_token, '', '',$type, $title, $msg);
        $data1['user_id']    = $input['user_id'];
        $data1['title']      = $title;
        $data1['message']    = $msg;
        $data1['type']       = $type;
        $data1['status']       = 0;
        $data1['created_at']   = date('Y-m-d H:i:s');
        $this->db->insert('la_notification', $data1);

        return $this->db->get_where('la_order_details', ['order_id'=>$input['order_id']])->first_row();

	}

	public function semua_data($shop_id)
	{
		
		$penjualan = $this->db->where('order_status', '0')
							  ->where('shop_id', $shop_id)
							  ->order_by('s_no', 'desc')
							  ->get('la_order_details')
							  ->result();

		if (!empty($penjualan)) {
			
			foreach ($penjualan as $key => $value) {
				$service = json_decode($value->item_details);
				$total_pcs = count($service);
				$value->total_pcs = $total_pcs;
				$value->nama_service = $service[0]->service_name;

				$hasil[] = [
					'order_id' => $value->order_id,
					'nama_service' => $service[0]->service_name,
					'total_harga' => 'Rp. '.number_format($value->final_price),
					'total_pcs' => $total_pcs
				];
			}

			return $hasil;

		}

	}


	public function selengkapnya($order_id)
	{
		
		$data = $this->db->get_where('la_order_details', ['order_id'=>$order_id])->first_row();

		if (!empty($data)) {
			
			$item_detail = json_decode($data->item_details);
			$data->item_details = $item_detail;

			return $data;

		}

	}	

}

/* End of file Model_penjualan.php */
/* Location: ./application/models/mitra/Model_penjualan.php */