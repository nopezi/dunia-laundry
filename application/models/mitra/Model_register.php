<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_register extends CI_Model {

	public function daftar($data)
	{

		#cek email sudah terdaftar
		$cek_email = $this->db->get_where('la_user', ['email'=>$data['email']])->first_row();
		$cek_hp    = $this->db->get_where('la_user', ['email'=>$data['mobile']])->first_row();

		if (!empty($cek_email)) {
			return [
				'status'  => false,
				'message' => 'Email sudah terdaftar, silahkan gunakan email lain',
				'data'	  => null
			];
		} else if (!empty($cek_hp)) {
			return [
				'status'  => false,
				'message' => 'Nomor handphone sudah terdaftar, silahkan gunakan nomor lain',
				'data'    => null
			];
		} else if ($data['password'] != $data['password_replace']) {
			return [
				'status'   => false,
				'message'  => 'Maaf, password tidak sinkron',
				'data'     => null
			];
		}

		$user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
        $otp_sms = random_char(5);

        $data['user_id']     = $user_id;
        $data['otp_sms']     = $otp_sms;
        $data['created_at']  = date('Y-m-d H:i:s');
        $data['status']      = '0';
        $data['device_type'] = 'ANDROID';
        $data['type'] = '2';
        unset($data['password_replace']);

		$this->db->insert('la_user', $data);
		$sukses = $this->db->affected_rows();

		if (!empty($sukses)) {
			$last_id = $this->db->insert_id();
			$inserted = $this->db->get_where('la_user', ['user_id'=>$data['user_id']])->first_row();
			
			return [
				'status'   => true,
				'message'  => 'Berhasil daftar',
				'data'     => $inserted
			];

		}


	}

	public function daftar_mitra($input)
	{

		$shop_id = substr( "MITRA" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

		$otp_sms = random_char(5);
		$this->db->update('la_user', ['otp_sms'=>$otp_sms]);
		$data_user = $this->db->get_where('la_user', ['user_id'=>$input['user_id']])->first_row();
		
		$data_input = [
			'shop_id'    => $shop_id, # random_char('M', 5),
			'user_id'    => $input['user_id'],
			'shop_name'  => $input['nama_mitra'],
			'address'    => $input['alamat'],
			'latitude'   => $input['latitude'],
			'longitude'  => $input['longitude'],
			'created_at' => date('Y-m-d H:i:s'),
			'country_code' => '62',
			'mobile'	   => $data_user->mobile
		];

		$this->db->insert('la_laundry_shop', $data_input);
		$data_mitra = $this->db->get_where('la_laundry_shop', ['shop_id'=>$shop_id])->first_row();

        $url = base_url().'/api/userActive?id=' . base64_encode($input['user_id']);
        $subject = "[Win Laundry] Registrasi Akun Baru";
        $msg = "Terima kasih telah mendaftar dengan Win Laundry! Akun Anda telah dibuat, Harap verifikasi akun Anda, Kode otp Winlaundry anda adalah ".$otp_sms."  atau dengan tautan di bawah ini. " . $url;

        $kirim_email = $this->send_email_by_msg($data_user->email, $subject, $msg);

        # ganti 0 pada nomor hp menjadi +62
        $no_hp = '+62'.substr($data_user->mobile, 1, 15);
        $pesan_sms = "<#> Kode otp Winlaundry anda adalah ".$otp_sms;
        # kirim otp ke sms
        $from  = "+17042865978";
        $to    = $no_hp;
        $pesan = "From=".$from."&To=".$to."&Body=".$pesan_sms;
        $cek   = api_twilio($pesan);

        # kirim otp ke whatsapp
        $cek_wa = kirim_pesan_wa($msg, $no_hp);

        return [
        	'status'  => true,
        	'message' => 'Berhasil daftar data mitra',
        	'data'    => $data_mitra
        ];

	}

	public function send_email_by_msg($email_id,$subject,$msg)
    {
       $KEY=$this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));
       $authKey=$KEY->msg91_key;

        // $authKey = MSG_AUTH_KEY;
        $from= SENDER_EMAIL;

        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'to' => $email_id,
            'from' => $from,
            'subject' => $subject,
            'body' => $msg
        );
       //API URL
        $url="https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $authKey ) ."&to=" . urlencode( $email_id ) . "&from=" . urlencode( $from ) ."&body=" . urlencode( $msg ) ."&subject=" . urlencode( $subject );
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);
        //Print error if any
        if(curl_errno($ch))
        {
          echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
    }

}

/* End of file Model_register.php */
/* Location: ./application/models/mitra/Model_register.php */