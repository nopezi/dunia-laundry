<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_layanan extends CI_Model {

	public function service($shop_id)
    {
    	
    	$result = $this->db->get_where('la_service', ['shop_id'=>$shop_id])->result();
    	return $result;
    }

    public function submitService($input,$img)
    {

    	$id = $input['service_id'];

    	# update data
        if(!empty($id)) {

        	$config['upload_path']   = 'assets/images/Service/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite']     = TRUE;
            $config['max_size']      = 10000;
            $config['file_name']     = time();
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            $image = "";
            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data(); 
                $image = $uploadData['file_name']; 
            }

            if($img == '') {

                $data = [
                	'service_name' => $input['service_name'],
	                'description' => $input['description'],
	                'updated_at' => date('Y-m-d H:i:s'),
                ];

            } else {

                $data = [
                	'service_name' => $input['service_name'],
	                'description' => $input['description'],
	                'image' => $image,
	                'updated_at' => date('Y-m-d H:i:s'),
                ];

            }
            $this->db->where('service_id',$id);
            $this->db->update('la_service',$data);
            $this->session->set_flashdata('success', DATA_UPDATE);
            redirect('mitra_agen/layanan');

        } else {

        	$img = $_FILES['image']['name'];
	        if($img == '') {
	            $image = "default.png";
	        } else {
	            $config['upload_path']   = 'assets/images/Service/';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            $config['overwrite']     = TRUE;
	            $config['max_size']      = 10000;
	            $config['file_name']     = time();
	            $this->load->library('upload',$config);
	            $this->upload->initialize($config);
	            $image = "";
	            if($this->upload->do_upload('image')) {
	                $uploadData = $this->upload->data(); 
	                $image = $uploadData['file_name']; 
	            }
	        }
	        
	        $service_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
	        $data = [
	        	'service_id' => $service_id,
	        	'shop_id' => $input['shop_id'],
	            'service_name' => $input['service_name'],
	            'description' => $input['description'],
	            'image' => $image,
	            'created_at' => date('Y-m-d H:i:s'),
	        ];
	        
	        $this->db->insert('la_service', $data);
	        $this->session->set_flashdata('success', DATA_SUBMIT);
	        redirect('mitra_agen/layanan');

        }

    }

    public function updateService($id)
    {
        $query = $this->db->where('service_id',$id)
        				  // ->where('')
        				  ->get('la_service')
        				  ->row_array();
        return $query;
    }	

}

/* End of file Model_layanan.php */
/* Location: ./application/models/mitra/Model_layanan.php */