<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pesan extends CI_Model {

	public function semua_pesan($user_id)
	{
		
		$pesan = $this->db->select('la_message_head.message_head_id,
									la_message_head.from_user_id,
									la_message_head.to_user_id,
									la_message_head.updated_at,
									la_user.name as customer_name')
						  ->join('la_user', 'la_user.user_id = la_message_head.from_user_id')
						  ->order_by('message_head_id', 'desc')
						  ->where('to_user_id', $user_id)
						  ->get('la_message_head')
						  ->result();

		if (!empty($pesan)) {
			
			foreach ($pesan as $key => $value) {

				$pesan_terakhir = $this->db->order_by('message_id', 'desc')
										   ->where('to_user_id', $user_id)
										   ->get('la_message')
										   ->first_row();
				
				$value->pesan_terakhir = null;
				$value->jam_pesan = null;

				if (!empty($pesan_terakhir)) {
					$value->pesan_terakhir = $pesan_terakhir->message;
					$jam_pesan = explode(' ', $value->updated_at);
					$jam_pesan = date('H:i', strtotime($jam_pesan[1]));
					$value->jam_pesan = $jam_pesan;
				}

			}

			return $pesan;

		}

	}

	public function kirim_pesan($input, $media)
	{
		
		$to_user_id = $this->db->where('to_user_id', $input['user_id'])
							   ->get('la_message')
							   ->first_row();

		$gambar = null;

		$config['upload_path']   = 'assets/images/pesan/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 10000;
        $config['file_name']     = time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('media')) {
            $uploadData = $this->upload->data(); 
            $gambar = $uploadData['file_name']; 
        }

		$kirim = [
			'message_head_id' => $input['message_head'],
			'from_user_id'    => $input['user_id'],
			'to_user_id'      => $to_user_id->from_user_id,
			'message'		  => $input['message'],
			'media'			  => $gambar,
			'created_at'      => date('Y-m-d H:i:s'),
			'updated_at'      => date('Y-m-d H:i:s')
		];

		$this->db->insert('la_message', $kirim);

		# ambil data pesan
		$pesan = $this->db->order_by('message_id', 'desc')
						  ->get_where('la_message', ['message_head_id'=>$input['message_head']])
						  ->result();

		foreach ($pesan as $key => $value) {

			$jam = explode(' ', $value->created_at);
			$jam = date('H:i A', strtotime($jam[1]));
			$value->jam = $jam;

			$value->url_media = null;
			if (!empty($value->media)) {
				$value->url_media = base_url().'assets/images/pesan/'.$value->media;
			}
		}

		return $pesan;

	}

}

/* End of file Model_pesan.php */
/* Location: ./application/models/mitra/Model_pesan.php */