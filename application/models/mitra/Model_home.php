<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_home extends CI_Model {

	public function pesan_belum_read($id_user)
	{
		
		$data = $this->db->where('to_user_id', $id_user)
						 ->where('is_read', 0)
						 ->or_where('from_user_id', $id_user)
						 ->where('is_read', 0)
						 ->get('la_message')
						 ->num_rows();

		return $data;

	}

	public function pesanan_baru($id_user)
	{
		
		$pesanan_baru = $this->db->select('lo.*')
								 ->from('la_laundry_shop ls')
								 ->join('la_order_details lo', 'lo.shop_id = ls.shop_id')
								 ->where('ls.user_id', $id_user)
								 ->where('lo.order_status != ', '6')
								 ->get()
								 ->num_rows();

		return $pesanan_baru;

	}

	public function pesanan_komplain($id_user)
	{
		
		$pesanan_baru = $this->db->select('lo.*')
								 ->from('la_laundry_shop ls')
								 ->join('la_order_details lo', 'lo.shop_id = ls.shop_id')
								 ->where('ls.user_id', $id_user)
								 ->where('lo.order_status', '6')
								 ->get()
								 ->num_rows();

		return $pesanan_baru;

	}

	public function pesanan_selesai($id_user)
	{
		
		$pesanan_baru = $this->db->select('lo.*')
								 ->from('la_laundry_shop ls')
								 ->join('la_order_details lo', 'lo.shop_id = ls.shop_id')
								 ->where('ls.user_id', $id_user)
								 ->where('lo.order_status', '5')
								 ->get()
								 ->num_rows();

		return $pesanan_baru;

	}

	public function pendapatan_bersih($id_user)
	{
		
		# ambil data user mitra
		$user = $this->db->get_where('la_user', ['user_id'=>$id_user])->first_row();
		# ambil data persentasi komisi
		$nilai_persentasi = $this->Order_model->cekPersentasi($user->type);

		$pendapatan   = $this->db->select('lo.*')
								 ->from('la_laundry_shop ls')
								 ->join('la_order_details lo', 'lo.shop_id = ls.shop_id')
								 ->where('ls.user_id', $id_user)
								 ->where_in('order_status', 4,5)
								 // ->where('payment_status', 1)
								 ->get()
								 ->result();

		$pendapatan_bersih_baru = [];
		if (!empty($pendapatan)) {
			
			foreach ($pendapatan as $key => $value) {
				
				$income = $value->final_price * $nilai_persentasi['nilai'];
				$pendapatan_bersih_baru[] = $income;

			}

		}

		$pendapatan_bersih = 'Rp. '.number_format(array_sum($pendapatan_bersih_baru));

		return $pendapatan_bersih;

	}	

}

/* End of file Model_home.php */
/* Location: ./application/models/mitra/Model_home.php */