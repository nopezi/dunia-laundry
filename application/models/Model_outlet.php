<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_outlet extends CI_Model {

	public function get_outlet($id)
	{

		return $this->db->get_where('la_laundry_shop', ['shop_id'=>$id])->row_array();

	}

    public function getCountryCode()
    {
        $result = $this->db->get('la_country')->result();
        return $result;
    }

    public function updateLaundryShop($shop_id)
    {
        $query = $this->db->where('shop_id',$shop_id)->get('la_laundry_shop')->row_array();
        return $query;
    }

    public function get_mitra()
    {
        $ses = $_SESSION;
        # jika user franchise
        if ($ses['type'] == 3) {
            $this->db->where('regional', $ses['regional']);
        }
        $result = $this->db->where('type', '2')
                           ->get('la_user')
                           ->result();
        
        return $result;
    }

	public function data_outlet()
	{

		$ses = $_SESSION;
        if($ses['status'] == '2')
        {
            $result = $this->db->where('user_id',$ses['id'])->get('la_laundry_shop')->result();
            return $result;
        } else {
            
            $this->db->select('ls.*');
            $this->db->from('la_laundry_shop ls');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');

            # jika user franchise
            if ($ses['type'] == 3) {
                $this->db->where('lu.regional', $ses['regional']);
            }

            $this->db->where('lu.type', '2');
            $this->db->order_by('ls.s_no','DESC');
            $query = $this->db->get();
            return $query->result();
        }

	}

    public function ganti_status($id)
    {
        $this->db->where('shop_id',$id);
        $query = $this->db->get('la_laundry_shop');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('shop_id',$id);
            $this->db->update('la_laundry_shop',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('shop_id',$id);
            $this->db->update('la_laundry_shop',$data);
        }
    }

	public function delete($shop_id)
	{
        $gambar = $this->db->get_where('la_laundry_shop', ['shop_id'=>$shop_id])->first_row();
        unlink('assets/images/Laundry/'.$gambar->image);
		$this->db->delete('la_laundry_shop', ['shop_id'=>$shop_id]);
	}

	public function tambah($input,$img)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
    	$this->form_validation->set_rules('shop_name', 'shop_name', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        if ($this->form_validation->run() == false)
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        if($img == '') {

            $image = "default.png";

        } else {

            $config['upload_path']   = 'assets/images/Laundry/';
            // return $config['upload_path']; exit();
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite']     = TRUE;
            $config['max_size']      = 10000;
            $config['file_name']     = time();
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            $image = "";
            if($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data(); 
                $image = $uploadData['file_name']; 
            }

        }

        $shop_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

    	$data = [
            'shop_id' => $shop_id,
            'user_id' => $input['user_id'],
            'shop_name' => $input['shop_name'],
            'country_code' => '62',
            'mobile' => $input['mobile'],
            'address' => $input['address']?$input['address']:'',
            'latitude' => $input['latitude']?$input['latitude']:'',
            'longitude' => $input['longitude']?$input['longitude']:'',
            'opening_time' => $input['opening_time']?$input['opening_time']:'',
            'closing_time' => $input['closing_time']?$input['closing_time']:'',
            'mulai_hari' => $input['mulai_hari'],
            'sampai_hari' => $input['sampai_hari'],
            'description' => $input['description']?$input['description']:'',
            'image' => $image,
            'type_shop' => $input['type_shop'],
            'created_at' => date('Y-m-d H:i:s')
        ];
	    $this->db->insert('la_laundry_shop',$data);
		$this->session->set_flashdata('success',DATA_SUBMIT);

        $cek_redirect = $this->db->get_where('la_user', ['user_id'=>$input['user_id']])->first_row();

        return 'berhasil';

    }

    public function update($input, $img = null)
    {

    	if(!empty($input['shop_id'])) {

            if(!empty($img)) {

                $config['upload_path']   = 'assets/images/Laundry/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => '62',
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'image' => $image,
                    'type_shop'  => $input['type_shop'],
                    'description' => $input['description']?$input['description']:'',
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

            } else {

                $config['upload_path']   = 'assets/images/Laundry/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => '62',
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'type_shop'  => $input['type_shop'],
                    'description' => $input['description']?$input['description']:'',
                    // 'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

            }

            $this->db->update('la_laundry_shop',$data, ['shop_id'=>$input['shop_id']]);
            $this->session->set_flashdata('success', DATA_UPDATE);
            return 'berhasil';

        } else {
        	$this->session->set_flashdata('error', 'data tidak boleh kosong');
            redirect('laundryShop');
        }

    }

}

/* End of file Model_outlet.php */
/* Location: ./application/models/Model_outlet.php */