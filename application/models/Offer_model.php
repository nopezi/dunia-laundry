<?php
class Offer_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function offer()
    {
        $ses = $_SESSION;
        if($ses['status'] == '2')
        {
            $this->db->select('o.*');
            $this->db->from('la_offer o');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = o.shop_id');
            // $this->db->where('o.status','1');
            $this->db->where('ls.user_id',$ses['id']);
            $query = $this->db->get();
            return $query->result();
        }
        else
        {
            $result = $this->db->get('la_offer')->result();
            return $result;
        }
    }

    public function getAllLaundryShop()
    {
        $ses = $_SESSION;
        if($ses['status'] == '2')
        {
        	$res = $this->db->where('user_id',$ses['id'])->where('status','1')->get('la_laundry_shop')->result();
        	return $res;
        }
        else
        {
            $res = $this->db->where('status','1')->get('la_laundry_shop')->result();
            return $res;
        }
    }

    public function getAllService()
    {
        $res = $this->db->where('status','1')->get('la_service')->result();
        return $res;
    }

    public function submitOffer($input,$img)
    {
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        $this->form_validation->set_rules('offer_name', 'offer_name', 'required');
        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('amount_type', 'amount_type', 'required');
        $this->form_validation->set_rules('applicable_on_service', 'applicable_on_service', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('offer');
            exit();
        }

        if($input['offer_id'] !='')
        {
            // UPDATE DATA HEAR...!!

            if($input['amount_type'] == 'num')
            {
                $c = $this->db->where('status','1')->get('la_currency')->row_array();
                $amount_type = $c['currency_symbol'];
            }else{
                $amount_type = $input['amount_type'];
            }

            if($img == '')
            {
                $config['upload_path']   = 'assets/images/Offer/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'offer_name' => $input['offer_name'],
                    'amount' => $input['amount'],
                    'amount_type' => $amount_type,
                    'applicable_on_service' => $input['applicable_on_service'],
                    'end_date' => $input['end_date'],
                    'title' => $input['title']?$input['title']:'',
                    'description' => $input['description']?$input['description']:'',
                    'updated_at' => date('Y-m-d H:i:s'),
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/Offer/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'offer_name' => $input['offer_name'],
                    'amount' => $input['amount'],
                    'amount_type' => $amount_type,
                    'applicable_on_service' => $input['applicable_on_service'],
                    'end_date' => $input['end_date'],
                    'title' => $input['title']?$input['title']:'',
                    'description' => $input['description']?$input['description']:'',
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('offer_id',$input['offer_id']);
            $this->db->update('la_offer',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('offer');
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/Offer/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            $offer_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

            $length = 6;
            $promocode = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet.= "0123456789";
            $max = strlen($codeAlphabet); // edited

            for ($i=0; $i < $length; $i++) {
                $promocode .= $codeAlphabet[random_int(0, $max-1)];
            }

            if($input['amount_type'] == 'num')
            {
                $c = $this->db->where('status','1')->get('la_currency')->row_array();
                $amount_type = $c['currency_symbol'];
            }else{
                $amount_type = $input['amount_type'];
            }

            $data = array( 
                'offer_id' => $offer_id,
                'shop_id' => $input['shop_id'],
                'offer_name' => $input['offer_name'],
                'promocode' => $promocode,
                'amount' => $input['amount'],
                'amount_type' => $amount_type,
                'applicable_on_service' => $input['applicable_on_service'],
                'end_date' => $input['end_date'],
                'title' => $input['title']?$input['title']:'',
                'description' => $input['description']?$input['description']:'',
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('la_offer',$data);
        
            $this->session->set_flashdata('error',DATA_SUBMIT);
            redirect('offer');

        }
        
    }

    public function getShopName($id)
    {
        $q = $this->db->where('shop_id',$id)->get('la_laundry_shop')->row_array();
        return $q['shop_name'];
    }

    public function changeOfferStatus($id)
    {
        $this->db->where('offer_id',$id);
        $query = $this->db->get('la_offer');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('offer_id',$id);
            $this->db->update('la_offer',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('offer_id',$id);
            $this->db->update('la_offer',$data);
        }
    }

    public function updateOffer($id)
    {
        $query = $this->db->where('offer_id',$id)->get('la_offer')->row_array();
        return $query;
    }

    public function getServiceName($id)
    {
        $q = $this->db->where('service_id',$id)->get('la_service')->row_array();
        return $q['service_name'];
    }


}