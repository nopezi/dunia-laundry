<?php
class Laundry_shop_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function laundryShop()
    {
        $ses = $_SESSION;
        if($ses['status'] == '2')
        {
            $result = $this->db->where('user_id',$ses['id'])->get('la_laundry_shop')->result();
            return $result;
        }
        else
        {
            $this->db->select('ls.*');
            $this->db->from('la_laundry_shop ls');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');

            # jika user franchise
            if ($ses['type'] == 3) {
                $this->db->where('lu.regional', $ses['regional']);
            }

            $this->db->where('lu.type', '2');
            $this->db->order_by('ls.s_no','DESC');
            $query = $this->db->get();
            return $query->result();
        }
        
    }

    public function laundryShopAgen()
    {
        $ses = $_SESSION;
        
        $this->db->select('ls.*, lu.image as foto_profile');
        $this->db->from('la_laundry_shop ls');
        $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
        $this->db->where('lu.type', '1');
        if ($ses['type'] == 3) {
            $this->db->where('lu.regional', $ses['regional'])
                     ->where('lu.user_id != ', $ses['id']);
        }
        $this->db->order_by('ls.s_no','DESC');
        $query = $this->db->get();

        return $query->result();
        
    }

    public function getAllLaundryOwner($type=null)
    {
        if ($_SESSION['type'] == 3) {

            if (!empty($type)) {
                $this->db->where('type', $type);
            }

            $result = $this->db->where('regional', $_SESSION['regional'])
                               ->where('user_id != ', $_SESSION['id'])
                               ->get('la_user')
                               ->result();

        } else{

            if (!empty($type)) {
                $this->db->where('type', $type);
            } else {
                $this->db->where_in('type',['2','1']);
            }
            $result = $this->db->where('status','1')->get('la_user')->result();    

        }
        
        return $result;
    }

    public function submitLaundryShop($input,$img)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
    	$this->form_validation->set_rules('shop_name', 'shop_name', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        if ($this->form_validation->run() == false)
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            redirect('laundryShop');
            exit();
        }

        if($input['shop_id'] !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/Laundry/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'description' => $input['description']?$input['description']:'',
                    'updated_at' => date('Y-m-d H:i:s'),
                );
            }
            else
            {
                $config['upload_path']   = 'assets/images/Laundry/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'user_id' => $input['user_id'],
                    'shop_name' => $input['shop_name'],
                    'country_code' => $input['country_code'],
                    'mobile' => $input['mobile'],
                    'address' => $input['address']?$input['address']:'',
                    'latitude' => $input['latitude']?$input['latitude']:'',
                    'longitude' => $input['longitude']?$input['longitude']:'',
                    'opening_time' => $input['opening_time']?$input['opening_time']:'',
                    'closing_time' => $input['closing_time']?$input['closing_time']:'',
                    'mulai_hari' => $input['mulai_hari'],
                    'sampai_hari' => $input['sampai_hari'],
                    'description' => $input['description']?$input['description']:'',
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                    );
            }
            $this->db->where('shop_id',$input['shop_id']);
            $this->db->update('la_laundry_shop',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);

            if ( 'agen' === $input['type_laundry'] ) {
                redirect('laundry/laundryShopAgen');
            } else {
                redirect('laundryShop');
            }
        }
        else
        {
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/Laundry/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $shop_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

        	$data = array( 
                'shop_id' => $shop_id,
                'user_id' => $input['user_id'],
	    		'shop_name' => $input['shop_name'],
                'country_code' => $input['country_code'],
                'mobile' => $input['mobile'],
                'address' => $input['address']?$input['address']:'',
                'latitude' => $input['latitude']?$input['latitude']:'',
                'longitude' => $input['longitude']?$input['longitude']:'',
                'opening_time' => $input['opening_time']?$input['opening_time']:'',
                'closing_time' => $input['closing_time']?$input['closing_time']:'',
                'mulai_hari' => $input['mulai_hari'],
                'sampai_hari' => $input['sampai_hari'],
                'description' => $input['description']?$input['description']:'',
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
	    	);
    	    $this->db->insert('la_laundry_shop',$data);
    		$this->session->set_flashdata('error',DATA_SUBMIT);

            $cek_redirect = $this->db->get_where('la_user', ['user_id'=>$input['user_id']])->first_row();

            if ($cek_redirect->type == '1') {
                redirect('laundry/laundryShopAgen','refresh');
            } else {
                redirect('laundryShop');
            }

        }
    }

    public function changeLaundryShopStatus($id)
    {
        $this->db->where('shop_id',$id);
        $query = $this->db->get('la_laundry_shop');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('shop_id',$id);
            $this->db->update('la_laundry_shop',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('shop_id',$id);
            $this->db->update('la_laundry_shop',$data);
        }
    }

    public function updateLaundryShop($shop_id)
    {
        $query = $this->db->where('shop_id',$shop_id)->get('la_laundry_shop')->row_array();
        return $query;
    }  
    
    public function getOwnerName($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row_array();

        if (!empty($query['name'])) {
            return $query['name'];
        } else {
            return '-';
        }
    }

    public function getCountryCode()
    {
        $result = $this->db->get('la_country')->result();
        return $result;
    }

    public function getShopCount()
    {

        if ($_SESSION['type'] == 3) {
            $num = $this->db->from('la_laundry_shop ls')
                            ->where('lu.regional', $_SESSION['regional'])
                            ->join('la_user lu', 'lu.user_id=ls.user_id')
                            ->get()
                            ->num_rows();
        } else {
            $num = $this->db->get('la_laundry_shop')->num_rows();
        }
        
        return $num;
    }

    public function delete($id)
    {
        return $this->db->delete('la_laundry_shop', array('shop_id' => $id));
    }
    	


}