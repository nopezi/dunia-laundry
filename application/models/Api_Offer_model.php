<?php
class Api_Offer_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getAllOffer($count)
    {
        $this->form_validation->set_rules('count', 'count', 'required');
        if ($this->form_validation->run() == false) {
          $this->Api_Auth_model->responseFailed(0, "Please fill all field");
          exit();
        }
    	$query = $this->db->where('status','1')->get('la_offer')->num_rows();
    	if($query > 0)
    	{
            // $this->db->select('*,CONCAT("'.base_url().'assets/images/Offer/",image) as image');
            // $this->db->where('status','1');
            // $this->db->limit($count);
            // $res = $this->db->get('la_offer')->result();
            // return $res;
            $this->db->select('o.*,s.service_name,l.shop_name,l.address,l.rating');
            $this->db->select('CONCAT("assets/images/Offer/",o.image) as image');
            $this->db->select('CONCAT(o.amount," ", o.amount_type ," off here your code ends ") as detail');
            $this->db->select('CONCAT("Get ",o.amount," ", o.amount_type ," off on ",s.service_name) as description');
            $this->db->from('la_offer o');
            $this->db->join('la_laundry_shop l', 'l.shop_id = o.shop_id');
            $this->db->join('la_service s', 's.service_id = o.applicable_on_service');
            $this->db->where('o.status','1');
            $this->db->limit($count);
            $query = $this->db->get();
            return $query->result();
    	}else{
    		return 0;
    	}
    }

    public function getOfferForLaundryShop($shop_id)
    {
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }
        $query = $this->db->where('shop_id',$shop_id)->where('status','1')->get('la_offer')->num_rows();
        if($query > 0)
        {
            $this->db->select('o.*,s.service_name,l.shop_name,l.address,l.rating');
            $this->db->select('CONCAT("assets/images/Offer/",o.image) as image');
            $this->db->select('CONCAT(o.amount," ", o.amount_type ," off here your code ends ") as detail');
            $this->db->select('CONCAT("Get ",o.amount," ", o.amount_type ," off on ",s.service_name) as description');
            $this->db->from('la_offer o');
            $this->db->join('la_laundry_shop l', 'l.shop_id = o.shop_id');
            $this->db->join('la_service s', 's.service_id = o.applicable_on_service');
            $this->db->where('o.status','1');
            $this->db->limit($count);
            $query = $this->db->get();
            return $query->result();
        }else{
            return 0;
        }
    }

    public function applyPromocode($input)
    {
        $this->form_validation->set_rules('promocode', 'promocode', 'required');
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        $this->form_validation->set_rules('total_price', 'total_price', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
            exit();
        }

        $shopExist = $this->db->where('shop_id',$input['shop_id'])->get('la_offer')->num_rows();
        if($shopExist > 0)
        {
            $codeExist = $this->db->where('shop_id',$input['shop_id'])->where('promocode',$input['promocode'])->get('la_offer');
            if($codeExist->num_rows() > 0)
            {
                $codeActive = $this->db->where('shop_id',$input['shop_id'])->where('promocode',$input['promocode'])->where('status','1')->get('la_offer');
                if($codeActive->num_rows() > 0)
                {
                    $ce = $codeActive->row_array();
                    if($ce['amount_type'] != '%')
                    {
                        $re_amt = ($input['total_price'] - $ce['amount']);
                        if($re_amt > 0)
                        {
                            $this->responseSuccess(1,'Promocode', $re_amt);
                        }else{
                            $this->responseSuccess(1,'Promocode', '0');
                        }
                    }else{
                        $re_amt_per = ($input['total_price'] * $ce['amount'] /100);
                        $tot_amt = $input['total_price']-$re_amt_per;
                        if($tot_amt > 0)
                        {
                            $this->responseSuccess(1,'Promocode', $tot_amt);
                        }else{
                            $this->responseSuccess(1,'Promocode', '0');
                        }
                    }
                }
                else
                {
                    $this->responseFailed(0,'Promocode is expired','');
                    exit(); 
                }
                
            }
            else
            {
                $this->responseFailed(0,'Promocode is not available','');
                exit(); 
            }
        }
        else
        {
            $this->responseFailed(0,'Shop id is not available','');
            exit(); 
        }

    }

    public function responseSuccessImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailedImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}