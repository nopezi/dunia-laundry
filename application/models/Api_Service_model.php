<?php
class Api_Service_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function getAllService()
    {
    	$query = $this->db->where('status','1')->get('la_service');
    	if($query->num_rows() > 0)
    	{
            $this->db->select('*, CONCAT("assets/images/Service/",image) as image');
            $this->db->from('la_service');
            $this->db->where('status','1');
            $query =$this->db->get(); 
            return $query->result(); 
    		// $res = $query->result();
    		// return $res;
    	}else{
    		return 0;
    	}
    }

    public function getShopServices($shop_id)
    {
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        if ($this->form_validation->run() == false) {
          $this->Api_Auth_model->responseFailed(0, "Please fill all field");
          exit();
        }
        
        $query = $this->db->where('status','1')->get('la_price_list');
        if($query->num_rows() > 0)
        {
            $this->db->select('s.service_name,s.description,CONCAT("assets/images/Service/",s.image) as image');
            $this->db->from('la_service s');
            $this->db->join('la_price_list pl', 'pl.service_id = s.service_id');
            $this->db->where('pl.status','1');
            $this->db->where('pl.shop_id',$shop_id);
            $this->db->group_by('s.service_id'); 
            $this->db->group_by('s.service_name'); 
            $this->db->group_by('s.description'); 
            $this->db->group_by('image'); 
            $query = $this->db->get();
            return $query->result();
        }else{
            return 0;
        }
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseSuccessImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailedImage($status, $message, $data, $img)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data, 'image_url' => $img); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}