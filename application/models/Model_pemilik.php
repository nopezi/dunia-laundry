<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pemilik extends CI_Model {

	public function get_pemilik()
    {
        if ($_SESSION['type'] == 3) {
            $result = $this->db->where('regional', $_SESSION['regional'])
                               ->where('user_id != ', $_SESSION['id'])
                               ->where('type !=', '3')
                               ->order_by('s_no', 'desc')
                               ->get('la_user')
                               ->result();
        } else {
            $result = $this->db->where_in('type',['2','1'])->order_by('s_no', 'desc')->get('la_user')->result();
        }
    	
    	return $result;
    }

    public function detail($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_user')->row();
        return $query;
    }

    public function payment_history($user_id)
    {
        $query = $this->db->where('user_id',$user_id)->get('la_payments')->result();
        return $query;
    }

    public function submitOwnerPackage($input)
    {
        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('package_id', 'package_id', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        $payment_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

        $user_id = $input['user_id'];
        $package_id = $input['package_id'];
        $pkg = $this->db->where('package_id',$package_id)->get('la_subscription_package')->row_array();
        $no_of_days = $pkg['no_of_days'];
        $no = $no_of_days+'1';
        $date = date('Y-m-d', strtotime("+$no days"));
        $data = [
               'user_id' => $user_id,
               'payment_id' => $payment_id,
               'amount' => $pkg['amount'],
               'package_id' => $package_id,
               'end_subscription_date' => $date,
               'payment_type' => '2',
               'created_at' => date('Y-m-d H:i:s'),
            ];

        $insert = $this->db->insert('la_payments', $data);

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        if(!empty($user['end_subscription_date'])) {

            if($user['end_subscription_date'] >= date('Y-m-d')) {

                $end_subscription_date = $user['end_subscription_date'];
                $now = time(); // or your date as well
                $your_date = strtotime("$end_subscription_date");
                $datediff =  $your_date - $now;

                $day = round($datediff / (60 * 60 * 24));
                $num = $day+$no;
                $dates = date('Y-m-d', strtotime("+$num days"));
                // echo $dates; die;
            } else {
                $no = $no_of_days+'1';
                $dates = date('Y-m-d', strtotime("+$no days"));
            }
            
        } else {
            $no = $no_of_days+'1';
            $dates = date('Y-m-d', strtotime("+$no days"));
        }

        $data1 = array(
            'end_subscription_date' => $dates
        );
        $sucess = $this->db->where('user_id',$user_id)->update('la_user',$data1);
        if($sucess) {
            $email = $user['email'];
            $name = $user['name'];
            $amount = $pkg['amount'];
            $subject = SUBSCRIPTION_HEAD;
            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $amount Rs. and it will be available till $dates, thank you.";
            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
        }
        $this->session->set_flashdata('success',DATA_SUBMIT);
        return 'berhasil';
        
    }

    public function tambah($input,$img)
    {

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('regional', 'regional', 'required');

        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);
            return 'error';
        }

        $email = $input['email'];
        $getdata = $this->db->where('email',$email)->get('la_user')->num_rows();

        if($getdata > 0) {

            $this->session->set_flashdata('error', EMAIL_EXIST);
            return 'error';

        } else {

            if(empty($img)) {
                $image = "default.png";
            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = null;
                if($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }

            $user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);

            $data = [
                'user_id'      => $user_id,
                'name'         => $input['name'],
                'email'        => $input['email'],
                'country_code' => '62',
                'mobile'       => $input['mobile'],
                'password'     => $input['password'],
                'address'      => $input['address']?$input['address']:'',
                'regional'     => $input['regional'],
                'distrik'      => $input['distrik'],
                'latitude'     => $input['latitude']?$input['latitude']:'',
                'longitude'    => $input['longitude']?$input['longitude']:'',
                'image'        => $image,
                'type'         => $input['type'],
                'created_at'   => date('Y-m-d H:i:s')
            ];
            $this->db->insert('la_user',$data);
        
            $this->session->set_flashdata('success',DATA_SUBMIT);
            return 'berhasil';

        }
    }

    public function update($input,$img)
    {

        // $this->form_validation->set_rules('name', 'name', 'required');
        // $this->form_validation->set_rules('email', 'email', 'required');
        // $this->form_validation->set_rules('mobile', 'mobile', 'required');
        // $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('regional', 'regional', 'required');
        if ($this->form_validation->run() == false) 
        {
            $this->session->set_flashdata('error',ALL_FIELD_MANDATORY);

            return 'error';
        }

        if(!empty($input['user_id'])) {

            // UPDATE DATA HEAR...!!
            if(empty($img)) {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => '62',
                    'mobile'       => $input['mobile'],
                    'password'     => '123456',//$input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'type'         => $input['type'] ?:'1',
                    'premium'      => $input['premium'],
                    'updated_at'   => date('Y-m-d H:i:s'),
                ];

            } else {

                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = [
                    'name'         => $input['name'],
                    'email'        => $input['email'],
                    'country_code' => '62',
                    'mobile'       => $input['mobile'],
                    'password'     => '123456',//$input['password'],
                    'address'      => $input['address'] ?:'',
                    'regional'     => $input['regional'],
                    'distrik'      => $input['distrik'],
                    'latitude'     => $input['latitude'] ?:'',
                    'longitude'    => $input['longitude'] ?:'',
                    'image'        => $image,
                    'type'         => $input['type'] ?:'1',
                    'premium'      => $input['premium'],
                    'updated_at'   => date('Y-m-d H:i:s'),
                ];
            }

            $this->db->update('la_user',$data, ['user_id'=>$input['user_id']]);
            $this->session->set_flashdata('success', DATA_UPDATE);

            if (!empty($input['email'])) {
                $subject = "[Dunia Laundry] Update data ".$input['name'];
                $msg = "Akun anda telah di update oleh admin dengan password 123456";

                $kirim_email = $this->send_email_by_msg($input['email'], $subject, $msg);
            }

            return 'berhasil';

        } else {
        	$this->session->set_flashdata('error', 'id user tidak boleh kosong');
        	return 'error';
        }

    }

    public function send_email_by_msg($email_id,$subject,$msg)
    {
           $KEY=$this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));
           $authKey=$KEY->msg91_key;

            // $authKey = MSG_AUTH_KEY;
            $from= SENDER_EMAIL;

            //Prepare you post parameters
            $postData = array(
                'authkey' => $authKey,
                'to' => $email_id,
                'from' => $from,
                'subject' => $subject,
                'body' => $msg
            );
           //API URL
            $url="https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $authKey ) ."&to=" . urlencode( $email_id ) . "&from=" . urlencode( $from ) ."&body=" . urlencode( $msg ) ."&subject=" . urlencode( $subject );
            // init the resource
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
            ));

            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            //get response
            $output = curl_exec($ch);
            //Print error if any
            if(curl_errno($ch))
            {
              echo 'error:' . curl_error($ch);
            }
            curl_close($ch);
    }

    public function ganti_status($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('la_user');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('user_id',$id);
            $this->db->update('la_user',$data);
        }
    }

    public function delete($id)
    {
        $gambar = $this->db->get_where('la_user', ['user_id'=>$id_user])->first_row();
        unlink('assets/images/user/'.$gambar->image);
        return $this->db->delete('la_user', ['user_id' => $id]);
    }

}

/* End of file Model_pemilik.php */
/* Location: ./application/models/Model_pemilik.php */