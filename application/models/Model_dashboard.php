<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {

	public function total_all_komisi()
    {

    	$persentasi_komisi = $this->Order_model->cekPersentasi(1);

    	$data = $this->db->select('final_price')
    					 ->from('la_order_details')
    					 ->where('order_status !=', '6')
    					 ->get()
    					 ->result();

    	if (empty($data)) {
    		return 'Rp. 0';
    	} else {

    		foreach ($data as $key => $value) {
    			$cek_dolar  = str_replace('$', '', $value->final_price);
	            $cek_rupiah = str_replace('Rp', '', $cek_dolar);
	            $cek_rupiah = str_replace('₹', '', $cek_rupiah);
	            $cek_rupiah = str_replace('?', '', $cek_rupiah);
    			$total[]    = $cek_rupiah * $persentasi_komisi['nilai'];
    		}

    		return 'Rp.'.number_format(array_sum($total));

    	}

    }

    public function getUserCount()
    {
        $num = $this->db->where('type !=','0')
        			 	->get('la_user')
        			 	->num_rows();
        return $num;
    }

    public function komisi_all_franchise()
    {

        $p_franchise = $this->Order_model->cekPersentasi(3);
        $persentasi_franchise = $p_franchise['persentasi'] / 100;

        # data seluruh user franchise
        $user_franchise = $this->db->get_where('la_user', ['type'=>3])->result();

        $data = [];
        foreach ($user_franchise as $key => $uf) {

            $lokasi_regional = $this->db->get_where('la_regencies', ['id'=>$uf->regional])->first_row();
            
            if (!empty($uf->regional)) {
                
                $d_order_user = $this->db->select('ls.user_id, 
                                                   ls.shop_id, 
                                                   ls.shop_name,
                                                   lu.regional,
                                                   lu.type')
                                         ->distinct()
                                         ->from('la_order_details lo')
                                         ->join('la_laundry_shop ls', 'ls.shop_id=lo.shop_id')
                                         ->join('la_user lu', 'lu.user_id=ls.user_id')
                                         ->where('lo.status', '1')
                                         ->where('lu.regional', $uf->regional)
                                         ->get()
                                         ->result();

                if (!empty($d_order_user)) {

                    foreach ($d_order_user as $key => $value) {

                       $where = [
                            'user_id' => $value->user_id,
                            // 'type'    => $type
                       ];
                       $user_type = $this->db->get_where('la_user', $where)->first_row();

                       if (!empty($user_type)) {

                           $d_order = $this->komisi_all($value->shop_id);
                           $persentasi_agen = $this->cekPersentasi($value->type);
                           $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];

                           $detail = [];

                           if (!empty($d_order['detail'])) {
                               foreach ($d_order['detail'] as $key => $dt) {
                                   $mata_uang = $dt->currency_code?:'Rp';
                                   $total_pendapatan = $dt->final_price * $persentasi_agen['nilai'];
                                   $pendapatan_franchise = $dt->final_price - $total_pendapatan;
                                   $pendapatan_franchise = $pendapatan_franchise * $persentasi_franchise;
                                   $detail[] = [
                                        'id_pesanan' => $dt->order_id,
                                        'komisi'     => $persentasi_agen['persentasi'].'%',
                                        'komisi_franchise' => $p_franchise['persentasi'].'%',
                                        'harga'      => $mata_uang.' '.number_format((float)$dt->final_price),
                                        'pendapatan_bersih' => $mata_uang.' '.number_format((float)$dt->final_price * $persentasi_agen['nilai']),
                                        'pendapatan_franchise' => $mata_uang.' '.number_format((float)$pendapatan_franchise)
                                    ];
                               }
                           }

                           // $persentasi_agen = $this->cekPersentasi($du->type);
                            $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];
                            // $total_franchise = ($u->final_price - $income) * $persentasi_franchise;
                            $total_franchise = ($d_order['total']->total_pendapatan - $income) * $persentasi_franchise;

                           $data[] = [
                                'user_id' => $uf->user_id,
                                'username'=> $uf->name,
                                'shop_id' => $value->shop_id,
                                'outlet'  => $value->shop_name,
                                'region'  => $lokasi_regional->name,
                                'type'    => $this->cekTypeUser(3),
                                'persentasi' => $persentasi_agen['persentasi'].'%',
                                'persentasi_franchise' => $p_franchise['persentasi'].'%',
                                'total_pendapatan_kotor'  => 'Rp '.number_format($d_order['total']->total_pendapatan),
                                'total_pendapatan_bersih' => 'Rp '.number_format($income),
                                'total_pendapatan_franchise' => 'Rp '.number_format($total_franchise),
                                'detail'  => $detail
                            ];

                       } 

                    }

                    // return $data;

                }

            }

        }

        return $data;

    }

}

/* End of file Model_dashboard.php */
/* Location: ./application/models/Model_dashboard.php */