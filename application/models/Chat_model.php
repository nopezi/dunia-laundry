<?php
class Chat_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function chat()
    {
        $ses = $_SESSION;
        if($ses['status'] == '2')
        {
        	$res = $this->db->select('from_user_id,to_user_id,message_head_id')->where('to_user_id',$ses['id'])->order_by('message_head_id','DESC')->get('la_message_head')->result();
        	return $res;
        }
        else
        {
            $res = $this->db->select('from_user_id,to_user_id,message_head_id')->order_by('message_head_id','DESC')->get('la_message_head')->result();
            return $res;
        }
    }

    public function getUserDetail($user_id)
    {
    	$q = $this->db->where('user_id',$user_id)->get('la_user')->row();
    	return $q;
    }

    public function getShopDetail($user_id)
    {
        $q = $this->db->where('user_id',$user_id)->get('la_laundry_shop')->row();
        return $q;
    }

    public function getAllMessageData($message_head_id)
    {
        $res = $this->db->where('message_head_id',$message_head_id)->get('la_message')->result();
        return $res;
    }

    public function chatOwner($id)
    {
        $res = $this->db->where('message_head_id',$id)->get('la_message')->result();
        return $res;
    }

    public function setLaundryOwnerMsg($message_head_id,$message,$to_user_id,$from_user_id)
    {
        $data = array(
            'message_head_id' => $message_head_id,
            'message' => $message,
            'to_user_id' => $to_user_id,
            'from_user_id' => $from_user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('la_message',$data);

        $user = $this->db->where('user_id',$to_user_id)->get('la_user')->row_array();
        $device_token = $user['device_token'];
        $type = 7003;
        $data = $this->Notification_model->firebase_with_class($device_token, '', '',$type, 'Chat', $message);
        return $user;
    }

}