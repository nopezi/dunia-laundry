<?php
class Product_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function product()
    {
        $ses = $_SESSION;
        if($ses['status'] == '2'){
            $this->db->select('pl.*');
            $this->db->from('la_price_list pl');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = pl.shop_id');
            //$this->db->where('pl.status','1');
            $this->db->where('ls.user_id',$ses['id']);
            $query = $this->db->get();
            return $query->result();

            // $res = $this->db->get('la_price_list')->result();
            // return $res;
        } else {
            $res = $this->db->get('la_price_list')->result();
            return $res;
        }
    	
    }

    public function getLaundryShop()
    {
    	$res = $this->db->where('status','1')->get('la_laundry_shop')->result();
    	return $res;
    }

    public function getLaundryShopByUserId($user_id)
    {
        $res = $this->db->where('user_id',$user_id)->where('status','1')->get('la_laundry_shop')->result();
        return $res;
    }

    public function getService()
    {
        if ($_SESSION['premium'] == 1) {
            $shop_id = $this->db->get_where('la_laundry_shop', ['user_id'=>$_SESSION['id']])->first_row();
            $this->db->where('shop_id', $shop_id->shop_id);
        }
    	$res = $this->db->where('status','1')->get('la_service')->result();
    	return $res;
    }

    public function submitProduct($input,$img)
    {
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        $this->form_validation->set_rules('service_id', 'service_id', 'required');
        $this->form_validation->set_rules('item_name', 'item_name', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red', ALL_FIELD_MANDATORY);
            redirect('product');
            exit();
        }

        $id = $input['item_id'];
        if($id !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/Product/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'service_id' => $input['service_id'],
                    'item_name' => $input['item_name'],
                    'type' => $input['type'],
                    'price' => $input['price'],
                    'updated_at' => date('Y-m-d H:i:s')
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/Product/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'service_id' => $input['service_id'],
                    'item_name' => $input['item_name'],
                    'type' => $input['type'],
                    'price' => $input['price'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                        );
            }
            $this->db->where('item_id',$id);
            $this->db->update('la_price_list',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('product');
        }
        else
        {
            // INSERT DATA HEAR...!!
            $img = $_FILES['image']['name'];
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/Product/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            
            $item_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
            $data = array(
                'item_id' => $item_id,
                'shop_id' => $input['shop_id'],
                'service_id' => $input['service_id'],
                'item_name' => $input['item_name'],
                'type' => $input['type'],
                'price' => $input['price'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('la_price_list', $data);
            $this->session->set_flashdata('error', DATA_SUBMIT);
            redirect('product');
            
        }
    }

    public function submitProduct_v2($input,$img)
    {
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        $this->form_validation->set_rules('service_id', 'service_id', 'required');
        $this->form_validation->set_rules('item_name', 'item_name', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red', ALL_FIELD_MANDATORY);
            redirect('mitra_agen/product');
            exit();
        }

        $id = $input['item_id'];
        if($id !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '') {
                $config['upload_path']   = 'assets/images/Product/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'service_id' => $input['service_id'],
                    'item_name' => $input['item_name'],
                    'type' => $input['type'],
                    'price' => $input['price'],
                    'updated_at' => date('Y-m-d H:i:s')
                        );
            } else {
                $config['upload_path']   = 'assets/images/Product/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'shop_id' => $input['shop_id'],
                    'service_id' => $input['service_id'],
                    'item_name' => $input['item_name'],
                    'type' => $input['type'],
                    'price' => $input['price'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                        );
            }
            $this->db->where('item_id',$id);
            $this->db->update('la_price_list',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('mitra_agen/product');
        } else {
            // INSERT DATA HEAR...!!
            $img = $_FILES['image']['name'];
            if($img == '') {
                $image = "default.png";
            } else {
                $config['upload_path']   = 'assets/images/Product/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            
            $item_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
            $data = array(
                'item_id' => $item_id,
                'shop_id' => $input['shop_id'],
                'service_id' => $input['service_id'],
                'item_name' => $input['item_name'],
                'type' => $input['type'],
                'price' => $input['price'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('la_price_list', $data);
            $this->session->set_flashdata('error', DATA_SUBMIT);
            redirect('mitra_agen/product');
            
        }
    }

    public function getShopName($id)
    {
    	$que = $this->db->where('shop_id',$id)->get('la_laundry_shop')->row_array();
    	return $que['shop_name'];
    }

    public function getServiceName($id)
    {
    	$que = $this->db->where('service_id',$id)->get('la_service')->row_array();
    	return $que['service_name'];
    }

    public function changeProductStatus($id)
    {
    	$this->db->where('item_id',$id);
        $query = $this->db->get('la_price_list');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('item_id',$id);
            $this->db->update('la_price_list',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('item_id',$id);
            $this->db->update('la_price_list',$data);
        }
    }

    public function updateProduct($id)
    {
    	$data = $this->db->where('item_id',$id)->get('la_price_list')->row_array();
        return $data;
    }


}
