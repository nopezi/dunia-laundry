<?php
class Payment_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function subscriptionPackage()
    {
    	$res = $this->db->get('la_subscription_package')->result();
    	return $res;
    }

    public function submitSubscriptionPackage($input,$img)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('no_of_days', 'no_of_days', 'required');
        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_red', ALL_FIELD_MANDATORY);
            redirect('subscriptionPackage');
            exit();
        }

        $id = $input['package_id'];
        if($id !='')
        {
            // UPDATE DATA HEAR...!!
            if($img == '')
            {
                $config['upload_path']   = 'assets/images/package/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'no_of_days' => $input['no_of_days'],
                    'amount' => $input['amount'],
                    'description' => $input['description'],
                    'updated_at' => date('Y-m-d H:i:s')
                        );
            }
            else
            {
                $config['upload_path']   = 'assets/images/package/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data = array(
                    'title' => $input['title'],
                    'no_of_days' => $input['no_of_days'],
                    'amount' => $input['amount'],
                    'description' => $input['description'],
                    'image' => $image,
                    'updated_at' => date('Y-m-d H:i:s'),
                        );
            }
            $this->db->where('package_id',$id);
            $this->db->update('la_subscription_package',$data);
            $this->session->set_flashdata('error', DATA_UPDATE);
            redirect('subscriptionPackage');
        }
        else
        {
            // INSERT DATA HEAR...!!
            $img = $_FILES['image']['name'];
            if($img == '')
            {
                $image = "default.png";
            }else{
                $config['upload_path']   = 'assets/images/package/';
                // return $config['upload_path']; exit();
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 10000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }
            }
            
            //$item_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
            $data = array(
                'title' => $input['title'],
                'no_of_days' => $input['no_of_days'],
                'amount' => $input['amount'],
                'description' => $input['description'],
                'image' => $image,
                'created_at' => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('la_subscription_package', $data);
            $this->session->set_flashdata('error', DATA_SUBMIT);
            redirect('subscriptionPackage');
            
        }
    }

    public function changeSubscriptionPackageStatus($id)
    {
    	$this->db->where('package_id',$id);
        $query = $this->db->get('la_subscription_package');
        $data = $query->row_array();
        if($data['status'] == '1')
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('package_id',$id);
            $this->db->update('la_subscription_package',$data);
        }
        else
        {
            $data = array(
                'status' => '1'
            );
            $this->db->where('package_id',$id);
            $this->db->update('la_subscription_package',$data);
        }
    }

    public function updateSubscriptionPackage($id)
    {
    	$data = $this->db->where('package_id',$id)->get('la_subscription_package')->row_array();
        return $data;
    }

    public function Subscription()
    {
        $res = $this->db->where('status','1')->get('la_subscription_package')->result();
        return $res;
    }

    public function invoiceHistory()
    {
        $this->db->group_by('user_id');
        $this->db->order_by('id','DESC');
        $res = $this->db->get('la_payments')->result();
        return $res;
    }

    public function getPackageDetail($package_id)
    {
        $data = $this->db->where('package_id',$package_id)->get('la_subscription_package')->row_array();
        return $data;
    }

    public function myPackage()
    {
        $ses = $_SESSION;
        $res = $this->db->where('user_id',$ses['id'])->get('la_payments')->result();
        return $res;
    }



}