<?php
class Api_Rating_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function addRating($input)
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        $this->form_validation->set_rules('rating', 'rating', 'required');
        if ($this->form_validation->run() == false) {
            $this->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $userExist = $this->db->where('user_id',$input['user_id'])->where('status','1')->get('la_user')->num_rows();
        if($userExist > 0)
        {
        	$shopExist = $this->db->where('shop_id',$input['shop_id'])->where('status','1')->get('la_laundry_shop')->num_rows();
        	if($shopExist > 0)
        	{
        		$ratingExist = $this->db->where('shop_id',$input['shop_id'])->where('user_id',$input['user_id'])->get('la_rating')->num_rows();
        		if($ratingExist > 0)
        		{
        			return 0;
        		}else{

        			$data = array(
        				'user_id' => $input['user_id'],
        				'shop_id' => $input['shop_id'],
        				'rating' => $input['rating'],
        				'created_at' => date('Y-m-d H:i:s')
        				);
                    // echo "<pre>";
                    // print_r($data); die;
        			$sucess = $this->db->insert('la_rating',$data);
                    if($sucess)
                    {
                        $shop = $this->count_total_rating($input['shop_id']);
                        $upd = array(
                            'rating' => $shop->average,
                        );
                        // echo "<pre>";
                        // print_r($shop); die;
                        $this->db->where('shop_id',$input['shop_id']);
                        $this->db->update('la_laundry_shop',$upd);
                    }
        			return 1;
        		}
        	}else{
        		return 2;
        	}
        }else{
        	return 3;
        }
    }

    public function getSingleRow($table, $condition)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($condition);
        $query = $this->db->get();
        return $query->row();       
    }

    public function count_total_rating($shop_id) 
    {
        $shopExist = $this->db->where('shop_id',$shop_id)->get('la_rating')->num_rows();
        if($shopExist > 0)
        {
            $this->db->select('AVG(rating) as average');
            $this->db->where('shop_id', $shop_id);
            $this->db->from('la_rating');
            $query = $this->db->get();
            return $query->row(); 
        }else{
            return 0;
        }

          
    }

    public function responseSuccessWithOutData($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }
        
    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr);
    }

}