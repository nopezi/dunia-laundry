<?php
class Order_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function pembulatan($uang)
    {
         $ratusan = substr($uang, -3);
         if($ratusan<500)
         $akhir = $uang - $ratusan;
         else
         $akhir = $uang + (1000-$ratusan);
         return number_format($akhir, 2, ',', '.');;
    }

    public function order($status_pesanan = null, $tanggal = null)
    {

        $ses = $_SESSION;
        $regional = $this->db->get_where('la_user', ['user_id'=>$ses['id']])->first_row();

        if($ses['type'] == '2' || $ses['type'] == '1') {
            
        	$this->db->select('od.*')
                     ->from('la_order_details od')
                     ->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id')
                     ->where('ls.user_id',$ses['id']);
            
            if ($status_pesanan !== null){
                $this->db->where('order_status', $status_pesanan);
            } else {
                $this->db->where('order_status', 0);
            }

            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get()->result();

            if (!empty($query)) {
                
                foreach ($query as $key => $value) {
                    $d_user = $this->db->get_where('la_user', ['user_id'=>$value->user_id])->first_row();
                    $value->customer = $d_user->name;
                }

            }

            return $query;

        } elseif ($ses['type'] == '3') {
            
            $this->db->select('lo.*, lu.user_id')
                     ->from('la_order_details lo')
                     ->join('la_laundry_shop ll', 'll.shop_id = lo.shop_id')
                     ->join('la_user lu', 'lu.user_id=ll.user_id')
                     ->where('lu.regional', $ses['regional']);
            
            if ($status_pesanan !== null){
                $this->db->where('order_status', $status_pesanan);
            }else{
                $this->db->where('order_status', 0);
            }

            $this->db->order_by('lu.s_no','DESC');

            return $this->db->get()->result();

            // return $this->db->query($query)->result();

        } else {

            if (empty($tanggal)){
                $tanggal = date('Y-m-d');
            }

            $this->db->order_by('s_no','DESC');
            $this->db->like('created_at', $tanggal);

            if ($status_pesanan !== null)
                $this->db->where('order_status', $status_pesanan);
            else
                $this->db->where('order_status', 0);

            $res = $this->db->get('la_order_details')->result();
            return $res;

        }

    }

    public function komisi()
    {
        $ses = $_SESSION;
        if($ses['type'] == '2' || $ses['type'] == '1')
        {

            $this->db->select('od.*, lu.type');
            $this->db->from('la_order_details od');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
            $this->db->where('ls.user_id',$ses['id']);
            $this->db->where('od.payment_status', '1');
            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get();
            return $query->result();
        
        } else if ($ses['type'] == '3') {

            $regional = $this->db->get_where('la_user', ['user_id'=>$ses['id']])->first_row();

            $this->db->select('od.*, lu.type, lu.user_id as id_agen_mitra')
                     ->from('la_order_details od')
                     ->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id')
                     ->join('la_user lu', 'lu.user_id = ls.user_id')
                     // ->where('ls.user_id',$ses['id'])
                     ->where('lu.regional', $regional->regional)
                     ->where('od.payment_status', '1')
                     ->order_by('od.s_no','DESC');
            return $this->db->get()->result();
            

        } else {   

            $this->db->select('od.*, lu.type');
            $this->db->from('la_order_details od');
            $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
            $this->db->join('la_user lu', 'lu.user_id = ls.user_id');
            $this->db->where('od.payment_status', '1');
            $this->db->order_by('od.s_no','DESC');
            $query = $this->db->get();
            return $query->result();

        }

    }

    public function komisi_all($shop_id)
    {

        $this->db->select('la_order_details.s_no')
                 ->from('la_order_details')
                 ->where('shop_id', $shop_id)
                 ->where('status', '1')
                 ->order_by('s_no', 'desc');
        $total_pesanan = $this->db->get()->num_rows();

        $this->db->select('sum(final_price) as total_pendapatan')
                 ->from('la_order_details')
                 ->where('shop_id', $shop_id)
                 ->where('status', '1');
        $total = $this->db->get()->first_row();

        $this->db->select('*')
                 ->from('la_order_details')
                 ->where('shop_id', $shop_id)
                 ->where('status', '1')
                 ->order_by('s_no', 'desc');
        $detail = $this->db->get()->result();

        foreach ($detail as $key => $value) {
            $cek_dolar = str_replace('$', '', $value->final_price);
            $cek_rupiah = str_replace('Rp', '', $cek_dolar);
            $cek_rupiah = str_replace('₹', '', $cek_dolar);
            $cek_rupiah = str_replace('?', '', $cek_dolar);
            $value->final_price = $cek_rupiah;
        }

        return [
            'total'  => $total,
            'detail' => $detail,
            'total_pesanan' => $total_pesanan
        ];

    }

    public function komisi_all_per_type($type)
    {

        $d_order_user = $this->db->select('ls.user_id, ls.shop_id, ls.shop_name')
                                 ->distinct()
                                 ->from('la_order_details lo')
                                 ->join('la_laundry_shop ls', 'ls.shop_id=lo.shop_id')
                                 ->where('lo.status', '1')
                                 ->get()
                                 ->result();

        foreach ($d_order_user as $key => $value) {

           $where = [
                'user_id' => $value->user_id,
                'type'    => $type
           ];
           $user_type = $this->db->get_where('la_user', $where)->first_row();

           if (!empty($user_type)) {

               $d_order = $this->komisi_all($value->shop_id);
               $persentasi_agen = $this->cekPersentasi($type);
               $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];

               $detail = [];

               if (!empty($d_order['detail'])) {
                   foreach ($d_order['detail'] as $key => $dt) {
                       $mata_uang = $dt->currency_code?:'Rp';
                       $detail[] = [
                            'id_pesanan' => $dt->order_id,
                            'komisi'     => $persentasi_agen['persentasi'].'%',
                            'harga'      => $mata_uang.' '.number_format((float)$dt->final_price),
                            'pendapatan_bersih' => $mata_uang.' '.number_format((float)$dt->final_price * $persentasi_agen['nilai']),
                        ];
                   }
               }

               $data[] = [
                    'user_id' => $value->user_id,
                    'username'=> $user_type->name,
                    'shop_id' => $value->shop_id,
                    'outlet'  => $value->shop_name,
                    'region'  => $user_type->regional,
                    'type'    => $this->cekTypeUser($user_type->type),
                    'persentasi' => $persentasi_agen['persentasi'].'%',
                    'total_pendapatan_kotor'  => 'Rp '.number_format($d_order['total']->total_pendapatan),
                    'total_pendapatan_bersih' => 'Rp '.number_format($income),
                    'detail'  => $detail
                ];

           } 

        }

        return $data;

    }

    public function komisi_all_franchise()
    {

        $p_franchise = $this->Order_model->cekPersentasi(3);
        $persentasi_franchise = $p_franchise['persentasi'] / 100;

        # data seluruh user franchise
        $user_franchise = $this->db->get_where('la_user', ['type'=>3])->result();

        $data = [];
        foreach ($user_franchise as $key => $uf) {

            $lokasi_regional = $this->db->get_where('la_regencies', ['id'=>$uf->regional])->first_row();
            
            if (!empty($uf->regional)) {
                
                $d_order_user = $this->db->select('ls.user_id, 
                                                   ls.shop_id, 
                                                   ls.shop_name,
                                                   lu.regional,
                                                   lu.type')
                                         ->distinct()
                                         ->from('la_order_details lo')
                                         ->join('la_laundry_shop ls', 'ls.shop_id=lo.shop_id')
                                         ->join('la_user lu', 'lu.user_id=ls.user_id')
                                         ->where('lo.status', '1')
                                         ->where('lu.regional', $uf->regional)
                                         ->get()
                                         ->result();

                if (!empty($d_order_user)) {

                    foreach ($d_order_user as $key => $value) {

                       $where = [
                            'user_id' => $value->user_id,
                            // 'type'    => $type
                       ];
                       $user_type = $this->db->get_where('la_user', $where)->first_row();

                       if (!empty($user_type)) {

                           $d_order = $this->komisi_all($value->shop_id);
                           $persentasi_agen = $this->cekPersentasi($value->type);
                           $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];

                           $detail = [];

                           if (!empty($d_order['detail'])) {
                               foreach ($d_order['detail'] as $key => $dt) {
                                   $mata_uang = $dt->currency_code?:'Rp';
                                   $total_pendapatan = $dt->final_price * $persentasi_agen['nilai'];
                                   $pendapatan_franchise = $dt->final_price - $total_pendapatan;
                                   $pendapatan_franchise = $pendapatan_franchise * $persentasi_franchise;
                                   $detail[] = [
                                        'id_pesanan' => $dt->order_id,
                                        'komisi'     => $persentasi_agen['persentasi'].'%',
                                        'komisi_franchise' => $p_franchise['persentasi'].'%',
                                        'harga'      => $mata_uang.' '.number_format((float)$dt->final_price),
                                        'pendapatan_bersih' => $mata_uang.' '.number_format((float)$dt->final_price * $persentasi_agen['nilai']),
                                        'pendapatan_franchise' => $mata_uang.' '.number_format((float)$pendapatan_franchise)
                                    ];
                               }
                           }

                           // $persentasi_agen = $this->cekPersentasi($du->type);
                            $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];
                            // $total_franchise = ($u->final_price - $income) * $persentasi_franchise;
                            $total_franchise = ($d_order['total']->total_pendapatan - $income) * $persentasi_franchise;

                           $data[] = [
                                'user_id' => $uf->user_id,
                                'username'=> $uf->name,
                                'shop_id' => $value->shop_id,
                                'outlet'  => $value->shop_name,
                                'region'  => $lokasi_regional->name,
                                'type'    => $this->cekTypeUser(3),
                                'persentasi' => $persentasi_agen['persentasi'].'%',
                                'persentasi_franchise' => $p_franchise['persentasi'].'%',
                                'total_pendapatan_kotor'  => 'Rp '.number_format($d_order['total']->total_pendapatan),
                                'total_pendapatan_bersih' => 'Rp '.number_format($income),
                                'total_pendapatan_franchise' => 'Rp '.number_format($total_franchise),
                                'detail'  => $detail
                            ];

                       } 

                    }

                    // return $data;

                }

            }

        }

        return $data;

    }

    public function data_komisi_total_pertype()
    {

        $order = $this->komisi();

        foreach ($order as $key => $u) {

           # 1 agen, 2 = mitra, 3 = franchise
           if ($u->type == 1) {

               $persentasi_agen   = $this->cekPersentasi(1);
               $income = $u->final_price * $persentasi_agen['nilai'];
               $pendapatan_agen[] = $income;
               $type = 'Agen';

           } else if ($u->type == 2) {

               $persentasi_mitra = $this->cekPersentasi(2);
               $income = $u->final_price * $persentasi_mitra['nilai'];
               $pendapatan_mitra[] = $income;
               $type = 'Mitra';

           } else if ($u->type == 3) {
                // echo "string";
               // $persentasi_franchise = $this->Order_model->cekPersentasi(3);
               // $pendapatan_franchise[] = $u->final_price * $persentasi_franchise['nilai'];
           }

           $p_franchise = $this->cekPersentasi(3);
           $persentasi_franchise = $p_franchise['persentasi'] / 100;
           $total_franchise[] = ($u->final_price - $income) * $persentasi_franchise;
           $type = 'Franchise';
           
        }

        $data = [
            ['user'=> 'Agen', 'total_pendapatan'=>total_nilai($pendapatan_agen)],
            ['user'=> 'Mitra', 'total_pendapatan'=>total_nilai($pendapatan_mitra)],
            ['user'=> 'Franchise', 'total_pendapatan'=>total_nilai($total_franchise)]
        ];

        return $data;

    }

    public function getUserDetail($user_id)
    {
    	$q = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
    	return $q;
    }

    public function cekPencairanDana($order_id)
    {
        return $this->db->get_where('la_pencairan_dana', ['order_id'=>$order_id])->first_row();
    }

    public function cekPersentasi($type)
    {

        $data = $this->db->get_where('la_persentasi_komisi', ['type'=>$type])->first_row();

        $return['nilai'] = $data->persentasi / 100;
        $return['persentasi'] = $data->persentasi;

        return $return;

    }

    public function cekPersentasiFranchise($nilai)
    {
        $data = $this->db->get_where('la_persentasi_komisi', ['type'=>3])->first_row();
        return $data->persentasi / 100 * $nilai;
    }

    public function cekTypeUser($type)
    {

        if ($type == 1) {
            $type_user = '<label class="label label-info">Agen</label>';
        } else if ($type == 2) {
            $type_user = '<label class="label label-success">Mitra</label>';
        } else if ($type == 3) {
            $type_user = '<label class="label label-primary">Franchise</label>';
        }

        return $type_user;

    }

    public function getLaundryShopName($shop_id)
    {
    	$q = $this->db->where('shop_id',$shop_id)->get('la_laundry_shop')->row_array();
    	return $q['shop_name'];
    }

    public function changeOrderStatus($order_id,$order_status)
    {
    	
        $data = array(
            'order_status' => $order_status
        );
       	
        $this->db->where('order_id',$order_id);
        $this->db->update('la_order_details',$data);

        $od = $this->db->where('order_id',$order_id)->get('la_order_details')->row_array();

        if($order_status == '1')
        {
            $title = "Order Confirmed";
            $msg = "Your order no. is ".$od['order_id']. " confirmed now";
        }
        else if($order_status == '2')
        {
            $title = "Order picked up";
            $msg = "Your order no. is ".$od['order_id']. " picked up now";
        }
        else if($order_status == '3')
        {
            $title = "Order in progress";
            $msg = "Your order no. is ".$od['order_id']. " in progress now";
        }
        else if($order_status == '4')
        {
            $title = "Order shipped";
            $msg = "Your order no. is ".$od['order_id']. " shipped now";
        }
        else if($order_status == '5')
        {
            $title = "Order delivered";
            $msg = "Your order no. is ".$od['order_id']. " delivered now";
        }
        else if($order_status == '6')
        {
            $title = "Order cancel";
            $msg = "Your order no. is ".$od['order_id']. " cancel";
        }

        $getUser = $this->db->where('user_id',$od['user_id'])->get('la_user')->row_array();

        $device_token = $getUser['device_token']; 
        $this->Notification_model->firebase_with_class($device_token, '', '', '', $title, $msg);
        // $data1
        $data1['user_id']    = $od['user_id'];
        $data1['title']      = $title;
        $data1['message']    = $msg;
        $data1['status']    = $order_status;
        $data1['created_at']   = date('Y-m-d H:i:s');
        $this->db->insert('la_notification', $data1);

    }

    public function changePaymentStatus($id)
    {
        $this->db->where('order_id',$id);
        $query = $this->db->get('la_order_details');
        $data = $query->row_array();
        if($data['payment_status'] == '1')
        {
            $data = array(
                'payment_status' => '0'
            );
            $this->db->where('order_id',$id);
            $this->db->update('la_order_details',$data);
        }
        else
        {
            $data = array(
                'payment_status' => '1'
            );
            $this->db->where('order_id',$id);
            $this->db->update('la_order_details',$data);
        }
    }

    public function getTotalIncome_v2()
    {

        $p_franchise = $this->Order_model->cekPersentasi(3);
        $persentasi_franchise = $p_franchise['persentasi'] / 100;

        $pemilik_londri = $this->db->select('*')
                         ->from('la_user lu')
                         ->join('la_laundry_shop ls', 'ls.user_id=lu.user_id')
                         ->where('lu.regional', $_SESSION['regional'])
                         ->get()
                         ->result();

        foreach ($pemilik_londri as $key => $u) {
            # persentasi mitra atau agen
           $cek_persentasi = $this->cekPersentasi($u->type);
           $u->persentasi = $cek_persentasi['nilai'];
        }

        // echo "<pre>";
        // die(print_r($pemilik_londri));

        $hasil = [];

        foreach ($pemilik_londri as $key => $pm) {

           $d_order = $this->komisi_all($pm->shop_id);
           // $pendapatan_kotor_sementara = 
           $persentasi_agen = $this->cekPersentasi($pm->type);
           $income = $d_order['total']->total_pendapatan * $persentasi_agen['nilai'];
           $total_sementara = ($d_order['total']->total_pendapatan - $income) * $persentasi_franchise;
           if (!empty($total_sementara)) {
               $total_franchise[] = $total_sementara;
           }

            $res = $this->db->query("SELECT SUM(final_price) AS final_price FROM la_order_details WHERE shop_id = '$pm->shop_id'")->result();
            foreach ($res as $key => $r) {
                $hasil[] = $r->final_price;
            }
        }

        
        return $this->pembulatan(array_sum($total_franchise));
    }

    public function orderDetail($order_id)
    {
        $rowData = $this->db->where('order_id',$order_id)->get('la_order_details')->row_array();
        return $rowData;
    }

    public function getTotalOrder()
    {
        $ses = $_SESSION;
        $regional = $this->db->get_where('la_user', ['user_id'=>$ses['id']])->first_row();

        $this->db->select('od.*');
        $this->db->from('la_order_details od');
        $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
        if ($ses['type'] == 3) {

            $this->db->join('la_user lu', 'lu.user_id=ls.user_id')
                     ->where('lu.regional', $regional->regional);

        } else {
            $this->db->where('ls.user_id',$ses['id']);

        }

        $this->db->order_by('od.s_no','DESC');
        $num = $this->db->get()->num_rows();

        return $num;
    }

    public function getTotalUser()
    {
        $ses = $_SESSION;
        
        $this->db->select('od.*');
        $this->db->from('la_order_details od');
        $this->db->join('la_laundry_shop ls', 'ls.shop_id = od.shop_id');
        $this->db->where('ls.user_id',$ses['id']);
        $this->db->group_by('od.user_id'); 
        $num = $this->db->get()->num_rows();

        return $num;
    }

    public function updateItemDetails( $json, $price, $order_detail, $order_id )
    {
        $json = json_encode( $json );
        $percentage = ( ( $order_detail['price'] - $order_detail['discount'] ) / $order_detail['price'] ) * 100;
        $discount = $price - ( ( $price * $percentage ) / 100 );
        $final_price = $price - $discount;

        $data = array(
            'item_details' => $json,
            'price' => $price,
            'discount' => $discount,
            'final_price' => $final_price
        );
        $this->db->where('order_id', $order_id);
        $this->db->update('la_order_details',$data);
    }

}