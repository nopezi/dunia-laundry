<?php
class Api_Notif_model extends CI_Model
{
    
    function __construct()
    {
        parent:: __construct();
    }

    public function get_notification($user_id)
    {
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == false) {
			$this->Api_Auth_model->responseFailed(0, "Please fill all field");
			exit();
		}
    	$userExist = $this->db->where('user_id',$user_id)->where('status','1')->get('la_user')->num_rows();
    	if($userExist > 0)
    	{
    		$notif = $this->db->where('user_id',$user_id)->get('la_notification');
    		if($notif->num_rows() > 0)
    		{
    			$res = $this->db->select('notification_id,user_id,title,message,status,created_at')->where('user_id',$user_id)->order_by('notification_id','DESC')->get('la_notification')->result();
                foreach ($res as $i => $r) 
                {
                    $get_notif[$i] = array();
                    $get_notif[$i]['notification_id'] = $r->notification_id;
                    $get_notif[$i]['user_id'] = $r->user_id;
                    $get_notif[$i]['title'] = $r->title;
                    $get_notif[$i]['message'] = $r->message;
                    $get_notif[$i]['status'] = $r->status;
                    $get_notif[$i]['created_at'] = round(strtotime($r->created_at));
                }
    			$this->Api_Notif_model->responseSuccess(1, 'List', $get_notif);
    			exit();
    		}else{
    			$this->Api_Auth_model->responseFailed(0, NOTIF_NOT);
				exit();
    		}
    	}else{
			return 1;
		}
    }

    public function responseSuccess($status, $message, $data)
    {
        $arr = array('status' => $status,'message' => $message, 'data'=> $data); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

    public function responseFailed($status, $message)
    {
        $arr = array('status' => $status,'message' => $message); 
        header('Content-Type: application/json');      
        echo json_encode($arr); 
    }

}