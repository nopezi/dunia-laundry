<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function load_page($_this,$page,$data=[])
    {
		$_this->load->view('common/header');
        $_this->load->view('common/sidebar');
        $_this->load->view($page,$data);
        $_this->load->view('common/footer');
    }

    function load_page_franchise($_this,$page,$data=[])
    {
		$_this->load->view('franchise_admin/content/header');
        $_this->load->view('franchise_admin/content/sidebar');
        $_this->load->view($page,$data);
        $_this->load->view('franchise_admin/content/footer');
    }

    function load_page_mitra_agen($_this,$page,$data=[])
    {
        $_this->load->view('mitra_agen/content/header');
        $_this->load->view('mitra_agen/content/sidebar');
        $_this->load->view($page,$data);
        $_this->load->view('mitra_agen/content/footer');
    }

}