<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function response($_this,$page,$data=[])
    {
		$_this->load->view('common/header');
        $_this->load->view('common/sidebar');
        $_this->load->view($page,$data);
        $_this->load->view('common/footer');
    }   
}