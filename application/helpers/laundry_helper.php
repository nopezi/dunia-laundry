<?php

defined('BASEPATH') or exit('No direct script access allowed');

function kirim_firebase($data_notifikasi, $device_token)
{
    
    $url        = "https://fcm.googleapis.com/fcm/send";
    $token      = "/topics/newsBhayangkara";

    $serverKey  = 'AAAA7Y2kuS4:APA91bGHeujtCpT85p1e-RB4Q8m9MgytXhtAfZNRLkAaBdMlFrV4QdbZXvPWWf-PxqwJm-VNRaD50SaEM6xTV3afJCpZbILITaLbscb2lW4hC1-_vMM1zcpyaxDt1QXjaUXaj_RdFDLR';

    
    $fields = array(
        'to' => $device_token,
        'notification' => $data_notifikasi
    );

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key='. $serverKey;

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL             => $url,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_ENCODING        => "",
        CURLOPT_MAXREDIRS       => 10,
        CURLOPT_TIMEOUT         => 30,
        CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST   => "POST",
        CURLOPT_POSTFIELDS      => json_encode($fields),
        CURLOPT_HTTPHEADER      => $headers,
        CURLOPT_SSL_VERIFYPEER  => 0,
    ));

    $response = curl_exec($curl);
    curl_error($curl);
    curl_close($curl);

}

function kirim_email($data)
{

    # $authKey = MSG_AUTH_KEY;
    $from= SENDER_EMAIL;

    # Prepare you post parameters
    $postData = array(
        'authkey' => $data['key'],
        'to'      => $data['email'],
        'from'    => $from,
        'subject' => $data['subject'],
        'body'    => $data['msg']
    );

    print_r(json_encode($postData));

   # API URL
    $url  = "https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $data['key'] ); 
    $url .= "&to=" . urlencode( $data['email'] ) . "&from=" . urlencode( $from ); 
    $url .= "&body=" . urlencode( $data['msg'] ) ."&subject=" . urlencode( $data['subject'] );

    # init the resource
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postData
        //,CURLOPT_FOLLOWLOCATION => true
    ));

    # Ignore SSL certificate verification
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    // echo $url;

    # get response
    $output = curl_exec($ch);
    //Print error if any
    if(curl_errno($ch))
    {
      return 'error:' . curl_error($ch);
    }
    curl_close($ch);

    return $output;
}

function random_char($panjang)
{

    $karakter = '0123456789'; 
    $string = ''; 
    for ( $i = 0; $i < $panjang; $i++ ) { 
        $pos = rand( 0, strlen( $karakter ) - 1 ); 
        $string .= $karakter[$pos]; 
    } 
    return $string;

}

function total_nilai($data)
{

    $total = 0;
    foreach ($data as $key) {
        $total = $total + $key;
    }

    return number_format($total);

}

function kirim_pesan_wa($pesan, $no_telp){

    $key = 'a5869cafa320796f7ada95d3453e12b168235816bebff7df';
    $url='http://116.203.92.59/api/send_message';
    $data = array(
      "phone_no"=> $no_telp,
      "key"     =>$key,
      "message" =>$pesan
    );
    $data_string = json_encode($data);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string))
    );
    $res=curl_exec($ch);
    curl_close($ch);
    return $res;
    // return 'ada';
    
}

function api_twilio($request)
{
	$url = "https://api.twilio.com/2010-04-01/Accounts/ACe29b12dc0d4011601ca5e75e118de42b/Messages.json";
	$Authorization  = 'Basic QUNlMjliMTJkYzBkNDAxMTYwMWNhNWU3NWUxMThkZTQyYjo5Yzg5YzFiYzkwY2FlYWJjZTQyYzZiOTc4ZGI3ZjAxOA==';
    $headers 		= array();
    // $headers[] 		= 'Content-Type: application/json; charset=utf-8';
    $headers[] 		= 'Content-Type: application/x-www-form-urlencoded';
    // $headers[] 		= 'Accept: application/json';
    $headers[]      = '*/*';
    $headers[] 		= 'Authorization: ' . $Authorization;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    if ($request) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
    }
    $data = curl_exec($ch);

    if ($data === false) {
        echo 'Curl error: ' . curl_error($ch);
    } else {
        return $data;
    }

}

function format_hari_tanggal($waktu)
{
    $hari_array = array(
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jumat',
        'Sabtu'
    );
    $hr = date('w', strtotime($waktu));
    $hari = $hari_array[$hr];
    $tanggal = date('j', strtotime($waktu));
    $bulan_array = array(
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    );
    $bl = date('n', strtotime($waktu));
    $bulan = $bulan_array[$bl];
    $tahun = date('Y', strtotime($waktu));
    $jam = date( 'H:i:s', strtotime($waktu));
    
    //untuk menampilkan hari, tanggal bulan tahun jam
    //return "$hari, $tanggal $bulan $tahun $jam";

    //untuk menampilkan hari, tanggal bulan tahun
    return "$hari, $tanggal $bulan $tahun";
}

function tgl_indo($tanggal)
{
    $bulan = array(
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember',
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
}