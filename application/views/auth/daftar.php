<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?=base_url()?>assets/login-form-04/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/login-form-04/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/login-form-04/css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/login-form-04/css/style.css">

    <title>Register User Dunia Laundry</title>
  </head>
  <body>
  

  <div class="d-md-flex half">
    <div class="bg" style="background-image: url('<?=base_url()?>assets/plugins/images/login-register.jpg');"></div>
    <div class="contents">

      <div class="container">

        <div class="row align-items-center justify-content-center">

          <div class="col-lg-12">
            <?php
            $d_daftar = $this->session->flashdata('d_daftar');

            $nama     = !empty($d_daftar['name'])?$d_daftar['name']:null;
            $email    = !empty($d_daftar['email'])?$d_daftar['email']:null;
            $password = !empty($d_daftar['password'])?$d_daftar['password']:null;
            $mobile   = !empty($d_daftar['mobile'])?$d_daftar['mobile']:null;
            $type     = !empty($d_daftar['type'])?$d_daftar['type']:null;
            $nama_bank = !empty($d_daftar['nama_bank'])?$d_daftar['nama_bank']:null;
            $nama_rekening = !empty($d_daftar['nama_rekening'])?$d_daftar['nama_rekening']:null;
            $no_rekening = !empty($d_daftar['no_rekening'])?$d_daftar['no_rekening']:null;
            $nama_usaha = !empty($d_daftar['nama_usaha'])?$d_daftar['nama_usaha']:null;
            ?>
            
            <form class="form row" method="post" action="<?=base_url('daftar/simpan')?>" enctype="multipart/form-data">
            <?//=form_open_multipart('daftar/simpan')?>

              <div class="col-md-12 text-center h5">
                Form Daftar
                <hr>
              </div>

              <div class="form-group col-md-4">
                <label>Nama</label>
                <input type="text" name="name" class="form-control" value="<?=$nama?>" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Password</label>
                <input type="password" name="password" class="form-control" required="">
              </div>

              <div class="form-group col-md-4">
                <label>No Handphone</label>
                <input type="number" name="mobile" class="form-control" required="">
              </div>

              <div class="form-group col-md-2">
                <label>Type User</label>
                <select class="form-control" name="type" required="">
                  <?php if(!empty($type)):?>
                    <?php if($type == 1):?>
                      <option selected="" value="1">Agen</option>
                      <option value="2">Mitra</option>
                    <?php else:?>
                      <option value="1">Agen</option>
                      <option selected="" value="2">Mitra</option>
                    <?php endif?>
                  <?php else:?>
                    <option selected="" disabled="" value="">Pilih ..</option>
                    <option value="1">Agen</option>
                    <option value="2">Mitra</option>
                  <?php endif?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label>Foto Personal</label>
                <input type="file" name="foto[]" class="form-control" required="">
              </div>

              <div class="form-group col-md-3">
                <label>Foto KTP</label>
                <input type="file" name="foto[]" class="form-control" required="">
              </div>

              <div class="col-md-12 text-center h5">Domisili <hr></div>

              <div class="form-group col-md-4">
                  <label class="control-label">Provinsi</label>
                  <select class="form-control" id="pilih-provinsi" required="">
                    <option selected="" disabled="" value="">Pilih ...</option>
                  <?php if(!empty($provinsi)):?>
                      <?php foreach($provinsi as $pv): ?>
                          <option value="<?=$pv->id?>"><?=$pv->name?></option>
                      <?php endforeach ?>
                  <?php endif?>
                  </select>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label">Kota/Kabupaten</label>
                  <select class="form-control pilih-kecamatan" id="muncul" name="regional" required="">
                      <option disabled="" selected="" value="">Pilih Regional</option>
                  </select>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label">Kecamatan/Distrik</label>
                  <select class="form-control" id="muncul-kecamatan" name="distrik">
                      <option disabled="" selected="" value="">Pilih Kecamatan/Distrik</option>
                  </select>
              </div>

              <div class="form-group col-md-12">
                <label>Alamat Lengkap</label>
                <textarea class="form-control" style="height: 100px" name="address"></textarea>
              </div>

              <div class="col-md-12 text-center h5">Informasi Bank <hr></div>

              <div class="form-group col-md-4">
                <label>Nama Bank</label>
                <input type="text" name="bank_name" class="form-control" value="<?=$nama_bank?>" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Nama Rekening</label>
                <input type="text" name="account_name" class="form-control" value="<?=$nama_rekening?>" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Nomor Rekening</label>
                <input type="number" name="no_rekening" class="form-control" value="<?=$no_rekening?>" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Nama Usaha</label>
                <input type="text" name="nama_usaha" class="form-control" value="<?=$nama_usaha?>" required="">
              </div>

              <div class="form-group col-md-4">
                <label>Logo Usaha</label>
                <input type="file" name="foto[]" class="form-control" required="">
              </div>

              <div class="col-lg-1 offset-lg-9">
                <a href="<?=base_url()?>">
                  <button type="button" class="btn btn-md btn-warning">Batal</button>
                </a>
              </div>

              <div class="col-lg-1" style="margin-left: 35px">
                <button type="submit" class="btn btn-md btn-info">Daftar</button>
              </div>

            <?//=form_close()?>
            </form>

          </div>

        </div>


      </div>
    </div>

    
  </div>
    
    

<script src="<?=base_url()?>assets/login-form-04/js/jquery-3.3.1.min.js"></script>
<script src="<?=base_url()?>assets/login-form-04/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/login-form-04/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/login-form-04/js/main.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php if(!empty($d_daftar)):?>
<script type="text/javascript">
  swal("Gagal daftar", "<?=$d_daftar['pesan']?>", "error");
</script>
<?php endif?>

<script type="text/javascript">
function get_category(parent_cat_id)
{
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('admin/category/get_category'); ?>",
    data: "parent_cat_id="+parent_cat_id,
    success: function(data) 
    {
        $("#category_id").html(data);
    }
    });
}    
</script>

<script type="text/javascript">
    
    document.getElementById('pilih-provinsi').addEventListener('change', function() {
      // console.log('You selected: ', this.value);

      $.ajax({
        type: "get",
        dataType: "json",
        url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_regional",
        data: {
            id_provinsi:this.value,
            id_regencies: null
        },
        success: function (data){
            console.log(data);
            var html = '<option disabled="" selected="" value="">Pilih ...</option>';

            for (var i = data.length - 1; i >= 0; i--) {
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            $("#muncul").html(html);
            // console.log(html);

        }
      });

      
  });

  document.getElementById('muncul').addEventListener('change', function() {
    
      $.ajax({
        type: "get",
        dataType: "json",
        url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_kecamatan",
        data: {
            id_regencies:this.value,
            id_distrik: null
        },
        success: function (data){

            var html = '';

            for (var i = data.length - 1; i >= 0; i--) {
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            $("#muncul-kecamatan").html(html);

            // console.log(data);

        }
      });

      

  });
</script>


  </body>
</html>