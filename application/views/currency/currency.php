<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Currency</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                <!-- <ol class="breadcrumb">
                    <li class="active"><a class="active" href="<?php echo base_url('admin/currency/currency'); ?>">Currency</a></li>
                </ol> -->
            </div>
    </div>

         <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                    <?php
                    if($row['currency_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Add Currency</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Currency</h3>
                <?php }?>
                    <p class="text-muted m-b-30 font-13">Please fill all fields Properly </p> 

                    <form data-toggle="validator" method="post" action="<?php echo base_url('currency/submitCurrency'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label  class="control-label">Currency Name</label>
                                    <input type="hidden" name="currency_id" value="<?=@$row['currency_id']?>">
                                    <input type="" class="form-control" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' maxlength="40" name="currency_name" id="inputName1" placeholder="Enter Currency Name" value="<?=@$row['currency_name']?>" required> 
                                     <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                 <div class="form-group">
                                    <label  class="control-label">Currency Symbol</label>
                                    <input type="text" class="form-control" id="inputEmail2" placeholder="Enter Currency Symbol" name="currency_symbol" value="<?=@$row['currency_symbol']?>" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                         <div class="col-sm-6">
                                 <div class="form-group">
                                    <label  class="control-label">Currency Code</label>
                                    <input type="text" class="form-control" id="inputEmail2" placeholder="Enter Currency Code" name="currency_code" value="<?=@$row['currency_code']?>" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                          <div class="form-group">
                            <?php
                            if($row['currency_id'] !='')
                            {
                            ?>
                            <button type="submit" class="btn btn-info">Update</button>
                            <?php
                            }else{
                            ?>
                            <button type="submit" class="btn btn-info">Submit</button>
                        <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<!-- ################################################################################################################## -->
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                 <h3 class="box-title m-b-0">All Currency</h3>
                  <p class="text-muted m-b-30 font-13">You can see all currency list here</p>
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Currency Name</th>
                                <th>Currency Symbol</th>
                                <th>Code</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                 
                        <tbody>
                        <?php $i=0; foreach ($currency as $c) { $i++; ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $c->currency_name; ?></td>
                                <td><?php echo $c->currency_symbol; ?></td>
                                <td><?php echo $c->currency_code; ?></td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                        <li>
                                            <a title="Edit" href="<?=base_url()?>currency/updateCurrency/<?php echo $c->currency_id; ?>">Edit</a>
                                        </li>
                                        <li>
                                            <a title="Delete" href="<?=base_url()?>currency/deleteCurrency/<?php echo $c->currency_id; ?>">Delete</a>
                                        </li>
                                        </ul>
                                  </div>
                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        
    </div>
