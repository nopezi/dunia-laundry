


<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Currency Setting</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                <!-- <ol class="breadcrumb">
                    <li class="active"><a class="active" href="<?php echo base_url('admin/currency/currency'); ?>">Currency</a></li>
                </ol> -->
            </div>
    </div>
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                  <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                  <h3 class="box-title m-b-0">Currency Setting</h3>
                  <p class="text-muted m-b-30 font-13">You can change currency from here</p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('currency/updateCurrencySetting'); ?>" enctype="multipart/form-data">
                        <div class="row">
                         <div class="col-sm-6">
                            <div class="form-group">

                               <label for="">Currency Type</label>
                                       <select class="form-control" name="currency">
                      <?php foreach ($currency_setting as $currency_setting) { ?> 
                      <option value="<?php echo $currency_setting->currency_id; ?>"<?php if ($currency_setting->status == 1) echo ' selected="selected"'; ?>><?php echo $currency_setting->currency_name; ?> (<?php echo $currency_setting->currency_symbol; ?>)</option>
                      <?php } ?>
                    </select>
                  </div>
                         </div>

                         
                         <div class="col-sm-6"  style="margin-top: 30px;">
                            <div class="form-group">
                               <label for="">  </label>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
            
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
    </div>