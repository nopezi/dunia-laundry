<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Package</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Package</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">Assign Package</h3>
                      <p class="text-muted m-b-30 font-13">You can assign Package for Laundry Mitra</p>
                        <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a href="<?=base_url('franchise/pemilik')?>">
                                    <span> All Laundry Mitra & Agen</span>
                                </a>
                            </li>
                            <li role="presentation" >
                                <a href="<?=base_url('franchise/pemilik/detail/' . $owner->user_id); ?>" >
                                    <span>View</span>
                                </a>
                            </li>
                            <li role="presentation" >
                                <a href="<?=base_url('franchise/pemilik/payment_history/' . $owner->user_id); ?>" >
                                    <span>Payment history</span>
                                </a>
                            </li>
                            <li role="presentation" class="active">
                                <a href="<?=base_url('franchise/pemilik/owner_packages/' . $owner->user_id); ?>" aria-controls="owner_pkg" role="tab" data-toggle="tab" aria-expanded="true">
                                    <span>Owner</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="owner_pkg">
                                <div class="row">
                                    <!-- <h1>This tab is processing now</h1> -->
                                    
                                    <div class="col-sm-12">
                                        <div class="white-box">
                                            <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                                            <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                                            <?php
                                            if(@$row['id'] !='')
                                            {
                                            ?>
                                            <h3 class="box-title m-b-0">Update Package</h3>
                                            <?php
                                            }else{
                                            ?>
                                            <h3 class="box-title m-b-0">Add Package</h3>
                                            <?php
                                            }
                                            
                                            ?>
                                          <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                                            <form data-toggle="validator" method="post" action="<?=base_url('franchise/pemilik/aksi_packages')?>" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label  class="control-label">Title</label>
                                                            <input type="hidden" name="id" value="<?=@$row['id'];?>">
                                                            <input type="hidden" name="user_id" value="<?=@$owner->user_id;?>">
                                                            <select name="package_id" class="form-control" required>
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                foreach ($package as $p) 
                                                                {
                                                                    ?>
                                                                    <option value="<?=$p->package_id?>"><?=$p->title?> (<?=$p->no_of_days;?>)</option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                    
                                                <div class="form-group">
                                                    <?php 
                                                    if(@$row['id'] == '')
                                                    {
                                                    ?>
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    <?php
                                                    }else{
                                                    ?>
                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                    <?php }?>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <!-- /.row -->
                                <!-- ============================================================== -->

                                <!-- /row -->
                                <!-- <div class="row"> -->
                                    <div class="col-sm-12">
                                        <div class="white-box">
                                             <h3 class="box-title m-b-0">All Offline Package</h3>
                                              <p class="text-muted m-b-30 font-13">You can see all details of Offline Package List</p>
                                            <div class="table-responsive" style="overflow: auto;">
                                                <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No.</th>
                                                            <th>Date</th>
                                                            <th>Payment Id</th>
                                                            <th>Package Title</th>
                                                            <th>No of days</th>
                                                            <th>End subscription date</th>
                                                            <th>Amount</th>  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                <?php $i=1; foreach ($history as $b):
                                                  $pkg = $this->Payment_model->getPackageDetail($b->package_id);
                                                ?> 
                                                    
                                                    <tr>
                                                        <td><?=$i++?></td>
                                                        <td><?=date('d-m-Y h:i:s a', strtotime($b->created_at));?></td>
                                                        <td><?=$b->payment_id?></td>
                                                        <td><?=$pkg['title']?></td>
                                                        <td><?=$pkg['no_of_days']?></td>
                                                        <td><?=$b->end_subscription_date?></td>
                                                        <td><?=$b->amount?></td>
                                                        </tr>
                                                <?php endforeach ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>