
<!-- Page Content -->
<!-- ============================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Update Data Pemilik</h4> 
            </div>
        </div>

        <div class="col-md-12">
       
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <h3 class="box-title m-b-0">Update Data Pemilik</h3>
                  <p class="text-muted m-b-30 font-13">Harap isi semua bidang dengan benar. </p>
                    <form data-toggle="validator" method="post" action="<?=base_url('franchise/pemilik/aksi_update'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Nama</label>
                                    <input type="hidden" name="user_id" value="<?=@$row['user_id'];?>">
                                    <input type="text" class="form-control" name="name" value="<?=@$row['name'];?>" id="inputName1" placeholder="Masukan Nama" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Email</label>
                                    <input type="email" class="form-control" name="email" value="<?=@$row['email'];?>" id="inputName1" placeholder="Masukan Email" required> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                <!-- selain login USER FRANCHISE -->
                                <?php if($_SESSION['type'] != 3):?>
                                    <label  class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password" value="<?=@$row['password'];?>" id="inputName1" placeholder="Masukan password" required>
                                <?php else:?>
                                    <label  class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password" value="123456" id="inputName1" placeholder="Masukan password" disabled="">
                                <?php endif?> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">No Hape</label>
                                    <input type="text" class="form-control groupOfTexbox" name="mobile" value="<?=@$row['mobile'];?>" id="inputName1" placeholder="Masukan No Hape" required maxlength="16"> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Alamat</label>
                                    <input type="text" class="form-control" name="address" value="<?=@$row['address'];?>" id="inputName1" placeholder="Masukan Alamat" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Type</label>
                                    <select name="type" class="form-control" required>
                                      <option value="1" <?=@ $row['type'] == '1' ? "selected" :  "" ;?> >
                                          Agen
                                      </option>
                                      <option value="2" <?=@ $row['type'] == '2' ? "selected" :  "" ;?> >
                                          Mitra
                                      </option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Latitude</label>
                                    <input type="text" class="form-control" name="latitude" value="<?=@$row['latitude'];?>" id="inputName1" placeholder="Enter latitude" maxlength="20" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Longitude</label>
                                    <input type="text" class="form-control" name="longitude" value="<?=@$row['longitude'];?>" id="inputName1" placeholder="Enter longitude" maxlength="20" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="inputEmail2" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <? if(@$row['image'] !=''):?>
                                        <img src="<?=base_url()?>assets/images/user/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <? else:?>
                                    <? endif?>
                                </div>
                            </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Provinsi</label>
                                <select class="form-control" id="pilih-provinsi" required="">
                                <?php if(!empty($provinsi_ada->name)): ?>
                                    <option disabled="" selected="" value="<?=$provinsi_ada->id?>"><?=$provinsi_ada->name?></option>
                                <?php else:?>
                                    <option disabled="" selected="">Pilih Provinsi</option>
                                <?php endif ?>
                                <?php if(!empty($provinsi)):?>
                                    <?php foreach($provinsi as $pv): ?>
                                        <option value="<?=$pv->id?>"><?=$pv->name?></option>
                                    <?php endforeach ?>
                                <?php elseif(!empty($provinsi_ada)): ?>
                                    <?php foreach($provinsi_ada as $pv): ?>
                                        <option value="<?=$pv->id?>"><?=$pv->name?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Kota/Kabupaten</label>
                                <select class="form-control pilih-kecamatan" id="muncul" name="regional">
                                    <option disabled="" selected="">Pilih Regional</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Kecamatan/Distrik</label>
                                <select class="form-control" id="muncul-kecamatan" name="distrik">
                                    <option disabled="" selected="">Pilih Kecamatan/Distrik</option>
                                </select>
                            </div>
                        </div>

                        </div>
                            
                        <div class="form-group">
                            <a href="<?=base_url('franchise/pemilik')?>" class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
<script type="text/javascript">
function get_category(parent_cat_id)
{
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('admin/category/get_category'); ?>",
    data: "parent_cat_id="+parent_cat_id,
    success: function(data) 
    {
        $("#category_id").html(data);
    }
    });
}    
</script>

<script type="text/javascript">
    
    document.getElementById('pilih-provinsi').addEventListener('change', function() {
      // console.log('You selected: ', this.value);

      $.ajax({
        type: "get",
        dataType: "json",
        url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_regional",
        data: {
            id_provinsi:this.value,
            id_regencies: null
        },
        success: function (data){
            console.log(data);
            var html = '<option disabled="" selected="" value="">Pilih ...</option>';

            for (var i = data.length - 1; i >= 0; i--) {
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            $("#muncul").html(html);
            // console.log(html);

        }
      });

      
  });

  document.getElementById('muncul').addEventListener('change', function() {
    
      $.ajax({
        type: "get",
        dataType: "json",
        url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_kecamatan",
        data: {
            id_regencies:this.value,
            id_distrik: null
        },
        success: function (data){

            var html = '';

            for (var i = data.length - 1; i >= 0; i--) {
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            $("#muncul-kecamatan").html(html);

            // console.log(data);

        }
      });

      

  });
</script>

<?php if(!empty($regional)):?>

    <script type="text/javascript">
        
        var id_regional = <?=$regional->id?>;
        
        $.ajax({
            type: "get",
            dataType: "json",
            url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_regional",
            data: {
                id_provinsi:<?=$regional->province_id?>,
                id_regencies: id_regional
            },
            success: function (data){
                // console.log(data);
                var html = '<option selected="" value="<?=$regional->id?>"><?=$regional->name?></option>';

                for (var i = data.length - 1; i >= 0; i--) {
                    html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                $("#muncul").html(html);
                // console.log(html);

            }
          });

        <?php if (!empty($distrik)): ?>
            var id_distrik = <?=$distrik->id?>;
            var html = '<option selected="" value="<?=$distrik->id?>"><?=$distrik->name?></option>';
        <?php else:?>
            var id_distrik = null;
            var html = '<option disabled="" selected="" value="">Pilih ...</option>';
        <?php endif?>

        $.ajax({
            type: "get",
            dataType: "json",
            url: "<?=base_url()?>"+"Admin/LaundryOwnerController/data_kecamatan",
            data: {
                id_regencies:<?=$regional->id?>,
                id_distrik: id_distrik
            },
            success: function (data){

                for (var i = data.length - 1; i >= 0; i--) {
                    html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                $("#muncul-kecamatan").html(html);

            }
          });


    </script>

<?php endif?>