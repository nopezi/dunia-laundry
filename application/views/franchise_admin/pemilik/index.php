<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Pemilik</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <a href="<?=base_url('franchise/pemilik/tambah')?>" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;">
                  <i class="ti-user"> </i> Tambah Data Pemilik
                </a>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">

                <h3 class="box-title m-b-0">Data Pemilik</h3>
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Image</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>No WA/HP</th>
                              <th>Alamat</th>
                              <th>Type</th>
                              <th>Status</th>
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No WA/HP</th>
                                <th>Alamat</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Kelola</th>                           
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php if(!empty($user)):?>
                          <?php $i=1; foreach($user as $u):?> 
                              <tr>
                                  <td><?=$i++?></td>
                                  <td>
                                    <a href="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" target="_blank">
                                      <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" alt="Image not Available" />
                                    </a>
                                  </td>
                                  <td><?=$u->name; ?></td>       
                                  <td><?=$u->email; ?></td>
                                  <td><?=$u->mobile?></td>
                                  <td><?=$u->address; ?></td>
                                  <td>
                                    <?php if($u->type==1): ?>
                                      Agen
                                    <?php elseif($u->type==2): ?>
                                      Mitra
                                    <?php elseif($u->type=3):?>
                                      Franchise
                                    <?php  endif ?> 
                                  </td>
                                  <td>
                                    <?php if($u->status==1){ ?>
                                    <label class="badge badge-teal">Aktif</label>
                                    <?php }else if($u->status==0){ ?>
                                    <label class="badge badge-danger">Non Aktif</label>
                                    <?php  } ?> 
                                  </td>
                                  <td>
                                    <select class="form-control" onchange="location = this.value;">
                                      <option disabled="" selected="">Kelola</option>
                                    
                                      <option value="<?=base_url('franchise/pemilik/ganti_status/').$u->user_id?>" <?=$u->status==1?'disabled':''?>>
                                        Aktifkan
                                      </option>

                                      <option value="<?=base_url('franchise/pemilik/ganti_status/').$u->user_id?>" <?=$u->status==0?'disabled':''?>>
                                        Non Aktifkan
                                      </option>

                                      <option value="<?=base_url('franchise/pemilik/update/')?><?=$u->user_id;?>">
                                        Update
                                      </option>

                                      <option value="<?=base_url('franchise/pemilik/detail/').$u->user_id?>">
                                        Lihat Detail
                                      </option>

                                      <option value="<?=base_url('franchise/pemilik/delete/').$u->user_id?>">
                                        Hapus
                                      </option>

                                    </select>

                                  </td>
                              </tr>
                           <?php endforeach?>
                         <?php endif?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>