<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation">
   <div class="sidebar-nav slimscrollsidebar">
      <div class="sidebar-head">
         <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Dunia Laundry</span></h3>
      </div>
      <ul class="nav" id="side-menu">

        <li> 
          <a href="<?php echo base_url('franchise/home'); ?>" class="waves-effect">
            <i class="mdi mdi-av-timer fa-fw"></i><span class="hide-menu">Beranda</span>
          </a>
        </li>

         <!-- ################################################################################# -->
        <?php $ses = $_SESSION;?>
         <!-- 1 => admin , 2 => laundry owner -->
        <?php if($ses['type']=='10'):?>
           <li>
            <a href="<?php echo base_url('service'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-servicestack"></i> &nbsp;<span class="hide-menu"> Layanan</span></a>
          </li>
        <?php endif ?>

        <li class="<?php echo(isset($page) && $page == 'packages') ? 'selected': '' ?>">
            <a href="javascript:void(0);" class="waves-effect <?php echo(isset($page) && $page == 'packages') ? 'active': '' ?>">
             &nbsp;<i class="icon-settings" data-icon="v"></i> &nbsp;<span class="hide-menu">Mitra / Agen<span class="fa arrow"></span> </span>
            </a>
            <ul class="nav nav-second-level">
            
              <li>
                <a href="<?=base_url('franchise/pemilik'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-user"></i> &nbsp;<span class="hide-menu">Data Pemilik</span>
                </a>
              </li>

              <li>
                <a href="<?=base_url('franchise/outlet'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-columns"></i> &nbsp;<span class="hide-menu">Data Outlet</span></a>
              </li>

              <li>
                <a href="<?php echo base_url('franchise/agen'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-columns"></i> &nbsp;<span class="hide-menu">Data Agen</span>
                </a>
              </li>
            </ul>
        </li>

        <li>
            <a href="<?=base_url('franchise/pesanan'); ?>?status_pesanan=1" class="waves-effect"> 
              &nbsp;<i class="fas fa-cart-arrow-down"></i> &nbsp;<span class="hide-menu"> Pesanan</span>
            </a>
          </li>
        

        <li>
          <a href="<?php echo base_url('franchise/komisi'); ?>" class="waves-effect"> 
            &nbsp;<i class="fas fa-calculator"></i> &nbsp;<span class="hide-menu"> Komisi</span>
          </a>
        </li>

        <li>
          <a href="<?=base_url('franchise/profile')?>?id=<?=$ses['id']?>" class="waves-effect"> 
            &nbsp;<i class="fas fa-user"></i> &nbsp;<span class="hide-menu"> Profile</span>
          </a>
        </li>
      
      </ul>
   </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ==============================================================
