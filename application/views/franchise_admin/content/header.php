<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="5x5" href="<?php echo base_url('assets/plugins'); ?>/images/favicon.png">
    <title>Laundry</title>
    <!-- <?php echo base_url('assets/'); ?> Core CSS -->
    <link href="<?php echo base_url('assets/'); ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
    <!-- Date picker plugins css -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins'); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url('assets');?>/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Popup CSS -->
    <link href="<?php echo base_url('assets');?>/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url('assets'); ?>/css/colors/megna-dark.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/plugins/bower_components/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url('assets/plugins'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="<?php echo base_url('user/dashboard'); ?>">
                    <b>
                        <img src="<?php echo base_url('assets/plugins'); ?>/images/win_logo.png" alt="home" class="dark-logo" /><img src="<?php echo base_url('assets/plugins'); ?>/images/win_logo.png" alt="home" class="light-logo" height="60" width="160"/>
                    </b>
                    </a>
                      <!--  <img src="<?php echo base_url('assets/plugins'); ?>/images/h1.png" alt="home" class="dark-logo" /><img src="<?php echo base_url('assets/plugins'); ?>/images/h1.png" alt="home" class="light-logo" />  -->
                   <!--   </span> </a> -->
                </div>
                <!-- /Logo -->
                <?php $ses = $_SESSION;?>
                 <!-- Search input and Toggle icon -->
                 <!-- rahul -->
                <ul class="nav navbar-top-links navbar-left visible-xs-block">
                    <li><a href="javascript:void(0)" class="waves-effect waves-light navbar-toggle collapsed" style="color:#000;" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><i class="ti-menu"></i></a></li>
                </ul>
                <!-- Search input and Toggle icon -->
                
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?php echo base_url('assets/plugins'); ?>/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?=$ses['owner_name'];?></b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="<?php echo base_url('assets/plugins'); ?>/images/users/varun.jpg" alt="user" /></div>
                                    <div class="u-text">
                                        <h4><?=$ses['owner_name'];?></h4>
                                        <p class="text-muted"><?=$ses['email'];?></p>
                                        <?php
                                        if(@$ses['status']=='2')
                                        {
                                            $u = $this->db->where('user_id',$ses['id'])->get('la_user')->row();
                                        ?>
                                        <a href="javascript:void(0);" class="btn btn-rounded btn-danger btn-sm" data-toggle="modal" data-target="#myModal"><?=@$u->end_subscription_date;?></a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if($ses['status']=='2')
                            {
                                ?>
                                <li><a href="<?php echo base_url('laundryOwner/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                <?php
                            }else{
                            ?>
                            <li><a href="<?php echo base_url('logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                            <?php
                                }
                            ?>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>

                <div class="collapse navbar-collapse hidden-sm hidden-lg hidden-md" id="bs-example-navbar-collapse-1" style="width: 100%;">
                  <ul class="nav navbar-nav">
                    <?php include_once 'menu_mobile.php'; ?>
                  </ul>
                </div><!-- /.navbar-collapse -->
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->