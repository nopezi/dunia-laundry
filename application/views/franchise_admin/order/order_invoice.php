<!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Invoice Pesanan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <ol class="breadcrumb">
                            <li><a href="javascript:void(0)">Invoice</a></li>
                            <li class="active">Invoice Pesanan</li>
                        </ol>
                    </div>
                </div>
                <?php
                $user = $this->User_model->updateUser($order_detail['user_id']);
                $shop = $this->Laundry_shop_model->updateLaundryShop($order_detail['shop_id']);

                ?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box printableArea">
                            <h3><b>INVOICE</b> <span class="pull-right">#<?=$order_detail['order_id']?></span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3> &nbsp;<b class="text-danger"><?=$shop['shop_name'];?></b></h3>
                                            <p class="text-muted m-l-5"><?=$shop['address'];?></p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <h3>Kepada,</h3>
                                            <h4 class="font-bold"><?=$user['name'];?></h4>
                                            <h5 class="font-bold"><?=$user['mobile'];?></h5>
                                            <p class="text-muted m-l-30"><?=$order_detail['shipping_address'];?></p>
                                            <p class="m-t-30"><b>Tanggal Invoice :</b> <i class="fa fa-calendar"></i> <?=date('d-m-Y');?></p>
                                            <p><b>Tanggal Pesanan :</b> <i class="fa fa-calendar"></i> <?=date('d-m-Y h:i:s a', strtotime($order_detail['created_at']));?></p>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>Nama Produk</th>
                                                    <th class="text-right">Quantitas</th>
                                                    <th class="text-right">Biaya Satuan</th>
                                                    <th class="text-right">Total</th>
                                                </tr>
                                                
                                            </thead>
                                            <tbody>
                                                <?php
                                                // echo "<pre>";
                                                // print_r($order_detail);
                                                $i=0;
                                                $total = 0;
                                                $item_details = $order_detail['item_details'];
                                                $es1 = str_ireplace('"item_details":',"",$item_details);
                                                $json = json_decode($es1, true);
                                                foreach ($json as $key => $value) 
                                                {
                                                    $i++;
                                                    $total = $value['quantity'] * $value['price'];
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?=$i;?></td>
                                                    <td >
                                                        <h5><?=$value["item_name"]?></h5>
                                                        
                                                    </td>
                                                    <td class="text-right" align="center"><?=$value['quantity']?></td>
                                                    <td class="text-right"><?=$order_detail['currency_code'];?> <?= number_format( $value['price'] ); ?></td>
                                                    <td align="center" class="text-right font-500"><?=$order_detail['currency_code'];?> <?= number_format( $total ); ?></td>
                                                </tr>

                                                <?php
                                                    
                                                }
                                                ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">
                                        <p>Sub - Jumlah Total : <?=$order_detail['currency_code'];?> <?= number_format( $order_detail['price'] ); ?></p>
                                        <p>Diskon : <?=$order_detail['currency_code'];?> <?=$order_detail['discount']?> </p>
                                        <hr>
                                        <h3><b>Total :</b> <?=$order_detail['currency_code'];?> <?= number_format( $order_detail['final_price'] ); ?></h3> </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="text-right">
                                        <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Cetak</span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                <!-- /.row -->
                
            </div>
            
        </div>
        <!-- /#page-wrapper -->