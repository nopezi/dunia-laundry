<?php
$ses = $_SESSION;
?>
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <h4 class="page-title">Data Pesanan <?=$wilayah?></h4> 
        </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-5 col-sm-5 col-md-5 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Pesanan</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">

        <div class="col-md-6 col-md-offset-4">
          
          <form action="" method="get" class="row">
            <div class="form-group col-md-6">
              <!-- <label>PIlih tanggal</label> -->
              <input type="date" name="tanggal" class="form-control" value="<?=$tanggal?>">
              <input type="hidden" name="status_pesanan" value="<?=$_GET['status_pesanan']?>">
            </div>
            <div class="form-group col-md-1">
              <button type="submit" class="btn btn-info btn-sm">Cari</button>
            </div>
          </form>

        </div>

        <div class="col-sm-12">
          <?php if( $this->session->flashdata('success') ): ?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php endif; ?>

          <?php if( $this->session->flashdata('error') ): ?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('error'); ?>    
            </div>
          <?php endif; ?>
        </div>

        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified white-box text-center">
            <?php foreach($aktif as $ak):?>
              <li class="<?=$ak['active']?>">
                <a href="<?=base_url('franchise/pesanan')?>?tanggal=<?=$tanggal?>&status_pesanan=<?=$ak['id']?>"><?=$ak['name']?></a>
              </li>
            <?php endforeach?>
            </ul>
        </div>

        <div class="col-sm-12">
            <div class="table-responsive" style="overflow-x: auto;">
                <table id="example23" class="table  table table-hover  table-bordered" style="word-wrap: break-word;" cellspacing="0" width="100%">
                    <thead>
                        <tr class="white-box bg-info">
                          <th class="text-white text-center">No.</th>
                          <th class="text-white text-center">Id Pesanan</th>
                          <th class="text-white text-center">Outlet</th>
                          <th class="text-white text-center">Waktu Penjemputan</th>
                          <th class="text-white text-center">Waktu Pengiriman</th>
                          <th class="text-white text-center">Status Pesanan</th>
                          <th class="text-white text-center">Kelola Pesanan</th>
                          <th class="text-white text-center">Status Pembayaran</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="white-box bg-info">
                          <!-- <th class="text-white text-center">No.</th>
                          <th class="text-white text-center">Id Pesanan</th>
                          <th class="text-white text-center">Username</th>
                          <th class="text-white text-center">Outlet</th>
                          <th class="text-white text-center">No WA/HP</th>
                          <th class="text-white text-center">Harga</th>
                          <th class="text-white text-center">Diskon</th>
                          <th class="text-white text-center">Total Harga</th>
                          <th class="text-white text-center">Email</th>
                          <th class="text-white text-center">Waktu Penjemputan</th>
                          <th class="text-white text-center">Waktu Pengiriman</th>
                          <th class="text-white text-center">Status Pesanan</th>
                          <th class="text-white text-center">Kelola Pesanan</th>
                          <th class="text-white text-center">Status Pembayaran</th> -->
                          <th class="text-white text-center">No.</th>
                          <th class="text-white text-center">Id Pesanan</th>
                          <th class="text-white text-center">Outlet</th>
                          <th class="text-white text-center">Waktu Penjemputan</th>
                          <th class="text-white text-center">Waktu Pengiriman</th>
                          <th class="text-white text-center">Status Pesanan</th>
                          <th class="text-white text-center">Kelola Pesanan</th>
                          <th class="text-white text-center">Status Pembayaran</th>
                        
                        </tr>
                    </tfoot>
                    <tbody class="white-box">
                    <?php 
                    $i=0; 
                    foreach($order as $u): 
                      $i++; 
                      $user = $this->Order_model->getUserDetail($u->user_id);
                    ?> 
                        <tr>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td class="text-center"><?=$u->order_id?></td>
                            <!-- <td class="text-white text-center"><?php echo $user['name']; ?></td>      -->
                            <td class="text-center"><?php echo $this->Order_model->getLaundryShopName($u->shop_id); ?></td>
                            <!-- <td class="text-center"><?=$user['mobile'];?></td> -->
                            <!-- <td class="text-center"><?= $u->currency_code ?> <?=number_format( $u->price )?></td> -->
                            <!-- <td class="text-center"><?= $u->currency_code ?> <?=number_format( $u->discount )?></td> -->
                            <!-- <td class="text-center"><?= $u->currency_code ?> <?=number_format( $u->final_price )?></td> -->
                            <!-- <td class="text-center"><?=$user['email'];?></td> -->
                            <td class="text-center"><?= date( 'd F Y', strtotime( $u->pickup_date ) ) . ' ' . $u->pickup_time ?></td>
                            <td class="text-center">
                              <?= date( 'd F Y', strtotime( $u->delivery_date ) ) . ' ' . $u->delivery_time ?>
                            </td>
                            <td class="text-center">
                              <?php if($u->order_status==0){ ?>
                              <label class="badge badge-default">Menunggu <br> Konfirmasi</label>
                              <?php }else if($u->order_status==1){ ?>
                              <label class="badge badge-warning">Pesanan <br> baru</label>
                              <?php }else if($u->order_status==2){ ?>
                              <label class="badge badge-warning">Siap <br> diambil</label>
                              <?php }else if($u->order_status==3){ ?>
                              <label class="badge badge-info">Sedang <br> diproses</label>
                              <?php }else if($u->order_status==4){ ?>
                              <label class="badge badge-info">Siap <br> diantar</label>
                              <?php }else if($u->order_status==5){ ?>
                              <label class="badge badge-success">Proses <br> Jemput</label>
                              <?php  } else if($u->order_status==6){ ?>
                              <label class="badge badge-danger">Dibatalkan</label>
                              <?php  } ?>

                              <br/>
                              <br/>
                              <input type="hidden" name="order_id" id="order_id<?=$i;?>" value="<?=$u->order_id?>">
                                  <select class="btn btn-sm btn-info dropdown-toggle" id="order_status<?=$i;?>" onchange="changeOrderStatus(<?=$i;?>);" name="order_status">
                                    <option value="0" <?=$u->order_status==0?'selected':''?>>Pending</option>
                                    <option value="1" <?=$u->order_status==1?'selected':''?>>Confirmed</option>
                                    <!-- <option value="2" <?=$u->order_status==2?'selected':''?>>Picked up</option> -->
                                    <!-- <option value="3" <?=$u->order_status==3?'selected':''?>>In progress</option> -->
                                    <!-- <option value="4" <?=$u->order_status==4?'selected':''?>>Shipped</option> -->
                                    <option value="5" <?=$u->order_status==5?'selected':''?>>Delivered</option>
                                    <!-- <option value="5" <?=$u->order_status==6?'selected':''?>>Cancel</option> -->
                                  </select>
                            </td>
                            <td class="text-center col-1">
                                <a type="button" class="btn btn-sm btn-primary" href="<?=base_url('order/orderDetail');?>/<?=$u->order_id;?>">
                                  <i class="fas fa-eye"></i> 
                                </a>
                                <br/>
                                <br/>
                                <a type="button" class="btn btn-sm btn-warning" href="<?=base_url('pesanan/orderEdit');?>/<?=$u->order_id;?>">
                                  <i class="fas fa-edit"></i>
                                </a>
                                <br/>
                                <br/>
                                <a type="button" class="btn btn-sm btn-success" href="<?=base_url('order/orderInvoice');?>/<?=$u->order_id;?>">Invoice</a>
                            </td>
                            <td class="text-center">
                              <?php if($u->payment_status==0){ ?>
                              <label class="badge badge-danger">Pending</label>
                              <?php }else if($u->payment_status==1){ ?>
                              <label class="badge badge-success">Complete</label>
                              <?php  } ?> 
                            </td>
                          
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script type="text/javascript">
function changeOrderStatus(sno)
{
  var order_id = $("#order_id"+sno).val();
  var order_status = $("#order_status"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>