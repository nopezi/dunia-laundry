<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Webservice</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Webservice</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    
                        <h3 class="box-title m-b-0">Web Service List</h3>
                    
                  <p class="text-muted m-b-30 font-13">Web Services list with url and descriptions</p>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h4 >Live: <font color="#008b8b">http://phpstack-390430-1228229.cloudwaysapps.com/</font></h4>
                                     
                                </div>
                                <div class="form-group">
                                    <h4 >01) App User Signup (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/signup' <br>
                                        --form 'email=test1@gmail.com' \<br>
                                        --form 'name=Test' \<br>
                                        --form 'country_code=91' \<br>
                                        --form 'mobile=965874586' \<br>
                                        --form 'password=123456' \<br>
                                        --form 'device_type=ios' \<br>
                                        --form 'device_token=4d65f4d6f4sd65f4sd64f'</font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >02) App User ForgotPassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/forgotPassword' <br>
                                        --form 'email=rahulrajpoot14357@gmail.com'
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >03) App User VerifyOtp (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/verifyOtp' <br>
                                        --form 'email=test@gmail.com' \ <br>
                                        --form 'email_token=743302' \<br>
                                        --form 'password=12345'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >04) App User login (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/login' <br>
                                        --form 'email=test@gmail.com' \<br>
                                        --form 'password=12345' \<br>
                                        --form 'device_type=ios' \<br>
                                        --form 'device_token=dfsdfsdf56456sf'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >05) App User sendOtp (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/sendOtp' <br>
                                        --form 'email=rahulrajpoot14357@gmail.com' \<br>
                                        --form 'otp=1212'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >06) App User logOut (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/logOut' <br>
                                        --form 'user_id=144078'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >07) App User deleteAccount (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/deleteAccount' <br>
                                        --form 'user_id=144078'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >08) App User changePassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/changePassword' <br>
                                        --form 'user_id=144078' \<br>
                                        --form 'password=12345' \<br>
                                        --form 'new_password=111111'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >09) App User setMessage (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/setMessage' <br>
                                        --form 'from_user_id=PQ557a' \<br>
                                        --form 'to_user_id=zAd1d3' \<br>
                                        --form 'message=hello' \<br>
                                        --form 'media=@/path/to/file'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >10) App User getMessage (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getMessage' <br>
                                        --form 'user_id=PQ557a' \<br>
                                        --form 'to_user_id=zAd1d3'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >11) App User getMessageHistory (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getMessageHistory' <br>
                                        --form 'user_id=144078'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >12) App User addTiket (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/addTiket' <br>
                                        --form 'user_id=550247' \<br>
                                        --form 'title=Problem' \<br>
                                        --form 'description=I am facing some problem'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >13) App User tiketList (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/tiketList' <br>
                                        --form 'user_id=abbed1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >14) App User addTiketComment (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/addTiketComment' <br>
                                        --form 'tiket_id=2' \<br>
                                        --form 'message=Ok sir'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >15) App User getTiketComment (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getTiketComment' <br>
                                        --form 'tiket_id=1'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >16) App User addRating (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/addRating' <br>
                                        --form 'user_id=wxda3f' \<br>
                                        --form 'shop_id=YZf0f6' \<br>
                                        --form 'rating=4'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >17) App User getRating (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getRating' <br>
                                        --form 'user_id=abbed1' \<br>
                                        --form 'shop_id=YZf0f6'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >18) Get All Services (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getAllService' <br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >19) App User Rating (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/Rating' <br>
                                        --form 'user_id=wxda3f' \<br>
                                        --form 'shop_id=YZf0f6' \<br>
                                        --form 'rating=4'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >20) Get All Laundry Agen (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getAllLaundryShop' <br>
                                        --form 'count=2' \<br>
                                        --form 'latitude=22.7375' \<br>
                                        --form 'longitude=75.8699'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >21) Get All Offer (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getAllOffer' <br>
                                        --form 'count=3'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >22) Get Offer For Laundry Agen (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getOfferForLaundryShop' <br>
                                        --form 'shop_id=YZf0f6'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >23) Get Advertisement (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getAdvertisement' <br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >24) User Order Submit (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/orderSubmit' <br>
                                        --form 'order_id=8659452647' \<br>
                                        --form 'user_id=ABf291' \<br>
                                        --form 'shop_id=YZf0f6' \<br>
                                        --form 'price=200' \<br>
                                        --form 'discount=10' \<br>
                                        --form 'final_price=190' \<br>
                                        --form 'currency_code=$' \<br>
                                        --form 'item_details="item_details":[{"item_id":"123","item_name":"abcd","price":"10","image":"","currency_code":""},{"item_id":"456","item_name":"xyz","price":"20","image":"","currency_code":""}]' \<br>
                                        --form 'shipping_address=vijay nagar Indore,' \<br>
                                        --form 'landmark=rasoma' \<br>
                                        --form 'latitude=223.333' \<br>
                                        --form 'longitude=333.444' \<br>
                                        --form 'pickup_date=2020-04-23' \<br>
                                        --form 'pickup_time=10:00 am' \<br>
                                        --form 'delivery_date=2020-04-25' \<br>
                                        --form 'delivery_time=06:00 pm'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >25) Get Currency (GET WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getCurrency' <br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >26) User Shop Search (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/search' <br>
                                        --form 'shop_name=laundry'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >27) Get Home Data (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getHomeData' <br>
                                        --form 'latitude=22.7375' \<br>
                                        --form 'longitude=75.8759' \<br>
                                        --form 'user_id=vw1628'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >28) Get Item by ShopId (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getItembyShopId' <br>
                                        --form 'shop_id=zAd1d3'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >29) Get Shop Services (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getShopServices' <br>
                                        --form 'shop_id=zAd1d3'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >30) User Order Cancel (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/orderCancel' <br>
                                        --form 'order_id=5552985419'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >31) User apply Promocode (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/applyPromocode' <br>
                                        --form 'promocode=6A7ZT1' \<br>
                                        --form 'shop_id=zAd1d3' \<br>
                                        --form 'total_price=50'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >32) User Update (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/userUpdate' <br>
                                        --form 'user_id=144078' \<br>
                                        --form 'name=Testing' \<br>
                                        --form 'country_code=91' \<br>
                                        --form 'mobile=9999999999' \<br>
                                        --form 'address=indore' \<br>
                                        --form 'latitude=93.656' \<br>
                                        --form 'longitude=93.956' \<br>
                                        --form 'image=@/home/samyotech/Pictures/allu.jpeg'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >33) Get Win Laundry By Service (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getLaundryByService' <br>
                                        --form 'latitude=22.7375' \<br>
                                        --form 'longitude=75.8699' \<br>
                                        --form 'service_id=536opk'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >34) Get Booking List (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/getBookingList' <br>
                                        --form 'user_id=PQ557a'<br>
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >35) Get User Notification (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/get_notification' <br>
                                        --form 'user_id=550247'<br>
                                        </font>
                                    </p>
                                </div>

                            </div>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label  class="control-label">Description</label>
                                    <textarea class="form-control" name="description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div> -->
                            
                        </div>
                            
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>