
<!-- Page Content -->
<!-- ============================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User</h4> </div>
            <!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Laundry agen</li>
                </ol>
            </div> -->
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
       
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <?php if( $this->session->flashdata('success') ): ?>
                        <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
                      <?php endif; ?>

                      <?php if( $this->session->flashdata('error') ): ?>
                        <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
                      <?php endif; ?>
              
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['user_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update User</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add User</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('user/submitUser'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Name</label>
                                    <input type="hidden" name="user_id" value="<?=@$row['user_id'];?>">
                                    <input type="text" class="form-control" name="name" value="<?=@$row['name'];?>" id="inputName1" placeholder="Enter name" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Email</label>
                                    <input type="email" class="form-control" name="email" value="<?=@$row['email'];?>" id="inputName1" placeholder="Enter Email" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Password</label>
                                    <input type="text" class="form-control" name="password" value="<?=@$row['password'];?>" id="inputName1" placeholder="Enter password" required> 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Country Code</label>
                                    <select name="country_code" class="form-control" required>
                                        <option value="">Select</option>
                                        <?php
                                        foreach($count_code as $l)
                                        {
                                            $selected = '';
                                            if(@$row['country_code'] == $l->phonecode)
                                            {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?=$l->phonecode?>" <?=$selected?>><?=$l->phonecode?> (<?=$l->name?>)</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Mobile</label>
                                    <input type="text" class="form-control groupOfTexbox" name="mobile" value="<?=@$row['mobile'];?>" id="inputName1" placeholder="Enter mobile" required maxlength="16"> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Address</label>
                                    <input type="text" class="form-control" name="address" value="<?=@$row['address'];?>" id="inputName1" placeholder="Enter address" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Type</label>
                                    <select name="type" class="form-control" required>
                                        <option value="0" <?=@ $row['type'] == '0' ? "selected" :  "" ;?> >Pelanggan</option>
                                        <option value="1" <?=@ $row['type'] == '1' ? "selected" :  "" ;?> >Agen</option>
                                        <option value="2" <?=@ $row['type'] == '2' ? "selected" :  "" ;?> >Mitra</option>
                                        <!-- <option value="2" <?=@ $row['type'] == '3' ? "selected" :  "" ;?> >Franchise</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Latitude</label>
                                    <input type="text" class="form-control" name="latitude" value="<?=@$row['latitude'];?>" id="inputName1" placeholder="Enter latitude" maxlength="20" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Longitude</label>
                                    <input type="text" class="form-control" name="longitude" value="<?=@$row['longitude'];?>" id="inputName1" placeholder="Enter longitude" maxlength="20" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="inputEmail2" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/user/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['user_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
<script type="text/javascript">
function get_category(parent_cat_id)
{
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('admin/category/get_category'); ?>",
    data: "parent_cat_id="+parent_cat_id,
    success: function(data) 
    {
        $("#category_id").html(data);
    }
    });
}    
</script>