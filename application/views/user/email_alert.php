<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Email alert</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Email alert</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    
                <h3 class="box-title m-b-0">Email alert List</h3>
                    
                  <p class="text-muted m-b-30 font-13">Email alert list with url and descriptions</p>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h4 >Live: <font color="#008b8b">http://phpstack-390430-1228229.cloudwaysapps.com/</font></h4>
                                     
                                </div>
                                <div class="form-group">
                                    <h4 >01) User Signup (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/signup' <br>
                                        --form 'email=test1@gmail.com' \<br>
                                        --form 'name=Test' \<br>
                                        --form 'country_code=91' \<br>
                                        --form 'mobile=965874586' \<br>
                                        --form 'password=123456' \<br>
                                        --form 'device_type=ios' \<br>
                                        --form 'device_token=4d65f4d6f4sd65f4sd64f'</font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >02) User ForgotPassword (POST WEB SERVICE) </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/api/forgotPassword' <br>
                                        --form 'email=rahulrajpoot14357@gmail.com'
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >03) Assign Package for admin pannel </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/laundry/ownerPackages/userId' <br>
                                        
                                        </font>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <h4 >04) Assign Package for laundry owner pannel </h4>
                                    <p>
                                        Live Url <font color="#008b8b">'http://phpstack-390430-1228229.cloudwaysapps.com/admin/Subscription' <br>
                                        
                                        </font>
                                    </p>
                                </div>
                                

                            </div>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label  class="control-label">Description</label>
                                    <textarea class="form-control" name="description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div> -->
                            
                        </div>
                            
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>