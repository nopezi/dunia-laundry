<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Pengguna</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                <a href="user/add" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;"><i class="ti-user"> </i> Tambah Pengguna
                </a>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">Data Pengguna</h3>
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>Ho WA/Hape</th>
                              <th>Photo</th>
                              <th>Type</th>
                              <th>Status</th>
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Ho WA/Hape</th>
                                <th>Photo</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Kelola</th>                    
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php $i=0; foreach($user as $u) { $i++; ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $u->name; ?></td>       
                                <td><?php echo $u->email; ?></td>
                                <td><?=$u->mobile?></td>
                                <td>
                                  <?php
                                  if($u->image !='')
                                  {
                                    ?>
                                    <a href="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" target="_blank">
                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" alt="Image not Available" />
                                  </a>
                                    <?php
                                  }else{
                                  ?>
                                  <a href="<?= base_url(); ?>assets/images/user/default.png" target="_blank">
                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/default.png" alt="Image not Available" />
                                  </a>
                                  <?php
                                  }
                                  ?>
                                </td>
                                <td>
                                  <?php if($u->type==0){ ?>
                                    Pelanggan
                                  <?php }else if($u->type==1){ ?>
                                    Agen
                                  <?php }else if($u->type==2){ ?>
                                    Mitra
                                  <?php  } ?> 
                                </td>
                                <td>
                                  <?php if($u->status==1){ ?>
                                  <label class="badge badge-teal">Aktif</label>
                                  <?php }else if($u->status==0){ ?>
                                  <label class="badge badge-danger">Non Aktif</label>
                                  <?php  } ?> 
                                </td>
                                <td class="text-center">
                                  <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Kelola<span class="caret"></span></button>
                                  <ul class="dropdown-menu">
                                  <li>
                                    <a title="Verified" class="<?=$u->status==1?'disabled':''?>" href="<?php echo base_url('user/changeUserStatus');?>/<?php echo $u->user_id;?>">Aktifkan</a>
                                  </li>
                                  <li>
                                    <a title="Not Verified" class="<?=$u->status==0?'disabled':''?>" href="<?php echo base_url('user/changeUserStatus');?>/<?php echo $u->user_id;?>" >Non Aktifkan</a>
                                  </li>
                                  <li>
                                    <a title="Update" href="<?php echo base_url('user/updateUser');?>/<?php echo $u->user_id;?>" >Update</a>
                                  </li>
                                  </ul>
                                  </div>
                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>