<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Pemilik</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                <a href="laundry/laundryOwnerAdd" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;"><i class="ti-user"> </i> Tambah Data Pemilik
                </a>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>

                <h3 class="box-title m-b-0">Data Pemilik</h3>
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Image</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>No WA/HP</th>
                              <th>Alamat</th>
                              <th>Type</th>
                              <th>Status</th>
                              <!-- <th>Subscribe/Unsubscribe</th>
                              <th>End subscription date</th> -->
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No WA/HP</th>
                                <th>Alamat</th>
                                <th>Type</th>
                                <th>Status</th>
                                <!-- <th>Subscribe/Unsubscribe</th>
                                <th>End subscription date</th> -->
                                <th>Kelola</th>                           
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php $i=0; foreach($user as $u) 
                        { 
                          $i++; 

                          ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                  <a href="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" target="_blank">
                                  <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" alt="Image not Available" />
                                </a>
                                </td>
                                <td><?php echo $u->name; ?></td>       
                                <td><?php echo $u->email; ?></td>
                                <td><?=$u->mobile?></td>
                                <td><?php echo $u->address; ?></td>
                                <td>
                                  <?php if($u->type==1): ?>
                                    Agen
                                  <?php elseif($u->type==2): ?>
                                    Mitra
                                  <?php elseif($u->type=3):?>
                                    Franchise
                                  <?php  endif ?> 
                                </td>
                                <td>
                                  <?php if($u->status==1){ ?>
                                  <label class="badge badge-teal">Aktif</label>
                                  <?php }else if($u->status==0){ ?>
                                  <label class="badge badge-danger">Non Aktif</label>
                                  <?php  } ?> 
                                </td>
                                <!-- <td>
                                  <?php if($u->end_subscription_date !=''){ ?>
                                  <label class="badge badge-teal">Subscribed</label>
                                  <?php }else { ?>
                                  <label class="badge badge-danger">Unsubscribed</label>
                                  <?php  } ?> 
                                </td>
                                <td><?=$u->end_subscription_date?></td> -->
                                <td class="text-center">

                                  <!-- <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Kelola<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li>
                                      <a title="Verified" class="<?=$u->status==1?'disabled':''?>" href="<?php echo base_url('laundry/changeLaundryOwnerStatus');?>/<?php echo $u->user_id;?>">Aktifkan</a>
                                    </li>
                                    <li>
                                      <a title="Not Verified" class="<?=$u->status==0?'disabled':''?>" href="<?php echo base_url('laundry/changeLaundryOwnerStatus');?>/<?php echo $u->user_id;?>" >Non Aktifkan</a>
                                    </li>
                                    <li>
                                      <a title="Update" href="<?php echo base_url('laundry/updateLaundryOwner');?>/<?php echo $u->user_id;?>" >Update</a>
                                    </li>
                                    <li>
                                      <a title="View Detail" href="<?php echo base_url('laundry/viewOwnerDetail');?>/<?php echo $u->user_id;?>" >Lihat Detail</a> -->
                                      <!-- <a title="Payment history" href="<?php echo base_url('laundry/paymentHistory');?>/<?php echo $u->user_id;?>" >Payment history</a> -->
                                    <!-- </li>
                                    <li>
                                      <a title="Delete" onClick="return confirm('Apakah anda yakin menghapus data ini ?')" href="<?php echo base_url('laundry/deleteLaundryOwner');?>/<?php echo $u->user_id;?>" >Hapus</a>
                                    </li>
                                    </ul>
                                  </div> -->

                                  <select class="form-control" onchange="location = this.value;">
                                    <option disabled="" selected="">Kelola</option>
                                  
                                    <option value="<?=base_url('laundry/changeLaundryOwnerStatus');?>/<?=$u->user_id;?>" <?=$u->status==1?'disabled':''?>>
                                      Aktifkan
                                    </option>

                                    <option value="<?=base_url('laundry/changeLaundryOwnerStatus');?>/<?=$u->user_id;?>" <?=$u->status==0?'disabled':''?>>
                                      Non Aktifkan
                                    </option>

                                    <option value="<?=base_url('laundry/updateLaundryOwner');?>/<?=$u->user_id;?>">
                                      Update
                                    </option>

                                    <option value="<?=base_url('laundry/viewOwnerDetail');?>/<?=$u->user_id;?>">
                                      Lihat Detail
                                    </option>

                                    <option value="<?=base_url('laundry/deleteLaundryOwner');?>/<?=$u->user_id;?>">
                                      Hapus
                                    </option>

                                  </select>

                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>