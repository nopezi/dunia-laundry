<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/plugins/'); ?>images/1.jpg">
<title>Laundry</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/'); ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url('assets/'); ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets/'); ?>css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url('assets/'); ?>css/colors/default.css" id="theme"  rel="stylesheet">
<link href="<?php echo base_url('assets/plugins'); ?>/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Preloader -->
<div class="preloader">
<div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
  <div class="lg-info-panel">
    <div class="inner-panel">
        <a href="javascript:void(0)" class="p-20 di"><img src="<?php echo base_url('assets/plugins/'); ?>images/1.png" width="50" height="50"></a>
  <!--       <div class="lg-content">
            <h2>E-COMMERCE.COM<br>E-commerce System</h2> -->
           <!--  <p class="text-muted">Efforts Made To Maintain or Restore Physical, Mental, or Emotional well-being especially By Trained and Licensed Professionals.</p> -->
          <!--   <a href="javascript:void(0)" class="btn btn-rounded btn-danger p-l-20 p-r-20"> Visit Site</a> -->
        </div>
    </div>
  </div>
  <div class="new-login-box">
    <div class="white-box">
        <h3 class="box-title m-b-0">Sign In to Laundry Mitra / Franchise</h3>
        <small>Enter your details below</small>
         <?php if ($this->session->flashdata('error')) { ?>
          <p id="t_msg" class="t_msg_login"></p>
         <?php } ?>
      <form class="form-horizontal new-lg-form" id="loginform" method="post" action="<?php echo base_url('laundryOwner/log_in'); ?>">
        <div class="form-group  m-t-20">
          <div class="col-xs-12">
            <label>Email Address</label>
            <input class="form-control" type="text" name="email" required="" placeholder="Username">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <label>Password</label>
            <input class="form-control" type="password" name="password" required="" placeholder="Password">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-3" style="margin-bottom: 25px;margin-right: 23px">
            <a href="<?=base_url()?>" class="label label-info" style="padding: 12px 18px 12px;">Login Admin</a>
          </div>
          <div class="col-md-5" style="margin-bottom: 25px;margin-right: 5px">
            <a href="#" class="label label-default" style="padding: 12px 12px 12px;">Login Mitra/Franchise</a>
          </div>
          <div class="col-md-2">
            <a href="<?=base_url('daftar')?>" class="label label-success" style="padding: 12px 18px 12px;">Daftar</a>
          </div>
        </div>
      </form>
    </div>
  </div>            
</section>
<!-- jQuery -->
<script src="<?php echo base_url('assets/'); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('assets/'); ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url('assets/plugins/'); ?>bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url('assets/'); ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url('assets/'); ?>js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url('assets/'); ?>js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url('assets/plugins'); ?>/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
 <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/toast-master/js/jquery.toast.js"></script>
 <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/toast-master/js/jquery.toast.js"></script>
    <!--Style Switcher -->
<script src="<?php echo base_url('assets/plugins'); ?>/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    "use strict";
     // toat popup js
    if ($('#t_msg').hasClass('t_msg_login')){
     $.toast({
         heading: 'Invalid Authontication',
         // text: 'User name or Password Invalid.',
         text: '<?=$this->session->flashdata('error')?>',
         position: 'top-right',
         loaderBg: '#fff',
         icon: 'warning',
         hideAfter: 3500,
         loaderBg:'red',
         stack: 6
     })
   }
  }); 
</script>
</body>
</html>
