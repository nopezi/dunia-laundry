<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Owner Detail</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Owner Detail</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    
                    <h3 class="box-title m-b-0">Owner Detail</h3>
                    <p class="text-muted m-b-30 font-13">
                        <b><?php echo $owner->name; ?></b>
                    </p>
                    <hr/>
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a href="<?=base_url('admin/pemilik')?>">
                                    <span> All Laundry Mitra & Agen</span>
                                </a>
                            </li>
                            <li role="presentation" class="active">
                                <a href="<?=base_url('admin/pemilik/detail/' . $owner->user_id); ?>" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">
                                    <span>View</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="<?=base_url('admin/pemilik/payment_history/' . $owner->user_id); ?>">
                                    <span>Payment history</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="<?=base_url('admin/pemilik/owner_packages/' . $owner->user_id); ?>">
                                    <span>Owner</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <?php 
                                if($owner->image!=""){
                                    $img = base_url('assets/images/user/').$owner->image;
                                } else {
                                    $img = base_url('assets/images/default.png');
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <div class="" style="padding: 25px;">
                                            <div class="user-bg">
                                                <div class="overlay-box">
                                                    <div class="user-content" style="margin-top: 0px;">
                                                        <a href="javascript:void(0)">
                                                            <img src="<?php echo $img; ?>" class="thumb-lg img-circle" alt="img">
                                                        </a>
                                                        <h4 class="text-white">
                                                        <?php echo $owner->name; ?>
                                                        </h4>
                                                        <h5 class="text-white"><?php echo $owner->email; ?></h5><br/>
                                                        <?php if($owner->status==1){ ?>
                                                        <label class="badge badge-success">Active</label>
                                                        <?php }else if($owner->status==0){ ?>
                                                        <label class="badge badge-danger">Deactive</label>
                                                        <?php  } ?> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Owner Id</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->user_id; ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Name</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->name; ?></p>
                                        </div>
                                        
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->email; ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Password</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->password; ?></p>
                                        </div>
                                        
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile No.</strong>
                                            <br>
                                            <p class="text-muted">(<?=$owner->country_code?>) <?php echo $owner->mobile; ?></p>
                                        </div>
                                        
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>End subscription date</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->end_subscription_date; ?></p>
                                        </div>
                                        
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>Address</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $owner->address; ?></p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>       