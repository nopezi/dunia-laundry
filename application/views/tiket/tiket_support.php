<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Ticket</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Ticket Support</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">All Ticket</h3>
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>S. No.</th>
                              <th>RECENT TICKET</th>
                              <th>CHANGE STATUS</th>
                              <th>VIEW</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>S.No.</th>
                                <th>RECENT TICKET</th>
                                <th>CHANGE STATUS</th> 
                                <th>VIEW</th>                          
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php 
                        $i=0; foreach($tiket as $u) 
                        { 
                          $i++;
                          $user = $this->Order_model->getUserDetail($u->user_id);
                          $tiket_detail = $this->Tiket_model->getLastTiket($u->tiket_id);
                          ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                  <!-- <div class="comment-center p-t-10"> -->
                                      <div class="comment-body">
                                          <div class="user-img">
                                          <?php
                                          if($user['image'] !='')
                                          {
                                            ?>
                                            <img src="<?=base_url();?>assets/images/user/<?=$user['image'];?>" alt="user" class="img-circle" height="45">
                                            <?php
                                          }else{
                                          ?> 
                                            <img src="<?=base_url();?>assets/images/user/default.png" alt="user" class="img-circle" height="45">
                                            <?php } ?>
                                          </div>
                                          <div class="mail-contnet">
                                              <h5><?=$user['name'];?></h5>
                                              <!-- <span class="time">10:20 AM   20  may 2016</span>  -->
                                              <?php if($u->status==0){ ?>
                                              <span class="label label-rouded label-info pull-right">PENDING</span>
                                              <?php }else if($u->status==1){ ?>
                                              <span class="label label-warning label-rouded pull-right">INPROCESS</span>
                                              <?php }else if($u->status==2){ ?>
                                              <span class="label label-success label-rouded pull-right">COMPLETE</span>
                                              <?php }else if($u->status==3){ ?>
                                              <span class="label label-danger label-rouded pull-right">REJECTED</span>
                                              <?php }?>
                                              <h5><b>Title:-</b> <?=$u->title;?></h5>
                                              <span class="mail-desc"><?//=$tiket_detail['message'];?><?=$u->description;?> </span> </br>
                                              <input type="hidden" id="tiket_id<?=$i;?>" value="<?=$u->tiket_id?>">
                                              <?php if($u->status==0){ ?>
                                              <a href="#" class="btn btn btn-rounded btn-default btn-outline m-r-4" onclick="tiketApprve(1,<?=$i;?>);"><i class="ti-check text-success m-r-5"></i>Approve</a>
                                              <a href="#" class="btn-rounded btn btn-default btn-outline" onclick="tiketReject(3,<?=$i;?>);"><i class="ti-close text-danger m-r-5"></i> Reject</a> 
                                            <?php }?>
                                            </div>
                                      </div>
                                     
                                  <!-- </div> -->
                                </td>
                                <td class="text-center">
                                  <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li>
                                      <a title="Inprocess" class="<?=$u->status==1?'disabled':''?>" href="#" onclick="changeTiketStatus(1,<?=$i;?>);">Inprocess</a>
                                    </li>
                                    <li>
                                      <a title="Complete" class="<?=$u->status==2?'disabled':''?>" href="#" onclick="changeTiketStatus(2,<?=$i;?>);">Complete</a>
                                    </li>
                                    <li>
                                      <a title="Rejected" class="<?=$u->status==3?'disabled':''?>" href="#" onclick="changeTiketStatus(3,<?=$i;?>);">Rejected</a>
                                    </li>
                                    
                                    </ul>
                                  </div>
                                </td>
                                <td>
                                  <div class="white-box">
                                  <button class="btn dropdown-toggle" type="button" data-toggle="modal" data-target="#myModal<?php echo $i; ?>" class="model_img img-responsive" >Message</button>
                                    <div id="myModal<?php echo $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="panel panel-themecolor">
                                                <div class="modal-header" style="background-color: #2cabe3;">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                  <h3 class="modal-title" id="myModalLabel">Ticket Message</h3> 
                                                </div>
                                                <div class="panel-wrapper collapse in" aria-expanded="true" role="dialog">
                                                    <div class="panel-body">
                                                        <div class="chat-box" style="height: 300px;">
                                                            <ul class="chat-list slimscroll" style="overflow: hidden;" tabindex="5005">
                                                              <?php
                                                              $tiket_data = $this->Tiket_model->getAllTiketData($u->tiket_id);
                                                              foreach ($tiket_data as $td) 
                                                              {
                                                               // echo "<pre>";
                                                               // print_r($td);
                                                                if($td->is_admin == '0')
                                                                {

                                                              ?>
                                                                <li>
                                                                  <div class="chat-image"> <img src="<?=base_url();?>assets/images/user/<?=$user['image'];?>" alt="user" class="img-circle" height="30"> </div>
                                                                    <div class="chat-body">
                                                                        <div class="chat-text">
                                                                            <h4><?=$user['name'];?></h4>
                                                                            <p><?=$td->message;?></p> <b><?=date('h:i:s a d/m/Y', strtotime($td->created_at));?></b> </div>
                                                                    </div>
                                                                </li>
                                                                <?php
                                                                  } else{
                                                                ?>
                                                                <li class="odd">
                                                                    <div class="chat-body">
                                                                        <div class="chat-text">
                                                                            <h4>Admin</h4>
                                                                            <p> <?=$td->message;?> </p> <b>1<?=date('h:i:s a d/m/Y', strtotime($td->created_at));?></b> </div>
                                                                    </div>
                                                                </li>
                                                                <?php
                                                                } 
                                                              }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                                <!-- <input type="hidden" id="new_tiket_id<?=$i;?>" value="<?=$u->tiket_id?>"> -->
                                                                <textarea placeholder="Type your message here" name="message" id="message<?php echo $i; ?>" class="chat-box-input"></textarea>
                                                            </div>
                                                            <div class="col-xs-4 text-right">
                                                                <button class="btn btn-success btn-circle btn-xl" type="button" onclick="setAdminMsg(<?=$u->tiket_id?>,<?php echo $i; ?>);"><i class="fas fa-paper-plane"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      <!-- /.modal-dialog -->
                                  </div>
                                </div>
                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
.chat-list li {
    margin-bottom: 24px;
    /* overflow: auto; */
}
</style>
<script type="text/javascript">
function changeTiketStatus(status,sno)
{
  var tiket_id = $("#tiket_id"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('tiket/changeTiketStatus'); ?>",
    data: "status="+status+"&tiket_id="+tiket_id,
    success: function(data) 
    {
      // alert(data);
      location.reload();
    }
  });
}   

function tiketApprve(status,sno)
{
  var tiket_id = $("#tiket_id"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('tiket/tiketApprve'); ?>",
    data: "status="+status+"&tiket_id="+tiket_id,
    success: function(data) 
    {

      location.reload();
    }
  });
}
function tiketReject(status,sno)
{
  var tiket_id = $("#tiket_id"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('tiket/tiketReject'); ?>",
    data: "status="+status+"&tiket_id="+tiket_id,
    success: function(data) 
    {

      location.reload();
    }
  });
}

function setAdminMsg(tiket_id,sno)
{
  var message = $("#message"+sno).val();
  if(message == '')
  {
    alert("Please type any msg.");
  }else{
      $.ajax({
      type: "POST",
      url: "<?php echo base_url('tiket/setAdminMsg'); ?>",
      data: "tiket_id="+tiket_id+"&message="+message,
      success: function(data) 
      {
        // alert(data);
        location.reload();
      }
    });
  }
  
}
</script>