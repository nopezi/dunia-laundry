
<!-- Page Content -->
<!-- ============================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Promo</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Promo</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
       
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['offer_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Promo</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Tambah Promo</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Harap isi semua bidang dengan benar. </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('offer/submitOffer'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Outlet</label>
                                    <input type="hidden" name="offer_id" value="<?=@$row['offer_id'];?>">
                                    <select name="shop_id" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($shop as $l)
                                        {
                                            $selected = '';
                                            if(@$row['shop_id'] == $l->shop_id)
                                            {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?=$l->shop_id?>" <?=$selected?>><?=$l->shop_name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Nama Promo</label>
                                    <input type="text" class="form-control" name="offer_name" value="<?=@$row['offer_name'];?>" id="inputName1" placeholder="Masukan Nama Promo" required> 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Jumlah</label>
                                    <input type="text" class="form-control groupOfTexbox" name="amount" value="<?=@$row['amount'];?>" placeholder="Masukan Jumlah" required> 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Jumlah Type</label>
                                    <select class="form-control" name="amount_type" id="amount_type" required>
                                        <option value="">-Select-</option>
                                        <option value="%">Persentase</option>
                                        <option value="num">Nominal</option>
                                    </select>
                                    <script type="text/javascript">
                                        $("#amount_type option[value='<?=@$row['amount_type']?>']").prop('selected',true);
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Berlaku pada layanan</label>
                                    <select name="applicable_on_service" class="form-control" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($service as $s) 
                                        {
                                            $selected = '';
                                            if(@$row['applicable_on_service'] == $s->service_id)
                                            {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?=$s->service_id?>" <?=$selected?>><?=$s->service_name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                     
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Tanggal promo berakhir</label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="<?=@$row['end_date'];?>" id="inputName1" placeholder="yyyy-mm-dd" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Judul </label>
                                    <input type="text" class="form-control" name="title" value="<?=@$row['title'];?>" id="inputName1" placeholder="Masukan judul" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Deskripsi</label>
                                    <textarea class="form-control" name="description" placeholder="Masukan deskripsi" required><?=@$row['description'];?></textarea> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="inputEmail2" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/Offer/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['offer_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose:true,
        // endDate: "today",
        // maxDate: today
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });


    $('.datepicker').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9^-]/g, '');
        }
    });
});
</script>