============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">History Pencairan Dana</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">History Pencairan Dana</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">

        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified white-box text-center">
            <?php foreach($aktif as $ak):?>
              <li class="<?=$ak['active']?>">
                <a href="<?=base_url('pencairanDana')?>?status=<?=$ak['id']?>"><?=$ak['status']?></a>
              </li>
            <?php endforeach?>
            </ul>
        </div>


        <div class="col-sm-12">
            <div class="white-box">
              
                <!-- <h3 class="box-title m-b-0">Data Komisi</h3> -->
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">

                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Tanggal Pengajuan</th>
                          <th class="text-center">User</th>
                          <th class="text-center">Jumlah</th>
                          <th class="text-center">Status</th>
                          <th class="text-center"></th>
                        </tr>
                      </thead>

                      <tbody>
                      <?php if(!empty($riwayat_pengajuan)):?>
                        <?php for($i=0; $i<sizeof($riwayat_pengajuan); $i++):?>
                        <tr>
                          <td class="text-center"><?=$i+1?></td>
                          <td class="text-center"><?=$riwayat_pengajuan[$i]->tanggal?></td>
                          <td class="text-center"><?=$riwayat_pengajuan[$i]->username?></td>
                          <td class="text-center">Rp. <?=number_format($riwayat_pengajuan[$i]->jumlah)?></td>
                          <td class="text-center">
                            <p class="label <?=$riwayat_pengajuan[$i]->label?>"><?=$riwayat_pengajuan[$i]->status?></p>
                          </td>
                          <td class="text-center">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#cekDetail<?=$riwayat_pengajuan[$i]->id?>" data-whatever="@mdo">
                              <i class="glyphicon glyphicon-eye-open"></i>
                            </button>
                          </td>
                        </tr>
                        <?php endfor?>
                      <?php endif?>
                      </tbody>

                      <tfoot>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Tanggal Pengajuan</th>
                          <th class="text-center">User</th>
                          <th class="text-center">Jumlah</th>
                          <th class="text-center">Status</th>
                          <th class="text-center"></th>
                        </tr>
                      </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DETAIL DATA PENGAJUAN YANG TELAH DI KIRIM -->
<?php if(!empty($riwayat_pengajuan)):?>
  <?php foreach($riwayat_pengajuan as $pengajuan):?>
      <?php $userdana = $this->Order_model->getUserDetail($pengajuan->id_user);?>
      <div class="modal fade" id="cekDetail<?=$pengajuan->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="exampleModalLabel">Detail Permintaan Pencairan Dana</h4>
            </div>
            <?=form_open_multipart('Admin/pencairanDana/update_pencairan')?>   

            <div class="modal-body">
              
              <table class="table table-bordered">
                <tr>
                  <th>Tanggal</th>
                  <td><?=$pengajuan->created_at?></td>
                </tr>
                <tr>
                  <th>User</th>
                  <td>
                    <div class="row">
                      <div class="col-md-1"><?=$userdana['name']?></div>
                      <div class="col-md-3">
                        <a href="tel:<?=$userdana['mobile']?>" class="label label-info">
                         <i class="glyphicon glyphicon-phone"></i> <?=$userdana['mobile']?>
                        </a>
                      </div>
                      <div class="col-md-2">
                        <a href="mailto:<?=$userdana['email']?>" class="label label-info">
                          <i class="glyphicon glyphicon-envelope"></i> <?=$userdana['email']?>
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>Jumlah Pencairan</th>
                  <td>Rp. <?=number_format($pengajuan->jumlah)?></td>
                </tr>
                <tr>
                  <th>Bank</th>
                  <td><?=$pengajuan->bank?></td>
                </tr>
                <tr>
                  <th>Nomor Rekening</th>
                  <td><?=$pengajuan->no_rekening?></td>
                </tr>
                <tr>
                  <th>Pemilik Rekening</th>
                  <td><?=$pengajuan->pemilik_rekening?></td>
                </tr>
                <tr>
                  <th>KTP / SIM CARD</th>
                  <td>
                    <a href="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->id_card?>" target="_blank">
                      <img src="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->id_card?>" class="img thumbnail" style="max-width: 100px; max-height: 100px">
                    </a>
                  </td>
                </tr>
                <tr>
                  <th>Bukti Transfer</th>
                  <td>
                    <input type="file" class="form-control" name="image"><br>
                    <?php if(!empty($pengajuan->bukti_transfer)):?>
                    <a href="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->bukti_transfer?>" target="_blank">
                      <img src="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->bukti_transfer?>" class="img thumbnail" style="max-width: 100px; max-height: 100px">
                    </a>
                    <?php endif?>
                  </td>
                </tr>
                <tr>
                  <th>Status</th>
                  <td>
                    <div class="form-group">
                      <select class="form-control col-md-1" name="status">
                      <?php for($z=0; $z<sizeof($pengajuan->pilihan_status); $z++):?>
                        <option value="<?=$pengajuan->pilihan_status[$z]['id']?>"><?=$pengajuan->pilihan_status[$z]['nama']?></option>
                      <?php endfor?>
                      </select>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>Catatan</th>
                  <td>
                    <input type="hidden" name="id_pencairan_dana" value="<?=$pengajuan->id?>">
                    <textarea class="form-control" name="note" placeholder="Bisa diisi dengan informasi pengiriman dan lain-lain" rows="5"><?=$pengajuan->note?:''?></textarea>
                  </td>
                </tr>
              </table>

            </div>

            <?php if($pengajuan->status == 1):?>
            <div class="modal-footer">
              <button class="btn btn-sm btn-info" type="submit">Update Permintaan Pencairan</button>
            </div>
            <?php endif?>
            <?=form_close()?>
          </div>
        </div>
      </div>
  <?php endforeach?>
<?php endif?>
    <!-- END MODAL DETAIL DATA PENGAJUAN YANG TELAH DI KIRIM -->

    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>

<script type="text/javascript">
$(function() {
  $('.selectpicker').selectpicker();
});
</script>

<!-- NOTIFIKASI ALERT -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php if ($this->session->flashdata('gagal')):?>
<script type="text/javascript">
  swal("Gagal daftar", "<?=$this->session->flashdata('gagal')?>", "error");
</script>
<?php elseif($this->session->flashdata('berhasil')):?>
  <script type="text/javascript">
  swal("Berhasil", "<?=$this->session->flashdata('berhasil')?>", "success");
</script>
<?php endif?>
<!-- END NOTIFIKASI ALERT -->