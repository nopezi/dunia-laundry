
<!-- Page Content -->
<!-- ============================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Data Agen</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Data Agen</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
       
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <h3 class="box-title m-b-0">Update Data Agen</h3>
                  <p class="text-muted m-b-30 font-13">Harap isi semua bidang dengan benar. </p>
                    <form data-toggle="validator" method="post" action="<?=base_url('admin/agen/aksi_update')?>" enctype="multipart/form-data">
                        <div class="row">
                            <?php $ses = $_SESSION; if($ses['status'] == '2'){ ?>
                                <input type="hidden" name="shop_id" value="<?=@$row['shop_id'];?>">
                                <input type="hidden" name="user_id" value="<?=$ses['id']?$ses['id']:@$row['user_id'];?>">
                            <?php } else { ?>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    
                                    <input type="hidden" name="shop_id" value="<?=@$row['shop_id'];?>">
                                    
                                    <label  class="control-label">Nama Pemilik</label>
                                    <select class="form-control" name="user_id" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach($laundry as $l)
                                        {
                                            $selected = '';
                                            if(@$row['user_id'] == $l->user_id)
                                            {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?=$l->user_id?>" <?=$selected?>><?=$l->name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Nama Usaha/Agen</label>
                                    <input type="text" class="form-control" name="shop_name" value="<?=@$row['shop_name'];?>" id="inputName1" placeholder="Masukan nama outlet" required> 
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">No WA / Hape (format +62xxxx)</label>
                                    <input type="text" class="form-control groupOfTexbox" name="mobile" value="<?=@$row['mobile'];?>" id="inputName1" placeholder="Masukan No Hape" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">                                    
                                    <label  class="control-label">Mulai Hari</label>
                                    <select class="form-control" name="mulai_hari" required>
                                        <option value="">--Select--</option>
                                        <option value="Senin" <?php if(@$row['mulai_hari']=='Senin'){ print 'selected'; }?>>Senin</option>
                                        <option value="Selasa" <?php if(@$row['mulai_hari']=='Selasa'){ print 'selected'; }?>>Selasa</option>
                                        <option value="Rabu"  <?php if(@$row['mulai_hari']=='Rabu'){ print 'selected'; }?>>Rabu</option>
                                        <option value="Kamis"  <?php if(@$row['mulai_hari']=='Kamis'){ print 'selected'; }?>>Kamis</option>
                                        <option value="Jumat"  <?php if(@$row['mulai_hari']=='Jumat'){ print 'selected'; }?>>Jumat</option>
                                        <option value="Sabtu"  <?php if(@$row['mulai_hari']=='Sabtu'){ print 'selected'; }?>>Sabtu</option>
                                        <option value="Minggu" <?php if(@$row['mulai_hari']=='Minggu'){ print 'selected'; }?>>Minggu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group"> 
                                    <label  class="control-label">Sampai Hari</label>
                                    <select class="form-control" name="sampai_hari" required>
                                        <option value="">--Select--</option>
                                        <option value="Senin" <?php if(@$row['sampai_hari']=='Senin'){ print 'selected'; }?>>Senin</option>
                                        <option value="Selasa" <?php if(@$row['sampai_hari']=='Selasa'){ print 'selected'; }?>>Selasa</option>
                                        <option value="Rabu" <?php if(@$row['sampai_hari']=='Rabu'){ print 'selected'; }?>>Rabu</option>
                                        <option value="Kamis" <?php if(@$row['sampai_hari']=='Kamis'){ print 'selected'; }?>>Kamis</option>
                                        <option value="Jumat" <?php if(@$row['sampai_hari']=='Jumat'){ print 'selected'; }?>>Jumat</option>
                                        <option value="Sabtu" <?php if(@$row['sampai_hari']=='Sabtu'){ print 'selected'; }?>>Sabtu</option>
                                        <option value="Minggu" <?php if(@$row['sampai_hari']=='Minggu'){ print 'selected'; }?>>Minggu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Jam Buka</label>
                                    <input type="time" class="form-control" name="opening_time" value="<?=@$row['opening_time'];?>" id="inputName1" required> 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label  class="control-label">Jam Tutup</label>
                                    <input type="time" class="form-control" name="closing_time" value="<?=@$row['closing_time'];?>" id="inputName1" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Latitude</label>
                                    <input type="text" class="form-control" name="latitude" value="<?=@$row['latitude'];?>" id="inputName1" placeholder="Enter latitude" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Longitude</label>
                                    <input type="text" class="form-control" name="longitude" value="<?=@$row['longitude'];?>" id="inputName1" placeholder="Enter longitude" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Alamat</label>
                                    <textarea class="form-control" placeholder="Enter address" name="address" required><?=@$row['address'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Deskripsi</label>
                                    <textarea class="form-control" placeholder="Enter description" name="description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="inputEmail2" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/Laundry/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <a href="<?=base_url('admin/agen')?>" class="btn btn-default">Back</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
<script type="text/javascript">
function get_category(parent_cat_id)
{
    $.ajax({
    type: "POST",
    url: "<?php echo base_url('admin/category/get_category'); ?>",
    data: "parent_cat_id="+parent_cat_id,
    success: function(data) 
    {
        $("#category_id").html(data);
    }
    });
}    
</script>