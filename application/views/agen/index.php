<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->

<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Agen</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <a href="<?= base_url('admin/agen/tambah') ?>" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;"><i class="ti-user"> </i> Tambah Data Agen</a>

          </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>

                <!-- <h3 class="box-title m-b-0">Data <?= $heading ?></h3> -->
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Image</th>
                              <th>Nama Pemilik</th>
                              <th>Nama <?= $heading ?></th>
                              <th>No Hape</th>
                              <th>Alamat</th>
                              <th>Mulai Hari</th>
                              <th>Tutup Hari</th>
                              <th>Jam Buka</th>
                              <th>Jam Tutup</th>
                              <th>Status</th>
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Nama Pemilik</th>
                                <th>Nama <?= $heading ?></th>
                                <th>No Hape</th>
                                <th>Alamat</th>
                                <th>Mulai Hari</th>
                                <th>Tutup Hari</th>
                                <th>Jam Buka</th>
                                <th>Jam Tutup</th>
                                <th>Status</th>
                                <th>Kelola</th>                         
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php $i=0; foreach($laundryData as $u) { $i++; ?> 
                            <tr>
                                <td><?=$i?></td>
                                <td>
                                    <a href="<?=base_url(); ?>assets/images/Laundry/<?=$u->image?>" target="_blank">
                                      <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?=base_url(); ?>assets/images/Laundry/<?=$u->image?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td><?=$this->Laundry_shop_model->getOwnerName($u->user_id); ?></td>       
                                <td><?=$u->shop_name?></td>
                                <td><?=$u->mobile?></td>
                                <td><?=$u->address?></td>
                                <td><?=$u->mulai_hari?></td>
                                <td><?=$u->sampai_hari?></td>
                                <td><?=$u->opening_time?></td>
                                <td><?=$u->closing_time?></td>
                                <td>
                                  <?php if($u->status==1){ ?>
                                  <label class="badge badge-teal">Aktif</label>
                                  <?php }else if($u->status==0){ ?>
                                  <label class="badge badge-danger">Non Aktif</label>
                                  <?php  } ?> 
                                </td>
                                <td class="text-center">

                                  <select class="form-control" onchange="location = this.value;">
                                    <option disabled="" selected="">Kelola</option>
                                  
                                    <option value="<?=base_url('admin/agen/ganti_status');?>/<?=$u->shop_id;?>" <?=$u->status==1?'disabled':''?>>
                                      Aktifkan
                                    </option>

                                    <option value="<?=base_url('admin/agen/ganti_status');?>/<?=$u->shop_id;?>" <?=$u->status==0?'disabled':''?>>
                                      Non Aktifkan
                                    </option>

                                    <option value="<?=base_url('admin/agen/update/').$u->shop_id?>">
                                        Update
                                    </option>

                                    <option value="<?=base_url('laundry/viewOwnerDetail');?>/<?=$u->user_id;?>" >
                                      Lihat Detail
                                    </option>

                                    <option value="<?=base_url('admin/agen/delete/').$u->shop_id?>">
                                      Hapus
                                    </option>

                                  </select>

                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>

<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
