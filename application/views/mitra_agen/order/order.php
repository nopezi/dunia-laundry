<?php
$ses = $_SESSION;
?>
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <h4 class="page-title">Data Pesanan <?=$wilayah?></h4> 
        </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-5 col-sm-5 col-md-5 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Pesanan</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">

        <div class="col-sm-12">
          <?php if( $this->session->flashdata('success') ): ?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php endif; ?>

          <?php if( $this->session->flashdata('error') ): ?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('error'); ?>    
            </div>
          <?php endif; ?>
        </div>

        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified white-box text-center">
            <?php foreach($aktif as $ak):?>
              <li class="<?=$ak['active']?>">
                <a href="<?=base_url('mitra_agen/order')?>?status_pesanan=<?=$ak['id']?>"><?=$ak['name']?></a>
              </li>
            <?php endforeach?>
            </ul>
        </div>

        <div class="col-sm-12">
        <!-- <h3 class="box-title m-b-0">Data Pesanan</h3> -->
            <div class="table-responsive" style="overflow-x: auto;">
                <table id="example23" class="table  table table-hover  table-bordered" style="word-wrap: break-word;" cellspacing="0" width="100%">
                    <thead>
                        <tr class="white-box bg-info">
                          <th class="text-white text-center">No.</th>
                          <th class="text-white text-center">Id Pesanan</th>
                          <th class="text-white text-center">Username</th>
                          <th class="text-white text-center">Outlet</th>
                          <th class="text-white text-center">No WA/HP</th>
                          <th class="text-white text-center">Harga</th>
                          <th class="text-white text-center">Diskon</th>
                          <th class="text-white text-center">Total Harga</th>
                          <th class="text-white text-center">Email</th>
                          <th class="text-white text-center">Waktu Penjemputan</th>
                          <th class="text-white text-center">Waktu Pengiriman</th>
                        <?php if ($_SESSION['type'] != '3'):?>
                          <th class="text-white text-center">Status Pesanan</th>
                          <th class="text-white text-center">Kelola Pesanan</th>
                          <th class="text-white text-center">Status Pembayaran</th>
                          <?php if( $ses['type'] == '10' ): ?>
                          <th class="text-white text-center">Kelola Pembayaran</th>
                          <?php endif; ?>
                        <?php endif ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="white-box bg-info">
                          <th class="text-white text-center">No.</th>
                          <th class="text-white text-center">Id Pesanan</th>
                          <th class="text-white text-center">Username</th>
                          <th class="text-white text-center">Outlet</th>
                          <th class="text-white text-center">No WA/HP</th>
                          <th class="text-white text-center">Harga</th>
                          <th class="text-white text-center">Diskon</th>
                          <th class="text-white text-center">Total Harga</th>
                          <th class="text-white text-center">Email</th>
                          <th class="text-white text-center">Waktu Penjemputan</th>
                          <th class="text-white text-center">Waktu Pengiriman</th>
                        <?php if ($_SESSION['type'] != '3'):?>
                          <th class="text-white text-center">Status Pesanan</th>
                          <th class="text-white text-center">Kelola Pesanan</th>
                          <th class="text-white text-center">Status Pembayaran</th>
                          <?php if( $ses['type'] == '10' ): ?>
                          <th class="text-white text-center">Kelola Pembayaran</th>
                          <?php endif; ?>
                        <?php endif ?>
                        </tr>
                    </tfoot>
                    <tbody class="white-box">
                    <?php 
                    $i=0; 
                    foreach($order as $u): 
                      $i++; 
                      $user = $this->Order_model->getUserDetail($u->user_id);
                    ?> 
                        <tr>
                            <td class="col-1"><?php echo $i; ?></td>
                            <td class="col-1"><?=$u->order_id?></td>
                            <td class="col-1"><?php echo $user['name']; ?></td>     
                            <td class="col-1"><?php echo $this->Order_model->getLaundryShopName($u->shop_id); ?></td>
                            <td class="col-1"><?=$user['mobile'];?></td>
                            <td class="col-1"><?= $u->currency_code ?> <?=number_format( $u->price )?></td>
                            <td class="col-1"><?= $u->currency_code ?> <?=number_format( $u->discount )?></td>
                            <td class="col-1"><?= $u->currency_code ?> <?=number_format( $u->final_price )?></td>
                            <td class="col-1"><?=$user['email'];?></td>
                            <td class="col-1"><?= date( 'd F Y', strtotime( $u->pickup_date ) ) . ' ' . $u->pickup_time ?></td>
                            <td class="col-1"><?= date( 'd F Y', strtotime( $u->delivery_date ) ) . ' ' . $u->delivery_time ?></td>
                          <?php if($_SESSION['type']!=3): ?>
                            <td class="col-1 text-center">
                              <?php if($u->order_status==0){ ?>
                              <label class="badge badge-default">Pending</label>
                              <?php }else if($u->order_status==1){ ?>
                              <label class="badge badge-warning">Confirmed</label>
                              <?php }else if($u->order_status==2){ ?>
                              <label class="badge badge-warning">Picked up</label>
                              <?php }else if($u->order_status==3){ ?>
                              <label class="badge badge-info">In progress</label>
                              <?php }else if($u->order_status==4){ ?>
                              <label class="badge badge-info">Shipped</label>
                              <?php }else if($u->order_status==5){ ?>
                              <label class="badge badge-success">Delivered</label>
                              <?php  } else if($u->order_status==6){ ?>
                              <label class="badge badge-danger">Cancel</label>
                              <?php  } ?>

                              <br/>
                              <br/>
                              <input type="hidden" name="order_id" id="order_id<?=$i;?>" value="<?=$u->order_id?>">
                                  <select class="btn btn-sm btn-info dropdown-toggle" id="order_status<?=$i;?>" onchange="changeOrderStatus(<?=$i;?>);" name="order_status">
                                    <option value="0" <?=$u->order_status==0?'selected':''?>>Pending</option>
                                    <option value="1" <?=$u->order_status==1?'selected':''?>>Confirmed</option>
                                    <option value="2" <?=$u->order_status==2?'selected':''?>>Picked up</option>
                                    <option value="3" <?=$u->order_status==3?'selected':''?>>In progress</option>
                                    <option value="4" <?=$u->order_status==4?'selected':''?>>Shipped</option>
                                    <option value="5" <?=$u->order_status==5?'selected':''?>>Delivered</option>
                                    <option value="5" <?=$u->order_status==6?'selected':''?>>Cancel</option>
                                  </select>
                            </td>
                            <td class="text-center col-1">
                              <!-- <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button> -->
                                <!-- <form method="post" action="<?php echo base_url('order/changeOrderStatus');?>" enctype="multipart/form-data"> -->

                                  <a type="button" class="btn btn-sm btn-primary" href="<?=base_url('order/orderDetail');?>/<?=$u->order_id;?>">
                                    <i class="fas fa-eye"></i> 
                                  </a>
                                  <br/>
                                  <br/>
                                  <a type="button" class="btn btn-sm btn-warning" href="<?=base_url('order/orderEdit');?>/<?=$u->order_id;?>">
                                    <i class="fas fa-edit"></i>
                                  </a>
                                  <br/>
                                  <br/>
                                  <a type="button" class="btn btn-sm btn-success" href="<?=base_url('order/orderInvoice');?>/<?=$u->order_id;?>">Invoice</a>
                                <!-- </form> -->
                              <!-- <ul class="dropdown-menu">
                              <li>
                                <a title="Verified" class="<?=$u->order_status==0?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>">Pending</a>
                              </li>
                              <li>
                                <a title="Not Verified" class="<?=$u->order_status==1?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>" >Confirmed</a>
                              </li>
                              <li>
                                <a title="Not Verified" class="<?=$u->order_status==2?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>" >Picked up</a>
                              </li>
                              <li>
                                <a title="Not Verified" class="<?=$u->order_status==3?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>" >In progress</a>
                              </li>
                              <li>
                                <a title="Not Verified" class="<?=$u->order_status==4?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>" >Shipped</a>
                              </li>
                              <li>
                                <a title="Not Verified" class="<?=$u->order_status==5?'disabled':''?>" href="<?php echo base_url('order/changeOrderStatus');?>/<?php echo $u->order_id;?>" >Delivered</a>
                              </li>
                              </ul> -->
                              <!-- </div> -->
                            </td>
                            <td class="text-center">
                              <?php if($u->payment_status==0){ ?>
                              <label class="badge badge-danger">Pending</label>
                              <?php }else if($u->payment_status==1){ ?>
                              <label class="badge badge-success">Complete</label>
                              <?php  } ?> 
                            </td>
                            
                            <?php if( $ses['type'] == '10' ): ?>
                            <td class="text-center">
                              <div class="dropdown">
                                <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                  Kelola<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                <li>
                                  <a title="Verified" class="<?=$u->payment_status==0?'disabled':''?>" href="<?php echo base_url('order/changePaymentStatus');?>/<?php echo $u->order_id;?>">Pending</a>
                                </li>
                                <li>
                                  <a title="Not Verified" class="<?=$u->payment_status==1?'disabled':''?>" href="<?php echo base_url('order/changePaymentStatus');?>/<?php echo $u->order_id;?>" >Complete</a>
                                </li>
                                
                                </ul>
                              </div>
                            </td>
                            <?php endif; ?>
                          <?php endif ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>

        

    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script type="text/javascript">
function changeOrderStatus(sno)
{
  var order_id = $("#order_id"+sno).val();
  var order_status = $("#order_status"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>