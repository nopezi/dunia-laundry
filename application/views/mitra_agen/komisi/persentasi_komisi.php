============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Persentasi Komisi</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Komisi</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>
              
                <!-- <h3 class="box-title m-b-0">Data Komisi</h3> -->
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center">Type User</th>
                              <th class="text-center">Persentasi Komisi</th>
                              <th class="text-center">Setting</th>
                            </tr>
                        </thead>
                        <tbody>
                    	<?php if(!empty($persentasi)):?>
                    		<?php foreach($persentasi as $ps):?>
	                        <tr>
	                        	<td class="text-center">
	                        		<?php if($ps->type == 1):?>
	                        			Agen
	                        		<?php elseif($ps->type == 2):?>
	                        			Mitra
	                        		<?php else:?>
	                        			Franchise
	                        		<?php endif?>
	                        	</td>
	                        	<td class="text-center"><?=$ps->persentasi?>%</td>
	                        	<td class="text-center">
	                        		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editPersentasi<?=$ps->id?>" data-whatever="@mdo">
                                      Edit
                               </button>
	                        	</td>
	                        </tr>
		                    <?php endforeach?>
                        <?php endif?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php if(!empty($persentasi)):?>
	<?php foreach($persentasi as $ps):?>
    <!-- MODAL EDIT -->
    <div class="modal fade" id="editPersentasi<?=$ps->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Edit Jumlah Persentasi</h4>
          </div>
        <?=form_open_multipart('Admin/komisiController/update_persentasi')?>
          <div class="modal-body">
              <div class="form-group row">
                <div class="col-md-10">
                	<!-- <label for="recipient-name" class="control-label">Jumlah Persentasi</label> -->
	                <input type="hidden" name="id_persentasi" value="<?=$ps->id?>">
	                <input type="number" class="form-control" id="recipient-name" name="jumlah" value="<?=$ps->persentasi?>" required="">
                </div>
                <div class="col-md-1">
                	<label>%</label>
                </div>
              </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-info">Update</button>
          </div>
        <?=form_close()?>
        </div>
      </div>
    </div>
    <!-- END MODAL EDIT -->
	<?php endforeach?>
<?php endif?>

    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script type="text/javascript">
function changeOrderStatus(sno)
{
  var order_id = $("#order_id"+sno).val();
  var order_status = $("#order_status"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>

<script type="text/javascript">
$(function() {
  $('.selectpicker').selectpicker();
});
</script>

<!-- NOTIFIKASI ALERT -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- <script type="text/javascript">
	swal("Berhasil", "mantap", "success");
</script> -->
<?php if ($this->session->flashdata('gagal')):?>
<script type="text/javascript">
  swal("Gagal daftar", "<?=$this->session->flashdata('gagal')?>", "error");
</script>
<?php elseif($this->session->flashdata('berhasil')):?>
  <script type="text/javascript">
  swal("Berhasil", "<?=$this->session->flashdata('berhasil')?>", "success");
</script>
<?php endif?>
<!-- END NOTIFIKASI ALERT -->