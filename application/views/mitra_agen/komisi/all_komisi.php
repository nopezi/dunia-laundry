============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Komisi</div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Komisi</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">

    	<div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified white-box text-center">
            <?php foreach($aktif as $ak):?>
              <li class="<?=$ak['active']?>">
                <a href="<?=base_url('Admin/KomisiController/all_komisi/')?><?=$ak['id']?>"><?=$ak['status']?></a>
              </li>
            <?php endforeach?>
            </ul>
        </div>

        <div class="col-sm-12">
            <div class="white-box">
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>
              
                <!-- <h3 class="box-title m-b-0">Data Komisi</h3> -->
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
    						<tr>
    							<th class="text-center">No</th>
    							<th class="text-center">Type</th>
    							<th class="text-center">Username</th>
                  <?php if($type == 3):?>
                    <th class="text-center">Regional</th>
                  <?php else:?>
      							<th class="text-center">Outlet</th>
                  <?php endif?>
    							<th class="text-center">Total Pendapatan Bersih</th>
    							<th class="text-center">Detail</th>
    						</tr>
    					</thead>
                        <tbody>
                        	<?php if(!empty($komisi_all)):?>
                        		<?php $no=1; foreach($komisi_all as $ka):?>
	                        		<tr>
	                        			<td class="text-center"><?=$no++?></td>
	                        			<td class="text-center"><?=$ka['type']?></td>
	                        			<td class="text-center"><?=$ka['username']?></td>
                                <?php if($type == 3):?>
                                  <td class="text-center"><?=$ka['region']?></td>
                                  <td class="text-center"><?=$ka['total_pendapatan_franchise']?></td>
                                <?php else:?>
                                  <td class="text-center"><?=$ka['outlet']?></td>
                                  <td class="text-center"><?=$ka['total_pendapatan_bersih']?></td>
                                <?php endif?>
	                        			
	                        			
	                        			<td class="text-center">
	                        				<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#cekDetail<?=$ka['user_id']?>" data-whatever="@mdo">
		                                      <i class="glyphicon glyphicon-eye-open"></i>
		                                    </button>
	                        			</td>
	                        		</tr>
	                        	<?php endforeach?>
                        	<?php endif?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url('assets') ?>/plugins/bower_components/datatables/datatables.min.js"></script>
<?php if(!empty($komisi_all)):?>
	<?php foreach($komisi_all as $ka):?>

    <!-- MODAL DETAIL -->
    <div class="modal fade" id="cekDetail<?=$ka['user_id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Detail Komisi <b><?=$ka['username']?></b></h4>
          </div>
          <div class="modal-body">
          	
          	<table id="detailData<?=$ka['user_id']?>" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
          		<thead>
          			<tr>
          				<th class="text-center">No</th>
          				<th class="text-center">Id Pesanan</th>
          				<th class="text-center">Harga</th>
          				<th class="text-center">Komisi</th>
          				<th class="text-center">Pendapatan Bersih</th>
          			</tr>
          		</thead>
          		<tbody>
          			<?php if(!empty($ka['detail'])):?>
          				<?php $nomor=1; foreach($ka['detail'] as $kd):?>
          					<tr>
          						<td class="text-center"><?=$nomor++?></td>
          						<td class="text-center"><?=$kd['id_pesanan']?></td>
          						<td class="text-center"><?=$kd['harga']?></td>
                      <?php if($type == 3):?>
                        <td class="text-center"><?=$kd['komisi_franchise']?></td>
                      <?php else:?>
            						<td class="text-center"><?=$kd['komisi']?></td>
                      <?php endif?>
          						<td class="text-center"><?=$kd['pendapatan_bersih']?></td>
          					</tr>
          				<?php endforeach?>
          			<?php endif?>
          		</tbody>
          		<tfoot>
          			<tr>
          				<th class="text-center">No</th>
          				<th class="text-center">Id Pesanan</th>
          				<th class="text-center">Harga</th>
          				<th class="text-center">Komisi</th>
          				<th class="text-center">Pendapatan Bersih</th>
          			</tr>
          		</tfoot>
          	</table>

          </div>
        </div>
      </div>
    </div>
    <!-- END MODAL DETAIL -->

    <?php $id_table = $ka['user_id']?>
    <script type="text/javascript">
    	$('#detailData<?=$id_table?>').DataTable({
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    });

	    $('.example24<?=$id_table?>').DataTable({
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    });
    </script>

	<?php endforeach?>
<?php endif?>

    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script type="text/javascript">
function changeOrderStatus(sno)
{
  var order_id = $("#order_id"+sno).val();
  var order_status = $("#order_status"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>

<script type="text/javascript">
$(function() {
  $('.selectpicker').selectpicker();
});
</script>

<!-- NOTIFIKASI ALERT -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- <script type="text/javascript">
	swal("Berhasil", "mantap", "success");
</script> -->
<?php if ($this->session->flashdata('gagal')):?>
<script type="text/javascript">
  swal("Gagal daftar", "<?=$this->session->flashdata('gagal')?>", "error");
</script>
<?php elseif($this->session->flashdata('berhasil')):?>
  <script type="text/javascript">
  swal("Berhasil", "<?=$this->session->flashdata('berhasil')?>", "success");
</script>
<?php endif?>
<!-- END NOTIFIKASI ALERT -->