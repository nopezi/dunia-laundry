============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Komisi <?=$wilayah?></h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Komisi</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>
              
                <!-- <h3 class="box-title m-b-0">Data Komisi</h3> -->
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center">No.</th>
                              <th class="text-center">Id Pesanan</th>
                              <?php if($_SESSION['type'] == 3 || $_SESSION['role'] == 'admin'):?>
                                <th class="text-center">Type</th>
                              <?php endif?>
                              <th class="text-center">Username</th>
                              <th class="text-center">Outlet</th>
                              <th class="text-center">Harga</th>
                              <!-- <th class="text-center">Income</th> -->
                              <th class="text-center">Komisi Mitra / Agen</th>
                              <th class="text-center">Pendapatan Bersih Agen / Mitra</th>
                              <?php if($_SESSION['type']==3):?>
                                <th class="text-center">Komisi Franchise</th>
                                <th class="text-center">Pendapatan Bersih Franchise</th>
                              <?php endif?>
                              <?php if($_SESSION['role'] != 'admin'):?>
                                <th class="text-center">Pencairan Dana</th>
                              <?php endif?>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th class="text-center">No.</th>
                              <th class="text-center">Id Pesanan</th>
                              <?php if($_SESSION['type'] == 3 || $_SESSION['role'] == 'admin'):?>
                                <th class="text-center">Type</th>
                              <?php endif?>
                              <th class="text-center">Username</th>
                              <th class="text-center">Outlet</th>
                              <th class="text-center">Harga</th>
                              <!-- <th class="text-center">Income</th> -->
                              <th class="text-center">Komisi Mitra / Agen</th>
                              <th class="text-center">Pendapatan Bersih Agen / Mitra</th>
                              <?php if($_SESSION['type']==3):?>
                                <th class="text-center">Komisi Franchise</th>
                                <th class="text-center">Pendapatan Bersih Franchise</th>
                              <?php endif?>
                              <?php if($_SESSION['role'] != 'admin'):?>
                                <th class="text-center">Pencairan Dana</th>
                              <?php endif?>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php  $i=0; foreach($order as $u): 
                          $i++; 
                          $user = $this->Order_model->getUserDetail($u->id_agen_mitra);
                          $pengajuan = $this->Order_model->cekPencairanDana($u->order_id);
                          $nilai_persentasi = $this->Order_model->cekPersentasi($u->type);
                          $type_user = $this->Order_model->cekTypeUser($u->type);
                          $income = $u->final_price * $nilai_persentasi['nilai'];
                          $p_franchise = $this->Order_model->cekPersentasi(3);
                          // $total_franchise = $this->Order_model->cekPersentasiFranchise($p_franchise['nilai']);
                          $persentasi_franchise = $p_franchise['persentasi'] / 100;
                        ?> 
                            <tr>
                                <td class="text-center"><?php echo $i; ?></td>
                                <td class="text-center"><?=$u->order_id?></td>
                                <?php if($_SESSION['type'] == 3 || $_SESSION['role'] == 'admin'):?>
                                  <td class="text-center"><?=$type_user?></td>
                                <?php endif?>
                                <td class="text-center"><?php echo @$user['name']; ?></td>       
                                <td class="text-center"><?=$this->Order_model->getLaundryShopName($u->shop_id)?></td>
                                <td class="text-center">
                                  <?=$u->currency_code ?> <?=number_format( $u->final_price )?>
                                </td>
                                <td class="text-center">
                                  <?=$nilai_persentasi['persentasi']?>%
                                </td>
                                <td class="text-center">
                                  <?=$u->currency_code?> <?=number_format( $income )?>
                                </td>
                                <!-- <td class="text-center">
                                  <?php
                                  if ( '2' === $u->type ) {
                                    $income = $u->final_price * 0.7;
                                  } else {
                                    $income = $income * 0.12;
                                  }
                                  echo $u->currency_code . ' ' . number_format( $income );
                                  ?>
                                </td> -->
                                <?php if($_SESSION['type'] == 3):?>
                                  <td class="text-center">
                                    <?=$p_franchise['persentasi']?>%
                                  </td>
                                  <td class="text-center">
                                    <?php
                                    $total_franchise = ($u->final_price - $income) * $persentasi_franchise;
                                    echo $u->currency_code . ' ' . number_format($total_franchise);
                                    ?>
                                  </td>
                                <?php endif?>
                                <?php if($_SESSION['role'] != 'admin'):?>
                                  <td class="text-center">
                                  <?php if(empty($pengajuan)):?>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#withdraw<?=$u->order_id?>" data-whatever="@mdo">
                                      Withdraw
                                    </button>
                                  <?php elseif($pengajuan->status == 2):?>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#withdraw<?=$u->order_id?>" data-whatever="@mdo">
                                      Withdraw
                                    </button>
                                  <?php else:?>
                                    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#cekDetail<?=$pengajuan->id?>" data-whatever="@mdo">
                                      <i class="glyphicon glyphicon-eye-open"></i>
                                    </button>
                                  <?php endif?>
                                  </td>
                                <?php endif?>
                            </tr>
                         <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php if(!empty($order)):?>
  <?php foreach ($order as $key => $value):?>
    <?php
    $pengajuan = $this->Order_model->cekPencairanDana($value->order_id);
    $nilai_persentasi = $this->Order_model->cekPersentasi($value->type);
    $type_user = $this->Order_model->cekTypeUser($u->type);
    $income = $value->final_price * $nilai_persentasi['nilai'];
    $total_franchise = ($value->final_price - $income) * $persentasi_franchise;

    if ($_SESSION['type'] == 3) {
      $total_cair = $total_franchise;
    } else {
      $total_cair = $income;
    }
    // if ( '2' === $value->type ) {
    //   $income = $value->final_price * 0.7;
    // } else {
    //   $income = ( $value->final_price * 0.3 ) * 0.12;
    // }
    ?>

    <!-- MODAL PENGAJUAN DANA -->
    <div class="modal fade" id="withdraw<?=$value->order_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Pengajuan Pencairan Dana</h4>
          </div>
          <?=form_open_multipart('mitra_agen/komisi/withdraw')?>
          <div class="modal-body">
              <div class="form-group">
                <label for="recipient-name" class="control-label">Bank Tujuan Transfer</label>
                <input type="hidden" name="order_id" value="<?=$value->order_id?>">
                <input type="hidden" class="form-control" id="recipient-name" name="total_maksimal" value="<?=$total_cair?>">
                <div id="pilih_bank<?=$value->order_id?>"></div>
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Nomor Rekening</label>
                <input type="number" class="form-control" id="recipient-name" name="no_rekening" required="">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Nama Lengkap Pemilik Rekening</label>
                <input type="text" class="form-control" id="recipient-name" name="pemilik_rekening" required="">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">KTP / SIM</label>
                <input type="file" class="form-control" name="image" required="">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="control-label">Jumlah yang dicairkan</label>
                <div class="row">
                  <div class="col-md-1">
                    <label>Rp</label>
                  </div>
                  <div class="col-md-11">
                    <input type="number" class="form-control" id="recipient-name" name="jumlah" value="<?=number_format( $total_cair )?>" required="">
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-info">Kirim</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
    <!-- END MODAL PENGAJUAN DANA -->

    <!-- MODAL DETAIL DATA PENGAJUAN YANG TELAH DI KIRIM -->
    <?php if(!empty($pengajuan)):?>
      <?php $userdana = $this->Order_model->getUserDetail($pengajuan->id_user);?>
      <div class="modal fade" id="cekDetail<?=$pengajuan->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="exampleModalLabel">Detail Permintaan Pencairan Dana</h4>
            </div>
                
            <table class="table modal-body">
              <tr>
                <th>Tanggal</th>
                <td><?=$pengajuan->created_at?></td>
              </tr>
              <tr>
                <th>User</th>
                <td>
                  <?=$userdana['name']?> <span class="label label-info"><?=$userdana['mobile']?></span> <span class="label label-info"><?=$userdana['email']?></span>
                </td>
              </tr>
              <tr>
                <th>Jumlah Pencairan</th>
                <td>Rp. <?=number_format($pengajuan->jumlah)?></td>
              </tr>
              <tr>
                <th>Bank</th>
                <td><?=$pengajuan->bank?></td>
              </tr>
              <tr>
                <th>Nomor Rekening</th>
                <td><?=$pengajuan->no_rekening?></td>
              </tr>
              <tr>
                <th>Pemilik Rekening</th>
                <td><?=$pengajuan->pemilik_rekening?></td>
              </tr>
              <tr>
                <th>KTP / SIM CARD</th>
                <td>
                  <a href="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->id_card?>" target="_blank">
                    <img src="<?=base_url()?>assets/images/Laundry/<?=$pengajuan->id_card?>" class="img thumbnail" style="max-width: 100px; max-height: 100px">
                  </a>
                </td>
              </tr>
              <tr>
                <th>Status</th>
                <td>
                  <?php if(empty($pengajuan->status)):?>
                    <p class="label label-warning">Belum di proses</p>
                  <?php elseif($pengajuan->status == 1):?>
                    <p class="label label-success">Sudah di proses</p>
                    <!-- <p class="label label-info">Sedang di proses</p> -->
                  <?php elseif($pengajuan->status == 2):?>
                  <!-- <?php elseif($pengajuan->status == 3):?> -->
                    <p class="label label-info">Di tolak</p>
                  <?php endif?>
                </td>
              </tr>
            </table>

          </div>
        </div>
      </div>
    <?php endif?>
    <!-- END MODAL DETAIL DATA PENGAJUAN YANG TELAH DI KIRIM -->

    <!-- DATA PILIH BANK -->
    <script type="text/javascript">
      
      $.getJSON("https://raw.githubusercontent.com/mul14/gudang-data/master/bank/bank.json", function(hasil){

        var html = '<select class="form-control selectpicker" name="bank" data-live-search="true" required="">';
            html += '<option disabled="" selected="" value="">Pilih Bank</option>';

        for(var i=0; i<hasil.length; i++){
          html += '<option value="'+hasil[i].name+'">'+hasil[i].name+'</option>';
        }

        html += '</select>';
        $("#pilih_bank<?=$value->order_id?>").html(html);

      });

    </script>
    <!-- END DATA PILIH BANK -->

  <?php endforeach?>
<?php endif?>

    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
<script type="text/javascript">
function changeOrderStatus(sno)
{
  var order_id = $("#order_id"+sno).val();
  var order_status = $("#order_status"+sno).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>

<script type="text/javascript">
$(function() {
  $('.selectpicker').selectpicker();
});
</script>

<!-- NOTIFIKASI ALERT -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php if ($this->session->flashdata('gagal')):?>
<script type="text/javascript">
  swal("Gagal", "<?=$this->session->flashdata('gagal')?>", "error");
</script>
<?php elseif($this->session->flashdata('berhasil')):?>
  <script type="text/javascript">
  swal("Berhasil", "<?=$this->session->flashdata('berhasil')?>", "success");
</script>
<?php endif?>
<!-- END NOTIFIKASI ALERT -->

