<li> <a href="<?php echo base_url('user/dashboard'); ?>" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i><span class="hide-menu">Beranda</span></a> </li>

 <!-- ################################################################################# -->
 <?php 
 $ses = $_SESSION;
 // 1 => admin , 2 => laundry owner
if($ses['type']=='10')
{
?>
 
 <li><a href="<?php echo base_url('service'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-servicestack"></i> &nbsp;<span class="hide-menu"> Layanan</span></a></li>
 
<li class="dropdown <?php echo(isset($page) && $page == 'packages') ? 'selected': '' ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="icon-settings" data-icon="v"></i> &nbsp;<span class="hide-menu">Mitra / Agen Laundry<span class="fa arrow"></span> </span></a>
    <ul class="dropdown-menu">
    
     <li><a href="<?php echo base_url('laundryOwner'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-user"></i> &nbsp;<span class="hide-menu">Data Pemilik</span></a></li>

     <li><a href="<?php echo base_url('laundryShop'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-columns"></i> &nbsp;<span class="hide-menu">Data Outlet</span></a></li>

     <li><a href="<?php echo base_url('laundry/laundryShopAgen'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-columns"></i> &nbsp;<span class="hide-menu">Data Agen</span></a></li>
     
  </ul>
</li>
<?php
}
if($ses['type']=='10')
{
?>
 
<li class="dropdown <?php echo(isset($page) && $page == 'packages') ? 'selected': '' ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="icon-settings" data-icon="v"></i> &nbsp;<span class="hide-menu">Utils<span class="fa arrow"></span> </span></a>
    <ul class="dropdown-menu">
      
      <!-- <li><a href="<?php echo base_url('advertisement'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-adversal"></i> &nbsp;<span class="hide-menu"> Iklan</span></a></li> -->

      <li><a href="<?php echo base_url('offer'); ?>" class="waves-effect"> &nbsp;<i class="ti-gift"></i> &nbsp;<span class="hide-menu">Promo</span></a></li>

    </ul>
</li>    
<?php
}
 if($ses['type']=='10')
{
?>

 <li><a href="<?php echo base_url('users'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-user-plus"></i> &nbsp;<span class="hide-menu">Pengguna</span></a></li>
 <?php
}
if($ses['type']=='10' || $ses['type']=='2')
{
?>
 <li><a href="<?php echo base_url('product'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-product-hunt"></i> &nbsp;<span class="hide-menu"> Produk</span></a></li>
 <?php
}
if($ses['status']=='2')
{
 ?>
 <li><a href="<?php echo base_url('chat'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-rocketchat"></i> &nbsp;<span class="hide-menu">Chat</span></a></li>
<?php
}
if($ses['type']=='10')
{
 ?>
 <li class="dropdown <?php echo(isset($page) && $page == 'packages') ? 'selected': '' ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="icon-settings" data-icon="v"></i> &nbsp;<span class="hide-menu">Support<span class="fa arrow"></span> </span></a>
    <ul class="dropdown-menu">

      <li><a href="<?php echo base_url('chat'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-rocketchat"></i> &nbsp;<span class="hide-menu">Chat</span></a></li>
       <!-- ################################################################################# -->
       <li><a href="<?php echo base_url('tiketSupport'); ?>" class="waves-effect"> &nbsp;<i class="ti-ticket"></i> &nbsp;<span class="hide-menu">Ticket Support</span></a></li>

    </ul>
</li>   
 <?php
  }
  if($ses['status']=='2' || $ses['status']=='1')
  {
 ?>
 <!-- ################################################################################# -->
 <li><a href="<?php echo base_url('order'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-cart-arrow-down"></i> &nbsp;<span class="hide-menu"> Pesanan</span></a></li>
 <li><a href="<?php echo base_url('komisi'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-cart-arrow-down"></i> &nbsp;<span class="hide-menu"> Komisi</span></a></li>
 <?php } ?>

 <?php if( $ses['type'] == '2' || $ses['type'] == '1' ): ?>
  <li><a href="<?php echo base_url('user/updateUser/' . $ses['id'] ); ?>" class="waves-effect"> &nbsp;<i class="fas fa-user"></i> &nbsp;<span class="hide-menu"> Profile</span></a></li>
 <?php endif; ?>

 <?php
  if($ses['type']=='10')
  {
 ?>
 <!-- <li><a href="<?php echo base_url('subscriptionPackage'); ?>" class="waves-effect"> &nbsp;<i class="fas fa-money-bill-alt"></i> &nbsp;<span class="hide-menu"> Subscription Package</span></a></li> -->

 <!-- <li><a href="<?php echo base_url('invoiceHistory'); ?>" class="waves-effect"><i class="mdi mdi-cash-multiple"></i> &nbsp;<span class="hide-menu"> Invoice History</span></a></li> -->
  
 <!-- ################################################################################# -->

<li class="dropdown <?php echo(isset($page) && $page == 'packages') ? 'selected': '' ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="icon-settings" data-icon="v"></i> &nbsp;<span class="hide-menu">Settings<span class="fa arrow"></span> </span></a>
    <ul class="dropdown-menu">
      <!-- ################################################################################# -->
      <li><a href="<?php echo base_url('apiKeys'); ?>" class="waves-effect"> &nbsp;<i class="fa fa-key"></i> &nbsp;<span class="hide-menu"> Api keys </span></a></li>
      <li><a href="<?php echo base_url('notification'); ?>" class="waves-effect"> &nbsp;<i class="far fa-bell"></i> &nbsp;<span class="hide-menu"> Notification</span></a></li>
      <li class="<?php echo(isset($sub_page) && $sub_page == 'users') ? 'active': '' ?>"> <a href="<?php echo base_url('currency/currency'); ?>" class="<?php echo(isset($sub_page) && $sub_page == 'users') ? 'active': '' ?>"><i class="far fa-money-bill-alt"></i> <span class="hide-menu"> Currency</span></a> </li>
      <li class="<?php echo(isset($sub_page) && $sub_page == 'users') ? 'active': '' ?>"><a href="<?php echo base_url('currency/setting'); ?>" class="<?php echo(isset($sub_page) && $sub_page == 'users') ? 'active': '' ?>"><i class="fa-fw">C</i><span class="hide-menu">Currency Setting</span></a> </li>
    </ul>
 </li>
 <?php
  }
  if($ses['status']=='2' || $ses['status']=='1')
  {
 ?>
 <!-- ################################################################################### -->

 <li><a href="<?php echo base_url('logout'); ?>" class="waves-effect"> &nbsp;<i class="icon-logout"></i> &nbsp;<span class="hide-menu">Log out</span></a></li>
<?php }?>