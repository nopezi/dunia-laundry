 <!-- /.container-fluid -->
        <footer class="footer text-center"> 2020 &copy; WIN LAUNDRY</footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/'); ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url('assets/'); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets/'); ?>js/waves.js"></script>
    <!--Counter js -->
    
    <!--Morris JavaScript -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/morrisjs/morris.js"></script>
    <!-- chartist chart -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- Calendar JavaScript -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/moment/moment.js"></script>
    <script src='<?php echo base_url('assets/plugins'); ?>/bower_components/calendar/dist/fullcalendar.min.js'></script>
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/calendar/dist/cal-init.js"></script>
    <script src="<?php echo base_url('assets'); ?>/plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url('assets');?>/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <!-- Magnific popup JavaScript -->
    <script src="<?php echo base_url('assets');?>/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url('assets');?>/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/'); ?>js/validator.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/custom.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/dashboard1.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/dashboard2.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/jquery.PrintArea.js" type="text/JavaScript"></script>

    <!-- NOTIFIKASI ALERT -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <?php if ($this->session->flashdata('error')):?>
    <script type="text/javascript">
      swal("Gagal", "<?=$this->session->flashdata('error')?>", "error");
    </script>
    <?php elseif($this->session->flashdata('success')):?>
     <script type="text/javascript">
      swal("Berhasil", "<?=$this->session->flashdata('success')?>", "success");
    </script>
    <?php endif?>
    <!-- END NOTIFIKASI ALERT -->

    <!--Counter js -->
    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>
    <!-- Custom tab JavaScript -->
    <script src="<?php echo base_url('assets/'); ?>js/cbpFWTabs.js"></script>
    <script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
    </script>
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/toast-master/js/jquery.toast.js"></script>
     <script src="<?php echo base_url('assets') ?>/plugins/bower_components/datatables/datatables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
       $(function() {
        $('#myTable1').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
          $(function() {
        $('#myTable2').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('.example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary m-r-10');
    </script>
    <!--Style Switcher -->
    <script src="<?php echo base_url('assets/plugins'); ?>/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<?php if ($this->session->flashdata('msg')) { @$msg= $this->session->flashdata('msg'); ?>
<script type="text/javascript">
 $(document).ready(function () {
 "use strict";
         // toat popup js
         $.toast({
             heading: '<?php echo $msg; ?>',
             //text: 'Use the predefined ones, or specify a custom position object.',
             position: 'top-right',
             loaderBg: '#fff',
             icon: 'warning',
             hideAfter: 3500,
             stack: 6
         })
    });
</script>
<?php  } ?>
<script type="text/javascript">
 
</script>
<script src="<?php echo base_url('assets/'); ?>js/jasny-bootstrap.js"></script>
    <!-- jQuery file upload -->
    <script src="<?php echo base_url('assets'); ?>/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
    
</body>
</html>