<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation">
   <div class="sidebar-nav slimscrollsidebar">
      <div class="sidebar-head">
         <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Dunia Laundry</span></h3>
      </div>
      <ul class="nav" id="side-menu">

        <li> 
          <a href="<?php echo base_url('mitra_agen/home'); ?>" class="waves-effect">
            <i class="mdi mdi-av-timer fa-fw"></i><span class="hide-menu">Beranda</span>
          </a>
        </li>

        <?php $ses = $_SESSION;?>

        <!-- KHUSUS UNTUK MITRA PREMIUM -->
        <?php if ($ses['premium'] == 1):?>
          <li>
            <a href="<?=base_url('mitra_agen/product'); ?>" class="waves-effect">
             &nbsp;<i class="fab fa-product-hunt"></i> &nbsp;<span class="hide-menu"> Produk</span>
              </a>
          </li>

          <li>
            <a href="<?=base_url('mitra_agen/layanan'); ?>" class="waves-effect">
             &nbsp;<i class="fab fa-servicestack"></i> &nbsp;<span class="hide-menu"> Layanan</span>
              </a>
          </li>
        <?php endif?>

        <li><a href="<?=base_url('mitra_agen/chat'); ?>" class="waves-effect"> &nbsp;<i class="fab fa-rocketchat"></i> &nbsp;<span class="hide-menu">Chat</span></a></li>

        <li>
            <a href="<?=base_url('mitra_agen/order'); ?>?status_pesanan=1" class="waves-effect"> 
              &nbsp;<i class="fas fa-cart-arrow-down"></i> &nbsp;<span class="hide-menu"> Pesanan</span>
            </a>
          </li>

        <li>
          <a href="<?=base_url('mitra_agen/komisi'); ?>" class="waves-effect"> 
            &nbsp;<i class="fas fa-calculator"></i> &nbsp;<span class="hide-menu"> Komisi</span>
          </a>
        </li>

        <?php if(!empty($ses['type_shop'])):?>
          <li>
            <a href="<?php echo base_url('persentasi_komisi'); ?>" class="waves-effect"> 
              &nbsp;<i class="fas fa-book"></i> &nbsp;<span class="hide-menu"> Persen Komisi</span>
            </a>
          </li>
        <?php endif?>

        <li>
          <a href="<?=base_url('mitra_agen/user/updateUser/' . $ses['id'] ); ?>" class="waves-effect"> 
            &nbsp;<i class="fas fa-user"></i> &nbsp;<span class="hide-menu"> Profile</span>
          </a>
        </li>

        <li>
          <a href="<?php echo base_url('logout'); ?>" class="waves-effect"> &nbsp;<i class="icon-logout"></i> &nbsp;<span class="hide-menu">Log out</span></a>
        </li>
      
      </ul>
   </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ==============================================================
