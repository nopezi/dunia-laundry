<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Produk</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Produk</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['item_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Produk</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Tambah Product</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Harap isi semua bidang dengan benar. </p>
                    <form data-toggle="validator" method="post" action="<?=base_url('mitra_agen/product/submitProduct'); ?>" enctype="multipart/form-data">
                        <div class="row">

                            <?php
                            $shop_id_hidden = '2' == $_SESSION['status'] ? $laundry[0]->shop_id : 'YZ65d0';
                            foreach($laundry as $s) {
                                if( $s->shop_id == @$row['shop_id'] ) {
                                    $shop_id_hidden = $s->shop_id;
                                }
                            }
                            ?>
                            <input type="hidden" name="item_id" value="<?=@$row['item_id'];?>">
                            <input type="hidden" name="shop_id" value="<?= $shop_id_hidden; ?>">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Layanan</label>
                                    <select class="form-control" name="service_id" id="service_id" required>
                                        <option value="">--Select--</option>
                                        <?php if(!empty($service)):?>
                                            <?php foreach($service as $s):?>
                                                <option value="<?=$s->service_id?>">
                                                    <?=$s->service_name?>
                                                </option>
                                            <?php endforeach?>
                                        <?php endif?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">                                    
                                    <label  class="control-label">Satuan/Kiloan</label>
                                    <select class="form-control" name="type" required>
                                        <option value="">--Select--</option>
                                        <option value="satuan" <?php if($row['type']=='satuan'){ print 'selected'; }?>>Satuan</option>
                                        <option value="kiloan" <?php if($row['type']=='kiloan'){ print 'selected'; }?>>Kiloan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Nama</label>
                                    <input type="text" class="form-control" name="item_name" value="<?=@$row['item_name'];?>" id="inputName1" placeholder="Masukan Nama" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Harga</label>
                                    <input type="text" class="form-control groupOfTexbox" name="price" value="<?= @$row['price'] ?>" id="inputName1" placeholder="Masukan harga" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="inputEmail2" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/Product/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['item_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">Semua Produk</h3>
                      <p class="text-muted m-b-30 font-13">Anda dapat melihat semua daftar produk</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <!-- <th>Outlet</th> -->
                                    <th>Layanan</th>
                                    <th>Nama Baju</th>
                                    <th>Harga</th>
                                    <th>Image</th>
                                    <th>Status</th>  
                                    <th>Kelola</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($product as $c) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?=$i?></td>
                                <td>
                                    <?=$this->Product_model->getServiceName($c->service_id);?>
                                </td>
                                <td><?php echo $c->item_name; ?></td>
                                <td><?=$c->price?></td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/Product/<?php  echo $c->image; ?>" target="_blank">
                                        <img style="width: 50px;height: 40px;" src="<?= base_url(); ?>assets/images/Product/<?php echo $c->image; ?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td>
                                    <?php if($c->status==1){ ?>
                                    <label class="badge badge-teal">Aktif</label>
                                    <?php }else if($c->status==0){ ?>
                                    <label class="badge badge-danger">Non Aktif</label>
                                    <?php  } ?> 
                                </td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Kelola<span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <li>
                                            <a title="Verified" class="<?=$c->status==1?'disabled':''?>" href="<?=base_url('mitra_agen/product/changeProductStatus');?>/<?=$c->item_id;?>">Aktifkan</a>
                                        </li>
                                        <li>
                                            <a title="Not Verified" class="<?=$c->status==0?'disabled':''?>" href="<?=base_url('mitra_agen/product/changeProductStatus');?>/<?=$c->item_id;?>" >Non Aktifkan</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a title="Edit" href="<?=base_url()?>mitra_agen/product/updateProduct/<?=$c->item_id; ?>">Edit</a>
                                        </li>
                                     </ul> 
                                  </div>
                              </td>

                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>