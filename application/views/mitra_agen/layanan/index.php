<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Layanan</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Layanan</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <?php 
                    if(@$row['service_id'] == '')
                    {
                    ?>
                        <h3 class="box-title m-b-0">Tambah Layanan</h3>
                    <?php
                    }else{
                    ?>
                        <h3 class="box-title m-b-0">Update Layanan</h3>
                    <?php }?>
                    
                  <p class="text-muted m-b-30 font-13">Harap isi semua bidang dengan benar.</p>
                    <form data-toggle="validator" method="post" action="<?=base_url('mitra_agen/layanan/submitService'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label  class="control-label">Nama Layanan</label>
                                    <input type="hidden" name="shop_id" value="<?=$shop_id?>">
                                    <input type="hidden" name="service_id" value="<?=@$row['service_id'];?>">
                                    <input type="text" class="form-control" name="service_name" value="<?=@$row['service_name'];?>" id="service_name" placeholder="Masukan Nama Layanan" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label  class="control-label">Deskripsi</label>
                                    <textarea class="form-control" name="description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file"id="image" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/Service/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['service_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">Daftar Layanan</h3>
                      <p class="text-muted m-b-30 font-13">Anda dapat melihat semua daftar layanan</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Image</th>
                                    <th>Status</th>  
                                    <th>Kelola</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; foreach ($service as $b): $i++;?> 
                                <tr>
                                    <td><?=$i?></td>
                                    <td><?=$b->service_name?></td>
                                    <td><?=$b->description?></td>
                                    <td>
                                        <a href="<?=base_url(); ?>assets/images/Service/<?php  echo $b->image; ?>" target="_blank">
                                            <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/Service/<?php  echo $b->image; ?>" alt="Image not Available" />
                                        </a>    
                                    </td>
                                    <td>
                                        <?php if($b->status==1){ ?>
                                        <label class="badge badge-teal">Aktifkan</label>
                                        <?php }else if($b->status==0){ ?>
                                        <label class="badge badge-danger">Non Aktifkan</label>
                                        <?php  } ?> 
                                    </td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Kelola<span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a title="Verified" class="<?=$b->status==1?'disabled':''?>" href="<?=base_url('mitra_agen/layanan/changeServiceStatus');?>/<?=$b->service_id;?>">Aktifkan</a>
                                                </li>
                                                <li>
                                                    <a title="Not Verified" class="<?=$b->status==0?'disabled':''?>" href="<?=base_url('mitra_agen/layanan/changeServiceStatus');?>/<?php echo $b->service_id;?>" >Non Aktifkan</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a title="Edit" href="<?=base_url()?>mitra_agen/layanan/updateService/<?=$b->service_id?>">Edit</a>
                                                </li>
                                                <li>
                                                    <a title="Edit" href="<?=base_url()?>mitra_agen/layanan/hapus/<?=$b->service_id?>">Hapus</a>
                                                </li>
                                            </ul> 
                                        </div>
                                    </td>

                                </tr>
                             <?php endforeach?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>