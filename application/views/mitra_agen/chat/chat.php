<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Semua Chat</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Chat</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">Semua Chat</h3>
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>USER NAME</th>
                              <th>OUTLET NAME</th>
                              <th>LIHAT</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>USER NAME</th>
                                <th>OUTLET NAME</th>
                                <th>LIHAT</th>                          
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php 
                        $i=0; foreach($chat as $u) 
                        { 
                          $i++;
                          $user = $this->Chat_model->getUserDetail($u->from_user_id);
                          $shop = $this->Chat_model->getShopDetail($u->to_user_id);
                          ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?=@$user->name;?></td>
                                <td><?=@$shop->shop_name?></td>
                                <td>
                                  <button class="btn dropdown-toggle" type="button">
                                    <a title="Message" href="<?=base_url('mitra_agen/chat/chatOwner');?>/<?=$u->message_head_id;?>/<?=$u->from_user_id;?>/<?=$u->to_user_id;?>" >Message</a>
                                  </button>
                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
.chat-list li {
    margin-bottom: 24px;
    /* overflow: auto; */
}
</style>
<script type="text/javascript"> 

function setLaundryOwnerMsg(message_head_id,sno)
{
  var to_user_id = $("#to_user_id"+sno).val();
  var from_user_id = $("#from_user_id"+sno).val();
  var message = $("#message"+sno).val();
  if(message == '')
  {
    alert("Please type any msg..");
  }else{

    $.ajax({
    type: "POST",
    url: "<?=base_url();?>mitra_agen/chat/setLaundryOwnerMsg",
    data: "message_head_id="+message_head_id+"&to_user_id="+to_user_id+"&from_user_id="+from_user_id+"&message="+message,
    success: function(data) 
    {
      // location.reload();
    }
  });
    
  }
  
}
</script>