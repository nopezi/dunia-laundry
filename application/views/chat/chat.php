<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Semua Chat</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Chat</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">Semua Chat</h3>
                <div class="table-responsive" style="overflow: auto;">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>USER NAME</th>
                              <th>OUTLET NAME</th>
                              <th>LIHAT</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>USER NAME</th>
                                <th>OUTLET NAME</th>
                                <th>LIHAT</th>                          
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php 
                        $i=0; foreach($chat as $u) 
                        { 
                          $i++;
                          $user = $this->Chat_model->getUserDetail($u->from_user_id);
                          $shop = $this->Chat_model->getShopDetail($u->to_user_id);
                          ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?=@$user->name;?></td>
                                <td><?=@$shop->shop_name?></td>
                                <td>
                                  <button class="btn dropdown-toggle" type="button">
                                    <a title="Message" href="<?php echo base_url('chat/chatOwner');?>/<?php echo $u->message_head_id;?>/<?php echo $u->from_user_id;?>/<?php echo $u->to_user_id;?>" >Message</a>
                                  </button>
                                  <!-- <div class="white-box">
                                  <button class="btn dropdown-toggle" type="button" data-toggle="modal" data-target="#myModal<?php echo $i; ?>" class="model_img img-responsive" >Message</button>
                                    <div id="myModal<?php echo $i; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="panel panel-themecolor">
                                                <div class="modal-header" style="background-color: #2cabe3;">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                  <h3 class="modal-title" id="myModalLabel">Chat Message</h3> 
                                                </div>
                                                <div class="panel-wrapper collapse in" aria-expanded="true" role="dialog">
                                                    <div class="panel-body">
                                                        <div class="chat-box" style="height: 300px;">
                                                            <ul class="chat-list slimscroll" style="overflow: hidden;" tabindex="5005">
                                                              <?php
                                                              $msg_data = $this->Chat_model->getAllMessageData($u->message_head_id);
                                                              foreach ($msg_data as $md) 
                                                              {
                                                                if($md->from_user_id == $u->from_user_id)
                                                                {

                                                              ?>
                                                                <li>
                                                                  <div class="chat-image">
                                                                    <?php
                                                                    if($user->image !='')
                                                                    {
                                                                      ?>
                                                                      <img src="<?=base_url();?>assets/images/user/<?=$user->image;?>" alt="user" class="img-circle" height="30">
                                                                      <?php
                                                                    }else{
                                                                      ?>
                                                                      <img src="<?=base_url();?>assets/images/user/default.png" alt="user" class="img-circle" height="30">
                                                                      <?php
                                                                    }
                                                                    ?> 
                                                                  </div>
                                                                  <div class="chat-body">
                                                                      <div class="chat-text">
                                                                          <h4><?=$user->name;?></h4>
                                                                          <p><?=$md->message;?></p> 
                                                                          <b><?=date('h:i:s a d/m/Y', strtotime($md->created_at));?></b> </div>
                                                                  </div>
                                                                </li>
                                                                <?php
                                                                  } else{
                                                                ?>
                                                                <li class="odd">
                                                                  <div class="chat-body">
                                                                      <div class="chat-text">
                                                                          <p> <?=$md->message;?> </p> <b><?=date('h:i:s a d/m/Y', strtotime($md->created_at));?></b> </div>
                                                                  </div>
                                                                </li>
                                                                <?php
                                                                } 
                                                              }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                              <input type="hidden" id="to_user_id<?=$i;?>" value="<?=$u->from_user_id?>">
                                                              <input type="hidden" id="from_user_id<?=$i;?>" value="<?=$u->to_user_id?>">
                                                                <textarea placeholder="Type your message here" name="message" id="message<?php echo $i; ?>" class="chat-box-input"></textarea>
                                                            </div>
                                                            <div class="col-xs-4 text-right">
                                                                <button class="btn btn-success btn-circle btn-xl" type="button" onclick="setLaundryOwnerMsg(<?=$u->message_head_id?>,<?php echo $i; ?>);"><i class="fas fa-paper-plane"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div> -->
                                </td>
                            </tr>
                         <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
.chat-list li {
    margin-bottom: 24px;
    /* overflow: auto; */
}
</style>
<script type="text/javascript"> 

function setLaundryOwnerMsg(message_head_id,sno)
{
  var to_user_id = $("#to_user_id"+sno).val();
  var from_user_id = $("#from_user_id"+sno).val();
  var message = $("#message"+sno).val();
  if(message == '')
  {
    alert("Please type any msg..");
  }else{

    $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>chat/setLaundryOwnerMsg",
    data: "message_head_id="+message_head_id+"&to_user_id="+to_user_id+"&from_user_id="+from_user_id+"&message="+message,
    success: function(data) 
    {
      // location.reload();
    }
  });
    
  }
  
}
</script>