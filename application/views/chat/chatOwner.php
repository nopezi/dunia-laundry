<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Chat</h4> </div>
        <!-- /.col-lg-12 -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Chat</li>
                </ol>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
      <div class="col-lg-3 col-md-12 col-sm-12">
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="panel panel-themecolor">
                            <div class="panel-heading"> CHATBOX
                                <div class="pull-right"> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true" role="dialog">
                                <div class="panel-body">
                                    <div class="chat-box" style="height: 300px;">
                                        <ul class="chat-list slimscroll" style="overflow: hidden;" tabindex="5005">
                                          <?php
                                          foreach ($msg_data as $md) 
                                          {
                                            if($md->from_user_id == $from_user_id)
                                            {
                                              $user = $this->Chat_model->getUserDetail($from_user_id);
                                          ?>
                                            <li>
                                              <div class="chat-image">
                                                <?php
                                                if(isset($user->image) && $user->image !='')
                                                { 
                                                  ?>
                                                  <img src="<?=base_url();?>assets/images/user/<?= str_replace('assets/images/user/', '', (($user) ? $user->image : "..."));?>" alt="user" class="img-circle" height="30">
                                                  <?php
                                                }else{
                                                  ?>
                                                  <img src="<?=base_url();?>assets/images/user/default.png" alt="user" class="img-circle" height="30">
                                                  <?php
                                                }
                                                ?> 
                                              </div>
                                              <div class="chat-body">
                                                  <div class="chat-text">
                                                      <h4><?= ($user) ? $user->name : "Nama kosong";?></h4>
                                                      <p><?=$md->message;?></p> 
                                                      <b><?=date('h:i:s a d/m/Y', strtotime($md->created_at));?></b> </div>
                                              </div>
                                            </li>
                                            <?php
                                              } else{
                                            ?>
                                            <li class="odd">
                                              <div class="chat-body">
                                                  <div class="chat-text">
                                                      <!-- <h4>Admin</h4> -->
                                                      <p> <?=$md->message;?> </p> <b><?=date('h:i:s a d/m/Y', strtotime($md->created_at));?></b> </div>
                                              </div>
                                            </li>
                                            <?php
                                            } 
                                          }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                      <div class="col-xs-8">
                                        <input type="hidden" id="to_user_id" value="<?=$from_user_id?>">
                                        <input type="hidden" id="from_user_id" value="<?=$to_user_id?>">
                                          <textarea placeholder="Type your message here" name="message" id="message" class="chat-box-input"></textarea>
                                      </div>
                                      <div class="col-xs-4 text-right">
                                          <button class="btn btn-success btn-circle btn-xl" type="button" onclick="setLaundryOwnerMsg(<?=$message_head_id?>);"><i class="fas fa-paper-plane"></i></button>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
.chat-list li {
    margin-bottom: 24px;
    /* overflow: auto; */
}
</style>
<script type="text/javascript"> 

function setLaundryOwnerMsg(message_head_id)
{
  var to_user_id = $("#to_user_id").val();
  var from_user_id = $("#from_user_id").val();
  var message = $("#message").val();
  if(message == '')
  {
    alert("Please type any msg..");
  }else{

    $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>chat/setLaundryOwnerMsg",
    data: "message_head_id="+message_head_id+"&to_user_id="+to_user_id+"&from_user_id="+from_user_id+"&message="+message,
    success: function(data) 
    {
      // alert(data);
      location.reload();
    }
  });
    
  }
  
}
</script>