<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Franchise</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                <a href="<?=base_url('admin/franchise/tambah')?>" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;"><i class="ti-user"> </i> Tambah Data Franchise
                </a>
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>

                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Image</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>No WA/HP</th>
                              <th>Wilayah</th>
                              <th>Kecamatan</th>
                              <th>Alamat</th>
                              <th>Status</th>
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No WA/HP</th>
                                <th>Wilayah</th>
                                <th>Kecamatan</th>
                                <th>Alamat</th>
                                <th>Status</th>
                                <th>Kelola</th>                           
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php $i=1; foreach($user as $u):?> 
                            <tr>
                                <td><?=$i++; ?></td>
                                <td>
                                  <a href="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" target="_blank">
                                  <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" alt="Image not Available" />
                                </a>
                                </td>
                                <td><?=$u->name?></td>       
                                <td><?=$u->email?></td>
                                <td><?=$u->mobile?></td>
                                <td><?=$u->wilayah?></td>
                                <td><?=$u->kecamatan?></td>
                                <td><?=$u->address?></td>
                                <td>
                                  <?php if($u->status==1){ ?>
                                  <label class="badge badge-teal">Aktif</label>
                                  <?php }else if($u->status==0){ ?>
                                  <label class="badge badge-danger">Non Aktif</label>
                                  <?php } ?> 
                                </td>
                                <td class="text-center">
                                  <div class="dropdown">

                                    <select class="form-control" onchange="location = this.value;">
                                      <option disabled="" selected="">Kelola</option>
                                    
                                      <option value="<?=base_url('admin/franchise/ganti_status/');?><?=$u->user_id;?>" <?=$u->status==1?'disabled':''?>>
                                        Aktifkan
                                      </option>

                                      <option value="<?=base_url('admin/franchise/ganti_status/');?><?=$u->user_id;?>" <?=$u->status==0?'disabled':''?>>
                                        Non Aktifkan
                                      </option>

                                      <option value="<?=base_url('admin/franchise/update/');?><?=$u->user_id;?>">
                                        Update
                                      </option>

                                      <option value="<?=base_url('admin/franchise/detail/');?><?=$u->user_id;?>" >
                                        Lihat Detail
                                      </option>

                                      <option value="<?=base_url('admin/franchise/delete/');?><?=$u->user_id;?>">
                                        Hapus
                                      </option>

                                    </select>
                                  </div>
                                </td>
                            </tr>
                         <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>