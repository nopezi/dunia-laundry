<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Franchise</h4> </div>
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <!-- <a href="laundry/laundryOwnerAdd" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;">
                <i class="ti-plus"> </i> Tambah Data Franchise
              </a> -->
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="float: right;">
                <i class="ti-plus"> </i> Franchise
              </button>
          </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
              
              <?php if( $this->session->flashdata('success') ): ?>
                <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success'); ?></div>
              <?php endif; ?>

              <?php if( $this->session->flashdata('error') ): ?>
                <div class="alert alert-danger" role="alert"><?php echo $this->session->flashdata('error'); ?></div>
              <?php endif; ?>

                <h3 class="box-title m-b-0">Data Franchise</h3>
                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>No.</th>
                              <th>Image</th>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>No WA/HP</th>
                              <th>Alamat</th>
                              <th>Type</th>
                              <th>Status</th>
                              <th>Kelola</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No WA/HP</th>
                                <th>Alamat</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Kelola</th>                           
                            </tr>
                        </tfoot>
                        <tbody>
                      <?php if(!empty($user)):?>
                        <?php 
                        $i=0; foreach($user as $u):
                        $i++; 
                        ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                  <a href="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" target="_blank">
                                  <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/user/<?php  echo $u->image; ?>" alt="Image not Available" />
                                </a>
                                </td>
                                <td><?php echo $u->name; ?></td>       
                                <td><?php echo $u->email; ?></td>
                                <td><?=$u->mobile?></td>
                                <td><?php echo $u->address; ?></td>
                                <td>
                                  <?php if($u->type==1){ ?>
                                    Agen
                                  <?php }else if($u->type==2){ ?>
                                    Mitra
                                  <?php  } ?> 
                                </td>
                                <td>
                                  <?php if($u->status==1){ ?>
                                  <label class="badge badge-teal">Aktif</label>
                                  <?php }else if($u->status==0){ ?>
                                  <label class="badge badge-danger">Non Aktif</label>
                                  <?php  } ?> 
                                </td>
                                <td class="text-center">
                                  <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Kelola<span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li>
                                      <a title="Verified" class="<?=$u->status==1?'disabled':''?>" href="<?php echo base_url('laundry/changeLaundryOwnerStatus');?>/<?php echo $u->user_id;?>">Aktifkan</a>
                                    </li>
                                    <li>
                                      <a title="Not Verified" class="<?=$u->status==0?'disabled':''?>" href="<?php echo base_url('laundry/changeLaundryOwnerStatus');?>/<?php echo $u->user_id;?>" >Non Aktifkan</a>
                                    </li>
                                    <li>
                                      <a title="Update" href="<?php echo base_url('laundry/updateLaundryOwner');?>/<?php echo $u->user_id;?>" >Update</a>
                                    </li>
                                    <li>
                                      <a title="View Detail" href="<?php echo base_url('laundry/viewOwnerDetail');?>/<?php echo $u->user_id;?>" >Lihat Detail</a>
                                      <!-- <a title="Payment history" href="<?php echo base_url('laundry/paymentHistory');?>/<?php echo $u->user_id;?>" >Payment history</a> -->
                                    </li>
                                    <li>
                                      <a title="Delete" onClick="return confirm('Apakah anda yakin menghapus data ini ?')" href="<?php echo base_url('laundry/deleteLaundryOwner');?>/<?php echo $u->user_id;?>" >Hapus</a>
                                    </li>
                                    </ul>
                                  </div>
                                </td>
                            </tr>
                         <?php endforeach ?>
                      <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah User Franchise</h4>
          </div>
          <div class="modal-body">
            
            <form method="post" action="">
              
              <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" class="form-control">
              </div>

              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control">
              </div>

              <div class="form-group">
                <label>No HP</label>
                <input type="number" name="mobile" class="form-control">
              </div>

              <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" name="address" rows="5" cols="100"></textarea>
              </div>

              <div class="form-group">
                <label>Provinsi</label>
                <select id="provinsi" class="form-control" required="">
                  <option value="">Pilih Provinsi</option>
                <?php if (!empty($provinsi)):?>
                  <?php foreach($provinsi as $p):?>
                    <option value="<?=$p->id?>"><?=$p->name?></option>
                  <?php endforeach?>
                <?php endif?>
                </select>
              </div>

              <div class="form-group">
                <label>PIlih coba</label>
                <select id="benih" class="form-control">
                    <option lama="6 bulan" berat="1kg" harga="Rp 45.000">Kacang</option>
                    <option lama="5 bulan" berat="100kg" harga="Rp 700.000">Padi</option>
                    <option lama="4 bulan" berat="2kg" harga="Rp 10.000">Semangka</option>
                    <option lama="5 bulan" berat="3kg" harga="Rp 80.000">Semangka</option>
                    <option lama="3 bulan" berat="4kg" harga="Rp 4.500">Timun</option>
                </select>
              </div>

              <div class="form-group">
                <label>tes</label>
                <input type="text" name="" class="form-control" id="lama-tanam">
              </div>

              <div class="form-group">
                <label>berat</label>
                <input type="text" name="" class="form-control" id="berat">
              </div>

              <div class="form-group">
                <label>harga</label>
                <input type="text" name="" class="form-control" id="harga">
              </div>

            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>

</div>

<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>