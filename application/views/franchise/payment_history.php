<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Payment History</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Payment History</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Payment History list</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Payment History List</p>
                        <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a href="<?=base_url('admin/pemilik')?>">
                                    <span> All Laundry Mitra & Agen</span>
                                </a>
                            </li>
                            <li role="presentation" >
                                <a href="<?=base_url('admin/franchise/detail/' . $owner->user_id); ?>" >
                                    <span>View</span>
                                </a>
                            </li>
                            <li role="presentation" class="active">
                                <a href="<?=base_url('admin/franchise/payment_history/' . $owner->user_id); ?>" aria-controls="assign_class" role="tab" data-toggle="tab" aria-expanded="true">
                                    <span>Payment history</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="<?=base_url('admin/franchise/owner_packages/' . $owner->user_id); ?>">
                                    <span>Owner</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="assign_class">
                                <div class="row">

                                <div class="table-responsive" style="overflow: auto;">
                                    <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Date</th>
                                                <th>Payment id</th>
                                                <th>Package Title</th>
                                                <th>No of days</th>
                                                <th>End subcription date</th>
                                                <th>Amount</th>
                                                <th>Payment Status</th>  
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=0; 
                                        foreach ($history as $b) 
                                        { 
                                            $i++; 
                                            $pkg = $this->Payment_model->getPackageDetail($b->package_id);
                                        ?> 
                                        
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?=date('d-m-Y h:i:s a', strtotime($b->created_at));?></td>
                                            <td><?=$b->payment_id;?></td>
                                            <td><?php echo $pkg['title']; ?></td>
                                            <td><?=$pkg['no_of_days'];?></td>
                                            <td><?=$b->end_subscription_date?></td>
                                            <td><?=$b->amount?></td>
                                            <td>
                                                <?php if($b->payment_status==1){ ?>
                                                <label class="badge badge-teal">Sucess</label>
                                                <?php }else if($b->payment_status==0){ ?>
                                                <label class="badge badge-danger">Fail</label>
                                                <?php  } ?> 
                                            </td>

                                            </tr>
                                         <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>