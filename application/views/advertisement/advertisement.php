<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Advertisement</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Advertisement</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <h4 style="color: red;"><?php echo $this->session->flashdata('error_red'); ?></h4>
                    <?php
                    if(@$row['advertisement_id'] !='')
                    {
                    ?>
                    <h3 class="box-title m-b-0">Update Advertisement</h3>
                    <?php
                    }else{
                    ?>
                    <h3 class="box-title m-b-0">Add Advertisement</h3>
                    <?php
                    }
                    ?>
                  <p class="text-muted m-b-30 font-13">Please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('advertisement/submitAdvertisement'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Title</label>
                                    <input type="hidden" name="advertisement_id" value="<?=@$row['advertisement_id'];?>">
                                    <input type="text" class="form-control" name="title" value="<?=@$row['title'];?>" id="inputName1" placeholder="Enter title" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Url</label>
                                    <input type="text" class="form-control" name="url" value="<?=@$row['url'];?>" placeholder="Enter url" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Short description</label>
                                    <input type="text" class="form-control" name="short_description" value="<?=@$row['short_description'];?>" id="inputName1" placeholder="Enter short description" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Long description</label>
                                    <textarea class="form-control" name="long_description" placeholder="Enter long description" required><?=@$row['long_description'];?></textarea>
                                    
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file" id="image" name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="form-group">
                                    
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/advertise/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['advertisement_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Advertisement</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Advertisement List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Title</th>
                                    <th>Url</th>
                                    <th>Short description</th>
                                    <th>Image</th>
                                    <th>Status</th>  
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($advertisement as $c) 
                            { 
                                $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $c->title; ?></td>
                                <td>
                                    <a href="<?=$c->url?>" target="_blank"><?=$c->url?></a>
                                </td>
                                <td><?php echo $c->short_description; ?></td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/advertise/<?php  echo $c->image; ?>" target="_blank">
                                        <img style="width: 50px;height: 40px;" src="<?= base_url(); ?>assets/images/advertise/<?php echo $c->image; ?>" alt="Image not Available" />
                                    </a>
                                </td>
                                <td>
                                    <?php if($c->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($c->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                    <li>
                                        <a title="Verified" class="<?=$c->status==1?'disabled':''?>" href="<?php echo base_url('advertisement/changeAdvertisementStatus');?>/<?php echo $c->advertisement_id;?>">Activate</a>
                                    </li>
                                    <li>
                                        <a title="Not Verified" class="<?=$c->status==0?'disabled':''?>" href="<?php echo base_url('advertisement/changeAdvertisementStatus');?>/<?php echo $c->advertisement_id;?>" >Deactivate</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>advertisement/updateAdvertisement/<?php echo $c->advertisement_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                  </div>
                              </td>

                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>