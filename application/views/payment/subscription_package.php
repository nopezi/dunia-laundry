<!-- Page Content -->
<!-- ============================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
// jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.groupOfTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            // (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // Check minus and only once.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // Check dot and only once.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
</script>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Subscription Package</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Subscription Package</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h5 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h5>
                    <?php 
                    if(@$row['package_id'] == '')
                    {
                    ?>
                        <h3 class="box-title m-b-0">Add Subscription Package</h3>
                    <?php
                    }else{
                    ?>
                        <h3 class="box-title m-b-0">Update Subscription Package</h3>
                    <?php }?>
                    
                  <p class="text-muted m-b-30 font-13">please fill all of fields Properly </p>
                    <form data-toggle="validator" method="post" action="<?php echo base_url('package/submitSubscriptionPackage'); ?>" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Title</label>
                                    <input type="hidden" name="package_id" value="<?=@$row['package_id'];?>">
                                    <input type="text" class="form-control" name="title" value="<?=@$row['title'];?>" id="title" placeholder="Enter title" required> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">No of Days</label>
                                    <input type="text" class="form-control groupOfTexbox" name="no_of_days" value="<?=@$row['no_of_days'];?>" placeholder="Enter No of Days" maxlength="4" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Amount</label>
                                    <input type="text" class="form-control groupOfTexbox" name="amount" value="<?=@$row['amount'];?>" placeholder="Enter amount" maxlength="10" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Description</label>
                                    <textarea class="form-control" name="description" required><?=@$row['description'];?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label  class="control-label">Image</label>
                                       <input type="file"id="image"s name="image" accept="image/*" <?php if(@$row['image'] ==''){ echo "required"; }else{} ?>>
                                    <div class="help-block with-errors"></div>
                                    <?php
                                    if(@$row['image'] !='')
                                    {
                                    ?>
                                    <img src="<?=base_url()?>assets/images/package/<?=@$row['image']?>" alt="" class="d-flex align-self-start rounded mr-3" height="60">
                                    <?php
                                    }else{}
                                    ?>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <?php 
                            if(@$row['package_id'] == '')
                            {
                            ?>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            <?php
                            }else{
                            ?>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php }?>
                        </div>
                    </form>
                </div>
            </div>
        <!-- </div> -->
        <!-- /.row -->
        <!-- ============================================================== -->

        <!-- /row -->
        <!-- <div class="row"> -->
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">All Subscription Package list</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of Subscription Package List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>No of days</th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Status</th>  
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($package as $b) { $i++; 
                            ?> 
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <a href="<?= base_url(); ?>assets/images/package/<?php  echo $b->image; ?>" target="_blank">
                                        <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?= base_url(); ?>assets/images/package/<?php  echo $b->image; ?>" alt="Image not Available" />
                                    </a>    
                                </td>
                                <td><?php echo $b->title; ?></td>
                                <td><?=$b->no_of_days;?></td>
                                <td><?=$b->amount?></td>
                                <td><?=$b->description;?></td>
                                <td>
                                    <?php if($b->status==1){ ?>
                                    <label class="badge badge-teal">Active</label>
                                    <?php }else if($b->status==0){ ?>
                                    <label class="badge badge-danger">Deactive</label>
                                    <?php  } ?> 
                                </td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Manage<span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                    <li>
                                        <a title="Verified" class="<?=$b->status==1?'disabled':''?>" href="<?php echo base_url('package/changeSubscriptionPackageStatus');?>/<?php echo $b->package_id;?>">Activate</a>
                                    </li>
                                    <li>
                                        <a title="Not Verified" class="<?=$b->status==0?'disabled':''?>" href="<?php echo base_url('package/changeSubscriptionPackageStatus');?>/<?php echo $b->package_id;?>" >Deactivate</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a title="Edit" href="<?=base_url()?>package/updateSubscriptionPackage/<?php echo $b->package_id; ?>">Edit</a>
                                    </li>
                                     </ul> 
                                  </div>
                              </td>

                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>