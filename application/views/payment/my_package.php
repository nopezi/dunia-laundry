<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">My Package</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">My Package</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                     <h3 class="box-title m-b-0">My Package list</h3>
                      <p class="text-muted m-b-30 font-13">You can see all details of My Package List</p>
                    <div class="table-responsive" style="overflow: auto;">
                        <table id="myTable2" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Payment id</th>
                                    <th>Date</th>
                                    <th>Owner Name</th>
                                    <th>Package Title</th>
                                    <th>No of days</th>
                                    <th>End subscription date</th>
                                    <th>Amount</th>
                                    <th>Payment Status</th>  
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=0; 
                            foreach ($invoice as $b) 
                            { 
                                $i++; 
                                $pkg = $this->Payment_model->getPackageDetail($b->package_id);
                            ?> 
                            
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?=$b->payment_id?></td>
                                <td><?=date('d-m-Y h:i:s a', strtotime($b->created_at));?></td>
                                <td><?=$this->User_model->getUserName($b->user_id);?></td>
                                <td><?php echo $pkg['title']; ?></td>
                                <td><?=$pkg['no_of_days'];?></td>
                                <td><?=$b->end_subscription_date?></td>
                                <td><?=$b->amount?></td>
                                <td>
                                    <?php if($b->payment_status==1){ ?>
                                    <label class="badge badge-teal">Sucess</label>
                                    <?php }else if($b->payment_status==0){ ?>
                                    <label class="badge badge-danger">Fail</label>
                                    <?php  } ?> 
                                </td>

                                </tr>
                             <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    </div>
<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>