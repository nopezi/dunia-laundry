<!-- Page Content -->
<!-- ============================================================== -->

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Subscription Package</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Subscription Package</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <?php
        // $no_of_days = '10';
        // $no = $no_of_days+'1';
        // $date = date('Y-m-d', strtotime("+$no days"));
        // echo $date;
        ?>
        <div class="row">
        	<?php
        	foreach ($package as $p) 
        	{
        		
        	$cur = $this->Currency_model->getActiveCurrency();
        	?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <div class="product-img">
                        <img style="width: 150px;height: 200px;" src="<?= base_url(); ?>assets/images/package/<?php  echo $p->image; ?>" />
                        <div class="pro-img-overlay"><a href="javascript:void(0)" class="bg-info buy_now" data-days="<?=$p->no_of_days?>" data-amount="<?=$p->amount?>" data-id="<?=$p->package_id?>"><i class="ti-shopping-cart"></i></a></div>
                    </div>
                    <div class="product-text">
                        <span class="pro-price bg-danger"><?=$cur->currency_symbol;?> <?=$p->amount?></span>
                        <h3 class="box-title m-b-0"><?=$p->no_of_days?> Days</h3>
                        <h3 class="box-title m-b-0"><?=$p->title?></h3>
                        <small class="text-muted db"><?=$p->description?></small>
                    </div>
                </div>
            </div>
            <?php
        	}
            ?>
            
        </div>

        
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>     
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var SITEURL = "<?php echo base_url() ?>";
  $('body').on('click', '.buy_now', function(e){
  	var no_of_days = $(this).attr("data-days");
    var totalAmount = $(this).attr("data-amount");
    var package_id =  $(this).attr("data-id");
    var options = {
    // "key": "rzp_test_WU7Kfj9LTTV6i6",
    "key": "<?=$razorpay['razorpay_key'];?>",
    "amount": (Number(totalAmount)*100), // 2000 paise = INR 20
    "name": "Tutsmake",
    "description": "Payment",
    "image": "https://www.tutsmake.com/wp-content/uploads/2018/12/cropped-favicon-1024-1-180x180.png",
    "handler": function (response){
    	// alert(response.razorpay_payment_id);
          $.ajax({
            url: SITEURL + 'payment/razorPaySuccess',
            type: 'post',
            data: {
                razorpay_payment_id: response.razorpay_payment_id , totalAmount : totalAmount ,package_id : package_id, no_of_days : no_of_days,
            }, 
            success: function (msg) {
 				// alert(msg);
               swal("Success", "Payment successfully", "success");
            }
        });
      
    },
 
    "theme": {
        "color": "#528FF0"
    }
  };
  var rzp1 = new Razorpay(options);
  rzp1.open();
  e.preventDefault();
  });
 
</script>