  <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-danger"><i class="fas fa-user"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$user_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Pengguna</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$user_count?></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-warning"><i class="fas fa-user"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$owner_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Pemilik</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$owner_count?>% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-success"><i class="mdi mdi-account-multiple-plus fa-fw"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$shop_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Outlet</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$shop_count?>% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-info"><i class="mdi mdi-cash fa-fw"></i></span>
                                        </center>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$income_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Income</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only">40% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
               

            <?php if($_SESSION['role'] == 'hide'):?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Products Yearly Sales</h3>
                            <ul class="list-inline text-right">
                                <li>
                                    <h5><i class="fa fa-circle m-r-5 text-info"></i>Mac</h5>
                                </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5 text-danger"></i>Windows</h5>
                                </li>
                            </ul>
                            <div id="ct-visits" style="height: 285px;"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
                        <div class="bg-theme-alt">
                            <div id="ct-daily-sales" class="p-t-30" style="height: 300px"></div>
                        </div>
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2 class="m-b-0 font-medium">Week Sales</h2>
                                    <h5 class="text-muted m-t-0">Ios app - 160 sales</h5>
                                </div>
                                <div class="col-xs-4">
                                    <div class="circle circle-md bg-info pull-right m-t-10"><i class="ti-shopping-cart"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                        <div class="bg-theme white-box m-b-0">
                            <ul class="expense-box">
                                <li><i class="wi wi-day-cloudy text-white"></i>
                                    <div>
                                        <h1 class="text-white m-b-0">35<sup>o</sup></h1>
                                        <h4 class="text-white">Clear and sunny</h4>
                                    </div>
                                </li>
                            </ul>
                            <div id="ct-weather" style="height: 120px"></div>
                            <ul class="dp-table text-white">
                                <li>05 AM</li>
                                <li>10 AM</li>
                                <li>03 PM</li>
                                <li>08 PM</li>
                            </ul>
                        </div>
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2 class="m-b-0 font-medium">Sunday</h2>
                                    <h5 class="text-muted m-t-0">March 2017</h5>
                                </div>
                                <div class="col-xs-4">
                                    <div class="circle circle-md bg-success pull-right m-t-10"><i class="wi wi-day-sunny"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- Sales different chart widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-lg-6 col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h2 class="m-b-0 font-medium">4567</h2>
                                    <h5 class="text-muted m-t-0">Yearly Sales</h5>
                                </div>
                                <div class="col-sm-8">
                                    <div id="ct-main-bal" style="height: 70px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-3">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2 class="m-b-0 font-medium">$354.50</h2>
                                    <h5 class="text-muted m-t-0">Total Income</h5>
                                </div>
                                <div class="col-sm-6">
                                    <div id="ct-bar-chart" class="pull-right"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-3">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2 class="m-b-0 font-medium">356</h2>
                                    <h5 class="text-muted m-t-0">Monthly Sales</h5>
                                </div>
                                <div class="col-sm-6">
                                    <div id="ct-extra" style="height: 70px" class="pull-right"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            <?php endif?>
            
                <!-- ============================================================== -->
                <!-- Demo table -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">DAFTAR PENGGUNA</div>
                            <div class="table-responsive">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 70px">#</th>
                                            <th>PHOTO</th>
                                            <th>NAMA</th>
                                            <th>EMAIL</th>
                                            <th>DAFTAR</th>
                                            <!-- <th style="width: 300px">MANAGE</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i=0;
                                        foreach($user as $u)
                                        {
                                            $i++;
                                            $date = explode(" ",$u->created_at);
                                            $time = $date[1];
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$i;?></td>
                                            <td>
                                                <?php
                                                if($u->image !='')
                                                {
                                                    ?>
                                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?=base_url();?>assets/images/user/<?=$u->image;?>" alt="Image not Available">
                                                    <?php
                                                }else{
                                                    ?>
                                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?=base_url();?>assets/images/user/default.png" alt="Image not Available">
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td><span class="font-medium"><?=$u->name?></span>
                                                <br/><span class="text-muted"><?=$u->address?></span></td>
                                            <td><?=$u->email?>
                                                <br/><span class="text-muted"><?=$u->mobile?></span></td>
                                            <td><?=date("d-m-Y", strtotime($u->created_at))?>
                                                <br/><span class="text-muted"><?=date('h:i:s a', strtotime($time));?></span></td>
                                            <!-- <td>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-key"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="icon-trash"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil-alt"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-20"><i class="ti-upload"></i></button>
                                            </td> -->
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->