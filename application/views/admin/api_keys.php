<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">All Api Keys</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">All Api Keys</h3>

                <svg class="hidden">
                    <defs>
                        <path id="tabshape" d="M80,60C34,53.5,64.417,0,0,0v60H80z" /> 
                    </defs>
                </svg>
                <section class="m-t-40">
                    <div class="sttabs tabs-style-shape">
                        <nav>
                            <ul>
                                <li>
                                    <a href="#section-shape-1">
                                        <svg viewBox="0 0 80 60" preserveAspectRatio="none">
                                            <use xlink:href="#tabshape"></use>
                                        </svg> <span>Firebase Key</span> </a>
                                </li>
                                <li>
                                    <a href="#section-shape-2">
                                        <svg viewBox="0 0 80 60" preserveAspectRatio="none">
                                            <use xlink:href="#tabshape"></use>
                                        </svg>
                                        <svg viewBox="0 0 80 60" preserveAspectRatio="none">
                                            <use xlink:href="#tabshape"></use>
                                        </svg> <span>MSG91 Key</span> </a>
                                </li>
                                <li>
                                    <a href="#section-shape-3">
                                        <svg viewBox="0 0 80 60" preserveAspectRatio="none">
                                            <use xlink:href="#tabshape"></use>
                                        </svg>
                                        <svg viewBox="0 0 80 60" preserveAspectRatio="none">
                                            <use xlink:href="#tabshape"></use>
                                        </svg> <span>Razorpay Key</span> </a>
                                </li>
                                
                            </ul>
                        </nav>
                        <div class="content-wrap">
                            <section id="section-shape-1">
                                <h3>Update Firebase Key</h3>
                                <form data-toggle="validator" method="post" action="<?php echo base_url('apiKeys/updateFirebaseKey'); ?>" enctype="multipart/form-data">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Firebase Key</label>
                                              <input type="hidden" name="id" value="<?=@$firebase['id'];?>">
                                              <input type="text" class="form-control" name="firebase_key" value="<?=@$firebase['firebase_key'];?>" id="firebase_key" placeholder="Enter firebase key" required> 
                                          </div>
                                      </div>
                                  </div>
                                      
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                              </form>
                            </section>
                            <section id="section-shape-2">
                                <h3>Update MSG91 Key</h3>
                                <form data-toggle="validator" method="post" action="<?php echo base_url('apiKeys/updateMSG91Key'); ?>" enctype="multipart/form-data">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">MSG91 Key</label>
                                              <input type="hidden" name="id" value="<?=@$msg['id'];?>">
                                              <input type="text" class="form-control" name="msg91_key" value="<?=@$msg['msg91_key'];?>" id="msg91_key" placeholder="Enter MSG91 key" required> 
                                          </div>
                                      </div>
                                  </div>
                                      
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                              </form>
                            </section>
                            <section id="section-shape-3">
                                <h3>Update Razorpay Key</h3>
                                <form data-toggle="validator" method="post" action="<?php echo base_url('apiKeys/updateRazorpayKey'); ?>" enctype="multipart/form-data">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Razorpay Key</label>
                                              <input type="hidden" name="id" value="<?=@$razorpay['id'];?>">
                                              <input type="text" class="form-control" name="razorpay_key" value="<?=@$razorpay['razorpay_key'];?>" id="razorpay_key" placeholder="Enter Razorpay key" required> 
                                          </div>
                                      </div>
                                  </div>
                                      
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                              </form>
                            </section>
                            
                        </div>
                        <!-- /content -->
                    </div>
                    <!-- /tabs -->
                </section>

            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
