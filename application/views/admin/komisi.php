<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Komisi</h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--  <a href="<?php echo base_url('Admin/add_user'); ?>" class="fcbtn btn btn-success btn-outline btn-1b pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add Users</a> -->
                
            </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
              <h4 style="color: green;"><?php echo $this->session->flashdata('error'); ?></h4>
                <h3 class="box-title m-b-0">Komisi</h3>
                        <div class="content-wrap">
                                <h3>Update Komisi</h3>
                                <form data-toggle="validator" method="post" action="<?php echo base_url('komisi/updateKomisi'); ?>" enctype="multipart/form-data">
                                  <div class="row">
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Nominal Komisi Mitra</label>
                                              <input type="hidden" name="id" value="<?=@$komisi['id'];?>">
                                              <input type="text" class="form-control" name="komisi_mitra" value="<?=@$komisi['komisi_mitra'];?>" id="komisi_mitra" placeholder="Enter Komisi mitra" required> 
                                          </div>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Type Komisi Mitra</label>
                                              <input type="text" class="form-control" name="type_komisi_mitra" value="<?=@$komisi['type_komisi_mitra'];?>" id="type_komisi_mitra" placeholder="Type komisi mitra " required> 
                                          </div>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Nominal Komisi Agen</label>
                                              <input type="text" class="form-control" name="komisi_agen" value="<?=@$komisi['komisi_agen'];?>" id="komisi_agen" placeholder="Enter Komisi agen" required> 
                                          </div>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label  class="control-label">Type Komisi Agen</label>
                                              <input type="text" class="form-control" name="type_komisi_agen" value="<?=@$komisi['type_komisi_agen'];?>" id="type_komisi_agen" placeholder="Type komisi agen" required> 
                                          </div>
                                      </div>
                                  </div>
                                      
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                                </form>
                            
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
