  <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-md-12">
                                    <h3>Selamat Datang, <?php echo $_SESSION['owner_name']; ?> di Halaman <?=$halaman?> <?=$wilayah?></h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-danger"><i class="fas fa-cart-arrow-down"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$order_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Pesanan</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$order_count?></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-warning"><i class="fas fa-user"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$user_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Pelanggan</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$user_count?>% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-success"><i class="mdi mdi-cash fa-fw"></i></span>
                                        </li></center>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$shop_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Pendapatan</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only"><?=$shop_count?>% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <ul class="col-in">
                                        <li><center>
                                            <span class="circle circle-md bg-info"><i class="mdi mdi-cash fa-fw"></i></span>
                                        </center>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15"><?=$income_count?></h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Total Komisi</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only">40% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

            <?php if($_SESSION['type'] != 1 && $_SESSION['type'] != 2): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">DAFTAR PENGGUNA</div>
                            <div class="table-responsive">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 70px">#</th>
                                            <th>PHOTO</th>
                                            <th>NAMA</th>
                                            <th>EMAIL</th>
                                            <th>DAFTAR</th>
                                            <!-- <th style="width: 300px">MANAGE</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i=0;
                                        foreach($user as $u)
                                        {
                                            $i++;
                                            $date = explode(" ",$u->created_at);
                                            $time = $date[1];
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$i;?></td>
                                            <td>
                                                <?php
                                                if($u->image !='')
                                                {
                                                    ?>
                                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?=base_url();?>assets/images/user/<?=$u->image;?>" alt="Image not Available">
                                                    <?php
                                                }else{
                                                    ?>
                                                    <img style="border-radius: 50%;width: 40px;height: 40px;" src="<?=base_url();?>assets/images/user/default.png" alt="Image not Available">
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td><span class="font-medium"><?=$u->name?></span>
                                                <br/><span class="text-muted"><?=$u->address?></span></td>
                                            <td><?=$u->email?>
                                                <br/><span class="text-muted"><?=$u->mobile?></span></td>
                                            <td><?=date("d-m-Y", strtotime($u->created_at))?>
                                                <br/><span class="text-muted"><?=date('h:i:s a', strtotime($time));?></span></td>
                                            <!-- <td>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-key"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="icon-trash"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil-alt"></i></button>
                                                <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-20"><i class="ti-upload"></i></button>
                                            </td> -->
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif?>