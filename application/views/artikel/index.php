<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->

<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?=$judul?></h4> </div>
        <!-- /.col-lg-12 -->
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
         	<a href="<?=base_url('artikel/tambah')?>" class="btn btn-primary" type="button" aria-haspopup="true" aria-expanded="false" style="float: right;"><i class="ti-user"> </i>  Tambah Data
         	</a>      
         </div>
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">

                <div class="table-responsive">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center">No.</th>
                              <th class="text-center">Judul</th>
                              <th class="text-center">Gambar</th>
                              <th class="text-center">Waktu</th>
                              <th class="text-center">Setting</th>
                            </tr>
                        </thead>
                        <tfoot>
                        	<tr>
                              <th class="text-center">No.</th>
                              <th class="text-center">Judul</th>
                              <th class="text-center">Gambar</th>
                              <th class="text-center">Waktu</th>
                              <th class="text-center">Setting</th>
                            </tr>
                        </tfoot>

                        <tbody>
                    	<?php if(!empty($artikel)):?>
                    		<?php foreach($artikel as $key => $a):?>
                    			<tr>
                    				<td class="text-center"><?=$key+1?></td>
                    				<td class="text-center"><?=$a->title?></td>
                    				<td class="text-center">
                    					<img src="<?=base_url()?>assets/images/artikel/<?=$a->image?>" class="img-thumbnail" style="max-width: 100px; max-height: 100px">
                    				</td>
                    				<td class="text-center">
                    					<?=format_hari_tanggal($a->created_at)?>
                    				</td>
                    				<td class="text-center">
                              <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#detail<?=$a->id?>" data-whatever="@mdo">
                                <i class="glyphicon glyphicon-eye-open"></i>     
                              </button>
                    					<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit<?=$a->id?>" data-whatever="@mdo">
                                <i class="glyphicon glyphicon-pencil"></i>     
                              </button>
                              <a href="<?=base_url('artikel/hapus/').$a->id?>" class="btn btn-sm btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                              </a>
                    				</td>
                    			</tr>
                    		<?php endforeach?>
                        <?php endif?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('artikel/edit')?>
    <?php $this->load->view('artikel/detail')?>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>

<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>
