<?php if(!empty($artikel)): ?>

	<?php foreach($artikel as $a):?>
		<div class="modal fade" id="edit<?=$a->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			              <h4 class="modal-title" id="exampleModalLabel">Edit Artikel</h4>
					</div>
					<?=form_open_multipart('artikel/edit')?>
					<div class="modal-body">
						
						<div class="form-group">
							<label>Judul</label>
							<input type="text" name="title" class="form-control" value="<?=$a->title?>" required="">
						</div>

						<div class="form-group">
							<label>Isi</label>
							<textarea class="form-control" name="isi" rows="10" cols="10"><?=$a->description?></textarea>
						</div>

						<div class="form-group row">
							<div class="col-md-5">
								<input type="file" name="foto" class="form-control">
							</div>
							<div class="col-md-5">
								<img src="<?=base_url()?>assets/images/artikel/<?=$a->image?>" class="img-thumbnail" style="max-width: 100px; max-height: 100px">
							</div>
						</div>

					</div>
					<div class="modal-footer">
		              <button class="btn btn-sm btn-info" type="submit">Update</button>
		            </div>
		            <?=form_close()?>
				</div>
			</div>
		</div>
	<?php endforeach?>

<?php endif?>