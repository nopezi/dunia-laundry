<!-- tambah -->

<div id="page-wrapper">
<div class="container-fluid">
    <div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?=$judul?></h4> 
        </div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<form action="<?=base_url('artikel/aksi_tambah')?>" class="form" enctype="multipart/form-data" method="post">
					
					<div class="form-group">
						<label>Judul</label>
						<input type="text" name="title" class="form-control" required="">
					</div>

					<div class="form-group">
						<label>Isi</label>
						<textarea class="form-control" name="isi" rows="10" cols="10"></textarea>
					</div>

					<div class="form-group row">
						<div class="col-md-2">
							<input type="file" name="foto" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<a href="<?=base_url('artikel')?>" class="btn btn-sm btn-default">Kembali</a>
							</div>
							<button class="btn btn-sm btn-info" type="submit">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
a.disabled {
  pointer-events: none;
  cursor: default;
}
</style>