<?php if(!empty($artikel)): ?>

	<?php foreach($artikel as $a):?>
		<div class="modal fade" id="detail<?=$a->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<img src="<?=base_url()?>assets/images/artikel/<?=$a->image?>" class="modal-header img-thumbnail">
					
					<div class="modal-body">

						<b><h4><?=$a->title?></h4></b>
						<p><?=$a->description?></p>
						<small><?=format_hari_tanggal($a->created_at)?></small>

					</div>
				</div>
			</div>
		</div>
	<?php endforeach?>

<?php endif?>