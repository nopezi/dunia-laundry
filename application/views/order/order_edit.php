<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Detail Pesanan</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Pesanan</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- <div class="col-md-12"> -->
        <!-- .row -->
        <!-- <div class="row"> -->
        <!-- /.row -->
        <!-- ============================================================== -->
        <?php
        $user = $this->User_model->updateUser($order_detail['user_id']);
        ?>
        <div class="row">
            
            <form method="POST" action="<?php echo base_url('order/submitOrderUpdate'); ?>">
            <input type="hidden" name="order_id" value="<?= $order_detail['order_id'] ?>">
            <div class="col-md-9 col-lg-9 col-sm-7">
                <div class="panel panel-info">
                    <div class="panel-heading"> Detail Produk</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table product-overview">
                                    <thead>
                                        <tr>
                                            <th>Gambar</th>
                                            <th>Nama Produk</th>
                                            <th>Quantitas</th>
                                            <th>Harga</th>
                                            <th style="text-align:center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        // echo "<pre>";
                                        // print_r($order_detail);
                                        $total = 0;
                                        $item_details = $order_detail['item_details'];
                                        $es1 = str_ireplace('"item_details":',"",$item_details);
                                        $json = json_decode($es1, true);
                                        foreach ($json as $key => $value) 
                                        {
                                            // echo "<pre>";
                                            // print_r($value);
                                            $total = $value['quantity'] * $value['price'];
                                        ?>
                                        <tr>
                                            <td width="150"><img src="<?=$value['image'];?>" alt="iMac" width="80"></td>
                                            <td width="350">
                                                <h5 class="font-500"><?=$value["item_name"]?></h5>
                                                
                                            </td>
                                            <td width="150" align="center">
                                                <input type="text" class="form-control" name="quantity[<?= $value['s_no']?>]" value="<?= $value['quantity'] ?>">
                                            </td>
                                            <td width="70"><?=$order_detail['currency_code'];?> <?= number_format( $value['price'] ); ?></td>
                                            <td width="150" align="center" class="font-500"><?=$order_detail['currency_code'];?> <?= number_format( $total ); ?></td>
                                        </tr>

                                        <?php
                                            
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-lg-3 col-sm-5">
                <div class="white-box">
                    <h3 class="box-title">Action</h3>

                    <div class="table-responsive">
                        <table class="table product-overview">
                            <thead>
                                <tr>
                                    <th style="text-align:center">Diskon</th>
                                    <th style="text-align:center">Total Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"><?=$order_detail['currency_code'];?> <?= number_format( $order_detail['discount'] ) ?></td>
                                    <td align="center"><h3><?=$order_detail['currency_code'];?> <?= number_format( $order_detail['final_price'] ); ?></h3></td> 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <input type="submit" class="btn btn-primary" name="submit" value="Update">
                </div>
            </div>
            </form>

        </div>


            
        <!-- /row -->
            
        </div>
        <!-- /.row -->
    <!-- </div> -->
    <!-- </div> -->
<script type="text/javascript">
function changeOrderStatus()
{
  var order_id = $("#order_id").val();
  var order_status = $("#order_status").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('order/changeOrderStatus'); ?>",
    data: "order_id="+order_id+"&order_status="+order_status,
    success: function(data) 
    {
      location.reload();
    }
  });
}   
</script>