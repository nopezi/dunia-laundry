<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class HomeController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getHomeData()
    {
        $input = $this->input->post();
        $data = $this->Api_Home_model->getHomeData($input);
        if (empty($data)) {
            $this->Api_Home_model->responseFailed(0, 'No data found');
        } //empty($data)
        else {
            $this->Api_Home_model->responseSuccess(1, 'Get All Data', $data);
        }
    }

    public function search()
    {
    	$shop_name = $this->input->post('shop_name');
    	$this->Api_Home_model->search($shop_name);
    }
    


}