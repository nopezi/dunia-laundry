<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaundryController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getAllLaundryShop()
    {
        $input = $this->input->post();
    	$data = $this->Api_Laundry_model->getAllLaundryShop($input);
    	if($data !='0')
    	{
    		$this->Api_Laundry_model->responseSuccess(1, LSHOP, $data);
    	}else{
    		$this->Api_Laundry_model->responseFailed(0, LSHOP_NOT, ''); 
    	}
    }

    public function getLaundryByService()
    {
        $input = $this->input->post();
        $data = $this->Api_Laundry_model->getLaundryByService($input);
        if($data =='0')
        {
            $this->Api_Laundry_model->responseFailed(0, LSHOP_NOT, ''); 
        }
        else if($data =='1')
        {
            $this->Api_Laundry_model->responseFailed(0, SERVICE_NOT, ''); 
        }
        else{
            $this->Api_Laundry_model->responseSuccess(1, LSHOP, $data);
        }
    }

    public function getLaundryById()
    {
        $input = $this->input->post();
        $data = $this->Api_Laundry_model->getLaundryById($input);
        if($data =='0')
        {
            $this->Api_Laundry_model->responseFailed(0, LSHOP_NOT, ''); 
        }
        else if($data =='1')
        {
            $this->Api_Laundry_model->responseFailed(0, SERVICE_NOT, ''); 
        }
        else{
            $this->Api_Laundry_model->responseSuccess(1, LSHOP, $data);
        }
    }

}