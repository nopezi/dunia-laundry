<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class TiketController extends CI_Controller{

	public function __construct()
    {
        require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function addTiket()
    {
    	$input = $this->input->post();
    	$query = $this->Api_Tiket_model->addTiket($input);
    	if($query == 1)
        {
            $this->Api_Tiket_model->responseSuccess(1, 'Tiket create sucessfully', '');
        }
        else if($query == 2)
        {
            $this->Api_Tiket_model->responseFailed(0, SOMTHING);
        }
        else
        {
        	$this->Api_Tiket_model->responseFailed(0, 'User not exist');
        }
    }

    public function addTiketComment()
    {
        $input = $this->input->post();
        $query = $this->Api_Tiket_model->addTiketComment($input);
        if($query == 1)
        {
            $this->Api_Tiket_model->responseSuccess(1, 'Comment sucessfully', '');
        }
        else
        {
            $this->Api_Tiket_model->responseFailed(0, 'Tiket Id not exist');
        }
    }

    public function tiketList()
    {
        $input = $this->input->post();
        $data = $this->Api_Tiket_model->tiketList($input);
        if($data == 0)
        {
            $this->Api_Tiket_model->responseFailed(0,'User not exist');
        }
        else if($data == 1)
        {
            $this->Api_Tiket_model->responseFailed(0,'No ticket available');
        }
        else{
            $this->Api_Tiket_model->responseSuccess(1, 'Tiket list', $data);
        }
    }

    public function getTiketComment()
    {
        $tiket_id = $this->input->post('tiket_id');
        $data = $this->Api_Tiket_model->getTiketComment($tiket_id);
        if($data == 0)
        {
            $this->Api_Tiket_model->responseFailed(0,'No message available');
        }
        else{ 
            $this->Api_Tiket_model->responseSuccess(1, 'Comment list', $data);
        }
    }



}