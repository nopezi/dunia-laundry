<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class NotificationController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        // error_reporting(0);
    }

    public function get_notification()
    {
    	$user_id = $this->input->post('user_id');
    	$data = $this->Api_Notif_model->get_notification($user_id);
    	if($data ='1')
    	{
    		$this->Api_Notif_model->responseFailed(0, USER_NOT_EX);
    	}
    	
    }

    public function send_notification()
    {
        $factory = (new Factory)->withServiceAccount(FCPATH.'/win-laundry-firebase-adminsdk-t541y-3e4652faa9.json');
        $messaging = $factory->createMessaging();

        $message = CloudMessage::withTarget('token', "dbHJMy_xSMmRc3HUKGD4mG:APA91bGyMGwoEc-xU7cxNSADMMzsg8TPAeFW_rhcHNpwGeu2EPAydi6mk7ebLmFtHywaQUt_d0x5xhT4NADDrYGCErPLfmmt1EAB1NaxtXCySLJXQkb9emAquWdHrDC014AGy3hPHU30")
            ->withNotification(Notification::create('Chat baru dari laundry', 'Halo ada chat baru dari outlet nih, ayo cek skarang'))
            ->withData(['key' => 'value']);
        
        $messaging->send($message);

        $this->Api_Notif_model->responseSuccess(0, USER_NOT_EX, $factory);
    }

}