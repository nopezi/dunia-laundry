<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OfferController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getAllOffer()
    {
        $count = $this->input->post('count');
    	$data = $this->Api_Offer_model->getAllOffer($count);
    	if($data !='0')
    	{
    		$this->Api_Offer_model->responseSuccess(1, OFFER, $data);
    	}else{
    		$this->Api_Offer_model->responseFailed(0, OFFER_NOT, '');
    	}
    }

    public function getOfferForLaundryShop()
    {
        $shop_id = $this->input->post('shop_id');
        $data = $this->Api_Offer_model->getOfferForLaundryShop($shop_id);
        if($data !='0')
        {
            $this->Api_Offer_model->responseSuccess(1, OFFER_SHOP, $data);
        }else{
            $this->Api_Offer_model->responseFailed(0, OFFER_SHOP_NOT, '');
        }
    }

    public function applyPromocode()
    {
        $input = $this->input->post();
        $data = $this->Api_Offer_model->applyPromocode($input);
        
    }

}