<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdvertisementController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getAdvertisement()
    {
    	$data = $this->Api_Advertisement_model->getAdvertisement();
    	$img = 'assets/images/advertise/';
    	if($data !='0')
    	{
    		$this->Api_Advertisement_model->responseSuccessImage(1, ADVERTISE, $data,$img);
    	}else{
    		$this->Api_Advertisement_model->responseFailedImage(0, ADVERTISE_NOT, '','');
    	}
    }

}
