<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CurrencyController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getCurrency()
    {
    	$data = $this->Api_Currency_model->getCurrency();
    	if($data !='0')
    	{
    		$this->Api_Currency_model->responseSuccess(1, CURRENCY, $data);
    	}else{
    		$this->Api_Currency_model->responseFailed(0, CURRENCY_NOT);
    	}
    }

}