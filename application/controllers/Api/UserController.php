<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class UserController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function userUpdate()
    {
    	$img = @$_FILES['image']['name'];
        $input = $this->input->post();
        $user_id = $this->input->post('user_id');
        $data = $this->Api_User_model->userUpdate($input);
        if($data)
        {
            $img = @$_FILES['image']['name'];
            $background = @$_FILES['background']['name'];
            if($img !='')
            {
                $data['user_id']         = $this->input->post('user_id');
        
                $config['upload_path']   = 'assets/images/user/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 100000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $image = "";
                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data(); 
                    $image = $uploadData['file_name']; 
                }

                $data['image'] = $image;
                $where  = array(
                    'user_id' => $data['user_id']
                );
                $update = $this->Api_User_model->updateSingleRow('la_user', $where, $data);
            } 
            if ($background != ''){
                $data['user_id']         = $this->input->post('user_id');
        
                $config['upload_path']   = 'assets/images/user/background';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['overwrite']     = TRUE;
                $config['max_size']      = 100000000;
                $config['file_name']     = time();
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                $background = "";
                if ($this->upload->do_upload('background')) {
                    $uploadData = $this->upload->data(); 
                    $background = $uploadData['file_name']; 
                }

                $data['background'] = $background;
                $where  = array(
                    'user_id' => $data['user_id']
                );
                $update = $this->Api_User_model->updateSingleRow('la_user', $where, $data);
            }

            $userData = $this->Api_User_model->getUserDataById($user_id);
            $this->Api_User_model->responseSuccess(1, 'success', $userData);
            
        }
        else
        {
            $this->Api_User_model->responseFailed(0,SOMTHING);
        }
    }

}