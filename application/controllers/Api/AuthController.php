<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class AuthController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function sendOtp()
    {
        $input = $this->input->post();
        $data = $this->Api_Auth_model->sendOtp($input);
        if($data == '1')
        {
            $this->Api_Auth_model->responseSuccessWethoutData(1, OTP_SEND);
        }
        else if($data == '2')
        {
            $this->Api_Auth_model->responseFailed(0,USER_ALREADY);
        }
        else if($data == '3')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
    }

    public function signup()
    {
        $input = $this->input->post();
        
        $data = $this->Api_Auth_model->signup($input);
        if($data == '1')
        {
            $this->Api_Auth_model->responseFailed(0,EMAIL_EXIST);
            exit();
        }
        else if($data == '2')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
        else
        {
            $this->Api_Auth_model->responseSuccess(1, DATA_SUBMIT, $data);
        }
    }

    public function userActive()
    {
        $user_id = base64_decode($_GET['id']);
        $data = $this->Api_Auth_model->userActive($user_id);
        // redirect('Admin/post');
        $this->load->view('auth/activeUser');
        
    }

    public function otpSms()
    {

        $user_id = $_GET['user_id'];
        $otp     = $_GET['otp'];

        $where   = [
            'user_id' => $user_id,
            'otp_sms' => $otp
        ];

        $cek = $this->db->get_where('la_user', $where)->first_row();

        # jika otp nya sesuai
        # maka status user menjadi aktif
        # dan kode otp sms yang ada di table la_user, di hapus
        if (!empty($cek)) {
            
            $set = [
                'status'  =>'1',
                'otp_sms' => ''
            ];
            $where = ['user_id'=>$cek->user_id];
            $this->db->update('la_user', $set, $where);

            $this->Api_Auth_model->responseSuccess(1, 'Kode otp berhasil di verifikasi', $cek);

        } else {

            $this->Api_Auth_model->responseSuccess(0, 'Kode otp tidak sesuai', null);

        }

    }

    public function resendOtpSms()
    {

        $user_id = $_GET['user_id'];
        $otp_sms = random_char(5);

        $this->db->update('la_user', ['otp_sms'=>$otp_sms], ['user_id'=>$user_id]);
        $mobile = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

        # ganti 0 pada nomor hp menjadi +62
        $no_hp = '+62'.substr($mobile->mobile, 1, 15);
        $pesan_sms = "<#> Kode otp Winlaundry anda adalah ".$otp_sms;
        # kirim otp ke sms
        $from  = "+17042865978";
        $to    = $no_hp;
        $pesan = "From=".$from."&To=".$to."&Body=".$pesan_sms;
        $cek = api_twilio($pesan);

        $this->Api_Auth_model->responseSuccess(1, 'Berhasil resend otp sms', $mobile);

    }

    public function forgotPassword()
    {
        $email = $this->input->post('email');
        $query = $this->Api_Auth_model->forgotpassword($email);
        if($query == 1)
        {
            $this->Api_Auth_model->responseSuccess(1, PASS_SEND, '');
        }
        else if($query == 2)
        {
            $this->Api_Auth_model->responseFailed(0,VALID_EMAIL);
        }
        else{
            $this->Api_Auth_model->responseFailed(0,SOMTHING);
        }
    }

    public function verifyOtp()
    {
        $input = $this->input->post();
        $query = $this->Api_Auth_model->verifyOtp($input);
        if($query == 0)
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
        else if($query == 1)
        {
            $this->Api_Auth_model->responseFailed(0,OTP_WRONG);
        }
        else if($query == 2)
        {
            $this->Api_Auth_model->responseFailed(0,VALID_EMAIL);
        }
        else{
            
            $this->Api_Auth_model->responseSuccess(1, OTP_PASS, $query);
        }
    }

	
    public function login()
    {
        $input = $this->input->post();
        $getdata = $this->Api_Auth_model->login($input);
        if($getdata == '0')
        {
            $data = $this->Api_Auth_model->single_row_data($input);
            $this->Api_Auth_model->responseSuccess(1, LOGIN_SUCCESS, $data);
        }
        else if($getdata == '1')
        {
            $this->Api_Auth_model->responseFailed(0,USER_NOT);
        }
        else if($getdata == '2')
        {
            $this->Api_Auth_model->responseFailed(0,VALID_PASS);
        }
        else if($getdata == '3')
        {
            $this->Api_Auth_model->responseFailed(0,DEACTIVE_NEW);
        }
        else if($getdata == '4')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
        
    }

    public function loginGoogle()
    {
        $input = $this->input->post();
        $getdata = $this->Api_Auth_model->loginGoogle($input);
        if($getdata == '0')
        {
            $data = $this->Api_Auth_model->single_row_data_email($input);
            $this->Api_Auth_model->responseSuccess(1, LOGIN_SUCCESS, $data);
        }
        else if($getdata == '1')
        {
            $this->Api_Auth_model->responseFailed(0,USER_NOT);
        }
        else if($getdata == '2')
        {
            $this->Api_Auth_model->responseFailed(0,VALID_PASS);
        }
        else if($getdata == '3')
        {
            $this->Api_Auth_model->responseFailed(0,DEACTIVE_NEW);
        }
        else if($getdata == '4')
        {
            $this->Api_Auth_model->responseFailed(0,ALL_FIELD_MANDATORY);
        }
    }

    public function logOut()
    {
        $input = $this->input->post();
        $this->Api_Auth_model->logOut($input);
        $this->Api_Auth_model->responseSuccess(1, LOGOUT, '');
    }

    public function deleteAccount()
    {
        $user_id = $this->input->post('user_id');
        $query = $this->Api_Auth_model->deleteAccount($user_id);
        if($query == 1)
        {
            $this->Api_Auth_model->responseSuccess(1, 'Account deactive sucessfully', '');
        }
        else if($query == 2)
        {
            $this->Api_Auth_model->responseFailed(0, 'User not exist');
        }
        else
        {
            $this->Api_Auth_model->responseFailed(0, SOMTHING);
        }
    }

    public function changePassword()
    {
        $input = $this->input->post();
        $query = $this->Api_Auth_model->changePassword($input);
        if($query == 1)
        {
            $this->Api_Auth_model->responseSuccess(1, 'Password change sucessfully', '');
        }
        else if($query == 2)
        {
            $this->Api_Auth_model->responseFailed(0, 'User not exist');
        }
        else
        {
            $this->Api_Auth_model->responseFailed(0, SOMTHING);
        }
    }


}
