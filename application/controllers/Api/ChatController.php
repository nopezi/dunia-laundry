<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class ChatController extends CI_Controller{

	public function __construct()
    {
    	require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function setMessage()
    {
        $input = $this->input->post();
        $img = (isset($_FILES['media'])) ? $_FILES['media']['name'] : '';
        $message_head_id = $this->Api_Chat_model->set_message($input,$img);

        $toUserExst = $this->db->where('user_id',$input['to_user_id'])->get('la_user');
        if($toUserExst->num_rows() > 0)
        {
            $user = $toUserExst->row_array();
            $to_user_id = $user['user_id'];
        }else{
            $shopExist = $this->db->where('shop_id',$input['to_user_id'])->get('la_laundry_shop')->row_array();
            $to_user_id = $shopExist['user_id'];
        }

        if($img == '')
        {

            $data = array(
                'message_head_id' => $message_head_id,
                'from_user_id' => $input['from_user_id'],
                'to_user_id' => $to_user_id,
                'message' => $input['message'],
                'type' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                );
            $this->db->insert('la_message',$data);

        }else{

            $this->load->library('upload');
            $config['image_library']  = 'gd2';
            $config['upload_path']    = './assets/images/chat';
            $config['allowed_types']  = 'gif|jpg|jpeg|png';
            $config['max_size']       = 10000;
            $config['file_name']      = time();
            $config['create_thumb']   = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width']          = 250;
            $config['height']         = 250;
            $this->upload->initialize($config);
            $updateduserimage = "";
            if ($this->upload->do_upload('media') && $this->load->library('image_lib', $config)) {
                $updateduserimage = 'assets/images/chat' . $this->upload->data('file_name');
            } else {
                //  echo $this->upload->display_errors();
            }
            
            if ($updateduserimage) {
                $media = $updateduserimage;
            } else {
                $media = "";
            }

            $data1 = array(
                'message_head_id' => $message_head_id,
                'from_user_id' => $input['from_user_id'],
                'to_user_id' => $to_user_id,
                'message' => $input['message'],
                'type' => '2',
                'media' => $media,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                );
            $this->db->insert('la_message',$data1);
        }

        $su = $this->Api_Chat_model->getSingleRow(USR_TBL, array(
                        'user_id' => $input['from_user_id']
                    ));
        $ru    = $this->Api_Chat_model->getSingleRow(USR_TBL, array(
            'user_id' => $to_user_id
        ));
        
        $msg  = $su->name . ':' . $input['message'];
        $type = 7003;
        $this->firebase_with_class($ru->device_token, $input['from_user_id'], $su->name, "Chat", $type, $msg);

        $this->Api_Chat_model->responseSuccessWethoutdata(1, MSG_SEND);
    }

    public function getMessageHistory()
    {
        $user_id = $this->input->post('user_id');
        $this->Api_Chat_model->get_message_history($user_id);

    }

    public function getMessage()
    {
        $input = $this->input->post();
        $this->Api_Chat_model->get_message($input);
    }

    public function firebase_with_class($device_token, $sender_id, $senderName, $title, $type, $msg1)
    {
        $user = $this->Api_Chat_model->getSingleRow('la_user', array(
            'device_token' => $device_token
        ));
        
        $FIRE_BASE_KEY=$this->Base_model->getSingleRow('la_firebase_key',array('id'=>1));
        $api_key=$FIRE_BASE_KEY->firebase_key;

        if ($user->device_type == "ios") {
            $API_ACCESS_KEY = $api_key;
            
            $msg    = array(
                'body' => $msg1,
                'title' => $title,
                'icon' => 'myicon',
                'type' => $type,
                /*Default Icon*/
                'sound' => 'default'
                /*Default sound*/
            );
            $fields = array(
                'to' => $device_token,
                'notification' => $msg
            );
            
            
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            #Send Reponse To FireBase Server    
            $ch      = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            // echo "<pre>";
            // print_r($result); die;
            curl_close($ch);
        } else {
            $FCMPushNotification = new \BD\FCMPushNotification($api_key);
            
            $sDeviceToken = $device_token;
            
            $aPayload = array(
                'data' => array(
                    'title' => $title,
                    'type' => $type,
                    "sender_id" => $sender_id,
                    'body' => $msg1,
                    "senderName" => $senderName
                )
            );
            
            $aOptions = array(
                'time_to_live' => 15 //means messages that can't be delivered immediately are discarded. 
            );
            
            $aResult = $FCMPushNotification->sendToDevice($sDeviceToken, $aPayload, $aOptions // optional
                );
            
            return $aResult;
        }
    }
    

}
