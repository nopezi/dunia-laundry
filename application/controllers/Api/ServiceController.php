<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getAllService()
    {
    	$data = $this->Api_Service_model->getAllService();
    	if($data !='0')
    	{
    		$this->Api_Service_model->responseSuccess(1, SERVICE, $data);
    	}else{
    		$this->Api_Service_model->responseFailed(0, SERVICE_NOT, '');
    	}
    }

    public function getShopServices()
    {
        //$shop_id = $this->input->post('shop_id');
        $shop_id = 'YZ65d0';
        $data = $this->Api_Service_model->getShopServices($shop_id);
        if($data !='0')
        {
            $this->Api_Service_model->responseSuccess(1, SERVICE, $data);
        }else{
            $this->Api_Service_model->responseFailed(0, SERVICE_NOT, '');
        }
    }

}
