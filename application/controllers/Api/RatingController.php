<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class RatingController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function addRating()
    {
        $input  = $this->input->post();
        $data = $this->Api_Rating_model->addRating($input);
        if($data == 0)
        {
        	$this->Api_Rating_model->responseFailed(0, ALREADY_RATED);
        }else if($data == 1){
        	$this->Api_Rating_model->responseSuccessWithOutData(1, RATING);
        }else if($data == 2){
        	$this->Api_Rating_model->responseFailed(0, SHOP_NOT);
        }else if($data == 3){
        	$this->Api_Rating_model->responseFailed(0, USER_NOT_EX);
        }
    }
    
    
    public function getRating()
    {
    	$this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_rules('shop_id', 'shop_id', 'required');
        if ($this->form_validation->run() == false) {
            $this->Api_Rating_model->responseFailed(0, ALL_FIELD_MANDATORY);
            exit();
        }
        $user_id = $this->input->post('user_id', TRUE);
        $shop_id = $this->input->post('shop_id', TRUE);
        $userStatus = $this->Api_Rating_model->getSingleRow('la_user', array(
            'user_id' => $user_id
        ));
        if ($userStatus) {
            if ($userStatus->status == 1) {
                $avrage = $this->Api_Rating_model->count_total_rating($shop_id);
                // return round($avrage->average, 0);
                $this->Api_Rating_model->responseSuccess(1, GET_RATE, $avrage);
                
            } else {
                $this->Api_Rating_model->responseFailed(3, INACTIVATE);
            }
        } else {
            $this->Api_Rating_model->responseFailed(0, USER_NOT_EX);
            
        }
    }
    

}
