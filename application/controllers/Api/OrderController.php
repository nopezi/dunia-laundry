<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Twilio\Rest\Client;
use iPaymu\iPaymu;
use Faker\Factory;
use Illuminate\Http\Request;

class OrderController extends CI_Controller{

	public function __construct()
    {
        require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function orderSubmit()
    {
    	$input = $this->input->post();
    	$data = $this->Api_Order_model->orderSubmit($input);
    	if($data =='1')
    	{
    	    try {
                $this->notifTwilio($input['shop_id']);
            } catch (Exception $e) {
    
            }

            # kirim otp ke whatsapp
            $cek_wa = kirim_pesan_wa($msg, $no_hp);
            
    		$this->Api_Order_model->responseSuccess(1, ORDER, $data);
    	}
        else if($data =='2')
        {
            $this->Api_Order_model->responseFailed(0, 'Shop is not available', ''); 
        }
        else if($data =='3')
        {
            $this->Api_Order_model->responseFailed(0, 'User is not exist', ''); 
        }
        else if($data =='4')
        {
            $this->Api_Order_model->responseFailed(0, 'Order id already exist', '');  
        }
        else
        {
    		$this->Api_Order_model->responseFailed(0, SOMTHING);
    	}
    }

    public function orderCancel()
    {
        $order_id = $this->input->post('order_id');
        $data = $this->Api_Order_model->orderCancel($order_id);
        if($data == '1')
        {
            $this->Api_Order_model->responseSuccess(1, 'Order cancel sucessfully', '');
        }
        else if($data == '2')
        {
            $this->Api_Order_model->responseFailed(0, 'Order is not cancel');
        }
        else
        {
            $this->Api_Order_model->responseFailed(0, 'Order is not match');
        }
    }

    public function getBookingList()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->Api_Order_model->getBookingList($user_id);
        
        if($data == '0')
        {
            $this->Api_Order_model->responseFailed(0, BOOK_NOT);
        }
        else
        {
            $this->Api_Order_model->responseSuccess(1, BOOKING, $data);
        }
    }

    public function orderToPaymu()
    {
        $apiKey = 'B96DE3B5-5C69-4A99-84E4-D6379BE07FE5';
        $va = '1179007845174758';
        $production = true;

        $ipaymu = new iPaymu($apiKey, $va, $production);
        $order_id = $this->input->post('order_id');

        $ipaymu->setURL([
            'ureturn' => 'http://laundryapp.windigitalkhatulistiwa.com/thankyou_page',
            'unotify' => 'http://laundryapp.windigitalkhatulistiwa.com/notify_page?orderid=' . $order_id,
            'ucancel' => 'http://laundryapp.windigitalkhatulistiwa.com/cancel_page',
        ]);

        $temp = $this->input->post('item_details');
        
        $input_data = json_decode($temp, true);

        foreach($input_data as $i => $value) {
            $ipaymu->add($value['item_id'], $value['item_name'], $value['price'], $value['quantity'], $value['item_name'], rand(1, 5), rand(1, 5), rand(1, 5), rand(1, 5), "1:1:1");

        }

        $direct = $ipaymu->redirectPayment();

        $this->Api_Order_model->responseSuccess(1, BOOKING, $direct);
    }

    public function notifTwilio($shop_id = 'PQcf4f')
    {
        $sid    = "ACe29b12dc0d4011601ca5e75e118de42b"; 
        $token  = "9c89c1bc90caeabce42c6b978db7f018"; 
        $twilio = new Client($sid, $token); 
        $shop = $this->Api_Order_model->getDetailStore($shop_id);
        
                        
        $messagesms = $twilio->messages
                  ->create($shop['mobile'], // to
                           [
                               "body" => "Winlaudry dapet orderan buat kamu, cek diweb.",
                               "from" => "+17042865978"
                           ]
                  );
        
        $message = $twilio->messages 
                        ->create("whatsapp:" . $shop['mobile'], // to 
                                array( 
                                    "from" => "whatsapp:+6285295473861", // +6285295473861       
                                    "body" => "Hello, ada order baru nih yang udah siap di jemput. Ayo cek orderan nya di website sekarang juga." 
                                ) 
                        ); 

    }

    public function updateStatus()
    {
        $order_id = $this->input->get('orderid');
        $data = array(
            'payment_status' => '1'
                );
        $this->db->where('order_id',$order_id);
        $this->db->update('la_order_details',$data);
        $this->Api_Order_model->responseSuccess(1, BOOKING, $order_id);
    }

}