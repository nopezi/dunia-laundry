<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PriceListController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function getItembyShopId()
    {
    	//$shop_id = $this->input->post('shop_id');
        $shop_id = 'YZ65d0';
    	$data = $this->Api_Price_list_model->getItembyShopId($shop_id);

    	// $img = base_url().'assets/images/Product/';
    	if($data !='0')
    	{
    		$this->Api_Price_list_model->responseSuccess(1, PL, $data);
    	}else{
    		$this->Api_Price_list_model->responseFailed(0, PL_NOT, '');
    	}
	}
	

	public function getItembyServiceId()
    {
    	$shop_id = $this->input->post('shop_id');
    	$data = $this->Api_Price_list_model->getPriceforService($shop_id);
    	// $img = base_url().'assets/images/Product/';
    	if($data !='0')
    	{
    		$this->Api_Price_list_model->responseSuccess(1, PL, $data);
    	}else{
    		$this->Api_Price_list_model->responseFailed(0, PL_NOT, '');
    	}
    }

}
