<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaundryOwnerController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function laundryOwner()
    {

    	$data['user'] = $this->Laundry_Owner_model->laundryOwner();
        echo load_page($this,'laundry/laundry_owner',$data);
    }

    public function laundryOwnerAdd($franchise = null)
    {
        $franchise = $this->input->get('type');
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();

        if ($_SESSION['type'] == 3) {
            $data['provinsi_ada'] = $this->db->from('la_regencies lr')
                                        ->join('la_provinces lp', 'lp.id=lr.province_id')
                                        ->where('lr.id', $_SESSION['regional'])
                                        ->get()
                                        ->first_row();
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
        } else {

             $data['provinsi_ada'] = $this->db->get('la_provinces')->result();

        }

        if ($franchise == 'franchise') {
            $data['franchise'] = true;
        }

        echo load_page($this,'laundry/add_laundry_owner',$data);
    }

    public function submitLaundryOwner($franchise=null)
    {
        $input = $this->input->post();

        $image = $_FILES['image']['name'];
        $id = $this->Laundry_Owner_model->submitLaundryOwner($input,$image, $franchise);
    }

    public function changeLaundryOwnerStatus()
    {
        $id = $this->uri->segment('3');
        $this->Laundry_Owner_model->changeLaundryOwnerStatus($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('laundryOwner');
    }

    public function changeLaundryOwnerStatus_shop()
    {
        $id = $this->uri->segment('3');
        $cek = $this->Laundry_Owner_model->changeLaundryOwnerStatus_v2($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        
        redirect('laundryShop');
    }

    public function updateLaundryOwner()
    {

        $id = $this->uri->segment('3');
        $data['row'] = $this->Laundry_Owner_model->updateLaundryOwner($id);
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['user'] = $this->Laundry_Owner_model->laundryOwner();

        if (!empty($data['row']['regional'])) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$data['row']['regional']])->first_row();
            $data['provinsi_ada'] = $this->db->get_where('la_provinces', ['id'=>$data['regional']->province_id])->first_row();
            $data['provinsi']   = $this->db->where('id!=',$data['provinsi_ada']->id)->get('la_provinces')->result();
        } else {
            $data['provinsi']   = $this->db->get('la_provinces')->result();
        }

        if (!empty($data['row']['distrik'])) {
            $data['distrik'] = $this->db->get_where('la_districts', ['id'=>$data['row']['distrik']])->first_row(); 
        }
        
        $franchise = $this->input->get('type');
        if ($franchise == 'franchise') {
            $data['franchise'] = true;
        }

        echo load_page($this,'laundry/add_laundry_owner',$data);
    }

    public function data_regional()
    {

        $id_provinsi    = $_GET['id_provinsi'];
        $id             = $_GET['id_regencies'];
        if (!empty($id)) {
            $this->db->where('id!=', $id);
        }
        $this->db->where('province_id', $id_provinsi);
        $data_regensial = $this->db->get('la_regencies')->result();

        print_r(json_encode($data_regensial));

    }

    public function data_kecamatan()
    {

        $id_regencies    = $_GET['id_regencies'];
        $id_distrik      = $_GET['id_distrik'];
        if (!empty($id_distrik)) {
            $this->db->where('id!=', $id_distrik);
        }
        $this->db->where('regency_id', $id_regencies);
        $data_regensial = $this->db->get('la_districts')->result();

        print_r(json_encode($data_regensial));

    }

    public function viewOwnerDetail()
    {
        $id = $this->uri->segment('3');
        $data['owner'] = $this->Laundry_Owner_model->viewOwnerDetail($id);
        echo load_page($this,'laundry/view_owner_detail',$data);
    }

    public function paymentHistory()
    {
        $id = $this->uri->segment('3');
        $data['owner'] = $this->Laundry_Owner_model->viewOwnerDetail($id);
        $data['history'] = $this->Laundry_Owner_model->paymentHistory($id);
        echo load_page($this,'laundry/payment_history',$data);
    }

    public function ownerPackages()
    {
        $id = $this->uri->segment('3');
        $data['owner'] = $this->Laundry_Owner_model->viewOwnerDetail($id);
        $data['package'] = $this->Laundry_Owner_model->subscriptionPackage();
        $data['history'] = $this->Laundry_Owner_model->subscriptionPayments($id);
        echo load_page($this,'laundry/owner_packages',$data);
    }

    public function submitOwnerPackage()
    {
        $input = $this->input->post();
        $id = $this->Laundry_Owner_model->submitOwnerPackage($input);
    }

    public function deleteLaundryOwner()
    {
        $id = $this->uri->segment('3');
        $user = $this->db->get_where('la_user', ['user_id'=>$id])->first_row();
        $this->Laundry_Owner_model->delete( $id );
        $this->session->set_flashdata('success', 'Data berhasil dihapus');

        if (!empty($user) && $user->type==3) {
            redirect('franchise','refresh');
        } else {
            redirect('laundryOwner');    
        }
        
    }


}

