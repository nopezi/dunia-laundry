<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		
		$data['judul'] = 'Data Artikel';	
		$artikel = $this->db->order_by('id', 'desc')->get('la_artikel')->result();

		$data['artikel'] = $artikel;
		echo load_page($this,'artikel/index', $data);

	}

	public function tambah()
	{

		$data['judul'] = 'Tambah Artikel';
		echo load_page($this,'artikel/tambah', $data);

	}

	public function aksi_tambah()
	{
		
		$data = $this->input->post();

		$config['upload_path']   = 'assets/images/artikel/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 10000;
        $config['file_name']     = time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config); 
        $image = "";
        if($this->upload->do_upload('foto')) {
            $uploadData = $this->upload->data();
            $image = $uploadData['file_name']; 
        }

        $this->db->insert('la_artikel', [
        	'title'       => $this->input->post('title'),
        	'image'       => $image,
        	'description' => $this->input->post('isi')
        ]);

        $this->session->set_flashdata('success', 'Berhasil tambah artikel');
        redirect('artikel','refresh');

	}

	public function hapus()
	{
		$id = $this->uri->segment('3');
	}

}

/* End of file Article.php */
/* Location: ./application/controllers/Admin/Article.php */