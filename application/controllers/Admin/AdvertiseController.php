<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdvertiseController extends CI_Controller{

	public function __construct()
  	{
      	parent::__construct();
      	$this->load->database();

  	}

  	public function advertisement()
	{
		$data['advertisement'] = $this->Advertise_model->advertisement();
		echo load_page($this,'advertisement/advertisement',$data);
	}

	public function submitAdvertisement()
	{
		$input = $this->input->post();
		$img = $_FILES['image']['name'];
	    $data = $this->Advertise_model->submitAdvertisement($input,$img);
	}

	public function changeAdvertisementStatus()
	{
		$id = $this->uri->segment('3');
	    $this->Advertise_model->changeAdvertisementStatus($id);
	    $this->session->set_flashdata('error', STATUS_CHANGE);
	    redirect('advertisement');
	}

	public function updateAdvertisement()
	{
		$id = $this->uri->segment('3');
		$data['row'] = $this->Advertise_model->updateAdvertisement($id);
		$data['advertisement'] = $this->Advertise_model->advertisement();
		echo load_page($this,'advertisement/advertisement',$data);
	}

}