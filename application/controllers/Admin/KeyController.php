<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KeyController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

	public function apiKeys()
    {
        $data['firebase'] = $this->Key_model->getFirebaseKey();
        $data['msg'] = $this->Key_model->getMSG91Key();
        $data['razorpay'] = $this->Key_model->getRazorpayKey();
        echo load_page($this,'admin/api_keys',$data);
    }

    public function updateFirebaseKey()
    {
    	$input = $this->input->post();
    	$this->Key_model->updateFirebaseKey($input);
    }

    public function updateMSG91Key()
    {
    	$input = $this->input->post();
    	$this->Key_model->updateMSG91Key($input);
    }

    public function updateRazorpayKey()
    {
    	$input = $this->input->post();
    	$this->Key_model->updateRazorpayKey($input);
    }


}