<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class TiketController extends CI_Controller{

	public function __construct()
    {
        require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        error_reporting(0);
    }

    public function tiketSupport()
    {
    	$res['tiket'] = $this->Tiket_model->tiketSupport();
    	echo load_page($this,'tiket/tiket_support',$res);
    }

    public function tiketApprve()
    {
    	$status = $this->input->post('status');
    	$tiket_id = $this->input->post('tiket_id');
    	$this->Tiket_model->tiketApprve($status,$tiket_id);
        
    }

    public function tiketReject()
    {
    	$status = $this->input->post('status');
    	$tiket_id = $this->input->post('tiket_id');
    	$data = $this->Tiket_model->tiketReject($status,$tiket_id);
        // echo $data;
    }

    public function changeTiketStatus()
    {
    	$status = $this->input->post('status');
    	$tiket_id = $this->input->post('tiket_id');
    	$this->Tiket_model->changeTiketStatus($status,$tiket_id);
    }

    public function setAdminMsg()
    {
    	$message = $this->input->post('message');
    	$tiket_id = $this->input->post('tiket_id');
    	$data = $this->Tiket_model->setAdminMsg($message,$tiket_id);
        // echo $data;
    }


}