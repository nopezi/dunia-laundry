<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function index()
    {
        if (!empty($this->session->userdata('email'))){
            redirect('user/dashboard','refresh');
        } else {
            $this->load->view('auth/login');
        }
    }

    public function login()
    {
    	$email = $this->input->post('email');
    	$password = $this->input->post('password');
        $getdata = $this->Base_model->login($email,$password);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    

    // public function signup()
    // {
    //     $this->load->view('signup');
    // }

    // public function forgotpassword()
    // {
    //     $this->load ->view('forgotpassword');
    // }

    // public function reset_password()
    // {
    //     $email = $this->input->post('email');
    //     $getData = $this->Base_model->resetPassword($email);
    //     if($getData == 1)
    //     {
    //         $this->session->set_flashdata('error', RESET_FLASH_SUCC);
    //         redirect('AuthController/index');
    //     }
    //     else
    //     {
    //         $this->session->set_flashdata('error', VALID_EMAIL);
    //         redirect('AuthController/forgotpassword');
    //     }
    // }

}
