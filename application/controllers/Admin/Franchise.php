<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Franchise extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_franchise');
        if (empty($this->session->userdata('owner_name')))
            redirect('login','refresh');
	}

	public function index()
    {

        $data['user'] = $this->model_franchise->get_data();
        
        echo load_page($this,'franchise/index',$data);
    }

    public function detail()
    {
        $id = $this->uri->segment('4');
        $data['owner'] = $this->model_franchise->detail($id);
        echo load_page($this,'franchise/detail',$data);
    }

    public function payment_history()
    {
        $id = $this->uri->segment('4');
        $data['owner'] = $this->model_franchise->detail($id);
        $data['history'] = $this->model_franchise->payment_history($id);
        echo load_page($this,'franchise/payment_history',$data);
    }

    public function owner_packages()
    {
        $id = $this->uri->segment('4');
        $data['owner'] = $this->Laundry_Owner_model->viewOwnerDetail($id);
        $data['package'] = $this->Laundry_Owner_model->subscriptionPackage();
        $data['history'] = $this->Laundry_Owner_model->subscriptionPayments($id);
        echo load_page($this,'laundry/owner_packages',$data);
    }

	public function tambah()
	{

		$franchise = $this->input->get('type');
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();

        if ($_SESSION['type'] == 3) {
            $data['provinsi_ada'] = $this->db->from('la_regencies lr')
                                        ->join('la_provinces lp', 'lp.id=lr.province_id')
                                        ->where('lr.id', $_SESSION['regional'])
                                        ->get()
                                        ->first_row();
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
        } else {

             $data['provinsi_ada'] = $this->db->get('la_provinces')->result();

        }

        $data['franchise'] = true;
        echo load_page($this,'franchise/tambah_v2',$data);

	}

	public function aksi_tambah()
    {

        $input = $this->input->post();

        $image = $_FILES['image']['name'];

        $cek = $this->model_franchise->tambah($input, $image);

        if ($cek = 'berhasil') {
        	redirect('admin/franchise','refresh');
        } else if ($cek = 'error') {
        	redirect('admin/franchise/tambah','refresh');
        }
    }

    public function update()
    {

        $id = $this->uri->segment('4');

        $data['row'] = $this->Laundry_Owner_model->updateLaundryOwner($id);
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['user'] = $this->Laundry_Owner_model->laundryOwner();

        if (!empty($data['row']['regional'])) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$data['row']['regional']])->first_row();
            $data['provinsi_ada'] = $this->db->get_where('la_provinces', ['id'=>$data['regional']->province_id])->first_row();
            $data['provinsi']   = $this->db->where('id!=',$data['provinsi_ada']->id)->get('la_provinces')->result();
        } else {
            $data['provinsi']   = $this->db->get('la_provinces')->result();
        }

        if (!empty($data['row']['distrik'])) {
            $data['distrik'] = $this->db->get_where('la_districts', ['id'=>$data['row']['distrik']])->first_row(); 
        }
        
        $franchise = $this->input->get('type');
        if ($franchise == 'franchise') {
            $data['franchise'] = true;
        }

        echo load_page($this,'franchise/update',$data);
    }

    public function aksi_update()
    {

        $input = $this->input->post();

        $image = $_FILES['image']['name'];

        $cek = $this->model_franchise->update($input, $image);

        if ($cek = 'berhasil') {
        	redirect('admin/franchise','refresh');
        } else if ($cek = 'error') {
        	redirect('admin/franchise/update/'.$input['user_id']);
        }
    }

    public function ganti_status()
    {
        $id = $this->uri->segment('4');
        $this->model_franchise->ganti_status($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('admin/franchise');
    }

    public function hapus()
    {
        $id = $this->uri->segment('4');
        $user = $this->db->get_where('la_user', ['user_id'=>$id])->first_row();
        $this->model_franchise->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');

        redirect('admin/franchise');
        
    }

    public function aksi_packages()
    {
        $input = $this->input->post();
        $cek   = $this->model_franchise->submitOwnerPackage($input);

        redirect('admin/franchise/owner_packages/'.$input['user_id']);
    }

}

/* End of file FranchiseController.php */
/* Location: ./application/controllers/Admin/FranchiseController.php */