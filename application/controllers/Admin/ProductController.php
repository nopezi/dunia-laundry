<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller{

	public function __construct()
  	{
      	parent::__construct();
      	$this->load->database();
      	error_reporting(0);
  	}

  	public function product()
	{
		$ses = $_SESSION;
        if($ses['status'] == '2')
        {
			$data['laundry'] = $this->Product_model->getLaundryShopByUserId($ses['id']);
		}
		else
		{
			$data['laundry'] = $this->Product_model->getLaundryShop();
		}
		$data['product'] = $this->Product_model->product();
		$data['service'] = $this->Product_model->getService();
		echo load_page($this,'product/product',$data);
	}

	public function submitProduct()
	{
		$input = $this->input->post();
	    $img = $_FILES['image']['name'];
	    $data = $this->Product_model->submitProduct($input,$img);
	}

	public function changeProductStatus()
	{
		$id = $this->uri->segment('3');
	    $this->Product_model->changeProductStatus($id);
	    $this->session->set_flashdata('error', STATUS_CHANGE);
	    redirect('product');
	}

	public function updateProduct()
	{
		$id = $this->uri->segment('3');
		$data['row'] = $this->Product_model->updateProduct($id);
		$data['product'] = $this->Product_model->product();
		$data['laundry'] = $this->Product_model->getLaundryShop();
		$data['service'] = $this->Product_model->getService();
		echo load_page($this,'product/product',$data);
	}

}