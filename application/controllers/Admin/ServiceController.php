<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function service()
    {
    	$data['service'] = $this->Service_model->service();
    	echo load_page($this,'service/service',$data);
    }

    public function submitService()
  	{
  		$input = $this->input->post();
	    $img = $_FILES['image']['name'];
	    $data = $this->Service_model->submitService($input,$img);
  	}

  	public function updateService()
    {
		$id = $this->uri->segment('3');
		$res['row'] = $this->Service_model->updateService($id);
		$res['service'] = $this->Service_model->service();
		echo load_page($this,'service/service',$res);
    }

    public function changeServiceStatus()
    {
    	
		$id = $this->uri->segment('3');
		$this->Service_model->changeServiceStatus($id);
		$this->session->set_flashdata('error', STATUS_CHANGE);
		redirect('service');
    }


}
