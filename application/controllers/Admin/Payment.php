<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  
class Payment extends CI_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        // $this->load->library(array('form_validation','session'));
        // $this->load->helper(array('url','html','form'));
             
    }

    public function subscriptionPackage()
    {
        $data['package'] = $this->Payment_model->subscriptionPackage();
        echo load_page($this,'payment/subscription_package',$data);
    }

    public function submitSubscriptionPackage()
    {
        $input = $this->input->post();
        $img = $_FILES['image']['name'];
        $data = $this->Payment_model->submitSubscriptionPackage($input,$img);
    }

    public function changeSubscriptionPackageStatus()
    {
        $id = $this->uri->segment('3');
        $this->Payment_model->changeSubscriptionPackageStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('subscriptionPackage');
    }

    public function updateSubscriptionPackage()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Payment_model->updateSubscriptionPackage($id);
        $data['package'] = $this->Payment_model->subscriptionPackage();
        echo load_page($this,'payment/subscription_package',$data);
    }

    public function razorpay()
    {
        $this->load->view('payment/razorpay');
    } 
  
    public function Subscription()
    {
        $data['package'] = $this->Payment_model->Subscription();
        $data['razorpay'] = $this->Key_model->getRazorpayKey();
        echo load_page($this,'payment/subscription',$data);
    }   
    public function razorPaySuccess()
    { 
        $ses = $_SESSION;
        $user_id = $ses['id'];
        $no_of_days = $this->input->post('no_of_days');
        $totalAmount = $this->input->post('totalAmount');
        $no = $no_of_days+'1';
        $date = date('Y-m-d', strtotime("+$no days"));
        $data = [
               'user_id' => $user_id,
               'payment_id' => $this->input->post('razorpay_payment_id'),
               'amount' => $totalAmount,
               'package_id' => $this->input->post('package_id'),
               'end_subscription_date' => $date,
               'created_at' => date('Y-m-d H:i:s'),
            ];

        $insert = $this->db->insert('la_payments', $data);

        $user = $this->db->where('user_id',$user_id)->get('la_user')->row_array();
        if($user['end_subscription_date'] !='')
        {
        	if($user['end_subscription_date'] >= date('Y-m-d'))
        	{
        		$end_subscription_date = $user['end_subscription_date'];
        		$now = time(); // or your date as well
		        $your_date = strtotime("$end_subscription_date");
		        $datediff =  $your_date - $now;

		        $day = round($datediff / (60 * 60 * 24));
		        $num = $day+$no;
		        $dates = date('Y-m-d', strtotime("+$num days"));
		        // echo $dates; die;
        	}
        	else
        	{
        		$no = $no_of_days+'1';
        		$dates = date('Y-m-d', strtotime("+$no days"));
        	}
        	
        }
        else
        {
        	$no = $no_of_days+'1';
        	$dates = date('Y-m-d', strtotime("+$no days"));
        }

        $data1 = array(
            'end_subscription_date' => $dates
        );
        $sucess = $this->db->where('user_id',$user_id)->update('la_user',$data1);
        if($sucess)
        {
            $email = $user['email'];
            $name = $user['name'];
            $subject = SUBSCRIPTION_HEAD;
            $msg = "Hello $name this is your subscription confirmation message, Your subscription package is $no_of_days days it cost you $totalAmount Rs. and it will be available till $dates, thank you.";
            $this->Api_Auth_model->send_email_by_msg($email,$subject,$msg);
        }
        $arr = array('msg' => 'Payment successfully credited', 'status' => true);  
    }
    public function RazorThankYou()
    {
     $this->load->view('payment/razorthankyou');
    }

    public function invoiceHistory()
    {
        $data['invoice'] = $this->Payment_model->invoiceHistory();
        echo load_page($this,'payment/invoice_history',$data);
    }

    public function myPackage()
    {
        $data['invoice'] = $this->Payment_model->myPackage();
        echo load_page($this,'payment/my_package',$data);
    }

}