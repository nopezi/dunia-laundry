<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('model_franchise');
        $this->load->model('model_agen');
        $this->load->model('model_dashboard');
        if (empty($this->session->userdata('email')))
            redirect('login','refresh');

    }

	public function index()
	{
       
        if (isset($_SESSION['id'])) {

            $wilayah = null;

            $res['user'] = $this->User_model->user_limit();

            if (!empty($_SESSION['regional']) && $_SESSION['type'] == 3) {
              $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
              $wilayah = 'Wilayah '. $regional->name;
              $res['user'] = $this->User_model->user_limit($_SESSION['regional']);
            }

            $res['wilayah'] = $wilayah;

            $shop_id = $this->db->get_where('la_laundry_shop', ['user_id'=>$_SESSION['id']])->first_row();

            if ($_SESSION['type'] == 1) {

                $res['halaman']      = 'Agen';
                $res['shop_count']   = $this->model_agen->total_pendapatan($shop_id->shop_id);
                $res['income_count'] = $this->model_agen->total_komisi($shop_id->shop_id);
                $res['user_count']   = $this->model_agen->total_pembeli($shop_id->shop_id);
                $res['order_count']  = $this->model_agen->total_order($shop_id->shop_id);

            } else if ($_SESSION['type'] == 2) {

                $res['order_count']  = $this->Order_model->getTotalOrder();
                $res['user_count']   = $this->Order_model->getTotalUser();
                $res['halaman']      = 'Mitra';
                $res['income_count'] = '0';//$this->Order_model->getTotalIncome();
                $res['shop_count']   = '0';

            } else if ($_SESSION['type'] == 3) {

                $res['halaman'] = 'Franchise';
                // $res['income_count'] = $this->Order_model->getTotalIncome_v2();
                $komisi_franchise = $this->model_franchise->total_komisi();
                $income_count = $this->Order_model->komisi_all_franchise();
                $res['income_count'] = 'Rp.'.$komisi_franchise['total_pendapatan_bersih'];
                $res['shop_count']   = 'Rp.'.$komisi_franchise['total_pendapatan_kotor'];
                $res['order_count']  = $komisi_franchise['total_pesanan'];
                $res['user_count']   = $komisi_franchise['total_pembeli'];

            } else {

                $res['user_count']   = $this->model_dashboard->getUserCount();
                $res['owner_count']  = $this->User_model->getLoundryOwnerCount();
                $res['shop_count']   = $this->Laundry_shop_model->getShopCount();
                // $res['income_count'] = $this->Order_model->getTotalIncome();
                $res['income_count'] = $this->model_dashboard->total_all_komisi();

            }

            // echo "<pre>";
            // die(print_r($res['income_count']));
            // die();

            if ( isset( $_SESSION['role'] ) && 'mitra' === $_SESSION['role'] ) {
              echo load_page($this,'admin/dashboard_mitra',$res);
            } else {
		      echo load_page($this,'admin/dashboard',$res);
            }

        } else {
            redirect('login');
        }
    }
   
}
