<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PencairanDana extends CI_Controller {

	public function index()
	{

		$status_pesanan = $this->input->get('status')?:0;
		// $data = $this->db->order_by('id', 'desc')->get('la_pencairan_dana')->result();
		$data = $this->db->order_by('id', 'desc')->where('status', $status_pesanan)->get('la_pencairan_dana')->result();
		// foreach ($data as $key => $value) {
		for ($i=0; $i < sizeof($data); $i++) { 


			$data[$i]->tanggal = format_hari_tanggal($data[$i]->created_at);
			if (empty($data[$i]->status)) {
				$data[$i]->status = 'Request';
				$data[$i]->label  = 'label-warning'; 
				$data[$i]->pilihan_status = [
					['id' => '', 'nama' => 'Request'],
					['id' => 1, 'nama' => 'Dikirim'],
					['id' => 2, 'nama' => 'Ditolak']
				];
			} else if ($data[$i]->status == 1) {
				$data[$i]->status = 'Dikirim';
				$data[$i]->label  = 'label-info';
				$data[$i]->pilihan_status = [
					['id' => 1, 'nama' => 'Dikirim'],
					['id' => '', 'nama' => 'Request'],
					['id' => 2, 'nama' => 'Ditolak']
				];
			} else if ($data[$i]->status == 2) {
				$data[$i]->status = 'Ditolak';
				$data[$i]->label  = 'label-danger'; 
				$data[$i]->pilihan_status = [
					['id' => 2, 'nama' => 'Ditolak'],
					['id' => 1, 'nama' => 'Dikirim'],
					['id' => '', 'nama' => 'Request']
				];
			}

			if (!empty($data[$i]->id_user)) {
				$d_user = $this->db->get_where('la_user', ['user_id'=>$data[$i]->id_user])->first_row();

				if (empty($d_user)) {
					unset($data[$i]);	
				} else  {
					$data[$i]->username = $d_user->name;
				}
			}


		}

		// $nama_tab= [
		// 	['id'=>1, 'status'=>'Request'],
		// 	['id'=>2, 'status'=>'Di kirim'],
		// 	['id'=>3, 'status'=>'Di tolak'],
		// ];

		$nama_tab = ['Request', 'Dikirim', 'Ditolak'];
		// $data_riwayat = $this->db->order_by('id', 'desc')->where('status', $status_pesanan+1)->get('la_pencairan_dana')->result();
		// foreach ($data_riwayat as $key => $value) {
		// 	$value->tanggal = format_hari_tanggal($value->created_at);
		// 	if (!empty($value->id_user)) {
		// 		$d_user = $this->db->get_where('la_user', ['user_id'=>$value->id_user])->first_row();
		// 		$value->username = $d_user->name;
		// 	}
		// }
		for ($i=0; $i < sizeof($nama_tab); $i++) {

	      if ($status_pesanan == $i) {

	        $aktif[] = [
	          'id'     => $i,
	          'status'   => $nama_tab[$i],
	          'active' => 'active'
	        ];

	      } else {

	        $aktif[] = [
	          'id'     => $i,
	          'status'   => $nama_tab[$i],
	          'active' => false
	        ];

	      }

	    }

	    $d['aktif'] = $aktif;
		$d['riwayat_pengajuan'] = $data;

		// echo "<pre>";
		// die(print_r($d['aktif']));

		echo load_page($this,'pencairan_dana/index',$d);

		// echo "<pre>";
		// print_r($data);
		
	}

	public function update_pencairan()
	{

		$id     = $this->input->post('id_pencairan_dana');
		$note   = $this->input->post('note');
		$status = $this->input->post('status');

		$update['note']   = $note;
		$update['status'] = $status;

		if (!empty($_FILES['image']['name'])) {
			
			$config['upload_path']   = 'assets/images/Laundry/';
	        $config['allowed_types'] = 'gif|jpg|jpeg|png';
	        $config['overwrite']     = TRUE;
	        $config['max_size']      = 10000;
	        $config['file_name']     = 'bukti_transfer_'.time();
	        $this->load->library('upload',$config);
	        $this->upload->initialize($config);

	        $bukti = null;

	        if ($this->upload->do_upload('image')) {
	            $uploadData = $this->upload->data(); 
	            $bukti = $uploadData['file_name']; 
	        } else {
	            $msg = $this->upload->display_errors('', '');
	            $this->session->set_flashdata('gagal', $msg);
	            redirect('Admin/pencairanDana','refresh');
	        }

	        $update['bukti_transfer'] = $bukti;

		}
		
		$this->db->update('la_pencairan_dana', $update, ['id'=>$id]);

		$this->session->set_flashdata('berhasil', 'berhasil update pencairan dana');
		redirect('Admin/pencairanDana','refresh');

	}

}

/* End of file PencairanDana.php */
/* Location: ./application/controllers/Admin/PencairanDana.php */