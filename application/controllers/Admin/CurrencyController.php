<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CurrencyController extends CI_Controller
{

	public function __construct()
  	{
		parent::__construct();
		$this->load->database();
		error_reporting(0);
  	}

  	public function currency()
  	{
  		$data['currency'] = $this->Currency_model->currency();
  		echo load_page($this,'currency/currency',$data);
  	}

  	public function submitCurrency()
  	{
  		$input = $this->input->post();
  		$response = $this->Currency_model->submitCurrency($input);
  		if($response == '0')
  		{
  			$this->session->set_flashdata('error', DATA_UPDATE);
  		}else{
  			$this->session->set_flashdata('error', DATA_SUBMIT);
  		}
  		redirect('currency/currency');
  	}

  	public function setting()
  	{
  		$data['currency_setting'] = $this->Currency_model->currency();
        $currency_setting = $this->Currency_model->getActiveCurrency();
        $data['currency_type'] = $currency_setting->currency_symbol;
  		echo load_page($this,'currency/currency_setting',$data);
  	}

  	public function updateCurrencySetting()
  	{
  		$currency_id = $this->input->post('currency', TRUE);
		$this->Currency_model->updateSingleRow('la_currency', array('status'=>1), array('status'=>0));
		$this->Currency_model->updateSingleRow('la_currency', array('currency_id'=>$currency_id), array('status'=>1));
		$this->session->set_flashdata('error', 'Currency Type changed successfully.');
		redirect('currency/setting');
  	}

  	public function updateCurrency()
  	{
  		$id = $this->uri->segment('3');
  		$data['row'] = $this->Currency_model->updateCurrency($id);
  		$data['currency'] = $this->Currency_model->currency();
  		echo load_page($this,'currency/currency',$data);
  	}

  	public function deleteCurrency()
  	{
  		$id = $this->uri->segment('3');
  		$data = $this->Currency_model->deleteCurrency($id);
  		if($data == '1')
  		{
  			$this->session->set_flashdata('error', 'Currency Delete successfully.');
			redirect('currency/currency');
  		}
  	}


}