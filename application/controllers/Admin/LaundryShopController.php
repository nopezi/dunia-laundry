<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaundryShopController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('model_agen');

    }

    public function laundryShop()
    {
    	$data['laundryData'] = $this->Laundry_shop_model->laundryShop();

        $data['heading'] = 'Outlet';

        if ($_SESSION['type'] == 3) {
            $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul'] = 'Outlet Wilayah '.$regional->name;
        } else {
            $data['judul'] = 'Outlet';
        }
        $data['outlet'] = true;
        $data['type']   = 'shop';

    	echo load_page($this,'laundry/laundry_shop',$data);
    }

    public function laundryShopAdd()
    {

        $type = null;
        if (!empty($_GET['type'])) {
            if ($_GET['type'] == 'agen') {
                $type = '1';
                $data['link_kembali'] = base_url('laundry/laundryShopAgen');
                $data['nama_halaman'] = 'Agen';
            } else if ($_GET['type'] == 'outlet') {
                $type = '2';
                $data['link_kembali'] = base_url('laundryShop');
                $data['nama_halaman'] = 'Outlet';
            }
        } else {
            $data['link_kembali'] = base_url('laundryShop');
        }

        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['laundry'] = $this->Laundry_shop_model->getAllLaundryOwner($type);
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';
        $judul = 'Outlet';

        if ($_SESSION['type'] == 3) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul']    = $judul.' Wilayah '.$data['regional']->name;
            $data['halaman']  = ucfirst($judul);
        } else {
            $data['judul']    = $judul;
            $data['halaman']  = ucfirst($judul);
        }

        if ($_GET['type'] == 'agen') {
            echo load_page($this,'laundry/add_laundry_agen',$data);
        } else {
            echo load_page($this,'laundry/add_laundry_shop',$data);
        }
        
    }

    public function submitLaundryShop()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$id = $this->Laundry_shop_model->submitLaundryShop($input,$image);
    	
    }

    public function changeLaundryShopStatus()
    {
        $id = $this->uri->segment('3');
        $this->Laundry_shop_model->changeLaundryShopStatus($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('laundryShop');
    }

    public function updateLaundryShop()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Laundry_shop_model->updateLaundryShop($id);
        $data['laundry'] = $this->Laundry_shop_model->getAllLaundryOwner();
        $data['laundryData'] = $this->Laundry_shop_model->laundryShop();
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';

        $judul = 'Outlet';

        if (!empty($_GET['type'])) {
            $judul = $_GET['type'];
            $data['link_kembali'] = base_url('laundry/laundryShopAgen');
        } else {
            $data['link_kembali'] = base_url('laundryShop');
        }

        if (!empty($_GET['type'])) {
            if ($_GET['type'] == 'agen') {
                $type = '1';
                $data['link_kembali'] = base_url('laundry/laundryShopAgen');
                $data['nama_halaman'] = 'Agen';
            } else if ($_GET['type'] == 'outlet') {
                $type = '2';
                $data['link_kembali'] = base_url('laundryShop');
                $data['nama_halaman'] = 'Outlet';
            } else if ($_GET['type'] == 'franchise') {
                $type = '3';
                $data['link_kembali'] = base_url('franchise');
                $data['nama_halaman'] = 'Franchise';
            } else if ($_GET['type'] == 'shop') {
                $type = '3';
                $data['link_kembali'] = base_url('laundryShop');
                $data['nama_halaman'] = 'Shop';
            } 
        } else {
            $data['link_kembali'] = base_url('laundryShop');
            $data['nama_halaman'] = 'Shop';
        }

        if ($_SESSION['type'] == 3) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul']    = $judul.' Wilayah '.$data['regional']->name;
            $data['halaman']  = ucfirst($judul);
        } else {
            $data['judul']    = $judul;
            $data['halaman']  = ucfirst($judul);
        }

        echo load_page($this,'laundry/add_laundry_shop',$data);
    }

    public function deleteLaundryShop()
    {
        $id = $this->uri->segment('3');
        $this->Laundry_shop_model->delete( $id );
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('laundryShop');
    }


}