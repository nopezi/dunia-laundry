<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_outlet');
        if (empty($this->session->userdata('owner_name')))
            redirect('login','refresh');
	}

	public function index()
	{

        $data['heading'] = 'Outlet';

        if ($_SESSION['type'] == 3) {
            $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul'] = 'Outlet Wilayah '.$regional->name;
        } else {
            $data['judul'] = 'Outlet';
        }
        $data['outlet'] = true;
        $data['type']   = 'shop';

        $type = $this->input->get('type')?:0;
        $nama_tab = ['Biasa', 'Premium'];

        for ($i=0; $i < sizeof($nama_tab); $i++) { 

          if ($type == $i) {

            $aktif[] = [
              'id'     => $i,
              'name'   => $nama_tab[$i],
              'active' => 'active'
            ];

          } else {

            $aktif[] = [
              'id'     => $i,
              'name'   => $nama_tab[$i],
              'active' => false
            ];

          }

        }
        $data['aktif'] = $aktif;

        $data_outlet = $this->db->select('la_laundry_shop.*')
                                ->join('la_user', 'la_user.user_id=la_laundry_shop.user_id')
                                ->where('type_shop', $type)
                                ->where('la_user.type', '2')
                                ->order_by('la_laundry_shop.s_no', 'desc')
                                ->get('la_laundry_shop')
                                ->result();

        $data['laundryData'] = $data_outlet;

    	echo load_page($this,'outlet/index',$data);

	}

	public function tambah()
    {

        $data['count_code'] = $this->model_outlet->getCountryCode();
        $data['laundry']    = $this->model_outlet->get_mitra();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';

        echo load_page($this,'outlet/tambah',$data);
        
    }

    public function update()
    {

        $id = $this->uri->segment('4');
        $data['row']         = $this->model_outlet->updateLaundryShop($id);
        $data['laundry']     = $this->model_outlet->get_mitra();
        $data['laundryData'] = $this->model_outlet->data_outlet();
        $data['count_code']  = $this->model_outlet->getCountryCode();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';

        echo load_page($this,'outlet/update_outlet',$data);
    }

    public function aksi_tambah()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$cek   = $this->model_outlet->tambah($input,$image);

        if ($cek == 'berhasil') {
            redirect('admin/outlet','refresh');
        } else if ($cek == 'error') {
            redirect('admin/outlet/tambah','refresh');
        }
    }

    public function aksi_update()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$cek   = $this->model_outlet->update($input, $image);

        if ($cek == 'berhasil') {
            redirect('admin/outlet','refresh');
        } else if ($cek == 'error') {
            redirect('admin/outlet','refresh');
        }
    }

    public function ganti_status()
    {
        $id = $this->uri->segment('4');
        $this->model_outlet->ganti_status($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('admin/outlet');
    }

	public function delete()
    {
        $id = $this->uri->segment('4');
        $this->model_outlet->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/outlet');
    }

}

/* End of file OutletController.php */
/* Location: ./application/controllers/Admin/OutletController.php */