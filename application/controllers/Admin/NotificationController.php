<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationController extends CI_Controller{

	public function __construct()
    {
        require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();

    }

    public function notification()
    {
        $res['user'] = $this->Notification_model->activeUser();
        echo load_page($this,'notification/notification',$res);
    }

    public function send_notification()
    {
        $input = $this->input->post();
        $data = $this->Notification_model->send_notification($input);
        $this->session->set_flashdata('error', SEND_NOTIF);
        redirect('notification');
    }

}