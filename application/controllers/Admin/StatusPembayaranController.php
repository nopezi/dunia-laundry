<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class StatusPembayaranController extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	public function index()
	{
		$this->db->order_by('id', 'desc')->get('la_pencairan_dana')->result();
	}

}

/* End of file StatusPembayaranController.php */
/* Location: ./application/controllers/Admin/StatusPembayaranController.php */