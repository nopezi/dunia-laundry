<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OfferController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function offer()
    {
    	$data['offer'] = $this->Offer_model->offer();

        $res = $this->db->where('status','1')->where('end_date <',date('Y-m-d'))->get('la_offer')->result();
        // echo "<pre>";
        // print_r($res); die;
        foreach ($res as $r) 
        {
            $data1 = array(
                'status' => '0',
                );
            $this->db->where('offer_id',$r->offer_id);
            $this->db->update('la_offer',$data1);
        }
        echo load_page($this,'offer/offer',$data);
    }

    public function offerAdd()
    {
        $data['service'] = $this->Offer_model->getAllService();
    	$data['shop'] = $this->Offer_model->getAllLaundryShop();
    	echo load_page($this,'offer/add_offer',$data);
    }

    public function submitOffer()
    {
        $input = $this->input->post();
        $image = $_FILES['image']['name'];
        $id = $this->Offer_model->submitOffer($input,$image);
    }

    public function changeOfferStatus()
    {
        $id = $this->uri->segment('3');
        $this->Offer_model->changeOfferStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('offer');
    }

    public function updateOffer()
    {
        $id = $this->uri->segment('3');
        $data['row'] = $this->Offer_model->updateOffer($id);
        $data['service'] = $this->Offer_model->getAllService();
        $data['shop'] = $this->Offer_model->getAllLaundryShop();
        echo load_page($this,'offer/add_offer',$data);
    }

}