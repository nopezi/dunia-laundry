<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Komisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
        $this->load->model('Komisi_model');
		if (empty($this->session->userdata('owner_name')))
			redirect('laundryOwner/login','refresh');
	}

	public function index()
    {
        
        $ses = $_SESSION;
        $data['order'] = $this->Order_model->komisi();
        $data['type'] = $ses['type'] === '2' ? 'mitra' : 'agen';

        $data['persentasi'] = $this->db->get('la_persentasi_komisi')->result();

        $wilayah = null;

        if (!empty($_SESSION['regional']) && $_SESSION['type'] == 3) {
          $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
          $wilayah  = 'Wilayah '. $regional->name;
        }

        $data['wilayah'] = $wilayah;

        echo load_page_franchise($this,'franchise_admin/komisi/komisi',$data);

    }

	public function all_komisi($type=null)
    {
        
        if (empty($type)) {
            $type = 1;
        } else if ($type != 1 && $type != 2 && $type != 3) {
            $type = 1;
        }

        $nama_tab = [
            ['no'=>1, 'menu'=>'Agen'],
            ['no'=>2, 'menu'=>'Mitra'],
            ['no'=>3, 'menu'=>'Franchise'],
        ];

        for ($i=0; $i < sizeof($nama_tab); $i++) {

          if ($type == $nama_tab[$i]['no']) {

            $aktif[] = [
              'id'     => $nama_tab[$i]['no'],
              'status' => $nama_tab[$i]['menu'],
              'active' => 'active'
            ];

          } else {

            $aktif[] = [
              'id'     => $nama_tab[$i]['no'],
              'status' => $nama_tab[$i]['menu'],
              'active' => false
            ];

          }

        }

        $data['aktif'] = $aktif;

        if ($type == 3) {
            $data['komisi_all'] = $this->Order_model->komisi_all_franchise();
        } else {
            $data['komisi_all'] = $this->Order_model->komisi_all_per_type($type);
        }
        
        $data['persentasi'] = $this->db->get('la_persentasi_komisi')->result();
        $data['type'] = $type;

        echo load_page_franchise($this,'komisi/all_komisi', $data);
    }

    public function withdraw()
    {

        $input = $this->input->post();
        $total_maksimal = $this->input->post('total_maksimal');
        $jumlah_pengajuan = $this->input->post('jumlah');

        if ($jumlah_pengajuan > $total_maksimal) {
            $msg = "Jumlah pengajuan dana tidak boleh lebih dari pendapatan bersih";
            $this->session->set_flashdata('gagal', $msg);
            redirect('komisi','refresh');
        }

        $config['upload_path']   = 'assets/images/Laundry/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['overwrite']     = TRUE;
        $config['max_size']      = 10000;
        $config['file_name']     = 'ktp_sim_'.time();
        $this->load->library('upload',$config);
        $this->upload->initialize($config);

        $id_card = null;

        if ($this->upload->do_upload('image')) {
            $uploadData = $this->upload->data(); 
            $id_card = $uploadData['file_name']; 
        } else {
            $msg = $this->upload->display_errors('', '');
            $this->session->set_flashdata('gagal', $msg);
            redirect('komisi','refresh');
        }

        $input['id_user'] = $_SESSION['id'];
        $input['id_card'] = $id_card;

        $this->db->insert('la_pencairan_dana', $input);
        $this->session->set_flashdata('berhasil', 'Berhasil mengirim pengajuan dana');
        redirect('komisi','refresh');
    }

    public function persentasi_komisi()
    {

        $data['persentasi'] = $this->db->get('la_persentasi_komisi')->result();

        echo load_page_franchise($this, 'komisi/persentasi_komisi', $data);

    }

    public function update_persentasi()
    {

        $where['id'] = $this->input->post('id_persentasi');
        $set['persentasi'] = $this->input->post('jumlah');

        $this->db->update('la_persentasi_komisi', $set, $where);
        $this->session->set_flashdata('berhasil', 'Berhasil update jumlah persen komisi');
        redirect('persentasi_komisi','refresh');

    }

    public function updateKomisi()
    {
    	$input = $this->input->post();
    	$this->Komisi_model->updateKomisi($input);
    }

}

/* End of file Komisi.php */
/* Location: ./application/controllers/franchise/Komisi.php */