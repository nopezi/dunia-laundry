<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_profile');
		if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
	}

	public function index()
    {
        $id = $this->input->get('id');
        $data['row'] = $this->User_model->updateUser($id);
        $data['user'] = $this->User_model->user();
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        echo load_page_franchise($this,'franchise_admin/profile/index',$data);
    }

    public function aksi_update()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];

        $cek = $this->model_profile->update($input, $image);

        redirect('franchise/profile'.'?id='.$input['user_id']);
    	
    }

}

/* End of file Profile.php */
/* Location: ./application/controllers/franchise/Profile.php */