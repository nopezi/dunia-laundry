<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_outlet');
        if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
	}

	public function index()
	{
		
		$data['laundryData'] = $this->model_outlet->data_outlet();

        $data['heading'] = 'Outlet';

        if ($_SESSION['type'] == 3) {
            $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul'] = 'Outlet Wilayah '.$regional->name;
        } else {
            $data['judul'] = 'Outlet';
        }
        $data['outlet'] = true;
        $data['type']   = 'shop';

    	echo load_page_franchise($this,'franchise_admin/outlet/index',$data);

	}

	public function tambah()
    {

        $data['count_code'] = $this->model_outlet->getCountryCode();
        $data['laundry']    = $this->model_outlet->get_mitra();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';

        echo load_page_franchise($this,'franchise_admin/outlet/tambah',$data);
        
    }

    public function update()
    {

        $id = $this->uri->segment('4');
        $data['row']         = $this->model_outlet->updateLaundryShop($id);
        $data['laundry']     = $this->model_outlet->get_mitra();
        $data['laundryData'] = $this->model_outlet->data_outlet();
        $data['count_code']  = $this->model_outlet->getCountryCode();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';

        echo load_page_franchise($this,'franchise_admin/outlet/update_outlet',$data);
    }

    public function aksi_tambah()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$cek   = $this->model_outlet->tambah($input,$image);

        if ($cek == 'berhasil') {
            redirect('franchise/outlet','refresh');
        } else if ($cek == 'error') {
            redirect('franchise/outlet/tambah','refresh');
        }
    }

    public function aksi_update()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$cek   = $this->model_outlet->update($input, $image);

        if ($cek == 'berhasil') {
            redirect('franchise/outlet','refresh');
        } else if ($cek == 'error') {
            redirect('franchise/outlet','refresh');
        }
    }

    public function ganti_status()
    {
        $id = $this->uri->segment('4');
        $this->model_outlet->ganti_status($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('franchise/outlet');
    }

	public function delete()
    {
        $id = $this->uri->segment('4');
        $this->model_outlet->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('franchise/outlet');
    }

}

/* End of file Outlet.php */
/* Location: ./application/controllers/franchise/Outlet.php */