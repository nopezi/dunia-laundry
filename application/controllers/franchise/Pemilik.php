<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_pemilik');
		if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
	}

	public function index()
	{
		$data['user'] = $this->model_pemilik->get_pemilik();
        echo load_page_franchise($this,'franchise_admin/pemilik/index',$data);		
	}

	public function detail()
    {
        $id = $this->uri->segment('4');
        $data['owner'] = $this->model_pemilik->detail($id);
        echo load_page_franchise($this,'franchise_admin/pemilik/detail',$data);
    }

    public function payment_history()
    {
        $id = $this->uri->segment('4');
        $data['owner']   = $this->model_pemilik->detail($id);
        $data['history'] = $this->model_pemilik->payment_history($id);
        echo load_page_franchise($this,'franchise_admin/pemilik/payment_history',$data);
    }

    public function owner_packages()
    {
        $id = $this->uri->segment('4');
        $data['owner'] = $this->Laundry_Owner_model->viewOwnerDetail($id);
        $data['package'] = $this->Laundry_Owner_model->subscriptionPackage();
        $data['history'] = $this->Laundry_Owner_model->subscriptionPayments($id);
        echo load_page_franchise($this,'franchise_admin/pemilik/owner_packages',$data);
    }

	public function tambah()
    {

        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();

        if ($_SESSION['type'] == 3) {
            $data['provinsi_ada'] = $this->db->from('la_regencies lr')
                                        ->join('la_provinces lp', 'lp.id=lr.province_id')
                                        ->where('lr.id', $_SESSION['regional'])
                                        ->get()
                                        ->first_row();
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
        } else {

             $data['provinsi_ada'] = $this->db->get('la_provinces')->result();

        }

        echo load_page_franchise($this,'franchise_admin/pemilik/tambah',$data);
    }

    public function update()
    {

        $id = $this->uri->segment('4');
        $data['row'] = $this->Laundry_Owner_model->updateLaundryOwner($id);
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['user'] = $this->Laundry_Owner_model->laundryOwner();

        if (!empty($data['row']['regional'])) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$data['row']['regional']])->first_row();
            $data['provinsi_ada'] = $this->db->get_where('la_provinces', ['id'=>$data['regional']->province_id])->first_row();
            $data['provinsi']   = $this->db->where('id!=',$data['provinsi_ada']->id)->get('la_provinces')->result();
        } else {
            $data['provinsi']   = $this->db->get('la_provinces')->result();
        }

        if (!empty($data['row']['distrik'])) {
            $data['distrik'] = $this->db->get_where('la_districts', ['id'=>$data['row']['distrik']])->first_row(); 
        }

        echo load_page_franchise($this,'franchise_admin/pemilik/update',$data);
    }

    public function aksi_tambah()
    {

        $input = $this->input->post();

        $image = $_FILES['image']['name'];

        $cek = $this->model_pemilik->tambah($input, $image);

        if ($cek = 'berhasil') {
        	redirect('franchise/pemilik','refresh');
        } else if ($cek = 'error') {
        	redirect('franchise/pemilik/tambah','refresh');
        }

    }

    public function aksi_update()
    {

        $input = $this->input->post();

        $image = $_FILES['image']['name'];

        $cek = $this->model_pemilik->update($input, $image);

        if ($cek = 'berhasil') {
        	redirect('franchise/pemilik','refresh');
        } else if ($cek = 'error') {
        	redirect('franchise/pemilik/tambah'.$input['user_id']);
        }
        
    }

    public function ganti_status()
    {
        $id = $this->uri->segment('4');
        $this->model_pemilik->ganti_status($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        redirect('franchise/pemilik');
    }

    public function delete()
    {
        $id = $this->uri->segment('4');
        $user = $this->db->get_where('la_user', ['user_id'=>$id])->first_row();
        $this->model_pemilik->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');

        redirect('franchise/pemilik');
        
    }

    public function aksi_packages()
    {
        $input = $this->input->post();
        $cek   = $this->model_pemilik->submitOwnerPackage($input);

        redirect('franchise/pemilik/owner_packages/'.$input['user_id']);
    }

}

/* End of file Pemilik.php */
/* Location: ./application/controllers/franchise/Pemilik.php */