<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Agen extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('model_agen');
        if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');

    }

	public function index()
    {
        $data['laundryData'] = $this->model_agen->data_agen();

        $data['heading'] = 'Agen';

        if ($_SESSION['type'] == 3) {
            $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul'] = 'Agen Wilayah '.$regional->name;
        } else {
            $data['judul'] = 'Agen';
        }

        echo load_page_franchise($this,'franchise_admin/agen/index',$data);
    }

    public function tambah()
    {

        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        $data['laundry'] = $this->model_agen->get_mitra();
        $data['laundry_type'] = isset( $_GET['type'] ) ? $_GET['type'] : 'mitra';
        $judul = 'Agen';

        if ($_SESSION['type'] == 3) {
            $data['regional'] = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
            $data['judul']    = $judul.' Wilayah '.$data['regional']->name;
            $data['halaman']  = ucfirst($judul);
        } else {
            $data['judul']    = $judul;
            $data['halaman']  = ucfirst($judul);
        }

        $data['link_kembali'] = base_url('franchise/agen');
        $data['nama_halaman'] = 'Agen';

        echo load_page_franchise($this,'franchise_admin/agen/tambah_agen',$data);
        
    }

    public function aksi_tambah()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];
    	$cek = $this->model_agen->tambah($input,$image);
        redirect('franchise/agen','refresh');
    	
    }

	public function update()
    {

        $id = $this->uri->segment('4');
        $data['row'] = $this->model_agen->detail_agen($id);

        $data['laundry'] = $this->Laundry_shop_model->getAllLaundryOwner();
        $data['laundryData'] = $this->Laundry_shop_model->laundryShop();
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();

        echo load_page_franchise($this,'franchise_admin/agen/update_agen',$data);
    }

    public function aksi_update()
    {
        $input = $this->input->post();

        $image = !empty($_FILES['image']['name'])?:null;
        $cek = $this->model_agen->update($input,$image);

        if ($cek == 'berhasil') {
            redirect('franchise/agen','refresh');
        } else if ($cek == 'error') {
            redirect('franchise/agen/update/'.$input['shop_id']);
        }
    }

    public function ganti_status()
    {
        $id = $this->uri->segment('4');
        $cek = $this->model_agen->ganti_status($id);
        $this->session->set_flashdata('success', STATUS_CHANGE);
        
        redirect('franchise/agen');
    }

    public function delete()
    {
        $id = $this->uri->segment('4');
        $this->model_agen->delete($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('franchise/agen');
    }

}

/* End of file AgenController.php */
/* Location: ./application/controllers/Admin/AgenController.php */