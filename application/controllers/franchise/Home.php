<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_franchise');
		$this->load->model('model_dashboard');
		if (empty($this->session->userdata('owner_name')))
			redirect('laundryOwner/login','refresh');
	}

	public function index()
	{
		
		if (!empty($_SESSION['regional']) && $_SESSION['type'] == 3) {
          $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
          $res['wilayah'] = 'Wilayah '. $regional->name;
          $res['user'] = $this->User_model->user_limit($_SESSION['regional']);
        }

		$komisi_franchise = $this->model_franchise->total_komisi();
        $income_count = $this->Order_model->komisi_all_franchise();
        $res['income_count'] = 'Rp.'.$komisi_franchise['total_pendapatan_bersih'];
        $res['shop_count']   = 'Rp.'.$komisi_franchise['total_pendapatan_kotor'];
        $res['order_count']  = $komisi_franchise['total_pesanan'];
        $res['user_count']   = $komisi_franchise['total_pembeli'];

        echo load_page_franchise($this, 'franchise_admin/home', $res);

	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Franchise/Home.php */