<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_pemilik');
		$this->load->model('model_order');
		if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
	}

	public function index()
	{

		$status_pesanan = $this->input->get('status_pesanan');
	    $tanggal        = $this->input->get('tanggal');

	    $nama_tab = [
	    	'Menunggu Konfirmasi', 
	    	'Pesanan baru', 
	    	'Siap diambil', 
	    	'Sedang diproses', 
	    	'Proses jemput', 
	    	'Diantar ke lokasi', 
	    	'Dibatalkan'
	    ];

	    for ($i=0; $i < sizeof($nama_tab); $i++) { 

	      if ($status_pesanan == $i) {

	        $aktif[] = [
	          'id'     => $i,
	          'name'   => $nama_tab[$i],
	          'active' => 'active'
	        ];

	      } else {

	        $aktif[] = [
	          'id'     => $i,
	          'name'   => $nama_tab[$i],
	          'active' => false
	        ];

	      }

	    }

	    $data['aktif'] = $aktif;
		$data['order'] = $this->model_order->order($status_pesanan, $tanggal);
	    
	    $wilayah = null;

	    if (!empty($_SESSION['regional'])) {
	      $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
	      $wilayah = 'Wilayah '. $regional->name;
	    }

	    $data['wilayah'] = $wilayah;
	    $data['tanggal'] = $tanggal;

		echo load_page_franchise($this,'franchise_admin/order/order',$data);
		
	}

	public function orderEdit()
    {
        $order_id = $this->uri->segment('3');
        $data['order_detail'] = $this->Order_model->orderDetail($order_id);
        echo load_page_franchise($this,'franchise_admin/order/order_edit',$data);
    }

}

/* End of file Pesanan.php */
/* Location: ./application/controllers/franchise/Pesanan.php */