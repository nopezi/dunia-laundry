<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller{

	public function __construct()
  	{
  		require_once APPPATH . "/third_party/FCMPushNotification.php";
      	parent::__construct();
      	$this->load->database();
      	error_reporting(0);
        if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
  	}

  public function index()
	{
    $status_pesanan = $this->input->get('status_pesanan');
    $tanggal        = $this->input->get('tanggal');

    $nama_tab = ['Pending', 'Confirmed', 'Picked Up', 'In Progress', 'Shipped', 'Delivered', 'Cancel'];

    for ($i=0; $i < sizeof($nama_tab); $i++) { 

      if ($status_pesanan == $i) {

        $aktif[] = [
          'id'     => $i,
          'name'   => $nama_tab[$i],
          'active' => 'active'
        ];

      } else {

        $aktif[] = [
          'id'     => $i,
          'name'   => $nama_tab[$i],
          'active' => false
        ];

      }

    }

    $data['aktif'] = $aktif;

		$data['order'] = $this->Order_model->order($status_pesanan, $tanggal);
    
    $wilayah = null;

    if (!empty($_SESSION['regional'])) {
      $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
      $wilayah = 'Wilayah '. $regional->name;
    }

    $data['wilayah'] = $wilayah;

		echo load_page_mitra_agen($this,'mitra_agen/order/order',$data);
	}

	public function changeOrderStatus()
	{
		$order_id = $this->input->post('order_id');
		$order_status = $this->input->post('order_status');
	    $this->Order_model->changeOrderStatus($order_id,$order_status);
	    $this->session->set_flashdata('success', STATUS_CHANGE);
	    // redirect('order');
	}

	public function changePaymentStatus()
	{
		$id = $this->uri->segment('3');
        $this->Order_model->changePaymentStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('mitra_agen/order');
	}

	public function orderDetail()
    {
        $order_id = $this->uri->segment('3');
        $data['order_detail'] = $this->Order_model->orderDetail($order_id);
        echo load_page_mitra_agen($this,'mitra_agen/order/order_detail',$data);
    }

    public function orderInvoice()
    {
        $order_id = $this->uri->segment('3');
        $data['order_detail'] = $this->Order_model->orderDetail($order_id);
        echo load_page_mitra_agen($this,'mitra_agen/order/order_invoice',$data);

    }

    public function orderEdit()
    {
        $order_id = $this->uri->segment('3');
        $data['order_detail'] = $this->Order_model->orderDetail($order_id);
        echo load_page_mitra_agen($this,'mitra_agen/order/order_edit',$data);
    }

    public function submitOrderUpdate()
    {
      $order_id = $this->input->post('order_id');
      $quantity = $this->input->post('quantity');

      $order_detail = $this->Order_model->orderDetail($order_id);

      $es1 = str_ireplace('"item_details":',"",$order_detail['item_details']);
      $json = json_decode($es1, true);
      $price = '';

      foreach ($json as $key => $value) {

        if ( isset( $quantity[ $value['s_no'] ] ) ) {

          $json[ $key ]['quantity'] = $quantity[ $value['s_no'] ];

          # kalo si agen input pake koma, diganti jadi .
          # contoh 6,3 jadi 6.3
          $jumlah = str_replace(',', '.', $quantity[ $value['s_no'] ]);
          $jumlah_pembanding = substr($jumlah, 0,1);

          # 6.3 > 6
          if ($jumlah > $jumlah_pembanding) {

            $banding = $jumlah_pembanding.'.3';

            # jika lewat dari 0,3 kilo berarti tambah 1 kilo
            # 6.3 > atau sama dengan 6.3
            if ($jumlah >= $banding) {
               $jumlah = $jumlah_pembanding+1;
            } else {
               $jumlah = $jumlah_pembanding;
            }

          }
          
          $price +=  $jumlah * $value['price'];

        }

      }

      $this->session->set_flashdata('success', 'Data orderan berhasil diedit.');

      $this->Order_model->updateItemDetails( $json, $price, $order_detail, $order_id );
      
      redirect('mitra_agen/order');
    }

}