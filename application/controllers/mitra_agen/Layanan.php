<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('mitra/model_layanan');

    } 

    public function index()
    {
    	$shop = $this->db->get_where('la_laundry_shop', ['user_id'=>$_SESSION['id']])->first_row();
    	$data['service'] = $this->model_layanan->service($shop->shop_id);
    	$data['shop_id'] = $shop->shop_id;
    	echo load_page_mitra_agen($this,'mitra_agen/layanan/index',$data);
    }

    public function submitService()
  	{
  		$input = $this->input->post();
	    $img = $_FILES['image']['name'];
	    $data = $this->model_layanan->submitService($input,$img);
  	}

  	public function updateService()
    {
		$id = $this->uri->segment('4');
		$res['row'] = $this->model_layanan->updateService($id);
		$shop = $this->db->get_where('la_laundry_shop', ['user_id'=>$_SESSION['id']])->first_row();
    	$res['service'] = $this->model_layanan->service($shop->shop_id);
    	$res['shop_id'] = $shop->shop_id;
		echo load_page($this,'mitra_agen/layanan/index',$res);
    }

    public function changeServiceStatus()
    {
    	
		$id = $this->uri->segment('4');
		$this->Service_model->changeServiceStatus($id);
		$this->session->set_flashdata('success', STATUS_CHANGE);
		redirect('mitra_agen/layanan','refresh');
    }

    public function hapus($id)
    {

    	# hapus gambar layanan
    	$gambar = $this->db->get_where('la_service', ['service_id'=>$id])->first_row();
    	$url_gambar = getcwd().'/assets/images/Service/'.$gambar->image;
    	unlink($url_gambar);

    	#hapus semua produk yang brkaitan dengan layann yang di hapus
    	$this->db->delete('la_price_list', ['service_id'=>$id]);
    	$this->db->delete('la_service', ['service_id'=>$id]);
    	$this->session->set_flashdata('success', 'Berhasil hapus layanan');
    	redirect('mitra_agen/layanan','refresh');
    }

}

/* End of file Layanan.php */
/* Location: ./application/controllers/mitra_agen/Layanan.php */