<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_agen');
    }

	public function index()
	{
		
		$wilayah = null;

        $res['user'] = $this->User_model->user_limit();

        if (!empty($_SESSION['regional']) && $_SESSION['type'] == 3) {
          $regional = $this->db->get_where('la_regencies', ['id'=>$_SESSION['regional']])->first_row();
          $wilayah = 'Wilayah '. $regional->name;
          $res['user'] = $this->User_model->user_limit($_SESSION['regional']);
        }

        $res['wilayah'] = $wilayah;

        $shop_id = $this->db->get_where('la_laundry_shop', ['user_id'=>$_SESSION['id']])->first_row();

        if ($_SESSION['type'] == 1) {

            $res['halaman']      = 'Agen';
            $res['shop_count']   = $this->model_agen->total_pendapatan($shop_id->shop_id);
            $res['income_count'] = $this->model_agen->total_komisi($shop_id->shop_id);
            $res['user_count']   = $this->model_agen->total_pembeli($shop_id->shop_id);
            $res['order_count']  = $this->model_agen->total_order($shop_id->shop_id);

        } else if ($_SESSION['type'] == 2) {

            $res['order_count']  = $this->Order_model->getTotalOrder();
            $res['user_count']   = $this->Order_model->getTotalUser();
            $res['halaman']      = 'Mitra';
            $res['income_count'] = '0';//$this->Order_model->getTotalIncome();
            $res['shop_count']   = '0';

        }

        // echo "<pre>";
        // print_r ($_SESSION);
        // echo "</pre>";
        // die();

        echo load_page_mitra_agen($this,'admin/dashboard_mitra',$res);

	}

}

/* End of file Home.php */
/* Location: ./application/controllers/mitra_agen/Home.php */