<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function index()
    {
    	$data['user'] = $this->User_model->user();
        echo load_page_mitra_agen($this,'user/user',$data);
    }

    public function userAdd()
    {
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        echo load_page_mitra_agen($this,'user/add_user',$data);
    }

    public function submitUser()
    {
    	$input = $this->input->post();
    	$image = $_FILES['image']['name'];

        if ($_SESSION['role'] == 'admin') {
            $id = $this->User_model->submitUser($input,$image);
        } else {
            $id = $this->User_model->submitUser_v2($input,$image);
        }
    	
    }

    public function changeUserStatus()
    {
        $id = $this->uri->segment('3');
        $this->User_model->changeUserStatus($id);
        $this->session->set_flashdata('error', STATUS_CHANGE);
        redirect('users');
    }

    public function updateUser()
    {
        $id = $this->uri->segment('4');
        $data['row'] = $this->User_model->updateUser($id);
        $data['user'] = $this->User_model->user();
        $data['count_code'] = $this->Laundry_shop_model->getCountryCode();
        echo load_page_mitra_agen($this,'user/add_user',$data);
    }

    public function webservice()
    {
        echo load_page_mitra_agen($this,'user/webservice');
    }

    public function emailAlert()
    {
        echo load_page_mitra_agen($this,'user/email_alert');
    }

    public function franchise()
    {
        // $data['user'] = $this->Laundry_Owner_model->laundryOwner();
        $data['user'] = $this->db->get_where('la_user', ['type'=>3])->result();
        
        $data['provinsi'] = $this->db->get('la_provinces')->result();
        echo load_page_mitra_agen($this, 'franchise/tambah', $data);

    }

    public function add_franchise()
    {

        $data = $this->input->post();

        echo "<pre>";
        print_r($data);

    }


}