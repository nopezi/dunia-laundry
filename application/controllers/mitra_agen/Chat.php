<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class Chat extends CI_Controller{

	public function __construct()
    {
    	require_once APPPATH . "/third_party/FCMPushNotification.php";
        parent::__construct();
        $this->load->database();
        if (empty($this->session->userdata('owner_name')))
            redirect('laundryOwner/login','refresh');
    }

    public function index()
    {
    	$res['chat'] = $this->Chat_model->chat();
    	echo load_page_mitra_agen($this,'mitra_agen/chat/chat',$res);
    }

    public function setLaundryOwnerMsg()
    {
        $message_head_id = $this->input->post('message_head_id');
        $to_user_id = $this->input->post('to_user_id');
        $from_user_id = $this->input->post('from_user_id');
        $message = $this->input->post('message');

        $data = $this->Chat_model->setLaundryOwnerMsg($message_head_id,$message,$to_user_id,$from_user_id);

        $factory = (new Factory)->withServiceAccount(FCPATH.'/win-laundry-firebase-adminsdk-t541y-3e4652faa9.json');
        $messaging = $factory->createMessaging();

        $message = CloudMessage::withTarget('token', $data['device_token'])
            ->withNotification(Notification::create('Chat baru dari laundry', $message))
            ->withData(['key' => 'value']);
        
        $messaging->send($message);

        // echo $data;
    }

    public function chatOwner()
    {
        $id = $this->uri->segment('3');
        $data['message_head_id'] = $id;
        $from_user_id = $this->uri->segment('4');
        $data['from_user_id'] = $from_user_id;
        $to_user_id = $this->uri->segment('5');
        $data['to_user_id'] = $to_user_id;
        $data['msg_data'] = $this->Chat_model->chatOwner($id);
        echo load_page_mitra_agen($this,'mitra_agen/chat/chatOwner',$data);
    }

}