<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Penjualan extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mitra/model_penjualan');
	}

	public function index_get()
	{

		$shop_id = $this->get('shop_id');

		$hasil = $this->model_penjualan->semua_data($shop_id);

		if (!empty($hasil)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Data berhasil didapat',
				'data'    => $hasil
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Data gagal didapat',
				'data'    => []
			], 200);			

		}
		
	}

	public function tambah_post()
	{
		
		$input = $this->post();

		$hasil = $this->model_penjualan->tambah($input);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil tambah data penjualan',
			'data'    => $hasil
		], 200);

	}

	public function selengkapnya_get()
	{
		

		$order_id = $this->get('order_id');

		$hasil = $this->model_penjualan->selengkapnya($order_id);

		if (!empty($hasil)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Data berhasil didapat',
				'data'    => $hasil
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Data gagal didapat',
				'data'    => []
			], 200);			

		}

	}

	public function batal_get()
	{
		
		$order_id = $this->get('order_id');

		$this->db->update('la_order_details', ['order_status'=>'6'], ['order_id'=>$order_id]);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil ubah status penjualan',
			'data'    => null
		], 200);


	}

}

/* End of file Penjualan.php */
/* Location: ./application/controllers/api_v2/Penjualan.php */