<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Lainnya extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mitra/model_lainnya');
	}

	public function index_get()
	{
		
		$user_id = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		if (!empty($user)) {
			$this->response([
				'status'  => true,
				'message' => 'data berhasil didapat',
				'data'    => $user
			], 200);
		} else {
			$this->response([
				'status'  => false,
				'message' => 'data gagal didapat',
				'data'    => null
			], 200);
		}

	}

	public function pengajuan_dana_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$dana    = $this->post('dana');

		$cek_dana = $this->model_lainnya->cek_dana($user_id, $shop_id);

		if ($dana > $cek_dana['total_pendapatan_potongan']) {
			$this->response([
				'status'  => false,
				'message' => 'Maaf, anda tidak dapat melakukan penarikan dana yang melebihi total pendapatan anda',
				'data'    => null
			], 200);
		}

		$rekening = $this->db->order_by('id', 'desc')
							 ->get_where('la_bank', ['user_id'=>$user_id])
							 ->first_row();

		if (empty($rekening)) {
			$this->response([
				'status'  => false,
				'message' => 'Mohon lengkapi dulu data rekening anda',
				'data'    => null
			], 200);
		}

		$this->db->insert('la_pencairan_dana', [
			'user_id' => $user_id,
			'bank' => $rekening->bank_name,
			'no_rekening' => $rekening->number_bank,
			'pemilik_rekening' => $rekening->account_name,
			'jumlah' => $dana,
			'status' => '1'
		]);

		$this->response([
			'status'  => true,
			'message' => 'Pengajuan berhasil di lakukan, silahkan menunggu konfirmasi admin',
			'data'    => null
		], 200);

	}

	public function edit_foto_profile_post()
	{
		
		$user_id = $this->post('user_id');
		$img     = $_FILES['image']['name'];

		$hasil = $this->model_lainnya->update_foto_profile($user_id, $img);

		if (!empty($hasil)) {
			$this->response([
				'status'  => true,
				'message' => 'foro profile berhasil di update',
				'data'    => $hasil
			], 200);
		}

	}

	public function edit_foto_background_post()
	{
		
		$user_id = $this->post('user_id');
		$img     = $_FILES['image']['name'];

		$hasil = $this->model_lainnya->update_foto_background($user_id, $img);

		if (!empty($hasil)) {
			$this->response([
				'status'  => true,
				'message' => 'background berhasil di update',
				'data'    => $hasil
			], 200);
		}

	}

	public function edit_akun_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$nama_mitra = $this->post('nama_mitra');
		$no_hp = $this->post('no_hp');
		$email = $this->post('email');

		$this->db->update('la_laundry_shop', ['shop_name'=>$nama_mitra], ['shop_id'=>$shop_id]);
		$this->db->update('la_user', [
			'email'  => $email,
			'mobile' => $no_hp
		], ['user_id'=>$user_id]);

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data',
			'data'    => $user
		], 200);

	}

	public function edit_jam_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$jam_buka  = $this->post('jam_buka');
		$jam_tutup = $this->post('jam_tutup');

		$this->db->update('la_laundry_shop', [
			'opening_time' => $jam_buka,
			'closing_time' => $jam_tutup
		], ['shop_id'=>$shop_id]);

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data',
			'data'    => $user
		], 200);

	}

	public function edit_alamat_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$alamat  = $this->post('alamat');

		$this->db->update('la_laundry_shop', ['address'=>$alamat], ['shop_id'=>$shop_id]);

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data alamat',
			'data'    => $user
		], 200);

	}

	public function edit_rekening_post()
	{
		

		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$nama_bank  = $this->post('nama_bank');
		$no_rekening = $this->post('no_rekening');
		$pemilik = $this->post('nama_pemilik');

		$cek = $this->db->get_where('la_bank', ['user_id'=>$user_id])->first_row();

		if (!empty($cek)) {
			
			$this->db->update('la_bank', [
				'bank_name' => $nama_bank,
				'account_name' => $pemilik,
				'number_bank' => $no_rekening
			], ['user_id'=>$user_id]);

		} else {

			$this->db->insert('la_bank', [
				'user_id' => $user_id,
				'bank_name' => $nama_bank,
				'account_name' => $pemilik,
				'number_bank' => $no_rekening
			]);

		}

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data alamat',
			'data'    => $user
		], 200);

	}

	public function edit_password_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$password_lama  = $this->post('password_lama');
		$password_baru = $this->post('password_baru');
		$password_baru_ulang = $this->post('password_baru_ulang');

		$cek = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

		if ($cek->password != $password_lama) {
			$this->response([
				'status'  => false,
				'message' => 'Password lama tidak sesuai',
				'data'    => null
			], 200);
		} else if ($password_baru != $password_baru_ulang) {
			$this->response([
				'status'  => false,
				'message' => 'password baru tidak cocok',
				'data'    => null
			], 200);
		}

		$this->db->update('la_user', ['password'=>$password_baru], ['user_id'=>$user_id]);

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data alamat',
			'data'    => $user
		], 200);

	}

	public function edit_notif_get()
	{
		
		$user_id = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$cek = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

		$notif = ($cek->notif_firebase == 1)?'':'1';
		$this->db->update('la_user', ['notif_firebase'=>$notif], ['user_id']);

		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data alamat',
			'data'    => $user
		], 200);

	}

	public function edit_notif_email_get()
	{
		
		$user_id = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$cek = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

		$notif = ($cek->notif_email == 1)?'':'1';
		$this->db->update('la_user', ['notif_email'=>$notif], ['user_id']);
		
		$user = $this->model_lainnya->get_profile($user_id, $shop_id);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update data alamat',
			'data'    => $user
		], 200);

	}

	public function tiket_get()
	{
		
		$user_id = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$tiket = $this->db->order_by('tiket_id', 'desc')->get('la_tiket')->result();

		if (!empty($tiket)) {

			foreach ($tiket as $key => $value) {
				if ($value->status == 0 || empty($value->status)) {
					$status = 'Pending';
				} else if ($value->status == 1) {
					$status = 'Sedang diprogres';
				} else if ($value->status == 2) {
					$status = 'Selesai';
				} else if ($value->status == 3) {
					$status = 'Batal';
				}
				$value->status = $status;
			}
			
			$this->response([
				'status'  => true,
				'message' => 'Data tiket berhasil didapat',
				'data'    => $tiket
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada data tiket',
				'data'    => []
			], 200);

		}

	}

	public function tambah_tiket_post()
	{
		
		$user_id = $this->post('user_id');
		$shop_id = $this->post('shop_id');
		$title   = $this->post('title');
		$description = $this->post('description');

		$this->db->insert('la_tiket', [
			'tiket_no'    => rand(111100000,999999999),
    		'user_id'     => $user_id,
			'title'       => $title,
            'description' => $description,
            'status'      => '1',
    		'created_at'  => date('Y-m-d H:i:s')
		]);

		$this->response([
			'status'  => true,
			'message' => 'berhasil tambah tiket',
			'data'    => null
		], 200);

	}

}

/* End of file Lainnya.php */
/* Location: ./application/controllers/api_v2/Lainnya.php */