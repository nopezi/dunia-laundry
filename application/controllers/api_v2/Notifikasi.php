<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Notifikasi extends REST_Controller {

	public function index_get()
	{
		
		$user_id = $this->get('user_id');

		$notif = $this->db->order_by('notification_id', 'desc')
						  ->where('user_id', $user_id)
						  ->get('la_notification')
						  ->result();

		if (!empty($notif)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Berhasil mendapatkan data notifikasi',
				'data'    => $notif
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada data notifikasi',
				'data'    => []
			], 200);

		}

	}

}

/* End of file Notifikasi.php */
/* Location: ./application/controllers/api_v2/Notifikasi.php */