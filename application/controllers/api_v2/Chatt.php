<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Chatt extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mitra/model_pesan');
	}

	public function index_get()
	{
		
		$user_id = $this->get('user_id');

		$pesan = $this->model_pesan->semua_pesan($user_id);

		if (!empty($pesan)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Data pesan berhasil didapat',
				'data'    => $pesan
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada chat tersedia',
				'data'    => []
			], 200);

		}

	}

	public function detail_pesan_get()
	{
		
		$message_head = $this->get('message_head_id');
		$user_id      = $this->get('user_id');

		# baca pesan
		$read = $this->db->set('is_read', '1')
						 ->where('message_head_id', $message_head)
						 ->where('to_user_id', $user_id)
						 ->update('la_message');

		# ambil data pesan
		$pesan = $this->db->order_by('message_id', 'desc')
						  ->get_where('la_message', ['message_head_id'=>$message_head])
						  ->result();

		if (!empty($pesan)) {

			foreach ($pesan as $key => $value) {

				$jam = explode(' ', $value->created_at);
				$jam = date('H:i A', strtotime($jam[1]));
				$value->jam = $jam;

				$value->url_media = null;
				if (!empty($value->media)) {
					$value->url_media = base_url().'assets/images/pesan/'.$value->media;
				}
			}
			
			$this->response([
				'status'  => true,
				'message' => 'Data pesan berhasil didapat',
				'data'    => $pesan
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada chat tersedia',
				'data'    => []
			], 200);

		}

	}

	public function kirim_pesan_post()
	{
		
		$input = $this->post();
		$media = $_FILES['media']['name'];

		$hasil = $this->model_pesan->kirim_pesan($input, $media);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil kirim pesan',
			'data'    => $hasil
		], 200);

	}

	public function hubungi_konsumen_get()
	{
		
		$konsumen_id = $this->get('konsumen_id');
		$user_id = $this->get('user_id');

		# ambil dta message head
		$mh = $this->db->get_where('la_message_head', [
						'from_user_id' => $konsumen_id,
						'to_user_id'   => $user_id
					])->first_row();
		$message_head = $mh->message_head_id;

		# baca pesan
		$read = $this->db->set('is_read', '1')
						 ->where('message_head_id', $message_head)
						 ->where('to_user_id', $user_id)
						 ->update('la_message');

		# ambil data pesan
		$pesan = $this->db->order_by('message_id', 'desc')
						  ->get_where('la_message', ['message_head_id'=>$message_head])
						  ->result();

		if (!empty($pesan)) {

			foreach ($pesan as $key => $value) {

				$jam = explode(' ', $value->created_at);
				$jam = date('H:i A', strtotime($jam[1]));
				$value->jam = $jam;

				$value->url_media = null;
				if (!empty($value->media)) {
					$value->url_media = base_url().'assets/images/pesan/'.$value->media;
				}
				
			}
			
			$this->response([
				'status'  => true,
				'message' => 'Data pesan berhasil didapat',
				'data'    => $pesan
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada chat tersedia',
				'data'    => []
			], 200);

		}

	}

}

/* End of file Chatt.php */
/* Location: ./application/controllers/api_v2/Chatt.php */