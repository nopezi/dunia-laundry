<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Home extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mitra/model_home');
	}

	public function index_get()
	{
		
		$id_user = $this->get('id_user');

		

		$pesanan_baru = $this->model_home->pesanan_baru($id_user);
		$pendapatan_bersih = $this->model_home->pendapatan_bersih($id_user);
		$pesanan_komplain  = $this->model_home->pesanan_komplain($id_user);
		$pesanan_selesai   = $this->model_home->pesanan_selesai($id_user);
		$pesan_belum_baca  = $this->model_home->pesan_belum_read($id_user);
		
		// echo "<pre>";
		// print_r ($pendapatan_bersih);
		// echo "</pre>";
		// die();

		$semua = [
			'pesanan_baru' 			 => $pesanan_baru,
			'pendapatan_bersih_baru' => $pendapatan_bersih,
			'pesanan_komplain' 		 => $pesanan_komplain?:0,
			'chat_belum_dibaca' 	 => $pesan_belum_baca?:0,
			'pesanan_selesai' 	  	 => $pesanan_selesai?:0,
			'ulasan_selesai' 		 => 0
		];

		$this->response([
			'status'  => true,
			'message' => 'Data berhasil didapat',
			'data'    => $semua
		], 200);

	}

	public function artikel_get()
	{
		
		$artikel  = $this->db->order_by('id', 'desc')->get('la_artikel')->result();

		if (!empty($artikel)) {

			foreach ($artikel as $key => $value) {
				if (!empty($value->image)) {
					$value->url_image = base_url().'assets/images/artikel/'.$value->image;
				} else{
					$value->url_image = base_url().'assets/images/artikel/default.png';
				}
			}
			
			$this->response([
				'status'  => true,
				'message' => 'Data artikel berhasil di dapat',
				'data'    => $artikel
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Data artikel kosong',
				'data'    => []
			], 200);

		}
		

	}

	public function terlaris_get()
	{
		
		$id_user = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$terlaris = $this->db->get_where('la_order_details', ['shop_id'=>$shop_id])->result();

		if (!empty($terlaris)) {
			
			foreach ($terlaris as $key => $value) {
				if (!empty($value->item_details)) {
					$item = json_decode($value->item_details);
					for ($i=0; $i < sizeof($item); $i++) { 
						
						// if ($i > 1) {
						// 	// code...
						// } else {

						// 	$item_terlaris[] = [
						// 		'item_id' => $i->item_id,
						// 		'item_name' => $i->item_name
						// 	];

						// }

					}
				}
			}

		}

		$user = $this->db->get_where('la_user', ['user_id'=>$id_user])->first_row();
		if ($user->premium == 1) {
			$shop_id = $shop_id;
		 } else {
		 	$shop_id = 'YZ65d0';
		 }

		$produk_terlaris = $this->db->order_by('s_no', 'desc')
	 								 ->get_where('la_service', ['shop_id'=>$shop_id])
	 								 ->result();

	 	foreach ($produk_terlaris as $key => $value) {
	 		$value->url_image = null;
	 		if (!empty($value->image)) {
	 			$value->url_image = base_url().'assets/images/Service/'.$value->image;
	 		}
	 	}

		$this->response([
			'status'  => true,
			'message' => 'layanan terlaris data berhasil didapat',
			'data'    => $produk_terlaris
		], 200);

	}

}

/* End of file Home.php */
/* Location: ./application/controllers/api_v2/Home.php */