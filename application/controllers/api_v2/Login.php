<?php 
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

# Include the Autoloader (see "Libraries" for install instructions)
require 'vendor/autoload.php';
use Mailgun\Mailgun;

class Login extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_login');
	}

	public function index_post()
	{
		
		$data_post = $this->post();

		$cek = $this->db->where('email', $data_post['email'])
						->where('password', $data_post['password'])
						->where('type', '2')
						->get('la_user')
						->first_row();

		if (!empty($cek)) {

			if ($cek->status == 1) {
				
				$this->response([
					'status'  => true,
					'message' => 'Berhasil login',
					'data'    => $cek
				], 200);

			} else {

				$this->response([
					'status'  => false,
					'message' => 'Akun anda belum di aktivasi, silahkan hubungi admin untuk melakukan aktivasi',
					'data'    => null
				], 200);

			}
 
		} else {

			$this->response([
				'status'  => false,
				'message' => 'gagal login',
				'data'    => null
			], 200);

		}

	}

	public function lupa_password_get()
	{
		
		$email = $this->get('email');
		$cek = $this->db->get_where('la_user', ['email'=>$email])->first_row();

		if (empty($cek)) {

			$this->response([
				'status'  => false,
				'message' => 'Email yang anda input, belum terdaftar',
				'data'    => null
			], 200);

		}

		$this->db->update('la_user', ['password'=>'123456'], ['user_id'=>$cek->user_id]);

		$KEY = $this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));

		$subject = "[Win Laundry] Reset Password";
        $msg = "Pasword anda berhasil di reset. 123456 adalah password default anda";

        // $kirim_email = $this->send_email_by_msg($data_user->email, $subject, $msg);
        // $kirim_email = kirim_email([
        // 					'email'   => 'snopezi@gmail.com',//$data_user->email,
        // 					'subject' => $subject,
        // 					'msg'	  => $msg,
        // 					'key'     => $KEY->msg91_key
				    //    ]);

		// $kirim_email = $this->model_login->reset_password();
		// $kirim_email = $this->email();

		
		# Instantiate the client.
		$mgClient = new Mailgun('4fda8dc3aeb0a1ab0fea16893f8eb0f3-2b778fc3-77ca800c');
		$domain = "sandbox031c4d90276c4b4f922f23d33d1f81db.mailgun.org";
		# Make the call to the client.
		$kirim_email = $mgClient->sendMessage($domain, array(
			'from'	=> 'Excited User <info@dunialaundry.com>',
			'to'	=> 'snopezi@gmail.com',
			'subject' => 'Hello',
			'text'	=> $msg
		));

        $this->response([
        	'status'  => true,
        	'message' => 'Password berhasil di reset, silahkan periksa email anda',
        	'data'    => $kirim_email
        ], 200); 

	}

	public function email()
	{
		
		$this->load->library('email');

		$config = [];
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'mail.windigitalkhatulistiwa.com';
		// $config['smtp_host'] = 'mail.windigitalkhatulistiwa.com';
		$config['smtp_user'] = 'info@windigitalkhatulistiwa.com';
		$config['smtp_pass'] = '8Q?by$#qy9';
		// $config['smtp_port'] = 465;
		$config['smtp_port'] = 587;
		$config['mailtype']  = 'html';
		$config['charset']   = 'iso-8859-1';
		$config['newline']   = "\r\n";
		$this->email->initialize($config);
		
		$this->email->from('info@windigitalkhatulistiwa.com', 'Dunia Laundry');
		$this->email->to('snopezi@gmail.com');
		// $this->email->cc('snopezi@gmail.com');
		// $this->email->bcc('snopezi@gmail.com');
		
		$this->email->subject('subject');
		$this->email->message('message');
		
		$this->email->send();
		
		return $this->email->print_debugger();

	}

	public function google_post()
	{
		
		$input = $this->post();
		$cek = $this->db->get_where('la_user', [
							'email'        => $input['email'],
							'device_type'  => $input['device_type'],
							'device_token' => $input['device_token']
						 ])->first_row();

		if (!empty($cek)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Berhasil login',
				'data'    => $cek
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Gagal login',
				'data'    => null
			], 200);

		}

	}

}

/* End of file Login.php */
/* Location: ./application/controllers/api_v2/Login.php */