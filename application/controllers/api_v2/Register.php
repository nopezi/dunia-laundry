<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Register extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mitra/model_register');
	}

	public function index_post()
	{

		$input = $this->post();

		$cek = $this->model_register->daftar($input);

		$this->response($cek, 200);
		
	}

	public function foto_ktp_post()
	{
		
		$config['upload_path'] = 'assets/images/user';
		$config['allowed_types'] = 'gif|jpg|png|doc|txt|pdf';
		$config['max_size'] = 1024 * 10;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {

			$msg = $this->upload->display_errors('', '');
			$this->response([
				'status'  => false,
				'message' => $msg,
				'data'    => null
			], 200);

		}

		$data = $this->upload->data();
		$file = $data['file_name'];
		$user_id = $this->post('user_id');
		$update = [
			'image_ktp' => $file,
			'update_at' => date('Y-m-d H:i:s')
		];

		$this->db->update('la_user', $update, ['user_id'=>$user_id]);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil upload foto',
			'data'    => [
				'user_id'  => $user_id,
				'foto_ktp' => base_url().'assets/images/user/'.$file
			]
		], 200);

	}

	public function mitra_post()
	{
		
		$nama_mitra = $this->post('nama_mitra');
		$alamat     = $this->post('alamat');
		$longitude  = $this->post('longitude');
		$latitude   = $this->post('latitude');

		if (empty($nama_mitra) || empty($alamat) || empty($longitude) || empty($latitude)) {

			if (empty($nama_mitra)) {
				$parameter = 'Nama mitra ';
			} else if (empty($alamat)) {
				$parameter = 'Alamat ';
			} else if (empty($longitude)) {
				$parameter = 'Longitude ';
			} else if (empty($latitude)) {
				$parameter = 'Latitude ';
			}

			$this->response([
				'status'  => false,
				'message' => $parameter.'tidak boleh kosong',
				'data'    => null
			], 200);

		}

		$input = $this->post();
		$hasil = $this->model_register->daftar_mitra($input);

		if (!empty($hasil)) {
			$this->response($hasil, 200);
		}

	}

	public function otp_sms_get()
	{

		$id_user = $this->get('id_user');
		$otp     = $this->get('otp');

		if (!empty($otp)) {
			
			$cek = $this->db->get_where('la_user', [
											'user_id' => $id_user,
											'otp_sms' => $otp
										])->first_row();

			if (!empty($cek)) {

				$this->db->update('la_user', ['status'=> '1'], ['user_id'=>$id_user]);
				
				$this->response([
					'status'  => true,
					'message' => 'Berhasil verifikasi otp',
					'data'    => $cek
				], 200);

			} else {

				$this->response([
					'status'  => false,
					'message' => 'Gagal verifikasi otp',
					'data'    => null
				], 200);

			}

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Otp tidak boleh kosong',
				'data'    => null
			], 200);

		}

	}

}

/* End of file Register.php */
/* Location: ./application/controllers/api_v2/Register.php */