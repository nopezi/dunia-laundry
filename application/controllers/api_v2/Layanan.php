<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Layanan extends REST_Controller {

	public function index_get()
	{
		
		$id_user = $this->get('user_id');
		$shop_id = $this->get('shop_id');

		$user = $this->db->get_where('la_user', ['user_id'=>$user_id])->first_row();

		if ($user->premium == 1) {
			$shop_id = $shop_id;
		} else {
			$shop_id = 'YZ65d0';
		}

		$layanan = $this->db->get_where('la_service', ['shop_id'=>$shop_id])->result();

		if (!empty($layanan)) {
			
			foreach ($layanan as $key => $value) {
				if (!empty($value->image)) {
					$value->url_image = base_url().'assets/images/Service/'.$value->image;
				}
			}

			$this->response([
				'status'  => true,
				'message' => 'Berhasil tampil data',
				'data'    => $layanan
			], 200);

		} else {

			$this->response([
				'status'  => true,
				'message' => 'Mitra belum menambahkan layanan',
				'data'    => []
			], 200);

		}

	}

	public function detail_layanan_get()
	{

		$service_id = $this->get('service_id');

		$service = $this->db->get_where('la_service', ['service_id'=>$service_id])->first_row();

		$service->url_image = base_url().'assets/images/Service/default.png';

		if (!empty($service)) {

			if (!empty($service->image)) {
				$service->url_image = base_url().'assets/images/Service/'.$service->image;
			}

			$this->response([
				'status'  => true,
				'message' => 'Berhasil tampil data service',
				'data'    =>$service
			], 200);

		} else {
			$this->response([
				'status'  => true,
				'message' => 'Gagal tampil data service',
				'data'    => null
			], 200);
		}

	}

	public function update_layanan_post()
	{
		
		$service_id = $this->post('service_id');
		$deskripsi = $this->post('description');
		$nama_service = $this->post('service_name');

		// print_r($this->post());
		// die();

		if (empty($service_id)) {
			$pesan_kosong = 'Service id tidak boleh kosong';
		} else if (empty($deskripsi)) {
			$pesan_kosong = 'Deskripsi tidak boleh kosong';
		} else if (empty($nama_service)) {
			$pesan_kosong = 'Nama service tidak boleh kosong';
		}

		if (!empty($pesan_kosong)) {
			$this->response([
				'status'  => false,
				'message' => $pesan_kosong,
				'data'    => null
			], 200);
		}

		$this->db->update('la_service', [
			'description' => $deskripsi,
			'service_name' => $nama_service
		], ['service_id'=>$service_id]);

		$hasil = $this->db->get_where('la_service', ['service_id'=>$service_id])->first_row();

		if (!empty($hasil)) {
			$this->response([
				'status'  => true,
				'message' => 'Berhasil update data',
				'data'    => $hasil
			], 200);
		}

	}

	public function update_status_get()
	{
		
		$service_id = $this->get('service_id');
		$service = $this->db->get_where('la_service', ['service_id'=>$service_id])->first_row();

		if (!empty($service)) {
			if ($service->status == 1) {
				$status = 0;
			} else {
				$status = 1;
			}
		}

		$this->db->update('la_service', [
			'status'     => $status,
			'updated_at' => date('Y-m-d H:i:s')
		], ['service_id'=>$service_id]);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update status',
			'data'    => $service
		], 200);

	}

	public function item_layanan_get()
	{

		$service_id = $this->get('service_id');

		$item = $this->db->get_where('la_price_list', ['service_id'=>$service_id])->result();

		if (!empty($item)) {

			foreach ($item as $key => $value) {
				$value->url_image = null;
				if (!empty($value->image)) {
					$value->url_image = base_url().'assets/images/Product/'.$value->image;
				}
			}
			
			$this->response([
				'status'  => true,
				'message' => 'data item didapat',
				'data'    => $item
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'data item tidak didapat',
				'data'    => []
			], 200);			

		}

	}

	public function item_detail_get()
	{
		
		$item_id = $this->get('item_id');

		$item_detail = $this->db->get_where('la_price_list', ['item_id'=>$item_id])->first_row();

		if (!empty($item_detail)) {
			
			$item_detail->url_image = null;
			if (!empty($item_detail->image)) {
				$item_detail->url_image = base_url().'assets/images/Product/'.$item_detail->image;
			}

			$this->response([
				'status'  => true,
				'message' => 'Data item berhasil didapat',
				'data'    => $item_detail
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Data item gagal didapat',
				'data'    => null
			], 200);

		}

	}

	public function edit_item_post()
	{
		
		$item_id = $this->post('item_id');
		$harga   = $this->post('price');
		$item_name = $this->post('item_name');

		if (empty($harga) || !empty($harga == '0')) {
			$pesan_kosong = 'Harga tidak boleh kosong';
		} else if (empty($item_name)) {
			$pesan_kosong = 'Nama item tidak boleh kosong';
		} else if (empty($item_id)) {
			$pesan_kosong = 'item id tidak boleh kosong';
		} else if (!empty($item_id)) {
			$cek = $this->db->get_where('la_price_list', ['item_id'=>$item_id])->first_row();
			if (empty($cek)) {
				$pesan_kosong = 'Data item sudah tidak ada';
			}
		}

		if (!empty($pesan_kosong)) {
			$this->response([
				'status'  => false,
				'message' => $pesan_kosong,
				'data'    => null
			], 200);
		}

		$this->db->update('la_price_list', [
			'price' => $harga,
			'item_name' => $item_name,
			'updated_at' => date('Y-m-d H:i:s')
		], ['item_id'=>$item_id]);

		$item = $this->db->get_where('la_price_list', ['item_id'=>$item_id])->first_row();

		$this->response([
			'status'  => true,
			'message' => 'Berhasil edit item layanan',
			'data'    => $item
		], 200);

	}

}

/* End of file Layanan.php */
/* Location: ./application/controllers/api_v2/Layanan.php */