<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {

	public function index()
	{

		$data['provinsi']   = $this->db->get('la_provinces')->result();

		$this->load->view('auth/daftar', $data);
		
	}

	public function simpan()
	{

		$cek      = $this->input->post();
		$regional = $this->input->post('regional');
		$distrik  = $this->input->post('distrik');

		$cek_email = $this->db->get_where('la_user', ['email'=>$cek['email']])->first_row();

		if (empty($regional) || empty($distrik)) {
			$cek['pesan'] = 'Mohon lengkapi data anda';
			$this->session->set_flashdata('d_daftar', $cek);
			redirect('daftar','refresh');
		} else if (!empty($cek_email)) {
			$cek['pesan'] = 'Email anda sudah terdaftar, mohon gunakan email lain';
			$this->session->set_flashdata('d_daftar', $cek);
			redirect('daftar','refresh');
		}

		$user_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,2 ) .substr( md5( time() ), 1,4);
    $otp_sms = random_char(5);

    $total = sizeof($_FILES['foto']['name']);

    for($i=0;$i<$total;$i++){
      
        if(!empty($_FILES['foto']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['foto']['name'][$i];
          $_FILES['file']['type'] = $_FILES['foto']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['foto']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['foto']['error'][$i];
          $_FILES['file']['size'] = $_FILES['foto']['size'][$i];
  
          // $config['upload_path']   = 'assets/images/Laundry/';
          $config['upload_path']   = 'assets/images/user/';
          $config['allowed_types'] = 'gif|jpg|jpeg|png';
          $config['overwrite']     = TRUE;
          $config['max_size']      = '5000';
          // $config['file_name']     = 'daftar_'.time();
          $config['encrypt_name'] = TRUE;
          $this->load->library('upload',$config);
          $this->upload->initialize($config);

          // $config['file_name'] = $_FILES['files']['name'][$i];
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){

            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
   
            $data_gambar[] = $filename;

          } else {

            $msg = $this->upload->display_errors('', '');
            $this->session->set_flashdata('d_daftar', $msg);
            redirect('daftar','refresh');
            // echo "error ".$msg;

          }

        }
   
    } // for

    $foto_ktp = "";
    $foto_personal = "";
    $logo_usaha = "";

    if (!empty($data_gambar[0])) {
      $foto_personal = $data_gambar[0];
    }

    if (!empty($data_gambar[1])) {
      $foto_ktp = $data_gambar[1];
    }

    if (!empty($data_gambar[2])) {
      $logo_usaha = $data_gambar[2];
    }

    $data_user = [
      'name'         => $cek['name'],
      'email'        => $cek['email'],
      'password'     => $cek['password'],
      'mobile'       => $cek['mobile'],
      'type'         => $cek['type'],
      'user_id'      => $user_id,
      'otp_sms'      => $otp_sms,
      'regional'     => $cek['regional'],
      'distrik'      => $cek['distrik'],
      'address'      => $cek['address'],
      'country_code' => '62',
      'status'       => 0,
      'created_at'   => date('Y-m-d H:i:s'),
      'latitude'     => 0,
      'longitude'    => 0,
      'image'        => $foto_personal,
      'image_ktp'    => $foto_ktp
    ];

    $data_bank = [
      'user_id'      => $user_id,
      'bank_name'    => $cek['bank_name'],
      'account_name' => $cek['account_name'],
      'number_bank'  => $cek['no_rekening'],
      'nama_usaha'   => $cek['nama_usaha'],
      'logo_usaha'   => $logo_usaha
    ];

    $shop_id = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,3 ) .substr( md5( time() ), 1,6);

    $data_usaha = [
      'shop_id'      => $shop_id,
      'user_id'      => $user_id,
      'shop_name'    => $cek['nama_usaha'],
      'country_code' => '62',
      'mobile'       => $cek['mobile'],
      'address'      => $cek['address'],
      'image'        => $logo_usaha,
      'created_at'   => date('Y-m-d H:i:s')
    ];
    
    // echo "<pre>";
    // print_r($data_user);
    // print_r($data_bank);
    // print_r($data_usaha);
    // die();

    $this->db->insert('la_user',$data_user);
    $this->db->insert('la_bank', $data_bank);
    $this->db->insert('la_laundry_shop', $data_usaha);
    
    $url = base_url().'/api/userActive?id=' . base64_encode($user_id);
    $subject = "[Win Laundry] Registrasi Akun Baru";
    $msg = "Terima kasih telah mendaftar dengan Win Laundry! Akun Anda telah dibuat, Harap verifikasi akun Anda dengan tautan di bawah ini. <br>" . $url;
    $this->send_email_by_msg($cek['email'], $subject, $msg);
    
    # ganti 0 pada nomor hp menjadi +62
    $no_hp = '+62'.substr($cek['mobile'], 1, 15);
    # $pesan_sms = "<#> Kode otp Winlaundry anda adalah ".$otp_sms;

    # kirim otp ke sms
    $from  = "+17042865978";
    $to    = $no_hp;
    $pesan = "From=".$from."&To=".$to."&Body=".$msg;
    $cek   = api_twilio($pesan);

		$this->session->set_flashdata('berhasil_daftar', true);
		redirect('login','refresh');

	}

	public function send_email_by_msg($email_id,$subject,$msg)
    {
         $KEY=$this->Base_model->getSingleRow('la_msg91_key',array('id'=>1));
         $authKey=$KEY->msg91_key;

          // $authKey = MSG_AUTH_KEY;
          $from= SENDER_EMAIL;

          //Prepare you post parameters
          $postData = array(
              'authkey' => $authKey,
              'to' => $email_id,
              'from' => $from,
              'subject' => $subject,
              'body' => $msg
          );
         //API URL
          $url="https://control.msg91.com/api/sendmail.php?authkey=" . urlencode( $authKey ) ."&to=" . urlencode( $email_id ) . "&from=" . urlencode( $from ) ."&body=" . urlencode( $msg ) ."&subject=" . urlencode( $subject );
          // init the resource
          $ch = curl_init();
          curl_setopt_array($ch, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_POST => true,
              CURLOPT_POSTFIELDS => $postData
              //,CURLOPT_FOLLOWLOCATION => true
          ));

          //Ignore SSL certificate verification
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

          //get response
          $output = curl_exec($ch);
          //Print error if any
          if(curl_errno($ch))
          {
            echo 'error:' . curl_error($ch);
          }
          curl_close($ch);
    }

}

/* End of file Daftar.php */
/* Location: ./application/controllers/Daftar.php */