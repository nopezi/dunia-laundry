<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
        $this->load->database();

    }

    public function index()
    {
        $this->load->view('laundry/login');
    }

    public function login()
    {
    	$email    = $this->input->post('email');
    	$password = $this->input->post('password');
        // $getdata = $this->Laundry_Owner_model->login($email,$password);
        $this->model_login->login($email, $password);
    }
	

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('laundryOwner/login');
    }

}