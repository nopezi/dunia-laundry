<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Twilio
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function api_twilio($request)
	{
		$url = "https://api.twilio.com/2010-04-01/Accounts/ACe29b12dc0d4011601ca5e75e118de42b/Messages.json";
		$Authorization  = 'Basic QUNlMjliMTJkYzBkNDAxMTYwMWNhNWU3NWUxMThkZTQyYjo5Yzg5YzFiYzkwY2FlYWJjZTQyYzZiOTc4ZGI3ZjAxOA==';
	    $headers 		= array();
	    // $headers[] 		= 'Content-Type: application/json; charset=utf-8';
	    $headers[] 		= 'Content-Type: application/x-www-form-urlencoded';
	    // $headers[] 		= 'Accept: application/json';
	    $headers[]      = '*/*';
	    $headers[] 		= 'Authorization: ' . $Authorization;

	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	    if ($request) {
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
	    }
	    $data = curl_exec($ch);

	    if ($data === false) {
	        echo 'Curl error: ' . curl_error($ch);
	    } else {
	        return $data;
	    }

	}

	public function kirim_pesan_wa($pesan, $no_telp){
	    // $key='5f956f4690253112bf69903e7fb9be0f08f36719ada9bfd6'; //this is demo key please change with your own key
	    $key = '4f27b623ecbec500bd7fc95e352b32032fa6fb7d2d57c275';
	    $url='http://116.203.92.59/api/send_message';
	    $data = array(
	      "phone_no"=> $no_telp,
	      "key"     =>$key,
	      "message" =>$pesan
	    );
	    $data_string = json_encode($data);

	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_VERBOSE, 0);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	      'Content-Type: application/json',
	      'Content-Length: ' . strlen($data_string))
	    );
	    $res=curl_exec($ch);
	    curl_close($ch);
	    return $res;
	    // return 'ada';
	}

	function connectCurl($url,$headers, $request=null, $method){

	  $ch = curl_init($url);
	  // show_array($method);
	  // curl_setopt($ch, CURLOPT_URL, $url);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	  if($request){
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

	    curl_setopt($ch, CURLOPT_POSTFIELDS, $request );
	  }
	  $data=curl_exec($ch);

	  if ($data=== false) {
	    // echo 'Curl error: ' . curl_error($ch);
	    echo json_encode(array('curl_error'=>curl_error($ch)));
	  } else {
	    return $data;
	  }

	}

	public function api_2($data)
	{

	    $url  = 'https://api.twilio.com/2010-04-01/Accounts/ACe29b12dc0d4011601ca5e75e118de42b/Messages.json';
	    
	    // $data = array(
	    //     'to' 		   => $token_user,
	    //     'collapse_key' => "type_a",
	    //     "data"         => $ambil
	    // );
	    // $data_string = json_encode($data);
	    // print_r($data_string);

	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_VERBOSE, 0);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    	  'Authorization: Basic QUNlMjliMTJkYzBkNDAxMTYwMWNhNWU3NWUxMThkZTQyYjo5Yzg5YzFiYzkwY2FlYWJjZTQyYzZiOTc4ZGI3ZjAxOA==',
		      'Content-Type: application/x-www-form-urlencoded',
		      // 'Content-Length: ' . strlen($data_string)
	  	  )
	    );
	    $res=curl_exec($ch);
	    curl_close($ch);
	    return $res;
	    // return 'ada';

	}

	

}

/* End of file Twilio.php */
/* Location: ./application/libraries/Twilio.php */
