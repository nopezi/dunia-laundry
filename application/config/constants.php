<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
|--------------------------------------------------------------------------
| FLASH MESSAGE
|--------------------------------------------------------------------------
*/

define('LOGIN_FLASH',"Please enter valid username or password");
define('PARENT_CAT_EXIST',"Parent category already Exist..");
define('DATA_SUBMIT',"Your data has been submitted successfully.");
define('STATUS_CHANGE',"Status change successfully..!!");
define('DATA_UPDATE',"Data has been update successfully..!!");
define('ALL_FIELD_MANDATORY', "Please fill all fields");
define('SUBSCRIPTION_HEAD', "Subscription sucessfull");

//APIS
define('VERI_OTP', "Please verify your OTP");
define('SENDER_EMAIL','info@samyotech.com');
define('OTP_SEND','We have sent you an otp on your mail Id, please check your email id.');
define('USER_ALREADY',"User already registered.");
define('EMAIL_EXIST',"Email Id already exist");
define('OTP', "Your OTP is");
define('VALID_EMAIL',"User not registered.");
define('SOMTHING',"Somthing Went Wrong..!!");
define('OTP_WRONG','You have entered wrong OTP');
define('OTP_PASS','OTP Verifie and password save successfully..!!');
define('LOGIN_SUCCESS', "Login successfully.");
define('USER_NOT',"User not found.");
define('VALID_PASS',"Please enter valid password..!!");
define('DEACTIVE',"Your account has been blocked. Please contact to admin.");
define('DEACTIVE_NEW',"First verify your mail id using link we have sent on your Email.");
define('LOGOUT','Logout successfully..!!');
define('TICKET_APPROVED',"Your ticket has been approved.");
define('TICKET_REJECTED',"Your ticket has been rejected.");


define('FORG_PASS', "Forgot password");
define('PASS_SEND','We have sent you password on your mail Id, please check your email id.');
define('LSHOP','Laundry shop list');
define('LSHOP_NOT','Laundry shop not available');
define('SERVICE','Service list');
define('SERVICE_NOT','Service not available');
define('OFFER','Offer list');
define('OFFER_NOT','Offer not available');
define('OFFER_SHOP','Offer list');
define('OFFER_SHOP_NOT','Offer is not available for this shop');
define('PL','Price list');
define('PL_NOT','Price list not available');
define('ADVERTISE','Advertisement list');
define('ADVERTISE_NOT','Advertisement not available');
define('ORDER','Your Order is submit');
define('CURRENCY','Currency');
define('CURRENCY_NOT','Currency not available');
define('MSG_SEND','Successfully message send');
define('RATING','Rating submit successfully');
define('ALREADY_RATED','You are already reated this shop');
define('SHOP_NOT','Shop is not exist');
define('USER_NOT_EX','User is not exist');
define('INACTIVATE','User is inactive');
define('GET_RATE','Average rating');
define('BOOK_NOT','Booking not available');
define('BOOKING','Booking list');
define('SEND_NOTIF','Notification send sucessfully');
define('NOTIF_NOT','Notification not available');

//Tables
define('USR_TBL','la_user');

//MSG_AUTH_KEY
define('MSG_AUTH_KEY', "205521Ay0uGpRMiR5da996d7");

define('FIRE_BASE_KEY','AAAANo6vPnk:APA91bG3eK0JSka8Nbsr3HYckWLoFBbFJSdaxsJP4mT8T7JtW1PhJ-qIIQ88zSRZo3bB36l-mpUmdj2Ylx1frzO9R_qSLoF93EN65UUonAwGtAJyqB4i_7NzCXdfLoMAF5AuCOuSZuO7');