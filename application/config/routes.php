<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['user/dashboard'] 							= 'Admin/DashboardController/index';
$route['login']										= 'Admin/AuthController/index';
$route['log_in'] 									= 'Admin/AuthController/login'; 
$route['logout'] 									= 'Admin/AuthController/logout';

//UserController
$route['users'] 									= 'Admin/UserController/user';
$route['user/add'] 									= 'Admin/UserController/userAdd';
$route['user/submitUser'] 							= 'Admin/UserController/submitUser';
$route['user/changeUserStatus/(:any)'] 				= 'Admin/UserController/changeUserStatus';
$route['user/updateUser/(:any)'] 					= 'Admin/UserController/updateUser';
$route['webservice'] 								= 'Admin/UserController/webservice';
$route['emailAlert'] 								= 'Admin/UserController/emailAlert'; 


// pencairan dana
$route['pencairanDana'] 							= 'Admin/PencairanDana/index';

//LaundryOwnerController
$route['laundryOwner'] 								= 'Admin/LaundryOwnerController/laundryOwner';
$route['laundry/laundryOwnerAdd'] 					= 'Admin/LaundryOwnerController/laundryOwnerAdd';
$route['laundry/submitLaundryOwner'] 				= 'Admin/LaundryOwnerController/submitLaundryOwner';
$route['laundry/changeLaundryOwnerStatus/(:any)'] 	= 'Admin/LaundryOwnerController/changeLaundryOwnerStatus';
$route['laundry/changeLaundryOwnerStatus_franchise/(:any)'] = 'Admin/LaundryOwnerController/changeLaundryOwnerStatus_franchise';
$route['laundry/changeLaundryOwnerStatus_shop/(:any)'] = 'Admin/LaundryOwnerController/changeLaundryOwnerStatus_shop';
$route['laundry/updateLaundryOwner/(:any)'] 		= 'Admin/LaundryOwnerController/updateLaundryOwner';
$route['laundry/viewOwnerDetail/(:any)'] 			= 'Admin/LaundryOwnerController/viewOwnerDetail';
$route['laundry/paymentHistory/(:any)'] 			= 'Admin/LaundryOwnerController/paymentHistory';
$route['laundry/ownerPackages/(:any)'] 				= 'Admin/LaundryOwnerController/ownerPackages';
$route['laundry/submitOwnerPackage'] 				= 'Admin/LaundryOwnerController/submitOwnerPackage';
$route['laundry/updateOwnerPackage/(:any)'] 		= 'Admin/LaundryOwnerController/updateOwnerPackage';
$route['laundry/deleteLaundryOwner/(:any)'] 		= 'Admin/LaundryOwnerController/deleteLaundryOwner';

//Laundry Shop
// $route['laundryShop'] 								= 'Admin/OutletController/index';
$route['laundry/laundryShopAdd'] 					= 'Admin/LaundryShopController/laundryShopAdd';
$route['laundry/submitLaundryShop'] 				= 'Admin/LaundryShopController/submitLaundryShop';
$route['laundry/changeLaundryShopStatus/(:any)'] 	= 'Admin/LaundryShopController/changeLaundryShopStatus';
$route['laundry/updateLaundryShop/(:any)'] 			= 'Admin/LaundryShopController/updateLaundryShop';
$route['laundry/deleteLaundryShop/(:any)'] 			= 'Admin/LaundryShopController/deleteLaundryShop';

# hapus outlet
$route['admin/outlet'] 								= 'Admin/Outlet';
$route['admin/outlet/delete/(:any)'] 				= 'Admin/Outlet/delete';
$route['admin/outlet/tambah'] 						= 'Admin/Outlet/tambah';
$route['admin/outlet/aksi_tambah']					= 'Admin/Outlet/aksi_tambah';
$route['admin/outlet/update/(:any)'] 				= 'Admin/Outlet/update';
$route['admin/outlet/aksi_update']					= 'Admin/Outlet/aksi_update';
$route['admin/outlet/ganti_status/(:any)']          = 'Admin/Outlet/ganti_status';

// Agen
$route['admin/agen'] 					            = 'Admin/Agen/index';
$route['admin/agen/tambah'] 					    = 'Admin/Agen/tambah';
$route['admin/agen/aksi_tambah'] 				    = 'Admin/Agen/aksi_tambah';
$route['admin/agen/update/(:any)'] 				    = 'Admin/Agen/update';
$route['admin/agen/aksi_update'] 				    = 'Admin/Agen/aksiUpdate';
$route['admin/agen/ganti_status/(:any)'] 		    = 'Admin/Agen/gantiStatus';
$route['admin/agen/delete/(:any)'] 			        = 'Admin/Agen/delete';

# menu pemilik halaman admin
$route['admin/pemilik']								= 'Admin/Pemilik/index';
$route['admin/pemilik/tambah']						= 'Admin/Pemilik/tambah';
$route['admin/pemilik/update/(:any)']				= 'Admin/Pemilik/update';
$route['admin/pemilik/aksi_update/(:any)']			= 'Admin/Pemilik/aksi_update';
$route['admin/pemilik/ganti_status/(:any)']			= 'Admin/Pemilik/ganti_status';
$route['admin/pemilik/aksi_tambah']				    = 'Admin/pemilik/aksi_tambah';
$route['admin/pemilik/aksi_update']                 = 'Admin/pemilik/aksi_update';
$route['admin/pemilik/delete/(:any)']               = 'Admin/pemilik/hapus';
$route['admin/pemilik/aksi_packages']				= 'Admin/pemilik/aksi_packages';

//Franchise
$route['laundryFranchise']					        = 'Admin/UserController/franchise';
$route['admin/franchise']					        = 'Admin/Franchise/index';
$route['admin/franchise/detail/(:any)']             = 'Admin/Franchise/detail';
$route['admin/franchise/payment_history/(:any)']    = 'Admin/Franchise/payment_history';
$route['admin/franchise/owner_packages/(:any)']     = 'Admin/Franchise/owner_packages';
$route['admin/franchise/ganti_status/(:any)']       = 'Admin/Franchise/ganti_status';
$route['admin/franchise/tambah']					= 'Admin/Franchise/tambah';
$route['admin/franchise/aksi_tambah']				= 'Admin/Franchise/aksi_tambah';
$route['admin/franchise/update/(:any)']             = 'Admin/Franchise/update';
$route['admin/franchise/aksi_update']               = 'Admin/Franchise/aksi_update';
$route['admin/franchise/delete/(:any)']             = 'Admin/Franchise/hapus';
$route['admin/franchise/aksi_packages']				= 'Admin/franchise/aksi_packages';

//Service
$route['service'] 									= 'Admin/ServiceController/service';
$route['service/submitService'] 					= 'Admin/ServiceController/submitService';
$route['service/changeServiceStatus/(:any)'] 		= 'Admin/ServiceController/changeServiceStatus';
$route['service/updateService/(:any)'] 				= 'Admin/ServiceController/updateService';

//Product 
$route['product'] 									= 'Admin/ProductController/product';
$route['product/submitProduct'] 					= 'Admin/ProductController/submitProduct';
$route['product/changeProductStatus/(:any)'] 		= 'Admin/ProductController/changeProductStatus';
$route['product/updateProduct/(:any)'] 				= 'Admin/ProductController/updateProduct';

//Offer
$route['offer'] 									= 'Admin/OfferController/offer';
$route['offer/offerAdd'] 							= 'Admin/OfferController/offerAdd';
$route['offer/submitOffer'] 						= 'Admin/OfferController/submitOffer';
$route['offer/changeOfferStatus/(:any)'] 			= 'Admin/OfferController/changeOfferStatus';
$route['offer/updateOffer/(:any)'] 					= 'Admin/OfferController/updateOffer';

//currency
$route['currency/currency'] 						= 'Admin/CurrencyController/currency';
$route['currency/submitCurrency'] 					= 'Admin/CurrencyController/submitCurrency';
$route['currency/setting'] 							= 'Admin/CurrencyController/setting';
$route['currency/updateCurrencySetting'] 			= 'Admin/CurrencyController/updateCurrencySetting';
$route['currency/updateCurrency/(:any)'] 			= 'Admin/CurrencyController/updateCurrency';
$route['currency/deleteCurrency/(:any)'] 			= 'Admin/CurrencyController/deleteCurrency';
//Advertisement 
$route['advertisement'] 									= 'Admin/AdvertiseController/advertisement';
$route['advertisement/submitAdvertisement'] 				= 'Admin/AdvertiseController/submitAdvertisement';
$route['advertisement/changeAdvertisementStatus/(:any)'] 	= 'Admin/AdvertiseController/changeAdvertisementStatus';
$route['advertisement/updateAdvertisement/(:any)'] 			= 'Admin/AdvertiseController/updateAdvertisement';
//Order
$route['order'] 									= 'Admin/OrderController/order';
$route['order/changeOrderStatus'] 					= 'Admin/OrderController/changeOrderStatus';
$route['order/changePaymentStatus/(:any)'] 			= 'Admin/OrderController/changePaymentStatus';
$route['order/orderDetail/(:any)'] 					= 'Admin/OrderController/orderDetail';
$route['order/orderInvoice/(:any)'] 				= 'Admin/OrderController/orderInvoice';
$route['order/orderEdit/(:any)'] 					= 'Admin/OrderController/orderEdit'; 
$route['order/submitOrderUpdate'] 					= 'Admin/OrderController/submitOrderUpdate';

//TeacketController
$route['tiketSupport'] 								= 'Admin/TiketController/tiketSupport';
$route['tiket/tiketApprve'] 						= 'Admin/TiketController/tiketApprve';
$route['tiket/tiketReject'] 						= 'Admin/TiketController/tiketReject';
$route['tiket/changeTiketStatus'] 					= 'Admin/TiketController/changeTiketStatus';
$route['tiket/setAdminMsg'] 						= 'Admin/TiketController/setAdminMsg'; 
//Chat
$route['chat'] 										= 'Admin/ChatController/chat'; 
$route['chat/setLaundryOwnerMsg'] 					= 'Admin/ChatController/setLaundryOwnerMsg';
$route['chat/chatOwner/(:any)/(:any)/(:any)'] 		= 'Admin/ChatController/chatOwner';
//Notification
$route['notification'] 								= 'Admin/NotificationController/notification';
$route['send_notification'] 						= 'Admin/NotificationController/send_notification';

//Payment
$route['subscriptionPackage'] 								= 'Admin/Payment/subscriptionPackage';
$route['package/submitSubscriptionPackage'] 				= 'Admin/Payment/submitSubscriptionPackage';
$route['package/changeSubscriptionPackageStatus/(:any)'] 	= 'Admin/Payment/changeSubscriptionPackageStatus';
$route['package/updateSubscriptionPackage/(:any)'] 			= 'Admin/Payment/updateSubscriptionPackage';
$route['invoiceHistory'] 									= 'Admin/Payment/invoiceHistory'; //admin
$route['myPackage'] 										= 'Admin/Payment/myPackage'; //owner

$route['razorpay'] 									= 'Admin/Payment/razorpay';
$route['Subscription'] 								= 'Admin/Payment/Subscription';
$route['payment/razorPaySuccess'] 					= 'Admin/Payment/razorPaySuccess';
$route['payment/RazorThankYou'] 					= 'Admin/Payment/RazorThankYou';

//keys
$route['apiKeys'] 									= 'Admin/KeyController/apiKeys';
$route['apiKeys/updateFirebaseKey'] 				= 'Admin/KeyController/updateFirebaseKey';
$route['apiKeys/updateMSG91Key'] 					= 'Admin/KeyController/updateMSG91Key';
$route['apiKeys/updateRazorpayKey'] 				= 'Admin/KeyController/updateRazorpayKey';

$route['komisi'] 									= 'Admin/KomisiController/komisi';
$route['komisi/updateKomisi'] 						= 'Admin/KomisiController/updateKomisi';

$route['persentasi_komisi']							= 'Admin/komisiController/persentasi_komisi';


//APIS
//AuthController
$route['api/sendOtp'] 							= 'Api/AuthController/sendOtp';
$route['api/signup'] 							= 'Api/AuthController/signup';
$route['api/login'] 							= 'Api/AuthController/login';
$route['api/loginGoogle'] 					    = 'Api/AuthController/loginGoogle';
$route['api/forgotPassword'] 					= 'Api/AuthController/forgotPassword';
$route['api/verifyOtp'] 						= 'Api/AuthController/verifyOtp'; 
$route['api/logOut'] 							= 'Api/AuthController/logOut';
$route['api/deleteAccount'] 					= 'Api/AuthController/deleteAccount';
$route['api/changePassword'] 					= 'Api/AuthController/changePassword';

$route['api/userActive'] 						= 'Api/AuthController/userActive';
$route['api/otpSms']							= 'Api/AuthController/otpSms';
$route['api/resendOtpSms']						= 'Api/AuthController/resendOtpSms';

$route['api/userUpdate'] 						= 'Api/UserController/userUpdate';

//SERVICE
$route['api/getAllService'] 					= 'Api/ServiceController/getAllService';
$route['api/getShopServices'] 					= 'Api/ServiceController/getShopServices';
$route['api/getAllLaundryShop'] 				= 'Api/LaundryController/getAllLaundryShop';
$route['api/getLaundryByService'] 				= 'Api/LaundryController/getLaundryByService';
$route['api/getLaundryById'] 				    = 'Api/LaundryController/getLaundryById';
$route['api/getAllOffer'] 						= 'Api/OfferController/getAllOffer';
$route['api/getOfferForLaundryShop'] 			= 'Api/OfferController/getOfferForLaundryShop';
$route['api/applyPromocode'] 					= 'Api/OfferController/applyPromocode';
$route['api/getItembyShopId'] 					= 'Api/PriceListController/getItembyShopId';
$route['api/getAdvertisement'] 					= 'Api/AdvertisementController/getAdvertisement';
$route['api/getCurrency'] 						= 'Api/CurrencyController/getCurrency';

//Order
$route['api/orderSubmit'] 						= 'Api/OrderController/orderSubmit';
$route['api/orderCancel'] 						= 'Api/OrderController/orderCancel';
$route['api/getBookingList'] 					= 'Api/OrderController/getBookingList';

//Order iPaymu
$route['api/orderipaymu'] 						= 'Api/OrderController/orderToPaymu';
$route['api/twilio']     						= 'Api/OrderController/notifTwilio';

//HomeController
$route['api/getHomeData'] 						= 'Api/HomeController/getHomeData';
$route['api/search'] 							= 'Api/HomeController/search';

//ChatController
$route['api/setMessage'] 						= 'Api/ChatController/setMessage';
$route['api/getMessageHistory'] 				= 'Api/ChatController/getMessageHistory';
$route['api/getMessage'] 						= 'Api/ChatController/getMessage';

//TeacketController
$route['api/addTiket'] 							= 'Api/TiketController/addTiket';
$route['api/addTiketComment'] 					= 'Api/TiketController/addTiketComment';
$route['api/tiketList'] 						= 'Api/TiketController/tiketList'; 
$route['api/getTiketComment'] 					= 'Api/TiketController/getTiketComment'; 

//Rating
$route['api/addRating'] 						= 'Api/RatingController/addRating';
$route['api/getRating'] 						= 'Api/RatingController/getRating';

//Notification
$route['api/get_notification'] 					= 'Api/NotificationController/get_notification'; 
$route['api/send_notification']                 = 'Api/NotificationController/send_notification'; 

//LAUNDRY OWNER 
$route['laundryOwner/login'] 				    = 'LaundryOwner/AuthController/index';
$route['laundryOwner/log_in'] 				    = 'LaundryOwner/AuthController/login';
$route['laundryOwner/logout'] 				    = 'LaundryOwner/AuthController/logout';

# artikel 
$route['artikel']								= 'Admin/Article';
$route['artikel/tambah']						= 'Admin/Article/tambah';
$route['artikel/aksi_tambah']					= 'Admin/Article/aksi_tambah';
$route['artikel/edit/(:any)']					= 'Admin/Article/edit';
$route['artikel/hapus/(:any)']					= 'Admin/Article/hapus';
//IPAYMU
$route['notify_page']                           = 'Api/OrderController/updateStatus';

$route['default_controller']   = 'welcome/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
