-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 04, 2020 at 03:33 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `la_admin`
--

CREATE TABLE `la_admin` (
  `id` int(1) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` varchar(22) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `la_admin`
--

INSERT INTO `la_admin` (`id`, `name`, `email`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '123456', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `la_laundry_shop`
--

CREATE TABLE `la_laundry_shop` (
  `shop_id` int(11) NOT NULL,
  `shop_name` text NOT NULL,
  `owner_name` varchar(111) NOT NULL,
  `owner_email` varchar(111) NOT NULL,
  `owner_mobile` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `latitude` varchar(22) NOT NULL,
  `longitude` varchar(22) NOT NULL,
  `opening_time` text NOT NULL,
  `closing_time` text NOT NULL,
  `about_us` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `la_offer`
--

CREATE TABLE `la_offer` (
  `offer_id` int(11) NOT NULL,
  `shop_id` int(22) NOT NULL COMMENT 'laundry shop id',
  `offer_name` text NOT NULL,
  `amount` int(22) NOT NULL,
  `amount_type` varchar(11) NOT NULL COMMENT 'percent/rupees',
  `applicable_on_service` int(11) NOT NULL COMMENT 'service_id',
  `end_date` text NOT NULL,
  `offer_code` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` text NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `la_price_list`
--

CREATE TABLE `la_price_list` (
  `id` int(111) NOT NULL,
  `shop_id` int(111) NOT NULL COMMENT 'laundry shop id',
  `category` text NOT NULL COMMENT 'Mens,women,kids,other',
  `cloth_name` text NOT NULL,
  `price` varchar(22) NOT NULL,
  `image` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1=active, 2=inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `la_service`
--

CREATE TABLE `la_service` (
  `service_id` int(11) NOT NULL,
  `service_name` text NOT NULL,
  `description` text NOT NULL,
  `service_image` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1=active, 0=inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `la_user`
--

CREATE TABLE `la_user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `user_image` text NOT NULL,
  `address` text NOT NULL,
  `device_id` text NOT NULL,
  `device_type` varchar(25) NOT NULL,
  `device_token` text NOT NULL,
  `email_token` varchar(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `created_at` text NOT NULL,
  `updated_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `la_admin`
--
ALTER TABLE `la_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_laundry_shop`
--
ALTER TABLE `la_laundry_shop`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `la_offer`
--
ALTER TABLE `la_offer`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `la_price_list`
--
ALTER TABLE `la_price_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_service`
--
ALTER TABLE `la_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `la_user`
--
ALTER TABLE `la_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `la_admin`
--
ALTER TABLE `la_admin`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `la_laundry_shop`
--
ALTER TABLE `la_laundry_shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_offer`
--
ALTER TABLE `la_offer`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_price_list`
--
ALTER TABLE `la_price_list`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_service`
--
ALTER TABLE `la_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_user`
--
ALTER TABLE `la_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
